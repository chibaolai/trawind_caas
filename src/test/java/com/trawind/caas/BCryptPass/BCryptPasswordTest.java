package com.trawind.caas.BCryptPass;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * 登陆密码加密单体
 * @author PC
 */


public class BCryptPasswordTest {
public static void main(String[] args) {
	
	String generatedSecuredPasswordHash = BCrypt.hashpw("user1", BCrypt.gensalt(12));
	System.out.println(generatedSecuredPasswordHash);
	
    boolean matched = BCrypt.checkpw("user2", generatedSecuredPasswordHash);
    System.out.println(matched);
}
}