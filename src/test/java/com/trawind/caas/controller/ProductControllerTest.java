package com.trawind.caas.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StopWatch;

import com.trawind.caas.Application;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.MWidget;
import com.trawind.caas.req.SaveProductAndDataReq;
import com.trawind.caas.security.SysUser;
import com.trawind.caas.security.SysUserRepository;
import com.trawind.caas.service.TEProductService;
import com.trawind.caas.web.LoginUser;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ProductControllerTest {
	
	@Autowired
	private ProductController productController;
	@Autowired
	private SysUserRepository userRepository;
	
	@Autowired
	private TEProductService productService;
    private MockHttpServletRequest request;  
    private MockHttpServletResponse response;  
	
	@Before
	public void setUp() {
        request = new MockHttpServletRequest();      
        request.setCharacterEncoding("UTF-8");    
		LoginUser loginUser = new LoginUser();
		SysUser userDetails = userRepository.findByUsername("admin");
		loginUser.setUserId(String.valueOf(userDetails.getId()));
		loginUser.setUsername(userDetails.getUsername());
		loginUser.setCorpId(userDetails.getCorpid());
		WebContext.setLoginUserToHttpSession(request.getSession(), loginUser);
        response = new MockHttpServletResponse();
	}
	@Test
	public void productAndDataTest() {
    	SaveProductAndDataReq productAndDataReq = new SaveProductAndDataReq();
    	List<MWidget> productDataJson = new ArrayList<>();
    	MWidget widiget = new MWidget();
    	widiget.setKey("100");
    	widiget.setType("input");
    	widiget.setValue("111");
    	productDataJson.add(widiget);
    	productAndDataReq.setFormId("11000");
    	productAndDataReq.setProductDataJson(productDataJson);
    	productAndDataReq.setProductId("11100");
//    	productController.saveProductAndData(null,productAndDataReq, request.getSession());
	}
	@Test
	public void updateProductTest() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        for (int i = 0; i < 100; i++) {
            final int count = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                	SaveProductAndDataReq productAndDataReq = new SaveProductAndDataReq();
                	List<MWidget> productDataJson = new ArrayList<>();
                	MWidget widiget = new MWidget();
                	widiget.setKey("100");
                	widiget.setType("input");
                	widiget.setValue("111"+count);
                	productDataJson.add(widiget);
                	productAndDataReq.setFormId("11000");
                	productAndDataReq.setProductDataJson(productDataJson);
                	productAndDataReq.setProductId("430d8e08925845bc9c73ce6e465ffb5e");
//                	productController.saveProductAndData(null,productAndDataReq, request.getSession());
                }
            }).run();
        }
        stopWatch.stop();
	}
}
