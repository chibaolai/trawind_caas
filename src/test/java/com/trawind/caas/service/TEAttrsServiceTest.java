package com.trawind.caas.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.trawind.caas.Application;
import com.trawind.caas.entity.TEAttr;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class TEAttrsServiceTest {
	@Autowired
	private TEAttrsService servcive;

	@Test
	public void testAttr() {
		List list = servcive.findBaseAttrList();
		System.out.println(list.size());
	}
	
	@Test
	public void testAttrList() {
		List<TEAttr> list= this.servcive.getAttrListByCorpCode("Trawind");
		System.out.println(list.size());
	}
}
