package com.trawind.drools.demotest;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StopWatch;

import com.trawind.caas.Application;
import com.trawind.caas.entity.TERules;
import com.trawind.caas.req.FormReq;
import com.trawind.caas.service.CAAS24Service;

import net.sf.json.JSONObject;

/**
 * 业务思考：
 * 1、属性是否有type？如何体现两个属性之间校验规则？长度和长度单位之间
 * 2、如果属性没有type，只有id的话，数据库中需有表管理 校验对象关系。
 * @author chibaolai
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class CAAS24Test {
    @Autowired
    private CAAS24Service caas24Service;
    public Logger log = LoggerFactory.getLogger(CAAS24Test.class);

    @Test
    public void testOrderValidation() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //定义页面传过来的参数START
        FormReq req = new FormReq();
        Long lenAttr = Long.valueOf(1);
        Long unitAttr = Long.valueOf(2);
        req.setWidgetIds(String.valueOf(lenAttr).concat(",").concat(String.valueOf(unitAttr)));
        JSONObject formDataReq = new JSONObject();
        formDataReq.put(lenAttr, "10");
        formDataReq.put(unitAttr, "");
        
        List<TERules> ruleList = initTERules(lenAttr,unitAttr);
        //定义页面传过来的参数END
        if(StringUtils.isEmpty(req.getWidgetIds())) {
        	System.out.println("参数错误，属性id列表为空。");
        }
        /**
         * 循环属性查询所有校验规则
         */
        String[] attrIds = req.getWidgetIds().split(",");
        for(String attrIdPrimary : attrIds) {
        	//获取属性值
        	JSONObject formData = formDataReq;
        	String valuePrimary = (String) formData.get(attrIdPrimary);
        	//获取属性关联的验证规则，查询条件：AUDIT_ENABLE = true,AUDIT_RULE_ATTR_ID = attrIdPrimary
			if (ruleList == null) {
				continue;
			}
        	for(TERules rule : ruleList) {
    			rule.setAUDIT_RULE_ATTR_VALUE(valuePrimary);
    			Long attrIdAssociate = rule.getAUDIT_RULE_CHECK_ATTR_ID();
    			String valueAssociate = (String) formData.get(attrIdAssociate);
    			rule.setAUDIT_RULE_CHECK_ATTR_VALUE(valueAssociate);
    			caas24Service.executeRuleEngine(rule);
//        		switch (rule.getAUDIT_RULE_TYPE()) {
//        		case "REQUIRED" :        			
//        			break;
//        		case "LOGICAL" :
//        			break;
//        		default:
//        			break;
//        		}
        	}
        }
        
        stopWatch.stop();
        log.info("Validation of {} took: " + stopWatch.getTotalTimeMillis() + "ms");
    }
    
    private List<TERules> initTERules(Long lenAttr,Long unitAttr) {
        TERules rules = new TERules();
        rules.setTP_AUDIT_RULE_ID(Long.valueOf(1));
        rules.setTP_AUDIT_RULE_GRP_ID(Long.valueOf(411));
        rules.setAUDIT_RULE_NAME("availEndDateToItemAvailDateAudit");
        rules.setAUDIT_RULE_ATTR_ID(Long.valueOf(716));
        rules.setAUDIT_RULE_TYPE("REQUIRED");
        rules.setAUDIT_RULE_LOGICAL_OP("REQUIRED");
        rules.setAUDIT_RULE_ATTR_ID(lenAttr);
        rules.setAUDIT_RULE_ERR_MSG("两个关联属性都不能为空。");
        rules.setAUDIT_RULE_IS_HIERARCHICAL(true);
        rules.setAUDIT_RULE_ATTR_NAME("长度");
        rules.setAUDIT_RULE_CHECK_HIER_LEVEL("ALL");
        rules.setAUDIT_RULE_CHECK_ATTR_ID(unitAttr);
        rules.setAUDIT_RULE_CHECK_ATTR_NAME("长度单位");
        rules.setAUDIT_ENABLE(true);
        List<TERules> ruleList = new ArrayList<TERules>();
        ruleList.add(rules);
        return ruleList;
    }
}
