package com.trawind.drools.demotest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.StopWatch;

import com.trawind.caas.Application;
import com.trawind.caas.config.DroolsConfig;
import com.trawind.caas.entity.TERules;
import com.trawind.caas.req.FormReq;
import com.trawind.caas.service.CAAS24Service;
//import com.trawind.drools.rules.Address;
//import com.trawind.drools.rules.Errors;
//import com.trawind.drools.rules.Order;
//import com.trawind.drools.rules.Seller;

import net.sf.json.JSONObject;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class CAAS25Test {
    @Autowired
    private CAAS24Service caas24Service;
    public Logger log = LoggerFactory.getLogger(CAAS25Test.class);

    @Test
    public void testInteger() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //定义页面传过来的参数START
        FormReq req = new FormReq();
        Long lenAttr = Long.valueOf(1);
        req.setWidgetIds(String.valueOf(lenAttr));
        JSONObject formDataReq = new JSONObject();
        formDataReq.put(lenAttr, "0");
        
        List<TERules> ruleList = initTERules(lenAttr);
        String[] attrIds = req.getWidgetIds().split(",");
        for(String attrIdPrimary : attrIds) {
        	//获取属性值
        	JSONObject formData = formDataReq;
        	String valuePrimary = (String) formData.get(attrIdPrimary);
        	//获取属性关联的验证规则，查询条件：AUDIT_ENABLE = true,AUDIT_RULE_ATTR_ID = attrIdPrimary
			if (ruleList == null) {
				continue;
			}
        	for(TERules rule : ruleList) {
    			rule.setAUDIT_RULE_ATTR_VALUE(valuePrimary);
    			caas24Service.executeRuleEngine(rule);
//        		switch (rule.getAUDIT_RULE_TYPE()) {
//        		case "REQUIRED" :        			
//        			break;
//        		case "LOGICAL" :
//        			break;
//        		default:
//        			break;
//        		}
        	}
        }
        stopWatch.stop();
        log.info("Validation of {} took: " + stopWatch.getTotalTimeMillis() + "ms");
    }

    private List<TERules> initTERules(Long lenAttr) {
        TERules rules = new TERules();
        rules.setTP_AUDIT_RULE_ID(Long.valueOf(1));
        rules.setTP_AUDIT_RULE_GRP_ID(Long.valueOf(411));
        rules.setAUDIT_RULE_NAME("availEndDateToItemAvailDateAudit");
        rules.setAUDIT_RULE_ATTR_ID(Long.valueOf(716));
        rules.setAUDIT_RULE_TYPE("INTEGER");
//        rules.setAUDIT_RULE_LOGICAL_OP("REQUIRED");
        rules.setAUDIT_RULE_ATTR_ID(lenAttr);
        rules.setAUDIT_RULE_ERR_MSG("属性必须为整数。");
        rules.setAUDIT_RULE_IS_HIERARCHICAL(true);
        rules.setAUDIT_RULE_ATTR_NAME("长度");
        rules.setAUDIT_RULE_CHECK_HIER_LEVEL("ALL");
//        rules.setAUDIT_RULE_CHECK_ATTR_ID(unitAttr);
//        rules.setAUDIT_RULE_CHECK_ATTR_NAME("长度单位");
        rules.setAUDIT_ENABLE(true);
        List<TERules> ruleList = new ArrayList<TERules>();
        ruleList.add(rules);
        return ruleList;
    }
}
