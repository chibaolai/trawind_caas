package com.trawind.caas.base;

import com.trawind.caas.base.ResponseCode;

public interface TradeCode extends ResponseCode{
	 public static final String SYSTEM_LEVEL ="01";
	 public static final String LOGIC_LEVEL ="02";
	/**
	 * 01 系统级别 02业务级别
	 * 01 COMM 02 USER 03 SHOP 04 TRADE 05 BUSINSS 06 DISH
	 * 001 002 003 ： XXX业务处理失败
	 * 
	 * 0201001
	 */
	public static final String DATA_EXIT = "0101001";
    /** 属性关联文件已存在，但文件内容不一致，请重新生成关联文件。 */
    public static final String file_content_changed = "0201001";
    /** 上传文件内容为空 */
    public static final String file_content_empty = "0201002";
}
