package com.trawind.caas.base;
/**
 * 用于错误信息返回
 * @author PC
 */
public class ResponseBase {

	/**返回码**/
	public String resultCode;
	/**内容**/
	public String resultMessage;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
}
