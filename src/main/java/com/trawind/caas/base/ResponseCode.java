package com.trawind.caas.base;

public interface ResponseCode{
	
	/** 处理成功 */
	public static final String SUCCESS = "0000";
	public static final String SUCCESS_MSG = "处理成功";

	/** 后台异常 */
	public static final String ERROR = "0099";
	public static final String ERROR_MSG = "后台异常";
}
