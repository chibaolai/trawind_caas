package com.trawind.caas.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.pay.alipay.config.AlipayConfig;
import com.trawind.caas.pay.alipay.util.AlipaySubmit;
import com.trawind.caas.req.AlipayReq;
import com.trawind.caas.res.AlipayResWeb;

/**
 * 我的账户
 * 
 * @author PC
 */

@RestController
@RequestMapping(value = "accountCtl")
public class AccountController {

	/**
	 * 客户在系统内下订单 -> 系统根据订单生成支付宝接口url -> 客户通过url使用支付宝（网上银行）付款 -> 支付宝将客户的付款完成信息发送给电子商务系统 -> 系统收到支付宝信息后确定客户订单已经付款 -> 对该账户进行处理。 
	 * @param httpSession
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getAlipayHtml")
	public AlipayResWeb getAlipaySubmitHtml(HttpSession httpSession, HttpServletResponse response,@RequestBody AlipayReq alypayseq) {
		if(Common.isNullOrEmpty(getRechargeAmount(alypayseq))){
			return null;
		}
		AlipayResWeb al = new AlipayResWeb();
		al.setsHtml(getSHtmlText(alypayseq));
		return al;
	}
	
	/**
	 * 即时到帐接口
	 *************************注意*****************
	 *如果您在接口集成过程中遇到问题，可以按照下面的途径来解决
	 *1、开发文档中心（https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.0.KvddfJ&treeId=62&articleId=103740&docType=1）
	 *2、商户帮助中心（https://cshall.alipay.com/enterprise/help_detail.htm?help_id=473888）
	 *3、支持中心（https://support.open.alipay.com/alipay/support/index.htm）
	 *如果不想使用扩展功能请把扩展功能参数赋空值。
	 **********************************************
	 */
	private String getSHtmlText(AlipayReq alypayseq){
		
		////////////////////////////////////请求参数//////////////////////////////////////

        //订单名称，必填
        String subject = "您在www.CAAS.com充值" + getRechargeAmount(alypayseq) + "元";

        //付款金额，必填
        String total_fee = getRechargeAmount(alypayseq);

        //商品描述，可空
        String body = "default";

		//////////////////////////////////////////////////////////////////////////////////
        
        
		//把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", AlipayConfig.service);
        sParaTemp.put("partner", AlipayConfig.partner);
        sParaTemp.put("seller_id", AlipayConfig.seller_id);
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("payment_type", AlipayConfig.payment_type);
		sParaTemp.put("notify_url", AlipayConfig.notify_url);
		sParaTemp.put("return_url", AlipayConfig.return_url);
		sParaTemp.put("anti_phishing_key", AlipayConfig.anti_phishing_key);
		sParaTemp.put("exter_invoke_ip", AlipayConfig.exter_invoke_ip);
		sParaTemp.put("out_trade_no", alypayseq.getTradeNo());
		sParaTemp.put("subject", subject);
		sParaTemp.put("total_fee", total_fee);
		sParaTemp.put("body", body);
		//其他业务参数根据在线开发文档，添加参数.文档地址:https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.0.O9yorI&treeId=62&articleId=103740&docType=1
        //如sParaTemp.put("参数名","参数值");
		
		//建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
		return sHtmlText;
	}
	private String getRechargeAmount(AlipayReq alypayseq){
		if(!Common.isNullOrEmpty(alypayseq.getPayType())){
			if("1".equals(alypayseq.getPayType())){
				//
				return "3000";
			}else{
				return "";
			}
		}
		return "";
	}
}
