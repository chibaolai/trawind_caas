package com.trawind.caas.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.TELayout;
import com.trawind.caas.service.TELayoutService;
import com.trawind.caas.web.LoginUser;

import net.sf.json.JSONArray;

@RestController
@RequestMapping(value = "widget")
public class WidgetController {

	@Autowired
	private TELayoutService layoutService;

	/**
	 * 获取页面布局信息
	 * layout
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getLayoutCompList" })
	public String getLayoutCompList(HttpSession httpSession) {

		// 取得loginUser
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		List<TELayout> layoutCompList = layoutService.getLayoutCompList(loginUser.getCorpId());

		return JSONArray.fromObject(layoutCompList).toString();
	}
}
