package com.trawind.caas.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.entity.MWelcomeData;
import com.trawind.caas.entity.TContact;
import com.trawind.caas.entity.TEService;
import com.trawind.caas.service.TContactService;
import com.trawind.caas.service.TEServService;
import com.trawind.caas.service.TEWelcomeMongoService;

@RestController
@RequestMapping("welcomePage")
public class WelcomeController {

	@Autowired
	private TEWelcomeMongoService welcomeMongoService;

	@Autowired
	private TContactService contactService;
	
	@Autowired
	private TEServService servService;

	/**
	 * 联系我们保存
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = { "/sendContactUs" })
	public TContact sendContactUs(@RequestBody final LinkedHashMap<String, String> dataReqMap) {

		String name = Common.getStringValue(dataReqMap.get("name"));
		String corpname = Common.getStringValue(dataReqMap.get("corpname"));
		String phone = Common.getStringValue(dataReqMap.get("phone"));
		String email = Common.getStringValue(dataReqMap.get("email"));
		String address = Common.getStringValue(dataReqMap.get("address"));
		String message = Common.getStringValue(dataReqMap.get("message"));

		TContact contact = new TContact();
		contact.setName(name);
		contact.setCorpname(corpname);
		contact.setPhone(phone);
		contact.setEmail(email);
		contact.setAddress(address);
		contact.setMessage(message);
		contact.setCtime(new Date());

		return contactService.save(contact);
	}

	/**
	 * 从mongoDB中获取welcome页面信息 
	 * @param langCode
	 * @return
	 */
	@RequestMapping(value = { "/getWelcomeData/{langCode}" })
	public MWelcomeData getWelcomeData(@PathVariable String langCode) {

		if (langCode == null) {
			return null;
		}

		if ("zh".equals(langCode)) {
			langCode = Common.LanguageType.Chinese.getDbValue();
		}

		MWelcomeData welcomeData = welcomeMongoService.getWelcomeData(langCode);

		return welcomeData;
	}
	/**
	 * 获取所有服务信息
	 * @return
	 */
	@RequestMapping(value = "getServiceList")
	public List<TEService> getServiceList(){
		return servService.findServiceList();
		
	}
}
