package com.trawind.caas.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.TEInformation;
import com.trawind.caas.res.InformationWeb;
import com.trawind.caas.security.SysUser;
import com.trawind.caas.security.SysUserRepository;
import com.trawind.caas.service.TEInformationService;
import com.trawind.caas.web.LoginUser;

import net.sf.json.JSONObject;

@RestController
@RequestMapping("information")
public class InformationController {

	enum Classify_Code {
		News("news"), Notice("notice"), SystemLog("syslog"), Link("link");

		private String mDbValue = Common.STRING_EMPTY;

		private Classify_Code(String dbValue) {
			mDbValue = dbValue;
		}

		public String getDbValue() {
			return mDbValue;
		}
	}

	@Autowired
	private TEInformationService informationService;

	@Autowired
	private SysUserRepository userRepository;

	/**
	 * 获取新闻grid信息List
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getNewsItemInfo" })
	public List<InformationWeb> getNewsItemInfo(HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();
		List<TEInformation> newsList = informationService.getFormListByUserIdAndClassifyCodeAndLanDefAndCorpId(
				loginUser.getUserId(), Classify_Code.News.getDbValue(), Common.LanguageType.English.getDbValue(),
				loginUser.getCorpId());
		setInformationWebList(informationWebList, newsList);

		return informationWebList;
	}

	/**
	 * 获取提示grid信息List
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getNoticeItemInfo" })
	public List<InformationWeb> getNoticeItemInfo(HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();
		List<TEInformation> newsList = informationService.getFormListByUserIdAndClassifyCodeAndLanDefAndCorpId(
				loginUser.getUserId(), Classify_Code.Notice.getDbValue(), Common.LanguageType.English.getDbValue(),
				loginUser.getCorpId());
		setInformationWebList(informationWebList, newsList);

		return informationWebList;
	}

	/**
	 * 获取系统日志grid List
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getSystemLogItemInfo" })
	public List<InformationWeb> getSystemLogItemInfo(HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();
		List<TEInformation> list = informationService.getFormListByUserIdAndClassifyCodeAndLanDefAndCorpId(
				loginUser.getUserId(), Classify_Code.SystemLog.getDbValue(), Common.LanguageType.English.getDbValue(),
				loginUser.getCorpId());
		setSysLogWebList(informationWebList, list);

		return informationWebList;
	}

	/**
	 * 获取链接grid信息List
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getLinkItemInfo" })
	public List<InformationWeb> getLinkItemInfo(HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();
		List<TEInformation> list = informationService.getFormListByUserIdAndClassifyCodeAndLanDefAndCorpId(
				loginUser.getUserId(), Classify_Code.Link.getDbValue(), Common.LanguageType.English.getDbValue(),
				loginUser.getCorpId());
		setInformationWebList(informationWebList, list);

		return informationWebList;
	}

	@RequestMapping(value = { "/getLinkSearchInfo" })
	public String getLinkSearchInfo(HttpSession httpSession, @RequestBody LinkedHashMap<String, String> formReqMap) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		JSONObject json = new JSONObject(false);
		String searchIdKey = "searchId";
		if (formReqMap != null && formReqMap.containsKey(searchIdKey)) {
			String id = formReqMap.get(searchIdKey);
			if (!Common.isNullOrEmpty(id)) {
				TEInformation information = informationService.getOneLinkInfoFormById(id, loginUser.getCorpId());
				if (information != null) {
					json = JSONObject.fromObject(information.getContent());
				}
			}
		}

		return json.toString();
	}

	private void setInformationWebList(List<InformationWeb> informationWebList, List<TEInformation> list) {
		InformationWeb informationWeb = null;

		if (list != null) {
			for (TEInformation info : list) {
				informationWeb = new InformationWeb();
				informationWeb.setId(info.getId());
				informationWeb.setName(info.getName());
				informationWeb.setClassifyCode(info.getClassifyCode());
				informationWeb.setContent(info.getContent());

				informationWebList.add(informationWeb);
			}
		}
	}

	private void setSysLogWebList(List<InformationWeb> informationWebList, List<TEInformation> list) {
		InformationWeb informationWeb = null;

		if (list != null) {
			for (TEInformation info : list) {
				if (Common.isNullOrEmpty(info.getContent())) {
					continue;
				}

				JSONObject json = JSONObject.fromObject(info.getContent());

				informationWeb = new InformationWeb();
				informationWeb.setId(json.getString("id"));
				informationWeb.setName(info.getName());
				informationWeb.setClassifyCode(info.getClassifyCode());
				informationWeb.setJournalType(json.getString("journalType"));

				if (!Common.isNullOrEmpty(json.getString("uUser"))) {
					SysUser user = userRepository.findOne(Long.valueOf(json.getString("uUser")));
					informationWeb.setUpdateUser(user.getUsername());
				}

				informationWeb.setUpdateTime(json.getString("uTime"));

				informationWebList.add(informationWeb);
			}
		}
	}

}
