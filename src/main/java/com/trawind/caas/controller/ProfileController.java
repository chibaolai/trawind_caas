package com.trawind.caas.controller;

import java.util.LinkedHashMap;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.entity.TEForm;
import com.trawind.caas.service.TEFormService;
import com.trawind.caas.utils.TikaUtils;

import net.sf.json.JSONObject;

@RestController
@RequestMapping("profile")
public class ProfileController {

	@Autowired
	private TEFormService formService;

	@RequestMapping(value = "/getProfile")
	public JSONObject getProfile() {
		JSONObject result = new JSONObject();
		String formType = "profile";
		TEForm form = formService.geTeFormsByFormType(formType);
		if (form != null) {
			result.put("form", form);
		}
		return result;
	}

	/**
	 * 获取xml结构
	 * @param httpSession
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = "/getXmlPageInfo")
	public JSONObject getXmlPageInfo(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {

		String inputFileId = Common.getStringValue(dataReqMap.get("inputFileId"), StringUtils.EMPTY);

		if (!Common.isNullOrEmpty(inputFileId)) {
			TikaUtils tika = new TikaUtils(inputFileId, 0, 1);

			JSONObject json = new JSONObject(false);
			json.put("xml", tika.nextXmlPage());
			return json;
		}

		return new JSONObject(false);
	}

	/**
	 * xml翻页 下一页
	 * @param httpSession
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = "/getNextXmlPageInfo")
	public JSONObject getNextXmlPageInfo(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {

		String inputFileId = Common.getStringValue(dataReqMap.get("inputFileId"), StringUtils.EMPTY);
		String currentPage = Common.getStringValue(dataReqMap.get("currentPage"), StringUtils.EMPTY);

		if (!Common.isNullOrEmpty(inputFileId) && !Common.isNullOrEmpty(currentPage)) {
			TikaUtils tika = new TikaUtils(inputFileId, Integer.valueOf(currentPage), 1);

			JSONObject json = new JSONObject(false);
			json.put("xml", tika.nextXmlPage());
			return json;
		}

		return new JSONObject(false);
	}

	/**
	 * xml翻页 上一页
	 * @param httpSession
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = "/getPreviousXmlPageInfo")
	public JSONObject getPreviousXmlPageInfo(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {

		String inputFileId = Common.getStringValue(dataReqMap.get("inputFileId"), StringUtils.EMPTY);
		String currentPage = Common.getStringValue(dataReqMap.get("currentPage"), StringUtils.EMPTY);

		if (!Common.isNullOrEmpty(inputFileId) && !Common.isNullOrEmpty(currentPage)) {
			TikaUtils tika = new TikaUtils(inputFileId, Integer.valueOf(currentPage), 1);

			JSONObject json = new JSONObject(false);
			json.put("xml", tika.previousXmlPage());
			return json;
		}

		return new JSONObject(false);
	}
}
