package com.trawind.caas.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSDBFile;
import com.trawind.caas.common.Common;
import com.trawind.caas.common.Constant;
import com.trawind.caas.common.FileUtil;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.controller.TEConfigController.ConfigKey;
import com.trawind.caas.entity.MAtrtributeID;
import com.trawind.caas.entity.MChangedAttrs;
import com.trawind.caas.entity.MProductData;
import com.trawind.caas.entity.MProductDataLog;
import com.trawind.caas.entity.MWidget;
import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TEConfig;
import com.trawind.caas.entity.TECorp;
import com.trawind.caas.entity.TEForm;
import com.trawind.caas.entity.TEIssuedRequest;
import com.trawind.caas.entity.TEProduct;
import com.trawind.caas.entity.TIssuedRelations;
import com.trawind.caas.req.ProductInputReq;
import com.trawind.caas.req.SaveProductAndDataReq;
import com.trawind.caas.res.InformationWeb;
import com.trawind.caas.res.ProductInputResWeb;
import com.trawind.caas.service.MGridFSImgService;
import com.trawind.caas.service.MGridFSMappingService;
import com.trawind.caas.service.MGridFSOutPutService;
import com.trawind.caas.service.TEAttrsService;
import com.trawind.caas.service.TEConfigService;
import com.trawind.caas.service.TECorpService;
import com.trawind.caas.service.TEFormMongoService;
import com.trawind.caas.service.TEFormService;
import com.trawind.caas.service.TEIssuedRequestService;
import com.trawind.caas.service.TEOutputFormatService;
import com.trawind.caas.service.TEProductLogMongoService;
import com.trawind.caas.service.TEProductService;
import com.trawind.caas.service.TIssuedRelationsService;
import com.trawind.caas.utils.DateUtils;
import com.trawind.caas.utils.JacksonJsonUtil;
import com.trawind.caas.utils.UUID;
import com.trawind.caas.web.LoginUser;
import com.trawind.caas.xml.XmlGenerate;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@RestController
@RequestMapping("product")
public class ProductController {
	@Autowired
	private TEProductService productService;

	@Autowired
	private TEFormService formService;

	@Autowired
	private TEFormMongoService mongoService;

	@Autowired
	private TEProductLogMongoService productLogMongoService;

	@Autowired
	private TEAttrsService attrsService;

	@Autowired
	private TEConfigService configService;

	@Autowired
	private MGridFSOutPutService mGridFSOutputService;

	@Autowired
	private MGridFSImgService mGridFSImgService;

	@Autowired
	private TEIssuedRequestService issuedRequestService;

	@Autowired
	private TECorpService corpService;

	@Autowired
	private TIssuedRelationsService issuedRelationsService;

	@Autowired
	private TEOutputFormatService outputFormatService;

	@Autowired
	private XmlGenerate xmlGenerate;

	@Autowired
	private MGridFSMappingService mGridFSMappingService;
	
	/**
	 * 获取所有关联form表单List
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getAllMappingTemplete")
	public String getAllMappingTemplete(HttpSession httpSession) {
		return mGridFSMappingService.getFileList().toString();
	}

	/**
	 * 获取该生产商所有分销商信息,用于弹出框选择分销商进行发布
	 * @param httpSession
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = "/getAllDistributorAndSelected")
	public JSONArray getAllDistributorAndSelected(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {

		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		String productId = Common.getStringValue(dataReqMap.get("productId"), StringUtils.EMPTY);

		List<TECorp> corpList = corpService.getAllOutOwn(loginUser.getCorpId());

		//获取分销商信息List
		List<TIssuedRelations> issuedRelationsList = issuedRelationsService.getSelectedDistributor(productId,
				loginUser.getCorpId());

		JSONArray jsonArr = new JSONArray();
		JSONObject json = null;
		for (TECorp corp : corpList) {
			json = new JSONObject(false);

			long count = issuedRelationsList.stream()
					.filter(ir -> ir.getDistributorId().longValue() == corp.getId().longValue()).count();

			json.put("selected", count > 0 ? true : false);
			json.put("data", corp);

			jsonArr.add(json);
		}

		return jsonArr;
	}

	/**
	 * 保存产品发布信息
	 * @param httpSession
	 * @param dataReqMap
	 */
	@RequestMapping(value = "/setDistributor")
	public void setDistributor(HttpSession httpSession, @RequestBody final LinkedHashMap<String, String> dataReqMap) {

		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		String productId = Common.getStringValue(dataReqMap.get("productId"), StringUtils.EMPTY);
		String selected = Common.getStringValue(dataReqMap.get("selected"), StringUtils.EMPTY);

		if (Common.isNullOrEmpty(selected)) {
			return;
		}

		String[] selArr = selected.split(",");

		List<TIssuedRelations> issuedRelationsList = new ArrayList<>();
		TIssuedRelations issuedRelations = null;
		for (String sel : selArr) {
			if (Common.isNullOrEmpty(sel) || !NumberUtils.isNumber(sel)) {
				continue;
			}

			issuedRelations = new TIssuedRelations();
			issuedRelations.setProductId(productId);
			issuedRelations.setManufacturerId(loginUser.getCorpId());
			issuedRelations.setDistributorId(Long.valueOf(sel));

			issuedRelationsList.add(issuedRelations);
		}

		issuedRelationsService.deleteByProductIdAndManufacturerId(productId, loginUser.getCorpId());
		if (!issuedRelationsList.isEmpty()) {
			issuedRelationsService.saveList(issuedRelationsList);
		}
	}

	/**
	 * 同意产品修改
	 * 
	 * @param httpSession
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = "/acceptProductData")
	public void acceptProductData(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		String productId = Common.getStringValue(dataReqMap.get("productId"), StringUtils.EMPTY);
		String productLogId = Common.getStringValue(dataReqMap.get("productLogId"), StringUtils.EMPTY);
		Long fromCorpId = Common.getLongValue(dataReqMap.get("corpId"), 0L);

		TEProduct product = productService.getProductById(productId);
		if (product == null) {
			return;
		}

		MProductData fromProductData = getProductDataJson(productId, fromCorpId);
		if (fromProductData == null) {
			return;
		}

		MProductDataLog productDataLog = productLogMongoService.getMProductDataLog(productLogId);
		if (productDataLog == null) {
			return;
		}

		MProductData productData = getProductDataJson(productId, loginUser.getCorpId());
		if (productData == null) {
			productData = mongoService.sendInitDataToParnter(fromProductData, loginUser.getCorpId());
		}

		List<MProductDataLog> productDataLogList = productLogMongoService.getAllProductData(productId,
				loginUser.getCorpId());

		if (productDataLogList.isEmpty()) {
			productLogMongoService.sendUpdateDataLogToParnter(productDataLog, loginUser.getCorpId());
		} else {
			// 通过partner 修改Log 变更所属数据，并生成新的log
			updateLogByaccept(productDataLog, loginUser.getCorpId());
		}

		List<TEIssuedRequest> issuedRequestList = issuedRequestService.getAllIssuedInfo(loginUser.getCorpId(),
				TEIssuedRequest.STATE_CODE_NONE, productId, productLogId);
		for (TEIssuedRequest issued : issuedRequestList) {
			issued.setState(TEIssuedRequest.STATE_CODE_ACCEPT);
			issued.setHandleTime(new Date());
			issuedRequestService.save(issued);
		}

	}

	/**
	 * 同意产品修改后 更新log
	 * 
	 * @param productDataLog
	 * @param corpId
	 */
	private void updateLogByaccept(MProductDataLog productDataLog, Long corpId) {
		MProductDataLog rollbackProductDataLog = new MProductDataLog();
		List<MChangedAttrs> rollbackAttrs = new ArrayList<MChangedAttrs>();
		String productId = productDataLog.getProductId();
		MProductData product = this.mongoService.getProductData(productId, corpId);
		List<MWidget> widgetList = product.getWidget();
		/**
		 * 生成回滚变更履历
		 */
		rollbackProductDataLog.setRollbackFlg(true);// 回滚标示符
		rollbackProductDataLog.setProductId(productId);
		rollbackProductDataLog.setDatetime(DateUtils.format(new Date(), Constant.date_format));
		// 设定回滚变更属性
		List<MChangedAttrs> chagedAttrs = productDataLog.getMChangedAttrs();
		for (MChangedAttrs changedAttr : chagedAttrs) {
			// 获取回滚前log
			MAtrtributeID atrtributeID = changedAttr.getMAtrtributeID();
			String attributeId = changedAttr.getAttributeId();
			Object currentValue = atrtributeID.getCurrentValue();
			Object previousValue = atrtributeID.getPreviousValue();
			// 做成回滚log
			MAtrtributeID atrtributeIDRollback = new MAtrtributeID();
			atrtributeIDRollback.setCurrentValue(currentValue);
			atrtributeIDRollback.setPreviousValue(previousValue);
			MChangedAttrs changedAttrsRollback = new MChangedAttrs();
			changedAttrsRollback.setAttributeId(attributeId);
			changedAttrsRollback.setMAtrtributeID(atrtributeIDRollback);
			rollbackAttrs.add(changedAttrsRollback);
			// 变更产品属性
			for (MWidget widget : widgetList) {
				if (widget.getKey().equals(attributeId)) {
					widget.setValue(currentValue);
				}
			}
		}

		rollbackProductDataLog.setCorpId(corpId);
		rollbackProductDataLog.setMChangedAttrs(rollbackAttrs);
		rollbackProductDataLog.setMode(Constant.mode_selft);
		this.productLogMongoService.saveProductDataLog(rollbackProductDataLog);
		/**
		 * 变更产品属性
		 */
		this.mongoService.saveProductData(product);

	}

	/**
	 * 驳回产品修改
	 * 
	 * @param httpSession
	 * @param dataReqMap
	 * @return
	 */
	@RequestMapping(value = "/rejectProductData")
	public void rejectProductData(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		String productId = Common.getStringValue(dataReqMap.get("productId"), StringUtils.EMPTY);
		String productLogId = Common.getStringValue(dataReqMap.get("productLogId"), StringUtils.EMPTY);

		List<TEIssuedRequest> issuedRequestList = issuedRequestService.getAllIssuedInfo(loginUser.getCorpId(),
				TEIssuedRequest.STATE_CODE_NONE, productId, productLogId);
		for (TEIssuedRequest issued : issuedRequestList) {
			issued.setState(TEIssuedRequest.STATE_CODE_REJECT);
			issued.setHandleTime(new Date());
			issuedRequestService.save(issued);
		}
	}

	/**
	 * 回滚上次修改
	 * 
	 * @param productLogId
	 */
	@RequestMapping(value = "/rollbackProductData")
	public List<InformationWeb> rollbackProductHistory(HttpSession httpSession,
			@RequestBody final LinkedHashMap<String, String> dataReqMap) {

		String productId = dataReqMap.get("productId");
		String productLogId = dataReqMap.get("productLogId");
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		MProductDataLog productDataLog = productLogMongoService.getMProductDataLog(productLogId);
		runRollback(productDataLog, loginUser.getCorpId());

		return getAllUpdateContentInfoByProductId(httpSession, productId);
	}

	/**
	 * 获取该产品所有修改记录
	 * 用于首页dashboard和dataGrid页
	 * @param httpSession
	 * @param productId
	 * @return
	 */
	@RequestMapping(value = { "/getAllUpdateContentInfo/{productId}" })
	public List<InformationWeb> getAllUpdateContentInfoByProductId(HttpSession httpSession,
			@PathVariable String productId) {

		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();

		if (Common.isNullOrEmpty(productId)) {
			return informationWebList;
		}

		TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
				loginUser.getCorpId());
		String lanCode = Common.getStringValue(config.getValue(), StringUtils.EMPTY);

		TEProduct product = productService.getProductById(productId);

		getProductLogInfo(product, lanCode, informationWebList, true, loginUser.getCorpId());

		return informationWebList;
	}

	/**
	 * 产品信息取得
	 * 
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getProductLinkItemInfo/{rowCount}" })
	public List<InformationWeb> getProductLinkItemInfo(HttpSession httpSession, @PathVariable int rowCount) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();

		TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
				loginUser.getCorpId());
		String lanCode = Common.getStringValue(config.getValue(), StringUtils.EMPTY);

		List<TEProduct> productList = productService.getProductList();

		for (TEProduct product : productList) {
			if (rowCount > 0 && informationWebList.size() >= rowCount) {
				break;
			}

			getProductLogInfo(product, lanCode, informationWebList, false, loginUser.getCorpId());
		}

		return informationWebList;
	}

	/**
	 * 产品审核信息取得
	 * 
	 * @param httpSession
	 * @param rowCount
	 * @return
	 */
	@RequestMapping(value = { "/getProductAuditingItemInfo/{rowCount}" })
	public List<InformationWeb> getProductAuditingItemInfo(HttpSession httpSession, @PathVariable int rowCount) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();

		TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
				loginUser.getCorpId());
		String lanCode = Common.getStringValue(config.getValue(), StringUtils.EMPTY);

		List<TEIssuedRequest> issuedList = issuedRequestService.getAllIssuedInfo(loginUser.getCorpId(),
				TEIssuedRequest.STATE_CODE_NONE);

		String productId = null;
		String productLogId = null;
		Long fromCorpId;
		MProductDataLog productDataLog = null;
		TEProduct product = null;
		for (TEIssuedRequest issuedInfo : issuedList) {
			productId = issuedInfo.getProductId();
			productLogId = issuedInfo.getProductLogId();
			fromCorpId = issuedInfo.getFromCorpId();

			product = productService.getProductById(productId);
			if (product == null) {
				continue;
			}

			MProductData productData = getProductDataJson(productId, fromCorpId);
			if (productData == null) {
				continue;
			}

			productDataLog = productLogMongoService.getMProductDataLog(productLogId);
			if (productDataLog == null) {
				continue;
			}

			getProductLogInfo(product, lanCode, informationWebList, false, fromCorpId);
		}

		return informationWebList;
	}

	/***
	 * 取得 name 和 primaryId 列的数据
	 * 
	 * @param product
	 * @return
	 */
	private List<MWidget> getNameAndPrimaryId(TEProduct product, Long corpId) {
		if (!Common.isNullOrEmpty(product.getShowAttrs())) {
			MProductData productData = getProductDataJson(product.getId(), corpId);
			if (productData != null) {
				List<MWidget> productDataJson = productData.getWidget();
				return productDataJson;
			}
		}

		return null;
	}

	/***
	 * 取得 updateContent 列的数据
	 * 
	 * @param product
	 * @param contentList
	 */
	private void getContent(String productId, String lanCode, MProductDataLog log, List<String> contentList) {
		String attrName = null;
		String attrPreviousValue = null;
		String attrCurrentValue = null;
		String contentStr = null;
		TEAttr attr = null;

		List<MChangedAttrs> changedAttrsList = log.getMChangedAttrs();
		for (MChangedAttrs changedAttr : changedAttrsList) {
			attrPreviousValue = Common.getStringValue(changedAttr.getMAtrtributeID().getPreviousValue(),
					StringUtils.EMPTY);

			attrCurrentValue = Common.getStringValue(changedAttr.getMAtrtributeID().getCurrentValue(),
					StringUtils.EMPTY);

			contentStr = String.format(Constant.FORMAT_CONTENT, attrName, attrPreviousValue, attrCurrentValue);
			contentList.add(contentStr);
		}
	}

	/**
	 * 执行回滚
	 * 
	 * @return
	 */
	private void runRollback(MProductDataLog productDataLog, Long corpId) {
		MProductDataLog rollbackProductDataLog = new MProductDataLog();
		List<MChangedAttrs> rollbackAttrs = new ArrayList<MChangedAttrs>();
		String productId = productDataLog.getProductId();
		MProductData product = this.mongoService.getProductData(productId, corpId);
		List<MWidget> widgetList = product.getWidget();
		/**
		 * 生成回滚变更履历
		 */
		rollbackProductDataLog.setRollbackFlg(true);// 回滚标示符
		rollbackProductDataLog.setProductId(productId);
		rollbackProductDataLog.setDatetime(DateUtils.format(new Date(), Constant.date_format));
		// 设定回滚变更属性
		List<MChangedAttrs> chagedAttrs = productDataLog.getMChangedAttrs();
		for (MChangedAttrs changedAttr : chagedAttrs) {
			// 获取回滚前log
			MAtrtributeID atrtributeID = changedAttr.getMAtrtributeID();
			String attributeId = changedAttr.getAttributeId();
			Object currentValue = atrtributeID.getCurrentValue();
			Object previousValue = atrtributeID.getPreviousValue();
			// 做成回滚log
			MAtrtributeID atrtributeIDRollback = new MAtrtributeID();
			atrtributeIDRollback.setCurrentValue(previousValue);
			atrtributeIDRollback.setPreviousValue(currentValue);
			MChangedAttrs changedAttrsRollback = new MChangedAttrs();
			changedAttrsRollback.setAttributeId(attributeId);
			changedAttrsRollback.setMAtrtributeID(atrtributeIDRollback);
			rollbackAttrs.add(changedAttrsRollback);
			// 变更产品属性
			for (MWidget widget : widgetList) {
				if (widget.getKey().equals(attributeId)) {
					widget.setValue(previousValue);
				}
			}
		}

		rollbackProductDataLog.setCorpId(corpId);
		rollbackProductDataLog.setMChangedAttrs(rollbackAttrs);
		rollbackProductDataLog.setMode(Constant.mode_selft);
		this.productLogMongoService.saveProductDataLog(rollbackProductDataLog);
		/**
		 * 变更产品属性
		 */
		this.mongoService.saveProductData(product);

	}

	/**
	 * 获取产品和产品的数据
	 * 
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getProductAndData")
	public ProductInputResWeb getProductAndData(@RequestBody ProductInputReq req, HttpSession httpSession,
			HttpServletResponse response) {
		ProductInputResWeb ret = new ProductInputResWeb();
		boolean isNewFlag = false;
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		String productId = req.getProductId();
		List<InformationWeb> formList = null;// 表单模版List
		if (StringUtils.isEmpty(productId)) {
			isNewFlag = true;
		}

		if (isNewFlag) {// 新规 获取最新表单
			if (StringUtils.isEmpty(req.getFormId())) {
				List<TEForm> fList = formService.getFormListByCategoryIdNotNull();
				for (TEForm form : fList) {

					if (Common.isNullOrEmpty(form.getCommand())) {
						continue;
					}

					JSONArray jsonArr = JSONArray.fromObject(form.getCommand());
					long count = jsonArr.stream().filter(m -> ((JSONObject) m).containsValue("edit")).count();

					if (count == 0) {
						ret.setForm(form);
						break;
					}
				}
			} else {
				TEForm form = formService.getFormById(req.getFormId());
				// 如果formId不等于空 则根据formID进行查询
				ret.setForm(form);
			}

			formList = new ArrayList<InformationWeb>();
			formList = getFormLinkItemInfo(null, isNewFlag);
			ret.setFormList(formList);

			return ret;
		} else {// 编辑 获取指定表单
			TEProduct product = productService.findOne(productId);
			if (product == null) {// 没有产品，报错
				System.err.println("指定产品没有找到");
			}

			formList = new ArrayList<InformationWeb>();
			formList = getFormLinkItemInfo(product.getCategoryId(), isNewFlag);
			ret.setFormList(formList);

			String formId = req.getFormId();
			if (Common.isNullOrEmpty(formId)) {
				formId = product.getFormId();
			}

			TEForm form = formService.getForm(formId, product.getCategoryId());
			if (form == null) {// 没有表单，获取最新表单
				form = formService.getForm(product.getCategoryId());
				ret.setForm(form);
				return ret;
			}
			ret.setForm(form);
			ret.setProduct(product);
			MProductData productData = getProductDataJson(productId, loginUser.getCorpId());
			if (productData != null) {
				for (MWidget mWidget : productData.getWidget()) {
					if (mWidget.getType().equals(Constant.type_image)) {
						List<Map> imageAttrList = (List<Map>) mWidget.getValue();
						if (imageAttrList != null && imageAttrList.size() > 0) {
							for (Map mImageDataMap : imageAttrList) {
								GridFSDBFile gridFSDBFile = mGridFSOutputService
										.findFileById(mImageDataMap.get("id").toString());
								ByteArrayOutputStream bos = new ByteArrayOutputStream();
								InputStream inputStream = gridFSDBFile.getInputStream();
								byte[] b = new byte[1024];
								int n;
								try {
									while ((n = inputStream.read(b)) != -1) {
										bos.write(b, 0, n);
									}
									byte[] buffer = bos.toByteArray();
									mImageDataMap.put("file", Base64Utils.encodeToString(buffer));
									inputStream.close();
									bos.close();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
				ret.setProductDataJson(productData.getWidget());
				ret.setTimeStamp(productData.getTimeStamp());
			}
		}
		return ret;
	}

	/**
	 * 发布数据给合作伙伴
	 * 首页发布
	 * 目前只导出xml ，后期需要加上传输协议发送出去得到回执进行相关操作
	 */
	@RequestMapping(value = "/issuedDataLogToPartner")
	private void issuedDataLogToPartner(HttpSession httpSession,
			@RequestBody final List<HashMap<String, String>> dataReqMapList) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		List<TIssuedRelations> issuedRelationsList = null;
		TEIssuedRequest issuedRequest = null;
		for (HashMap<String, String> argMap : dataReqMapList) {
			String productId = Common.getStringValue(argMap.get("productId"), StringUtils.EMPTY);
			String productLogId = Common.getStringValue(argMap.get("productLogId"), StringUtils.EMPTY);
			String mappingId = Common.getStringValue(argMap.get("mappingId"), StringUtils.EMPTY);

			issuedRelationsList = issuedRelationsService.getSelectedDistributor(productId, loginUser.getCorpId());
			if (issuedRelationsList.isEmpty()) {
				continue;
			}

			// 获取产品数据 By corpId
			MProductData productData = mongoService.getProductData(productId, loginUser.getCorpId());

			if (productData == null) {
				continue;
			}

			List<TEIssuedRequest> issuedRequestList = new ArrayList<>();
			for (TIssuedRelations ir : issuedRelationsList) {

				issuedRequest = new TEIssuedRequest();
				issuedRequest.setId(UUID.randomUUID(36));
				issuedRequest.setFromCorpId(loginUser.getCorpId());
				issuedRequest.setToCorpId(ir.getDistributorId());
				issuedRequest.setProductId(productId);
				issuedRequest.setProductLogId(productLogId);
				issuedRequest.setState(TEIssuedRequest.STATE_CODE_NONE);
				issuedRequest.setSendTime(new Date());

				issuedRequestList.add(issuedRequest);

				/**
				 * 生成ToCorp指定格式文件
				 */
				output(ir.getDistributorId(), productId, mappingId, loginUser.getCorpId());
			}

			if (!issuedRequestList.isEmpty()) {
				issuedRequestService.save(issuedRequestList);
			}
		}
	}

	/**
	 * 做成产品并向mongo插入产品数据
	 * 
	 * @param formDataReq
	 * @throws Exception
	 */
	@RequestMapping(value = "/saveProductAndData", consumes = MediaType.ALL_VALUE)
	public MProductDataLog saveProductAndData(
			@RequestParam(value = "file", required = false) List<MultipartFile> fileList,
			@RequestParam(value = "productAndDataReq") String productAndDataReqStr, HttpSession httpSession)
			throws Exception {
		SaveProductAndDataReq productAndDataReq = (SaveProductAndDataReq) JacksonJsonUtil
				.jsonToBean(productAndDataReqStr, SaveProductAndDataReq.class);
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		boolean isNew = false;
		if (productAndDataReq.getProductId() == null) {
			isNew = true;
		}
		TEProduct product = productService.findOne(productAndDataReq.getProductId());
		if (product == null) {
			isNew = true;
		}
		if (isNew) {
			TEForm form = formService.getFormById(productAndDataReq.getFormId());
			if (form != null) {
				TEProduct teproduct = saveProduct(loginUser, productAndDataReq.getFormId(),
						Constant.product_status_create, form.getCategoryId());
				if (teproduct != null) {
					saveProductDataJson(productAndDataReq.getProductDataJson(), teproduct.getId(), loginUser, fileList);// 做成产品数据
					MProductData productData = new MProductData();
					List<MWidget> widgetList = new ArrayList<>();
					productData.setWidget(widgetList);
					productData.setProductId(teproduct.getId());
					createProductDataLog(productAndDataReq.getProductDataJson(), productData, loginUser.getCorpId());// 做成产品数据log
				}
			}
		} else {
			return updateProduct(loginUser, productAndDataReq, product);
		}
		return null;
	}

	/**
	 * 保存产品
	 * 
	 * @param product
	 */
	private TEProduct saveProduct(LoginUser loginUser, String formId, String status, String categoryId) {
		TEProduct product = new TEProduct();
		product.setFormId(formId);
		product.setId(UUID.randomUUID());
		JSONObject showAttrs = new JSONObject();
		showAttrs.put("name", "SYFF2714");
		showAttrs.put("primaryId", "DXUQ7278");
		product.setShowAttrs(showAttrs.toString());
		product.setStatus(status);
		product.setCategoryId(categoryId);
		setDBCommonField(product, loginUser);

		return productService.saveProduct(product);
	}

	/**
	 * 更改产品
	 * 
	 * @param product
	 */
	private MProductDataLog updateProduct(LoginUser loginUser, SaveProductAndDataReq productAndDataReq,
			TEProduct product) {
		MProductData productData = mongoService.getProductData(product.getId(), loginUser.getCorpId());
		if (!productData.getTimeStamp().equals(productAndDataReq.getTimeStamp())
				&& productAndDataReq.isOverWriteFlag() != true) {
			// // 比较产品数据
			// MProductDataLog productDataLog =
			// compareProductData(productAndDataReq.getProductDataJson(),
			// productData.getWidget(),
			// productData.getProductId());
			MProductDataLog productDataLog = this.productLogMongoService.getLatestProductData(product.getId(),
					loginUser.getCorpId());
			// get AttributeName
			// LoginUser loginUser =
			// WebContext.getLoginUserFromHttpSession(httpSession);
			TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
					loginUser.getCorpId());
			String lanCode = Common.getStringValue(config.getValue(), StringUtils.EMPTY);
			getAttrNameById(productDataLog, lanCode);

			return productDataLog;
		} else {
			product.setStatus(Constant.product_status_update);
			setDBCommonField(product, loginUser);
			productService.saveProduct(product);
			updateProductDataJson(productAndDataReq.getProductDataJson(), productData, loginUser.getCorpId());
		}
		return null;

	}

	/**
	 * product change log get attributeName by AttributeId
	 * 
	 * @param log
	 * @param lanCode
	 */
	private void getAttrNameById(MProductDataLog log, String lanCode) {
		String attrName = null;
		TEAttr attr = null;
//		TDicAttrsName dicAttrsName = null;
		List<MChangedAttrs> newAttrsList = new ArrayList<MChangedAttrs>();

		List<MChangedAttrs> changedAttrsList = log.getMChangedAttrs();
		for (MChangedAttrs changedAttr : changedAttrsList) {
//			dicAttrsName = dicAttrsNameService.getAttrName(changedAttr.getAttributeId(), lanCode);
//			if (dicAttrsName == null) {
			//todo 从json中获取对应语言属性名
				attr = attrsService.getAttrById(changedAttr.getAttributeId());
				attrName = Common.getStringValue(attr.getName(), StringUtils.EMPTY);
//			} else {
//				attrName = Common.getStringValue(dicAttrsName.getAttrNm(), StringUtils.EMPTY);
//			}
			changedAttr.setAttributeId(attrName);
			newAttrsList.add(changedAttr);
		}
		log.setMChangedAttrs(newAttrsList);
	}

	/**
	 * 向mongo保存产品数据
	 * 
	 * @param productDataJson
	 */
	private void saveProductDataJson(List<MWidget> productDataJson, String tepproductid, LoginUser loginUser,
			List<MultipartFile> fileList) {
		for (MWidget mWidget : productDataJson) {
			if (mWidget.getType().equals(Constant.type_image)) {
				List<Map> imageAttrList = (List<Map>) mWidget.getValue();
				for (Map mImageDataMap : imageAttrList) {
					String fileName = (String) mImageDataMap.get("fileName");
					Iterator<MultipartFile> iterator = fileList.iterator();
					if (iterator.hasNext()) {
						MultipartFile multipartFile = iterator.next();
						if (multipartFile.getOriginalFilename().equals(fileName)) {
							DBObject metaData = new BasicDBObject();
							metaData.put("datasource", "this is a img");
							String id = mGridFSImgService.saveProductImg(multipartFile, fileName,
									multipartFile.getContentType(), metaData);
							mImageDataMap.put("id", id);
						}
					}
				}
			}
		}
		MProductData productData = new MProductData();
		productData.setProductId(tepproductid);
		productData.setWidget(productDataJson);
		productData.setCorpId(loginUser.getCorpId());
		productData.setTimeStamp(DateUtils.format(new Date(), Constant.date_format_stamp));
		mongoService.saveProductData(productData);
	}

	/**
	 * 向mongo更改产品数据
	 * 
	 * @param productDataJson
	 */
	private void updateProductDataJson(List<MWidget> productDataJson, MProductData productData, Long corpId) {
		createProductDataLog(productDataJson, productData, corpId);
		// productData.setPreWidget(productData.getWidget());
		productData.setWidget(productDataJson);
		productData.setTimeStamp(DateUtils.format(new Date(), Constant.date_format_stamp));
		mongoService.saveProductData(productData);
	}

	/**
	 * 生成产品变更履历
	 */
	private void createProductDataLog(List<MWidget> current, MProductData productData, Long corpId) {

		// 比较产品数据
		MProductDataLog productDataLog = compareProductData(current, productData.getWidget(),
				productData.getProductId(), corpId);

		if (productDataLog.getMChangedAttrs().size() > 0) {
			productLogMongoService.saveProductDataLog(productDataLog);
		}
	}

	/**
	 * 比较产品变更属性
	 */
	private MProductDataLog compareProductData(List<MWidget> currentData, List<MWidget> previousData, String productId,
			Long corpId) {
		MProductDataLog productDataLog = new MProductDataLog();
		List<MChangedAttrs> changedDataList = new ArrayList<>();
		if (previousData == null || previousData.size() == 0) {// 新规
			for (int i = 0; i < currentData.size(); i++) {
				// 修改前属性值
				String attrValPrevious = "";
				// 修改后属性值
				String attrIdCurrent = currentData.get(i).getKey();
				MChangedAttrs changedAttrs = new MChangedAttrs();
				MAtrtributeID atrtributeID = new MAtrtributeID();
				if (currentData.get(i).getType().equals(Constant.type_image)) {
					// todo
					// List<MImageData> attrValCurrentList = (List<MImageData>)
					// currentData.get(i).getValue();
					// if(attrValCurrentList != null &&
					// attrValCurrentList.size() >0) {
					//
					// }
				} else {
					Object attrValCurrent = currentData.get(i).getValue();
					atrtributeID.setCurrentValue(attrValCurrent);
				}
				atrtributeID.setPreviousValue(attrValPrevious);
				changedAttrs.setAttributeId(attrIdCurrent);
				changedAttrs.setMAtrtributeID(atrtributeID);
				changedDataList.add(changedAttrs);
			}
		} else {
			for (int i = 0; i < previousData.size(); i++) {// 变更
				// 修改前属性值
				String attrIdPrevious = previousData.get(i).getKey();
				// 修改后属性值
				String attrIdCurrent = currentData.get(i).getKey();
				if (currentData.get(i).getType().equals(Constant.type_image)) {
					// todo
				} else {
					Object attrValCurrent = currentData.get(i).getValue();
					Object attrValPrevious = previousData.get(i).getValue();
					if (attrIdPrevious.equals(attrIdCurrent) && !attrValPrevious.equals(attrValCurrent)) {// 不等->做成变更log
						MChangedAttrs changedAttrs = new MChangedAttrs();
						MAtrtributeID atrtributeID = new MAtrtributeID();
						atrtributeID.setCurrentValue(attrValCurrent);
						atrtributeID.setPreviousValue(attrValPrevious);
						changedAttrs.setAttributeId(attrIdCurrent);
						changedAttrs.setMAtrtributeID(atrtributeID);
						changedDataList.add(changedAttrs);
					}
				}

			}
		}
		productDataLog.setProductId(productId);
		productDataLog.setDatetime(DateUtils.format(new Date(), Constant.date_format));
		productDataLog.setMChangedAttrs(changedDataList);
		productDataLog.setCorpId(corpId);
		productDataLog.setMode(Constant.mode_selft);
		return productDataLog;
	}

	/**
	 * 从mongo获取产品数据
	 * 
	 * @param productId
	 * @return
	 */
	private MProductData getProductDataJson(String productId, Long corpId) {
		return mongoService.getProductData(productId, corpId);
	}

	/**
	 * 获取表单模版下拉列表
	 * 
	 * @param categoryId
	 * @return
	 */
	private List<InformationWeb> getFormLinkItemInfo(String categoryId, boolean isNew) {
		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();

		List<TEForm> formList = null;
		if (categoryId == null) {
			formList = formService.getFormListByCategoryIdNotNull();
		} else {
			formList = formService.getFormListByCategoryId(categoryId);
		}

		InformationWeb informationWeb = null;
		for (TEForm form : formList) {

			if (isNew) {
				if (Common.isNullOrEmpty(form.getCommand())) {
					continue;
				}

				JSONArray jsonArr = JSONArray.fromObject(form.getCommand());
				long count = jsonArr.stream().filter(m -> ((JSONObject) m).containsValue("edit")).count();

				if (count == 0) {
					continue;
				}
			}

			informationWeb = new InformationWeb();

			informationWeb.setId(form.getId());
			informationWeb.setName(form.getName());

			informationWebList.add(informationWeb);
		}

		return informationWebList;
	}

	private void setDBCommonField(TEProduct product, LoginUser loginUser) {
		String loginUserId = loginUser.getUserId();
		Long corpid = loginUser.getCorpId();
		product.setCorpid(corpid);
		String now = DateUtils.format(new Date(), Constant.date_format);
		product.setUserId(loginUserId);
		product.setDelFlag(0);
		product.setcTime(now);
		product.setcUser(loginUserId);
		product.setuTime(now);
		product.setuUser(loginUserId);
	}

	/**
	 * 获取产品修改历史记录
	 * @param product
	 * @param lanCode
	 * @param informationWebList
	 * @param hasHistory
	 * @param corpId
	 */
	private void getProductLogInfo(TEProduct product, String lanCode, List<InformationWeb> informationWebList,
			boolean hasHistory, Long corpId) {
		String name = StringUtils.EMPTY;
		String primaryId = StringUtils.EMPTY;
		List<String> contentList = null;

		List<MWidget> widgetArray = getNameAndPrimaryId(product, corpId);
		if (widgetArray != null) {
			JSONObject attrsJson = JSONObject.fromObject(product.getShowAttrs());
			String nameKey = attrsJson.getString("name");
			String primaryIdKey = attrsJson.getString("primaryId");

			for (MWidget o : widgetArray) {
				String key = o.getKey();
				if (!Common.isNullOrEmpty(key) && key.equals(nameKey)) {
					if (o.getType().equals(Constant.type_image)) {
						// todo
					} else {
						name = (String) o.getValue();
					}
				}
				if (!Common.isNullOrEmpty(key) && key.equals(primaryIdKey)) {
					if (o.getType().equals(Constant.type_image)) {
						// todo
					} else {
						primaryId = (String) o.getValue();
					}
				}
			}

			List<MProductDataLog> mongLogList = new ArrayList<>();
			if (hasHistory) {
				List<MProductDataLog> tmpList = productLogMongoService.getAllProductData(product.getId(), corpId);
				if (tmpList != null && !tmpList.isEmpty()) {
					mongLogList.addAll(tmpList);
				}
			} else {
				MProductDataLog mongLog = productLogMongoService.getLatestProductData(product.getId(), corpId);
				if (mongLog != null) {
					mongLogList.add(mongLog);
				}
			}

			for (MProductDataLog log : mongLogList) {
				contentList = new ArrayList<>();

				getContent(product.getId(), lanCode, log, contentList);

				setInformationWeb(informationWebList, log.getId(), product.getId(), name, primaryId, corpId,
						log.getDatetime(), contentList);
			}
		}
	}

	private void setInformationWeb(List<InformationWeb> informationWebList, String productLogId, String productId,
			String name, String primaryId, Long corpId, String datetime, List<String> contentList) {
		InformationWeb informationWeb = new InformationWeb();

		informationWeb.setId(productId);
		informationWeb.setProductLogId(productLogId);
		informationWeb.setName(name);
		informationWeb.setPrimaryId(primaryId);
		informationWeb.setUpdateTime(datetime);
		if (contentList.isEmpty()) {
			informationWeb.setContent(StringUtils.EMPTY);
			informationWeb.setFirstContent(StringUtils.EMPTY);
		} else {
			informationWeb.setContent(StringUtils.join(contentList, "\n"));
			informationWeb.setFirstContent(contentList.get(0));
		}
		informationWeb.setCorpId(corpId);

		informationWebList.add(informationWeb);
	}

	
	private void output(Long corpId, String productId, String mappingId, Long fromCorpId) {
		String outputFormat = outputFormatService.getOutputFormat(corpId);
		if (Constant.file_suffix_xml.equals(outputFormat)) {
			GridFSDBFile gfsMappingFile = mGridFSMappingService.findFileById(mappingId);
			try {
				File mappingFile = FileUtil.creatMappingFile();
				mGridFSMappingService.writeToFile(mappingFile, gfsMappingFile);
				File xmlFile = xmlGenerate.convert2Xml(mappingFile, productId, fromCorpId);
				mGridFSOutputService.saveXmlFormatDocument(xmlFile);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
