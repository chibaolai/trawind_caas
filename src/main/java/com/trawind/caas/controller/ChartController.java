package com.trawind.caas.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.TEChartData;
import com.trawind.caas.entity.TEChartType;
import com.trawind.caas.req.ChartReq;
import com.trawind.caas.service.TEChartDataService;
import com.trawind.caas.service.TEChartTypeService;
import com.trawind.caas.web.LoginUser;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 图表构建controller
 * @author PC
 */

@RestController
@RequestMapping("chart")
public class ChartController {

	@Autowired
	TEChartDataService chartDataService;

	@Autowired
	TEChartTypeService chartTypeService;

	/**
	 * 获取图表类型信息
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getChartTypeInfo" })
	public String getChartTypeInfo(HttpSession httpSession) {
		// 取得loginUserId
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		JSONArray jsonArray = new JSONArray();
		JSONObject jsonMap = new JSONObject();

		// 图表的类型
		List<TEChartType> chartTypeList = chartTypeService.getChartTypeList(loginUser.getUserId(),
				loginUser.getCorpId());
		for (TEChartType chartType : chartTypeList) {
			jsonMap = new JSONObject();

			jsonMap.put("styleId", String.valueOf(chartType.getStyleId()));
			jsonMap.put("typeId", String.valueOf(chartType.getTypeId()));
			jsonMap.put("name", chartType.getName());

			jsonArray.add(jsonMap);
		}

		return jsonArray.toString();
	}

	/**
	 * 获取图表数据
	 * @param chartReq
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getChartInfo" })
	public String getChartInfo(@RequestBody ChartReq chartReq, HttpSession httpSession) {
		// 取得loginUserId
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);

		JSONObject jsonObj = new JSONObject(false);
		// 前台传入参数
		if (chartReq != null) {
			// 图表的设定信息
			TEChartType chartType = chartTypeService.getChartType(chartReq.getStyleId(), chartReq.getTypeId(),
					loginUser.getUserId(), loginUser.getCorpId());
			if (chartType != null) {
				jsonObj = JSONObject.fromObject(chartType.getOptionItem());

				// legend ==> data
				Map<String, List<String>> legendDataMap = new HashMap<>();
				List<String> legendDataList = new ArrayList<>();
				// series ==> name type data stack
				List<Map<String, Object>> seriesList = new ArrayList<>();
				Map<String, Object> seriesMap = null;

				// 图表的表示数据
				List<TEChartData> chartDataList = chartDataService.getChartData(chartType.getStyleId(),
						chartType.getTypeId(), loginUser.getUserId(), loginUser.getCorpId());

				for (TEChartData chartData : chartDataList) {
					String itemName = Common.getStringValue(chartData.getItemName(), Common.STRING_EMPTY);
					String itemType = Common.getStringValue(chartData.getItemType(), Common.STRING_EMPTY);
					String itemData = Common.getStringValue(chartData.getItemData(), Common.STRING_EMPTY);
					String itemStack = Common.getStringValue(chartData.getItemStack(), Common.STRING_EMPTY);

					// legend ==> data
					if (!legendDataList.contains(itemName)) {
						legendDataList.add(itemName);
					}

					// series ==> name type data stack
					seriesMap = new HashMap<>();
					seriesMap.put("name", itemName);
					seriesMap.put("type", itemType);

					List<String> dataList = Common.trimArrayStrToList(itemData.split(","));
					seriesMap.put("data", dataList);

					if (!itemStack.isEmpty()) {
						seriesMap.put("stack", chartData.getItemStack());
					}

					seriesList.add(seriesMap);
				}

				// legend ==> data
				legendDataMap.put("data", legendDataList);
				jsonObj.put("legend", legendDataMap);
				// series ==> name type data stack
				jsonObj.put("series", seriesList);
			}
		}

		return jsonObj.toString();
	}
}
