package com.trawind.caas.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;

@RestController
@RequestMapping("config")
public class TEConfigController {
	public enum ConfigKey {
		LanguageCode("1"), CommandCode("2");

		private String mDbValue = Common.STRING_EMPTY;

		private ConfigKey(String dbValue) {
			mDbValue = dbValue;
		}

		public String getDbValue() {
			return mDbValue;
		}
	}

//	@Autowired
//	private TEConfigService configService;

//	@RequestMapping(value = { "/getLanguageCode" })
//	public String getLanguageCode(HttpSession httpSession) {
//		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
//
//		TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
//				loginUser.getCorpId());
//
//		JSONObject json = new JSONObject(false);
//		if (config == null) {
//			return json.toString();
//		}
//
//		json.put("code", config.getValue());
//		return json.toString();
//	}

//	@RequestMapping(value = { "/setLanguageCode/{code}" })
//	public String setLanguageCode(@PathVariable String code, HttpSession httpSession) {
//		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
//
//		TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
//				loginUser.getCorpId());
//
//		if (config == null) {
//			config = new TEConfig();
//			config.setId(UUID.randomUUID());
//			config.setCorpId(loginUser.getCorpId());
//			config.setUserId(loginUser.getUserId());
//			config.setKey(ConfigKey.LanguageCode.getDbValue());
//		}
//		config.setValue(code);
//
//		configService.setLanguageCode(config);
//
//		return (new JSONObject(false)).toString();
//	}

//	@RequestMapping(value = { "/getCommandCode" })
//	public String getCommandCode(HttpSession httpSession) {
//		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
//
//		TEConfig config = configService.getCommandCode(ConfigKey.CommandCode.getDbValue(), loginUser.getCorpId());
//
//		JSONObject json = new JSONObject(false);
//		if (config == null || Common.isNullOrEmpty(config.getValue())) {
//			return json.toString();
//		}
//
//		return JSONObject.fromObject(config.getValue()).toString();
//	}

//	@RequestMapping(value = { "/setCommandCode" })
//	public String setCommandCode(HttpSession httpSession, @RequestBody final LinkedHashMap<String, String> dataReqMap) {
//		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
//
//		TEConfig config = configService.getCommandCode(ConfigKey.CommandCode.getDbValue(), loginUser.getCorpId());
//
//		if (config == null) {
//			config = new TEConfig();
//			config.setId(UUID.randomUUID());
//			config.setCorpId(loginUser.getCorpId());
//			config.setUserId(null);
//			config.setKey(ConfigKey.CommandCode.getDbValue());
//		}
//
//		// TODO
//		config.setValue("");
//
//		configService.setCommandCode(config);
//
//		return (new JSONObject(false)).toString();
//	}
}
