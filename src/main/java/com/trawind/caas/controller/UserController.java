package com.trawind.caas.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.DateUtil;
import com.trawind.caas.common.MailUtil;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.TECompHome;
import com.trawind.caas.entity.TECorp;
import com.trawind.caas.entity.TEFormHome;
import com.trawind.caas.entity.TETrade;
import com.trawind.caas.pay.alipay.config.AlipayConfig;
import com.trawind.caas.pay.alipay.util.AlipaySubmit;
import com.trawind.caas.pay.alipay.util.UtilDate;
import com.trawind.caas.req.RegistReq;
import com.trawind.caas.res.RegistResWeb;
import com.trawind.caas.security.SysRole;
import com.trawind.caas.security.SysUser;
import com.trawind.caas.service.TECompHomeService;
import com.trawind.caas.service.TECorpService;
import com.trawind.caas.service.TEFormHomeService;
import com.trawind.caas.service.TESysRoleService;
import com.trawind.caas.service.TETradeService;
import com.trawind.caas.service.TEUserService;
import com.trawind.caas.utils.ConvertFactory;
import com.trawind.caas.utils.DozerConvertFactory;
import com.trawind.caas.utils.UUID;
import com.trawind.caas.web.LoginUser;

@RestController
@RequestMapping("userCtl")
public class UserController {
	private static final String DEFAULE_ROLE_NAME = "ROLE_ADMIN";

	@Autowired
	private TEUserService userService;
	@Autowired
	private TESysRoleService sysRoleService;
	@Autowired
	private TECorpService corpService;

	@Autowired
	private TETradeService tradeService;

	@Autowired
	private TEFormHomeService formHomeService;

	@Autowired
	private TECompHomeService compHomeService;

	@RequestMapping(value = "/getUserInfo")
	public LoginUser getUserInfo(HttpSession httpSession) {
		return WebContext.getLoginUserFromHttpSession(httpSession);
	}
	/**
	 * 登陆验证
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/loginCheck")
	public RegistResWeb loginCheck(@RequestBody RegistReq req) {
		RegistResWeb registRes = new RegistResWeb();
		registRes.setRegistResult("0");//正常
		SysUser sysUser = userService.findByUsername(req.getLoginname());
		if(sysUser == null){
			//该用户不存在
			registRes.setRegistResult("1");
		}else{
			boolean matched = BCrypt.checkpw(req.getPassword(), sysUser.getPassword());
			if(!matched){
				//该用户密码不正确
				registRes.setRegistResult("2");
			}
		}
		return registRes;
	}

	/**
	 * 新用户注册
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/regist")
	public RegistResWeb regist(@RequestBody RegistReq req) {

		RegistResWeb registRes = new RegistResWeb();
		registRes.setRegistResult("0");
		if (corpService.findByName(req.getCompanyname()) != null) {
			// 用户存在
			registRes.setRegistResult("3");
			return registRes;
		}
		String[] payTypeStr = req.getPayType().split(",");
		boolean payFlag = true;
		if (payTypeStr.length == 1 && payTypeStr[0].equals("0")) {
			payFlag = false;
		}

		TECorp corp = new TECorp();
		corp.setName(req.getCompanyname());
		corp.setcTime(DateUtil.getCurrentTime());
		corp.setuTime(DateUtil.getCurrentTime());
		corp.setcUser("SYSTEM");
		corp.setuUser("SYSTEM");
		corp.setHasDistributorRole(true);
		corp = corpService.saveTECorp(corp);
		SysUser sysUser = new SysUser();
		String password = "";
		if (!payFlag) {
			password = Common.generateNumber(8);
			sysUser.setPassword(Common.encodePassword(password));
			sysUser.setUsername(Common.randomString(8));
		} else {
			sysUser.setPassword(password);// 支付完成补齐
			sysUser.setUsername("");
		}
		sysUser.setEmail(req.getEmail());
		sysUser.setMobile(req.getMobile());
		sysUser.setRealname(req.getRealname());
		sysUser.setAddress(req.getAddress());
		sysUser.setCorpid(corp.getId());
		// 获取指定role
		SysRole role = sysRoleService.findSysRoleByName(DEFAULE_ROLE_NAME);
		List<SysRole> rowList = new ArrayList<SysRole>();
		rowList.add(role);
		sysUser.setRoles(rowList);

		SysUser newSysUser = userService.saveSysUser(sysUser);
		if (newSysUser != null) {
			// 创建订单 t_e_trade
			UtilDate date = new UtilDate();
			String trad_no = date.getOrderNum();
			if (!payFlag) {
				sysUser.setPassword(password);
				if (MailUtil.sendMail(sysUser)) {
					// 发送成功
				} else {
					// 发送失败
				}
				registRes.setRegistResult("2");// 普通注册成功
			} else {
				TETrade trade = new TETrade();
				trade.setCompanyId(corp.getId());// 公司ID
				trade.setOutTradeNo(trad_no);
				trade.setTradeStatus("0");
				trade = tradeService.saveTETrade(trade);

				// 更新中间表
				for (int j = 0; j < payTypeStr.length; j++) {
					if (!payTypeStr[j].equals("0")) {
						// 循环插入
					}
				}

				registRes.setRegistResult("1");
				registRes.setsHtmlText(getSHtmlText(req, trad_no));
			}

			// 默认主页模板建立
			TEFormHome formTmp = formHomeService.getDefaultForm();
			if (formTmp != null) {
				ConvertFactory cf = new DozerConvertFactory();
				TEFormHome newFormTmp = cf.convert(formTmp, TEFormHome.class);
				newFormTmp.setNo(null);
				newFormTmp.setId(UUID.randomUUID());
				newFormTmp.setCode(TEFormHomeService.CODE_TRAWIND);
				newFormTmp.setCorpid(newSysUser.getCorpid());
				newFormTmp.setUserId(String.valueOf(newSysUser.getId()));
				newFormTmp.setcUser(String.valueOf(newSysUser.getId()));
				newFormTmp.setuUser(String.valueOf(newSysUser.getId()));
				newFormTmp.setcTime(DateUtil.getCurrentTime());
				newFormTmp.setuTime(DateUtil.getCurrentTime());

				formHomeService.saveFormOne(newFormTmp);
			}

			// 主页默认控件
			List<TECompHome> newCompList = new ArrayList<>();

			List<TECompHome> tmpCompList = compHomeService.getLayoutCompList(0L);
			setNewComp(newSysUser, tmpCompList, newCompList);

			tmpCompList = compHomeService.getCustomCompList(0L);
			setNewComp(newSysUser, tmpCompList, newCompList);

			if (!newCompList.isEmpty()) {
				compHomeService.saveTECompHomeList(newCompList);
			}
		}
		return registRes;
	}

	private void setNewComp(SysUser newSysUser, List<TECompHome> tmpCompList, List<TECompHome> newCompList) {
		ConvertFactory cf = new DozerConvertFactory();
		TECompHome newCompTmp;
		for (TECompHome comp : tmpCompList) {
			newCompTmp = cf.convert(comp, TECompHome.class);

			newCompTmp.setNo(null);
//			newCompTmp.setId(UUID.randomUUID());
			newCompTmp.setcTime(DateUtil.getCurrentTime());
			newCompTmp.setcUser(String.valueOf(newSysUser.getId()));
			newCompTmp.setuTime(DateUtil.getCurrentTime());
			newCompTmp.setuUser(String.valueOf(newSysUser.getId()));
			newCompTmp.setUserId(String.valueOf(newSysUser.getId()));
			newCompTmp.setCorpId(newSysUser.getCorpid());

			newCompList.add(newCompTmp);
		}
	}

	/**
	 * 即时到帐接口 注意***************** 如果您在接口集成过程中遇到问题，可以按照下面的途径来解决
	 * 1、开发文档中心（https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.
	 * 0.KvddfJ&treeId=62&articleId=103740&docType=1）
	 * 2、商户帮助中心（https://cshall.alipay.com/enterprise/help_detail.htm?help_id=
	 * 473888） 3、支持中心（https://support.open.alipay.com/alipay/support/index.htm）
	 * 如果不想使用扩展功能请把扩展功能参数赋空值。
	 **********************************************
	 */
	private String getSHtmlText(RegistReq req, String trad_no) {

		//////////////////////////////////// 请求参数//////////////////////////////////////

		// 订单名称，必填
		String subject = "您在www.CAAS.com充值" + getRechargeAmount(req) + "元";

		// 付款金额，必填
		String total_fee = getRechargeAmount(req);

		// 商品描述，可空
		String body = "default";

		//////////////////////////////////////////////////////////////////////////////////

		// 把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", AlipayConfig.service);
		sParaTemp.put("partner", AlipayConfig.partner);
		sParaTemp.put("seller_id", AlipayConfig.seller_id);
		sParaTemp.put("_input_charset", AlipayConfig.input_charset);
		sParaTemp.put("payment_type", AlipayConfig.payment_type);
		sParaTemp.put("notify_url", AlipayConfig.notify_url);
		sParaTemp.put("return_url", AlipayConfig.return_url);
		sParaTemp.put("anti_phishing_key", AlipayConfig.anti_phishing_key);
		sParaTemp.put("exter_invoke_ip", AlipayConfig.exter_invoke_ip);
		sParaTemp.put("out_trade_no", trad_no);
		sParaTemp.put("subject", subject);
		sParaTemp.put("total_fee", total_fee);
		sParaTemp.put("body", body);
		// 其他业务参数根据在线开发文档，添加参数.文档地址:https://doc.open.alipay.com/doc2/detail.htm?spm=a219a.7629140.0.0.O9yorI&treeId=62&articleId=103740&docType=1
		// 如sParaTemp.put("参数名","参数值");

		// 建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp, "get", "确认");
		return sHtmlText;
	}

	private String getRechargeAmount(RegistReq req) {
		if (!Common.isNullOrEmpty(req.getPayType())) {
			if ("1".equals(req.getPayType())) {
				//
				return "3000";
			} else {
				return "";
			}
		}
		return "";
	}
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(4);
	}
	public static void main(String[] args) {
		String str = "1";

		System.out.println(str.split(",")[0]);
	}
}
