package com.trawind.caas.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.drools.core.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.MCorporationData;
import com.trawind.caas.entity.MFormtemplData;
import com.trawind.caas.entity.MProfileData;
import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TEForm;
import com.trawind.caas.entity.TETemp;
import com.trawind.caas.req.FormReq;
import com.trawind.caas.res.FormResWeb;
import com.trawind.caas.res.InformationWeb;
import com.trawind.caas.res.SaveUserFormsResWeb;
import com.trawind.caas.service.TEAttrsService;
import com.trawind.caas.service.TECorporationMongoService;
import com.trawind.caas.service.TEFormService;
import com.trawind.caas.service.TEProfileMongoService;
import com.trawind.caas.service.TETempService;
import com.trawind.caas.utils.DateUtils;
import com.trawind.caas.utils.UUID;
import com.trawind.caas.web.LoginUser;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@RestController
@RequestMapping("userForms")
public class FormsController {
	@Autowired
	private TEFormService formService;

	@Autowired
	private TETempService tempService;

	@Autowired
	private TEAttrsService attrsService;
	
	@Autowired
	private TEProfileMongoService profileMongoService; 
	
	@Autowired
	private TECorporationMongoService corporationMongoService;

	/**
	 * 保存自定义表单布局
	 * 
	 * @param formReq
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/saveUserForms" }, produces = { "application/json;charset=UTF-8" })
	public SaveUserFormsResWeb saveUserForms(@RequestBody FormReq formReq, HttpSession httpSession) {
		SaveUserFormsResWeb ret = new SaveUserFormsResWeb();
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		boolean isNewFlag = false;
		if (loginUser == null) {
			// return "login";
			System.out.println("login");
			return null;
		}
		if (StringUtils.isEmpty(formReq.getCorpCode()) && StringUtils.isEmpty(formReq.getCorpId())) {
			System.out.println("error");
			return null;
		}
		if (StringUtils.isEmpty(formReq.getFormId())) {
			isNewFlag = true;
		}
		TEForm form = null;
		if (isNewFlag) {// 新规
			form = new TEForm();
			form.setId(UUID.randomUUID());
			form.setCategoryId(formReq.getCategoryId());
		} else {// 编辑
			form = formService.getForm(loginUser.getCorpId(), formReq.getFormId());
			if (form == null) {
				System.out.println("error");
				return null;
			}
		}

		form.setCommand((new JSONArray()).toString());
		if (formReq.getSelCommandList() != null) {
			form.setCommand(formReq.getSelCommandList().toString());
		}
		
		form.setFormType(formReq.getFormType());
		form.setDirectJson(formReq.getFormLayoutJson().toString());
		form.setCorpCode(formReq.getCorpCode().trim());
		form.setCorpid(Long.valueOf(formReq.getCorpId().trim()));
		form.setName(formReq.getFormName() == null ? Common.STRING_EMPTY : formReq.getFormName().trim());
		form.setLayout(formReq.getLayout());
		form.setWidgetIds(formReq.getWidgetIds() == null ? Common.STRING_EMPTY : formReq.getWidgetIds().trim());
		this.setDBCommonField(form, httpSession);
		TEForm formRes = formService.saveFormOne(form, isNewFlag);
		ret.setFormId(formRes.getId());
		ret.setFormName(formRes.getName());
		return ret;
	}

	/**
	 * 从home页面FormLink调转
	 * 
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/getFormLinkItemInfo" })
	public List<InformationWeb> getFormLinkItemInfo(HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		List<InformationWeb> informationWebList = new ArrayList<InformationWeb>();

		List<TEForm> formList = formService.getFormListByCorpid(loginUser.getCorpId());

		InformationWeb informationWeb = null;
		for (TEForm form : formList) {
			informationWeb = new InformationWeb();

			informationWeb.setId(form.getId());
			informationWeb.setName(form.getName());

			informationWebList.add(informationWeb);
		}

		return informationWebList;
	}

	/**
	 * 获取默认模板
	 * 
	 * @return
	 */
	private TETemp getDefaultTemp() {
		List<TETemp> tempList = tempService.getList();
		if (tempList == null) {
			System.err.println("模板为空");
		}
		return tempList.get(0);
	}

	/**
	 * 获取表单
	 * 
	 * @param req
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getUserForms")
	public FormResWeb getUserForms(@RequestBody FormReq req, HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		FormResWeb ret = new FormResWeb();
		String newFormId = req.getFormId();// 获取指定form
		if (StringUtils.isEmpty(newFormId)) {// 没有表单，获取默认模板
			ret.setTemp(getDefaultTemp());
			// 获取基本属性--START
			List<TEAttr> attrsList = attrsService.findBaseAttrList();
			if (attrsList != null) {
				JSONArray baseAttrList = new JSONArray();
				for (TEAttr attr : attrsList) {
					JSONObject directJson = JSONObject.fromObject(attr.getDirecJson());
					directJson.put("type", attr.getWidgetType());
					directJson.put("id", attr.getId());
					JSONObject baseAttr = new JSONObject();
					baseAttr.put(attr.getId(), directJson);
					baseAttrList.add(baseAttr);
				}
				ret.setBaseAttrList(baseAttrList);
			}
			// 获取基本属性--END
			return ret;
		}

		TEForm form = formService.getForm(loginUser.getCorpId(), newFormId);
		if (form == null) {// 没有表单，获取默认模板
			ret.setTemp(getDefaultTemp());
			return ret;
		}
		ret.setForm(form);
		return ret;
	}

	private void setDBCommonField(TEForm form, HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		String loginUserId = loginUser.getUserId();
		Long corpid = loginUser.getCorpId();
		form.setCorpid(corpid);
		String now = DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
		form.setUserId(loginUserId);
		form.setDelFlag(0);
		form.setcTime(now);
		form.setcUser(loginUserId);
		form.setuTime(now);
		form.setuUser(loginUserId);
	}
	/**
	 * 根据类型获取表单信息
	 * -用于用户管理页面corporation、profile
	 * @param type
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getFormByType")
	public JSONObject getUserForms(@RequestParam(value="type") String type, HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		JSONObject result = new JSONObject();
		TEForm form = formService.geTeFormsByFormType(type);
		if (form != null) {
			result.put("form", form);
		}
		String userId = loginUser.getUserId();
		String corId = String.valueOf(loginUser.getCorpId());
		
		switch (type) {
			case "profile":
				MProfileData profile = profileMongoService.findByUserId(userId);
				if (profile == null) {
					profile = new MProfileData();
				}
				result.put("formData", profile.getProfileDataList());
				break;
			case "corporation":
				MCorporationData corp = corporationMongoService.findByCorporationId(corId);
				if (corp == null) {
					corp = new MCorporationData();
				}
				result.put("formData", corp.getCorporationDataList());
				break;
	
			default:
				break;
		}
		return result;  
	}
	/**
	 * 根据类型保存表单信息		
	 * @param formtempl
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value="/saveFormData")
	public JSONObject saveFormData(@RequestBody MFormtemplData formtempl, HttpSession httpSession){
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		JSONObject result = new JSONObject();
		if (loginUser == null) {
			result.put("success", false);
			return result;
		}
		String userId = loginUser.getUserId();
		String corId = String.valueOf(loginUser.getCorpId());
		switch (formtempl.getCategory()) {
			case "profile":
				MProfileData profileData = new MProfileData();
				profileData= profileMongoService.findByUserId(userId);
				if (profileData == null) {
					profileData = new MProfileData();
					profileData.setUserId(userId);
				}
				profileData.setProfileDataList(formtempl.getWidgets());
				profileMongoService.saveProfile(profileData);
				break;
			case "corporation":
				MCorporationData data = new MCorporationData();
				data = corporationMongoService.findByCorporationId(corId);
				if (data == null) {
					data = new MCorporationData();
					data.setCorporationId(corId);
				}
				data.setCorporationDataList(formtempl.getWidgets());
				corporationMongoService.saveCorporation(data);
				break;
	
			default:
				break;
		}
		result.put("success", true);
		return result;
	}
}
