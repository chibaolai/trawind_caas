package com.trawind.caas.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TEImgCategory;
import com.trawind.caas.entity.TELang;
import com.trawind.caas.entity.TEWidgetType;
import com.trawind.caas.req.SaveAttrReq;
import com.trawind.caas.service.TEAttrsService;
import com.trawind.caas.service.TEImgCategoryService;
import com.trawind.caas.service.TELangService;
import com.trawind.caas.service.TEWidgetTypeService;

@RestController
@RequestMapping(value = "attrCtl")
public class AttrsController {
	@Autowired
	private TEAttrsService attrsService;
	
	@Autowired
	private TEImgCategoryService imgCategoryService;
	
	@Autowired
	private TELangService teLangService;
	
	@Autowired
	private TEWidgetTypeService widgetTypeService;
	
	/**
	 * 获取所有属性List
	 * @param httpSession
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/getAttr")
	public List<TEAttr> getAttr(HttpSession httpSession,HttpServletResponse response) {
		List<TEAttr> attrList = null;
		attrList = attrsService.getAttrListByCorpCode("Trawind");
		return attrList;
	}
	
	/**
	 * 保存属性
	 * @param req
	 * @return
	 */
	 @RequestMapping(value = "/saveAttr")
	 public TEAttr saveAttr(@RequestBody SaveAttrReq req) {
		 TEAttr attr = new TEAttr();
		 attr.setId(Common.randomAttrId());
		 if(null != req.getCorpCode()) {			 
			 attr.setCorpCode(req.getCorpCode());
		 }
		 if(null !=req.getAttrName()) {
			 attr.setName(req.getAttrName());
		 }
		 if(null !=req.getWidgetType()) {
			 attr.setWidgetType(req.getWidgetType());
		 }
		 if(null != req.getImgCategoryId()) {
			 attr.setImgCategoryId(req.getImgCategoryId());
		 }
		 if(null != req.getLayoutJson()) {
			 attr.setDirecJson(req.getLayoutJson());
		 }
		 /**
		  * 2.保存自定义属性表
		  */
		 TEAttr retAttr = attrsService.saveAttr(attr);
		 return retAttr;
	 }
	 
	 /**
	  * 获取所有图像管理组件
	  * @return
	  */
	 @RequestMapping(value = "/getImgCategoryList")
	 public List<TEImgCategory> getImgCategoryList() {
		 return imgCategoryService.getImgCategoryList();
	 }
	 
	 @RequestMapping(value="/getLanguages")
	 public List<TELang> getLanguages(){
		 return teLangService.getLangList();
	 }
	 
	 @RequestMapping(value="/getWidgetTypes")
	 public List<TEWidgetType> getWidgetTypes(){
		 return widgetTypeService.getWidgetTypes();
	 }
	 
	 
}
