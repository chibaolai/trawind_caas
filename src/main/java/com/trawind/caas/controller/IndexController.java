package com.trawind.caas.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexController {
//	@Autowired
//	private TEInformationService informationService;


	@RequestMapping(value = { "/index" })
	public ModelAndView index(Model model) {
		return new ModelAndView("mainHome");
	}

//	@RequestMapping(value = { "/caas" })
//	public ModelAndView caasTemp(Model model, HttpServletRequest req) {
//		Msg msg = new Msg("测试标题", "测试内容", "额外信息，只对管理员显示");
//		model.addAttribute("msg", msg);
//		Object securityContext = req.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
//		// String loginUserId =
//		// WebContext.getLoginUserIdFromHttpSession(req.getSession());
//		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(req.getSession());
//		// link search
//		String searchId = req.getParameter("searchId");
//		TEInformation information = null;
//		if (!Common.isNullOrEmpty(searchId)) {
//			information = informationService.getOneLinkInfoFormById(searchId, loginUser.getCorpId());
//		} else {
//			information = new TEInformation();
//		}
//		model.addAttribute("linkInfo", information);
//
//		// form Id
//		String formId = req.getParameter("formId");
//		if (Common.isNullOrEmpty(formId)) {
//			formId = Common.STRING_EMPTY;
//		}
//		model.addAttribute("formId", formId);
//		req.getSession().setAttribute("formId", formId);
//
//		return new ModelAndView("caas");
//	}
}
