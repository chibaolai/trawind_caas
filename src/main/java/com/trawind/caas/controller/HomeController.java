package com.trawind.caas.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.entity.TECompHome;
import com.trawind.caas.entity.TEFormHome;
import com.trawind.caas.req.FormReq;
import com.trawind.caas.res.TemplateWeb;
import com.trawind.caas.service.TECompHomeService;
import com.trawind.caas.service.TEFormHomeService;
import com.trawind.caas.utils.DateUtils;
import com.trawind.caas.utils.UUID;
import com.trawind.caas.web.LoginUser;

import net.sf.json.JSONObject;
/**
 * dashboard页control
 * @author PC
 */


@RestController
@RequestMapping("homeForms")
public class HomeController {

	@Autowired
	private TECompHomeService compHomeService;

	@Autowired
	private TEFormHomeService formHomeService;

	/**
	 * 获取页面布局List 
	 * （例如：12：1 6：6 3：3：3） 
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getLayoutCompList")
	public List<TECompHome> getLayoutCompList(HttpSession httpSession) {
		// 取得loginUser
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		if (loginUser == null) {
			return null;
		}
		return compHomeService.getLayoutCompList(loginUser.getCorpId());
	}

	/**
	 * 获取左侧导航中取得自定义控件List
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getCustomCompList")
	public List<TECompHome> getCustomCompList(HttpSession httpSession) {
		// 取得loginUser
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		return compHomeService.getCustomCompList(loginUser.getCorpId());
	}

	/**
	 * 首页页面布局保存
	 * @param formReq
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = { "/saveHomeForms" })
	public String saveUserForms(@RequestBody FormReq formReq, HttpSession httpSession) {
		TEFormHome form = new TEFormHome();
		form.setId(UUID.randomUUID());
		form.setCode(Common.getStringValue(formReq.getCorpCode(), Common.STRING_EMPTY));
		form.setLayout(Common.getStringValue(formReq.getLayout(), Common.STRING_EMPTY));
		this.setCommon(form, httpSession);
		TEFormHome formRes = formHomeService.saveFormOne(form);
		httpSession.setAttribute("newFormId", formRes.getId());
		return new JSONObject(false).toString();
	}

	/**
	 * 根据公司ID和用户ID获取首页布局展示信息
	 * @param httpSession
	 * @return
	 */
	@RequestMapping(value = "/getHomePageFromDB")
	public List<TemplateWeb> getHomePageFromDB(HttpSession httpSession) {

		// 取得loginUser
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		if (loginUser == null) {
			return null;
		}
		List<TemplateWeb> templateWebList = new ArrayList<TemplateWeb>();
		List<TEFormHome> formList = formHomeService.getFormListByCorpidAndUserIdAndName(loginUser.getCorpId(),loginUser.getUserId());
		TemplateWeb tempWeb = new TemplateWeb();
		if (formList != null && !formList.isEmpty()) {
			TEFormHome form = formList.get(formList.size() - 1);
			tempWeb.setId(form.getId());
			tempWeb.setLayout(form.getLayout());

			templateWebList.add(tempWeb);
		}

		return templateWebList;
	}

	private void setCommon(TEFormHome form, HttpSession httpSession) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		String loginUserId = loginUser.getUserId();
		Long corpid = loginUser.getCorpId();
		form.setCorpid(corpid);
		String now = DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
		form.setUserId(loginUserId);
		form.setDelFlag(0);
		form.setcTime(now);
		form.setcUser(loginUserId);
		form.setuTime(now);
		form.setuUser(loginUserId);
	}
}
