package com.trawind.caas.controller;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import com.google.common.collect.Lists;
import com.trawind.caas.base.TradeCode;
import com.trawind.caas.common.Common;
import com.trawind.caas.common.Constant;
import com.trawind.caas.common.FileUtil;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.controller.TEConfigController.ConfigKey;
import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TEConfig;
import com.trawind.caas.entity.TECorp;
import com.trawind.caas.entity.TRChannel;
import com.trawind.caas.req.DataManagementReq;
import com.trawind.caas.res.Convert2XmlMappingRes;
import com.trawind.caas.res.DataManagementResWeb;
import com.trawind.caas.security.SysRole;
import com.trawind.caas.service.MGridFSMappingService;
import com.trawind.caas.service.TEAttrsService;
import com.trawind.caas.service.TEConfigService;
import com.trawind.caas.service.TECorpService;
import com.trawind.caas.service.TEFormMongoService;
import com.trawind.caas.service.TRChannelService;
import com.trawind.caas.utils.RespMsgUtils;
import com.trawind.caas.utils.TikaUtils;
import com.trawind.caas.web.LoginUser;
import com.trawind.caas.xml.XmlMappingGenerate;
import com.trawind.caas.xml.entity.XmlAttr;
import com.trawind.caas.xml.util.DomUtil;
import com.trawind.caas.xml.util.XmlUtil;

/**
 * 数据导出导入control
 * @author PC
 */


@RestController
@RequestMapping("dataManagementCtl")
public class DataManagementController {

	private static Logger logger = Logger.getLogger(DataManagementController.class);

	private static final String ROLE_ADMIN = "ROLE_ADMIN";

	@Autowired
	XmlMappingGenerate xmlMappingGenerate;
	@Autowired
	MGridFSMappingService mGridFSMappingService;
	@Autowired
	TEFormMongoService mongoService;
	@Autowired
	private TEConfigService configService;
	@Autowired
	private TEAttrsService attrService;
	@Autowired
	private TECorpService corpService;
	@Autowired
	private TRChannelService channelService;

	/**
	 * 解析上传文件
	 * 
	 * @param httpSession
	 * @param file
	 * @return
	 */
	@RequestMapping(value = "/analyzeFile", consumes = MediaType.ALL_VALUE)
	public DataManagementResWeb analyzeFile(HttpServletRequest req, HttpSession httpSession,
			@RequestParam(value = "multiFile", required = false) MultipartFile multiFile) {
		DataManagementResWeb dataResWeb = new DataManagementResWeb();
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		if (multiFile.isEmpty()) {
			dataResWeb.setResultCode(TradeCode.file_content_changed);
			dataResWeb.setResultMessage(RespMsgUtils.respCodeForMsg(TradeCode.file_content_empty, "zh"));
			return dataResWeb;
		}
		try {
			File file = File.createTempFile(XmlUtil.getPrefix(multiFile.getOriginalFilename()),
					XmlUtil.getSuffix(multiFile.getOriginalFilename()));
			multiFile.transferTo(file);// mutiFile convertTo File

			Convert2XmlMappingRes mappingRes = xmlMappingGenerate.convert2XmlMapping(file, loginUser);
			if (TradeCode.SUCCESS.equals(mappingRes.getResultCode())) {
				dataResWeb.setSchemaList(mappingRes.getResultValue());
			}
			dataResWeb.setResultCode(mappingRes.getResultCode());
			dataResWeb.setFileName(getFileName(file.getName()));
			TikaUtils tika = new TikaUtils(file);
			tika.doTikaParse();

			String contents = tika.nextXmlPage(tika.getFileId(), 0, 10);
			dataResWeb.setBaseFileFormatStr(contents);

			// 组织结构
			List<TECorp> corpList = null;
			List<SysRole> roleList = loginUser.getRoleList();
			for (SysRole role : roleList) {
				if (role.getName().equals(ROLE_ADMIN)) {
					corpList = corpService.getAllCorpInfo();
				}
			}
			dataResWeb.setCorpList(corpList);

		} catch (IOException e) {
			logger.error("MultipartFile convert File fail. " + e.getMessage());
			e.printStackTrace();
		}
		return dataResWeb;
	}

	/**
	 * 保存Mapping文件
	 * 
	 * @param httpSession
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "saveMappingFile")
	public DataManagementResWeb saveMappingFile(HttpSession httpSession, @RequestBody DataManagementReq req) {
		DataManagementResWeb resWeb = new DataManagementResWeb();
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		if (!Common.isNullOrEmpty(req.getJsonStr())) {
			Document mappingDocument;
			try {
				mappingDocument = DomUtil.parseXMLFileInString(req.getJsonStr());
				File mappingFile = File.createTempFile(Constant.file_prefix_mapping, Constant.file_suffix_xml);
				FileUtil.writeXMLInFile(mappingFile, mappingDocument);
				String mappingId = mGridFSMappingService.saveConfigDocument(mappingFile);
				TRChannel channel = new TRChannel();
				channel.setChannelRoute("FDA");
				channel.setMappingId(mappingId);
				channel.setProtocol("AS2");
				channel.setSourceId("");
				channel.setTagetId("");
				channel.setCorpId(loginUser.getCorpId());
				channelService.saveChannel(channel);
				resWeb.setResultCode(TradeCode.SUCCESS);
				resWeb.setResultMessage(TradeCode.SUCCESS_MSG);
			} catch (Exception e) {
				logger.error("保存Mapping文件error" + e.getMessage());
				resWeb.setResultCode(TradeCode.ERROR);
				resWeb.setResultMessage(TradeCode.ERROR_MSG);
				e.printStackTrace();
			}
		}
		return resWeb;
	}

	/**
	 * 模糊查询
	 * 
	 * @param httpSession
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "searchAliesAttrsByKeyWord")
	public List<XmlAttr> searchAliesAttrsByKeyWord(HttpSession httpSession, @RequestBody DataManagementReq req) {
		LoginUser loginUser = WebContext.getLoginUserFromHttpSession(httpSession);
		TEConfig config = configService.getLanguageCode(ConfigKey.LanguageCode.getDbValue(), loginUser.getUserId(),
				loginUser.getCorpId());
		String _lang = Common.getStringValue(config.getValue(), StringUtils.EMPTY);// 多语言部分
		// List<MWidget> widgetList = null;
		List<TEAttr> attrs = attrService.findAllAttrList();
		List<XmlAttr> xmlAttrs = XmlUtil.getAttrList(attrs, _lang);
		if (Common.isNullOrEmpty(req.getParam())) {
			return xmlAttrs;
		}
		return filterAttrListByKeyWord(xmlAttrs, _lang, req.getParam());
	}

	/**
	 * 根据关键词过滤
	 */
	private List<XmlAttr> filterAttrListByKeyWord(List<XmlAttr> xmlAttrs, String _lang, String keyWord) {
		List<XmlAttr> retXmlAttr = null;

		if (xmlAttrs != null) {
			for (XmlAttr xmlAttr : xmlAttrs) {
				if (xmlAttr.getLabel() != null && xmlAttr.getLabel().contains(keyWord)) {
					if (retXmlAttr == null) {
						retXmlAttr = Lists.newArrayList();
					}
					retXmlAttr.add(xmlAttr);
				}
			}
		}
		return retXmlAttr;
	}

	/**
	 * 获取Mapping文件名字
	 * 
	 * @param xsdFile
	 * @return
	 */
	public String getConfigFileName(String fileName) {
		if (fileName != null) {
			return fileName + "Config.xml";
		}
		return null;
	}

	/**
	 * 获取xsd文件名
	 * 
	 * @param originalFilename
	 * @return
	 */
	public String getFileName(String originalFilename) {
		return originalFilename.substring(0, originalFilename.indexOf("."));
	}
}
