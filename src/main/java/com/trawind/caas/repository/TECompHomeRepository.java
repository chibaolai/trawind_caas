package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TECompHome;

public interface TECompHomeRepository extends JpaRepository<TECompHome, Integer> {
	List<TECompHome> findAByCompGroupAndCorpIdAndUserId(String compGroup, Long corpId,
			String userId);

	List<TECompHome> findByCompGroupAndCorpIdOrderBySort(String compGroup,Long corpId);
}
