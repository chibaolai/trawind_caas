package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEInformation;

public interface TEInformationRepository extends JpaRepository<TEInformation, String> {
	public List<TEInformation> findByUserIdOrderByUTime(String userId);

	public List<TEInformation> findByUserIdAndClassifyCodeAndLanDefAndCorpIdOrderByUTimeDesc(String userId,
			String classifyCode, String landef, Long corpId);

	public TEInformation findByIdAndCorpIdOrderByUTime(String id, Long corpId);

}
