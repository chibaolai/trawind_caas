package com.trawind.caas.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.trawind.caas.entity.MCorporationData;
import java.util.List;

public interface TECorporationMongoRepository extends MongoRepository<MCorporationData, String>{
	public List<MCorporationData> findByCorporationId(String corporationId);
}
