package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEChartType;

public interface TEChartTypeRepository extends JpaRepository<TEChartType, Integer> {
	public TEChartType findByStyleIdAndTypeIdAndUserIdAndCorpId(Integer styleId, Integer typeId, String userId,
			Long corpId);

	public List<TEChartType> findByUserIdAndCorpId(String userId, Long corpId);
}
