package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.trawind.caas.entity.MProfileData;

public interface TEProfileMongoRepository extends MongoRepository<MProfileData, String>{
	public List<MProfileData> findByUserId(String userid);
}
