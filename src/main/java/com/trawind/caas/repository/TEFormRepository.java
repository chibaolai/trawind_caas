package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEForm;

public interface TEFormRepository extends JpaRepository<TEForm, Long> {
	public TEForm findOneByCorpidAndId(Long corpid, String id);

	public List<TEForm> findByUserIdOrderByUTimeDesc(String userId);

	public List<TEForm> findByCorpidOrderByUTimeDesc(Long corpid);

	public List<TEForm> findByUserIdAndNameOrderByUTime(String userId, String name);

	public TEForm findOneByIdAndCategoryId(String id, String categoryId);
	
	public TEForm findOneById(String id);

	public List<TEForm> findOneByCategoryIdOrderByUTimeDesc(String categoryId);

	public List<TEForm> findByCategoryIdOrderByUTimeDesc(String categoryId);

	public List<TEForm> findByCategoryIdNotNullOrderByUTimeDesc();
	
	public List<TEForm> findByFormTypeOrderByCTimeDesc(String formType);
}
