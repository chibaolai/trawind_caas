package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEProduct;

public interface TEProductRepository extends JpaRepository<TEProduct, Integer> {

	public List<TEProduct> findByCorpidOrderByUTimeDesc(Long corpid);
	
	public TEProduct findById(String id);
	
//	public TEProduct findByPrimaryId(String primaryId);
}
