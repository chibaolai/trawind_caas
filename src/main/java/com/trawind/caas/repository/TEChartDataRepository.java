package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEChartData;

public interface TEChartDataRepository extends JpaRepository<TEChartData, Integer> {
	public List<TEChartData> findByStyleIdAndTypeIdAndUserIdAndCorpId(Integer styleId, Integer typeId, String userId,
			Long corpId);
}
