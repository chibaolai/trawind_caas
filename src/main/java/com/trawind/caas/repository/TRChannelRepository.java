package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TRChannel;

public interface TRChannelRepository extends JpaRepository<TRChannel, Integer> {
	
	public TRChannel findByChannelRoute(String channelRoute);

}
