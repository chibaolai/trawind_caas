package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.trawind.caas.entity.TELang;

public interface TELangRepository extends JpaRepository<TELang, String> {
	
}
