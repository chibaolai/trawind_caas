package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.trawind.caas.entity.MProductDataLog;

public interface TEProductLogMongoRepository extends MongoRepository<MProductDataLog, String>{

	public List<MProductDataLog> findListByProductIdAndCorpIdOrderByDatetimeDesc(String productId,Long corpId);
	
	public MProductDataLog findLatestByProductIdAndCorpIdOrderByDatetimeDesc(String productId,Long corpId);
}
