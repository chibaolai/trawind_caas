package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEIssuedRequest;

public interface TEIssuedRequestRepository extends JpaRepository<TEIssuedRequest, String> {
	public List<TEIssuedRequest> findByToCorpIdAndState(Long corpId, int state);

	public List<TEIssuedRequest> findByToCorpIdAndStateAndProductIdAndProductLogId(Long corpId, int state,
			String productId, String productLogId);
}
