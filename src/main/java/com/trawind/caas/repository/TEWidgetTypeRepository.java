package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEWidgetType;

public interface TEWidgetTypeRepository extends JpaRepository<TEWidgetType, Integer> {
	
}
