package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.security.SysUser;
/**
 * 用户信息管理
 * @author PC
 *
 */
public interface TEUserRepository extends JpaRepository<SysUser, Integer> {
	public SysUser findByCorpid(Long corpid);
	public SysUser findByUsername(String username);
	public SysUser findByPassword(String password);
	
}
