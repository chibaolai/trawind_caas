package com.trawind.caas.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.trawind.caas.entity.MWelcomeData;

public interface TEWelcomeMongoRepository extends MongoRepository<MWelcomeData, String> {

	public MWelcomeData findByLanguageCode(String languageCode);
}
