package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TETemp;

public interface TETempRepository extends JpaRepository<TETemp,Integer>{


	public List<TETemp> findAllByOrderByNoDesc();
}
