package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.security.SysRole;
/**
 * 角色信息管理
 * @author PC
 *
 */
public interface TESysRoleRepository extends JpaRepository<SysRole, Integer> {
	public SysRole findByName(String name);
}
