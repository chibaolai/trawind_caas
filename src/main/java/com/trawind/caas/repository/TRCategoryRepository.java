package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TRCategory;

public interface TRCategoryRepository extends JpaRepository<TRCategory, Integer> {

}
