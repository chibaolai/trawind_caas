package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEFormHome;

public interface TEFormHomeRepository extends JpaRepository<TEFormHome, String> {
	public TEFormHome findById(String id);

	public List<TEFormHome> findByUserIdOrderByUTime(String userId);

	public List<TEFormHome> findByCorpidAndUserIdOrderByUTime(Long corpid, String userId);
}
