package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEImgCategory;

public interface TEImgCategoryRepository extends JpaRepository<TEImgCategory, Long>{

	public TEImgCategory findByType(String type);
	
	public TEImgCategory findById(String id);
}
