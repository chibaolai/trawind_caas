package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TELayout;

public interface TELayoutRepository extends JpaRepository<TELayout, Integer> {
	public List<TELayout> findByCorpid(Long corpId);
}
