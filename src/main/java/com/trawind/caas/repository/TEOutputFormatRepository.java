package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEOutputFormat;

public interface TEOutputFormatRepository extends JpaRepository<TEOutputFormat, Long>{
	public TEOutputFormat findByCorpId(Long corpId);
}
