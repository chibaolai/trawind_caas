package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TETempXmlElement;

public interface TETempXmlElementRepository extends JpaRepository<TETempXmlElement, Long> {
	public List<TETempXmlElement> findById(String id);
}
