package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TIssuedRelations;

public interface TIssuedRelationsRepository extends JpaRepository<TIssuedRelations, Integer> {
	List<TIssuedRelations> findByProductIdAndManufacturerId(String productId, Long manufacturerId);

	void deleteByProductIdAndManufacturerId(String productId, Long manufacturerId);
}
