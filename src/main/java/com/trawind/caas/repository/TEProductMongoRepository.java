package com.trawind.caas.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.trawind.caas.entity.MProductData;

public interface TEProductMongoRepository extends MongoRepository<MProductData, String>{

	public MProductData findByProductIdAndCorpId(String productId,Long corpId);
}
