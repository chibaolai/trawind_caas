package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEService;
/**
 * 订单管理
 * @author PC
 *
 */
public interface TEServiceRepository extends JpaRepository<TEService, Integer> {

	public List<TEService> findAll();
}
