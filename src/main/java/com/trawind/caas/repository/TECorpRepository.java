package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TECorp;

/**
 * 公司信息
 * 
 * @author PC
 *
 */
public interface TECorpRepository extends JpaRepository<TECorp, Integer> {

	TECorp findByName(String companyname);

	List<TECorp> findByIdNotAndHasDistributorRoleTrue(Long id);
	
	List<TECorp> findByHasDistributorRoleTrue();
}
