package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TETrade;
/**
 * 订单管理
 * @author PC
 *
 */
public interface TETradeRepository extends JpaRepository<TETrade, Integer> {

	public TETrade findByOutTradeNo(String tradeNo);
}
