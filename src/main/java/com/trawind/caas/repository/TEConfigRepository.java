package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEConfig;

public interface TEConfigRepository extends JpaRepository<TEConfig, String> {
	public List<TEConfig> findByKeyAndCorpId(String key, Long corpid);

	public List<TEConfig> findByKeyAndUserIdAndCorpId(String key, String userId, Long corpid);
}
