package com.trawind.caas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TEAttr;

public interface TEAttrsRepository extends JpaRepository<TEAttr, Integer> {
	public List<TEAttr> findByCorpCode(String corpCode);
	public TEAttr findById(String id);
	public List<TEAttr> findByBaseFlag(int baseFlag);
}
