package com.trawind.caas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trawind.caas.entity.TContact;

public interface TContactRepository extends JpaRepository<TContact, Integer> {

}
