package com.trawind.caas.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 配置跳转 
 * 不需要controller用这个跳转
 * @author PC
 *
 */

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/welcome").setViewName("welcome");
		registry.addRedirectViewController("", "/welcome");
		registry.addStatusController("/403", HttpStatus.FORBIDDEN);
		registry.addViewController("/add").setViewName("addAttr");
	}

//	@Bean
//	public CommonsMultipartResolver commonsMultipartResolver() {
//		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
//		multipartResolver.setMaxUploadSize(1024 * 1024 * 1024);
//		return multipartResolver;
//	}

}
