package com.trawind.caas.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "upload",locations = "classpath:config.properties")
public class UploadSettings {
	private String uploadUrl;
	private String xmlConfigUrl;
	private String xmlTempUrl;
	private String xmlUrl;
	public String getUploadUrl() {
		return uploadUrl;
	}
	public void setUploadUrl(String uploadUrl) {
		this.uploadUrl = uploadUrl;
	}
	public String getXmlConfigUrl() {
		return xmlConfigUrl;
	}
	public void setXmlConfigUrl(String xmlConfigUrl) {
		this.xmlConfigUrl = xmlConfigUrl;
	}
	public String getXmlTempUrl() {
		return xmlTempUrl;
	}
	public void setXmlTempUrl(String xmlTempUrl) {
		this.xmlTempUrl = xmlTempUrl;
	}
	public String getXmlUrl() {
		return xmlUrl;
	}
	public void setXmlUrl(String xmlUrl) {
		this.xmlUrl = xmlUrl;
	}
}
