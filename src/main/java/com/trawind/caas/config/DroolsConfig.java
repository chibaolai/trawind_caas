package com.trawind.caas.config;

import org.apache.commons.io.IOUtils;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.runtime.KieContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import java.io.IOException;

/**
 * 校验规则drools配置
 */
@Configuration
public class DroolsConfig {

    private Resource[] listRules() throws IOException {
        PathMatchingResourcePatternResolver pmrs = new PathMatchingResourcePatternResolver();
        Resource[] resources = pmrs.getResources("classpath*:com/trawind/drools/**/*.drl");
        return resources;
    }

    @Bean
    public KieContainer kieContainer() throws IOException {
        KieServices ks = KieServices.Factory.get();
        final KieRepository kr = ks.getRepository();
        kr.addKieModule(new KieModule() {
            @Override
            public ReleaseId getReleaseId() {
                return kr.getDefaultReleaseId();
            }
        });
        KieFileSystem kfs = ks.newKieFileSystem();
        Resource[] files = listRules();

        for(Resource file : files) {
            String myString = IOUtils.toString(file.getInputStream(), "UTF-8");
            kfs.write("src/main/resources/"+ file.getFilename(), myString);
        }

        KieBuilder kb = ks.newKieBuilder(kfs);
        kb.buildAll(); // kieModule is automatically deployed to KieRepository if successfully built.
        KieContainer kContainer = ks.newKieContainer(kr.getDefaultReleaseId());
        return kContainer;
    }

    @Bean
    public KieBase kieBase() throws IOException {
        KieBase kieBase = kieContainer().getKieBase();
        return kieBase;
    }
}