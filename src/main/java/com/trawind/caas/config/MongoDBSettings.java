package com.trawind.caas.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * mongoDB配置信息
 * @author PC
 *
 */

@ConfigurationProperties(prefix = "mongo",locations = "classpath:config.properties")
public class MongoDBSettings {
	private String dbName;
	private String fsNameImg;
	private String fsNameTemp;
	private String fsNameMapping;
	private String fsNameOutput;
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	public String getFsNameImg() {
		return fsNameImg;
	}
	public void setFsNameImg(String fsNameImg) {
		this.fsNameImg = fsNameImg;
	}
	public String getFsNameTemp() {
		return fsNameTemp;
	}
	public void setFsNameTemp(String fsNameTemp) {
		this.fsNameTemp = fsNameTemp;
	}
	public String getFsNameMapping() {
		return fsNameMapping;
	}
	public void setFsNameMapping(String fsNameMapping) {
		this.fsNameMapping = fsNameMapping;
	}
	public String getFsNameOutput() {
		return fsNameOutput;
	}
	public void setFsNameOutput(String fsNameOutput) {
		this.fsNameOutput = fsNameOutput;
	}
}
