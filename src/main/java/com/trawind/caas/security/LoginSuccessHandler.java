package com.trawind.caas.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.trawind.caas.common.WebContext;
import com.trawind.caas.web.LoginUser;

public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	
	@Autowired
	private SysUserRepository userRepository;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		// 获得授权后可得到用户信息 可使用SUserService进行数据库操作
		SysUser userDetails = (SysUser) authentication.getPrincipal();
//		System.out.println("login user："+userDetails.getUsername()+"login pass："+userDetails.getPassword()+"IP :"+getIpAddress(request));
		LoginUser loginUser = new LoginUser();
		loginUser.setUserId(String.valueOf(userDetails.getId()));
		loginUser.setUsername(userDetails.getUsername());
		loginUser.setCorpId(userDetails.getCorpid());
		loginUser.setRealname(userDetails.getRealname());
		loginUser.setLanCode(request.getParameter("lanCode").toString());
		loginUser.setRoleList(getNewsItemInfo(userDetails.getUsername())); 
		WebContext.setLoginUserToHttpSession(request.getSession(), loginUser);
//		response.sendRedirect("/index");
		request.getRequestDispatcher("/index").forward(request, response);
//		super.onAuthenticationSuccess(request, response, authentication);
	}

	public String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
	/**
	 * 获取当前登录用户角色
	 * @param httpSession
	 * @return
	 */
	public List<SysRole> getNewsItemInfo(String username) {
		SysUser user = (SysUser) userRepository.findByUsername(username);
		return user.getRoles();
	}
}
