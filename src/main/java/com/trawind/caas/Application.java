package com.trawind.caas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.trawind.caas.config.MongoDBSettings;
import com.trawind.caas.config.UploadSettings;


@ComponentScan("com.trawind.caas")
@SpringBootApplication
@EnableConfigurationProperties({UploadSettings.class,MongoDBSettings.class})
public class Application implements CommandLineRunner{

	@Autowired
	MongoTemplate template;
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	public void run(String... args) throws Exception {
		System.err.println(template);
	}
}
