package com.trawind.caas.res;

public class RegistResWeb {

	private String registResult;// 0:失败 1：成功 2：公司已经存在
	private String sHtmlText;

	public String getRegistResult() {
		return registResult;
	}

	public void setRegistResult(String registResult) {
		this.registResult = registResult;
	}
	public String getsHtmlText() {
		return sHtmlText;
	}

	public void setsHtmlText(String sHtmlText) {
		this.sHtmlText = sHtmlText;
	}

}
