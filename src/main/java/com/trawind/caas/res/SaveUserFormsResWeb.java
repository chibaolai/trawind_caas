package com.trawind.caas.res;

public class SaveUserFormsResWeb {
private String formId;
private String formName;
public String getFormId() {
	return formId;
}
public void setFormId(String formId) {
	this.formId = formId;
}
public String getFormName() {
	return formName;
}
public void setFormName(String formName) {
	this.formName = formName;
}
}
