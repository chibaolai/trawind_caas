package com.trawind.caas.res;

import java.util.List;

import com.trawind.caas.base.ResponseBase;
import com.trawind.caas.entity.TECorp;

public class DataManagementResWeb extends ResponseBase{

	// schemaXML解析之后的json
	private String schemaList;

//	private String resuleStr;// 返回结果 0 失败 1 成功

	private String fileName;

	private String baseFileFormatStr;// 源文件结构
	
	private List<TECorp> corpList = null; 

	public String getSchemaList() {
		return schemaList;
	}

	public void setSchemaList(String schemaList) {
		this.schemaList = schemaList;
	}

//	public String getResuleStr() {
//		return resuleStr;
//	}
//
//	public void setResuleStr(String resuleStr) {
//		this.resuleStr = resuleStr;
//	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getBaseFileFormatStr() {
		return baseFileFormatStr;
	}

	public void setBaseFileFormatStr(String baseFileFormatStr) {
		this.baseFileFormatStr = baseFileFormatStr;
	}

	public List<TECorp> getCorpList() {
		return corpList;
	}

	public void setCorpList(List<TECorp> corpList) {
		this.corpList = corpList;
	}

}
