package com.trawind.caas.res;

import java.util.List;

import com.trawind.caas.entity.MProductData;
import com.trawind.caas.entity.MWidget;
import com.trawind.caas.entity.TEForm;
import com.trawind.caas.entity.TEProduct;
import com.trawind.caas.entity.TETemp;

import net.sf.json.JSONObject;

public class ProductInputResWeb {
	//表单
	private TEForm form;
	//输入的数据(json格式)
	private List<MWidget> productDataJson;
	//更新时间戳
	private String timeStamp;
	// 当form为null，temp有值
	private TETemp temp;
	//form表单下拉列表
	private List<InformationWeb> formList = null;
	//产品
	private TEProduct product;
	
	public TEForm getForm() {
		return form;
	}
	public void setForm(TEForm form) {
		this.form = form;
	}
	public List<MWidget> getProductDataJson() {
		return productDataJson;
	}
	public void setProductDataJson(List<MWidget> productDataJson) {
		this.productDataJson = productDataJson;
	}
	public TETemp getTemp() {
		return temp;
	}
	public void setTemp(TETemp temp) {
		this.temp = temp;
	}
	public List<InformationWeb> getFormList() {
		return formList;
	}
	public void setFormList(List<InformationWeb> formList) {
		this.formList = formList;
	}
	public TEProduct getProduct() {
		return product;
	}
	public void setProduct(TEProduct product) {
		this.product = product;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
}
