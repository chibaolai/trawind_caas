package com.trawind.caas.res;

import com.trawind.caas.base.ResponseBase;

public class Convert2XmlMappingRes extends ResponseBase{

	public String resultValue;
	public String getResultValue() {
		return resultValue;
	}
	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}
}
