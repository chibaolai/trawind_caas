package com.trawind.caas.res;

import com.trawind.caas.entity.TEForm;
import com.trawind.caas.entity.TETemp;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class FormResWeb {
	//表单
	private TEForm form;
	// 当form为null，temp有值
	private TETemp temp;
	//基本属性
	private JSONArray baseAttrList;
	public TEForm getForm() {
		return form;
	}
	public void setForm(TEForm form) {
		this.form = form;
	}
	public TETemp getTemp() {
		return temp;
	}
	public void setTemp(TETemp temp) {
		this.temp = temp;
	}
	public JSONArray getBaseAttrList() {
		return baseAttrList;
	}
	public void setBaseAttrList(JSONArray baseAttrList) {
		this.baseAttrList = baseAttrList;
	}
	
}
