package com.trawind.caas.utils;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

import com.trawind.caas.base.ResponseCode;
import com.trawind.caas.base.TradeCode;
import com.trawind.caas.common.Common;

public class RespMsgUtils {
	/* first_code */
	private static final String SYSTEM_LEVEL = "01";
	private static final String LOGIC_LEVEL = "02";
	private static Map<String, ResourceBundle> rbMap = null;
	/* second_code */
	/* private static final String COMM_DOMAIN ="01";
	 private static final String USER_DOMAIN ="02";
	 private static final String SHOP_DOMAIN ="03";
	 private static final String TRADE_DOMAIN ="04";
	 private static final String BUSINSS_DOMAIN ="05";
	 private static final String DISH_DOMAIN ="06";*/

	private static ResourceBundle getMessageResourceBundle(String langCode) {

		ResourceBundle rb = null;
		Locale locale = null;

		if (rbMap == null) {
			rbMap = new HashMap<>();
		}

		switch (langCode) {
		case "zh":
			locale = Locale.SIMPLIFIED_CHINESE;
			break;
		case "en":
			locale = Locale.US;
			break;
		default:
			locale = Locale.US;
		}

		if (!rbMap.containsKey(langCode)) {
			// properties文件的名字是有规范的：一般的命名规范是： 自定义名_语言代码_国别代码.properties
			rb = ResourceBundle.getBundle("properties.message.message", locale);
			if (rb != null) {
				rbMap.put(langCode, rb);
			}
		}

		if (rbMap.containsKey(langCode)) {
			rb = rbMap.get(langCode);
		} else if (rbMap.containsKey(Locale.US.getLanguage())) {
			rb = rbMap.get(Locale.US.getLanguage());
		}

		return rb;
	}

	private static String getMessageString(String langCode, String key, Object[] args) {
		ResourceBundle rb = getMessageResourceBundle(langCode);
		if (rb != null && rb.containsKey(key)) {
			String rtnStr = rb.getString(key);
			if (Common.isNullOrEmpty(rtnStr)) {
				rtnStr = StringUtils.EMPTY;
			}

			if (args != null && args.length > 0) {
				// 动态参数是使用数组传递多个参数  ex. test={0}hello{1}
				rtnStr = MessageFormat.format(rtnStr, args);
			}

			return rtnStr;
		}

		return StringUtils.EMPTY;
	}

	/**
	 * 01 系统级别 02业务级别<br>
	 * 01 COMM 02 USER 03 SHOP 04 TRADE 05 BUSINSS 06 DISH<br>
	 * 001 002 003 ： XXX业务处理失败<br>
	 * 0201001
	 */
	public static String respCodeForMsg(String tradeCode, String langCode) {
		// TODO test
		System.out.println(getMessageString(langCode, "test", new Object[] { "A", "B" }));

		StringBuffer respMsg = new StringBuffer();
		if (tradeCode == ResponseCode.SUCCESS) {
			respMsg.append(ResponseCode.SUCCESS_MSG);
			return respMsg.toString();
		}
		if (tradeCode == ResponseCode.ERROR) {
			respMsg.append(ResponseCode.ERROR_MSG);
			return respMsg.toString();
		}
		if (null == tradeCode || tradeCode.length() < 7) {
			return "";
		}
		String first_code = tradeCode.substring(0, 2);
		if (SYSTEM_LEVEL.equals(first_code)) {
			switch (tradeCode) {
			case TradeCode.DATA_EXIT:
				respMsg.append("");
				break;
			}
		} else if (LOGIC_LEVEL.equals(first_code)) {
			respMsg.append(getMessageString(langCode, tradeCode, null));

			// switch (tradeCode) {
			// /* 02 COMM */
			// case TradeCode.file_content_changed:
			// respMsg.append("属性关联文件已存在，但文件内容不一致，请重新生成关联文件");
			// break;
			// case TradeCode.file_content_empty:
			// respMsg.append("上传文件内容为空");
			// break;
			// default:
			// break;
			// }
		}

		return respMsg.toString();
	}

	public static void main(String[] args) {
		System.out.println(RespMsgUtils.respCodeForMsg("0102001", "zh"));

	}
}
