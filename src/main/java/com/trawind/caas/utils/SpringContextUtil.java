package com.trawind.caas.utils;

import org.springframework.context.ApplicationContext;

/**
 * This utility is used to manage the spring framework context
 * 
 * @author chibaolai
 * 
 */
public class SpringContextUtil {

	private static ApplicationContext applicationContext;

	/**
	 * Manually set the spring application context. Used in non-web applications.
	 * 
	 * @param applicationContext
	 */
	public static void setAppCtx(ApplicationContext applicationContext) {
		SpringContextUtil.applicationContext = applicationContext;
	}

	/**
	 * Get spring framework application context
	 * 
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * Get spring framework managed bean by name
	 * 
	 * @param beanName
	 *            The bean name which is managed by spring framework
	 * @return
	 */
	public static Object getBean(String beanName) {
		return applicationContext.getBean(beanName);
	}

}
