package com.trawind.caas.utils;

import java.util.List;

public abstract interface ConvertFactory
{
  public abstract <T> T convert(Object paramObject, Class<T> paramClass);

  public abstract <T> List<T> convertList(Object paramObject, Class<T> paramClass);
}
