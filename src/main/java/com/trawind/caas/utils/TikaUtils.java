package com.trawind.caas.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;

import org.apache.commons.lang.StringUtils;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.txt.TXTParser;
import org.apache.tika.sax.ContentHandlerDecorator;

import com.mongodb.gridfs.GridFSDBFile;
import com.trawind.caas.common.Common;
import com.trawind.caas.pay.alipay.sign.MD5;
import com.trawind.caas.xml.handler.BodyExtendContentHandler;
import com.trawind.caas.xml.parser.XmlPageParser;

public class TikaUtils {

	private Metadata mMetadata = null;
	private String mContents = null;
	private String mFilePath = null;
	private String mFileType = null;
	private GridFSDBFile mGfsFile = null;
	private File mFile = null;
	private String mFileId = null;
	private int mCurrentPageIndex = 0;
	private int mPageSize = 0;

	public static final String APPLICATION_XML = "application/xml";
	public static final String PREFIX_XML = "xml";
	public static final String PREFIX_XSD = "xsd";
	public static final String PREFIX_IMAGE = "svg+xml";
	public static final String CODE_SPOT = ".";

	public Metadata getMetadata() {
		return mMetadata;
	}

	public String getContents() {
		return mContents;
	}

	public String getFileType() {
		return mFileType;
	}

	public String getFileId() {
		return mFileId;
	}

	public TikaUtils(String fileId, int currentPageIndex, int pageSize) {
		mFile = null;
		mFilePath = null;
		mGfsFile = null;

		mFileId = fileId;
		mCurrentPageIndex = currentPageIndex;
		mPageSize = pageSize;
	}

	public TikaUtils(String filePath) {
		mFilePath = filePath;
		mGfsFile = null;
		mFile = null;
	}

	public TikaUtils(GridFSDBFile gfsFile) {
		mGfsFile = gfsFile;
		mFilePath = null;
		mFile = null;
	}

	public TikaUtils(File file) {
		mFile = file;
		mGfsFile = null;
		mFilePath = null;
	}

	public void doTikaParse() {
		File file = null;
		if (mGfsFile != null) {
		} else if (mFilePath != null) {
			file = new File(mFilePath);

			if (!file.exists() || !file.isFile()) {
				return;
			}
		} else if (mFile != null) {
			file = mFile;

			if (!file.exists() || !file.isFile()) {
				return;
			}
		} else {
			return;
		}

		Tika tika = null;

		try {
			if (mGfsFile != null) {
				mFileType = mGfsFile.getContentType();
			} else {
				tika = new Tika(TikaConfig.getDefaultConfig());
				mFileType = tika.detect(file.getPath());
			}

			if (file != null && file.length() > Integer.MAX_VALUE) {
				readerBigFile(file, tika);
			} else {
				readerSmallFile(file);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

			mMetadata = new Metadata();
			mContents = StringUtils.EMPTY;
			mFileType = StringUtils.EMPTY;
			mFilePath = null;
			mGfsFile = null;
			mFile = null;
		}
	}

	public String nextXmlPage(String fileId, int currentPageIndex, int pageSize) {
		mFileId = fileId;
		mCurrentPageIndex = currentPageIndex;
		mPageSize = pageSize;

		return nextXmlPage();
	}

	public String nextXmlPage() {
		if (Common.isNullOrEmpty(mFileId)) {
			return StringUtils.EMPTY;
		}

		BodyExtendContentHandler handler = new BodyExtendContentHandler(-1);
		return handler.nextXml(mFileId, mCurrentPageIndex, mPageSize);
	}

	public String previousXmlPage() {
		mCurrentPageIndex = mCurrentPageIndex - 2;
		if (mCurrentPageIndex < 0) {
			return StringUtils.EMPTY;
		}

		return nextXmlPage();
	}

	public String previousXmlPage(String fileId, int currentPageIndex, int pageSize) {
		mFileId = fileId;
		mCurrentPageIndex = currentPageIndex;
		mPageSize = pageSize;

		return previousXmlPage();
	}

	private void readerBigFile(File file, Tika tika) throws Exception {
		Reader reader = null;
		mMetadata = new Metadata();

		try {
			reader = tika.parse(file, mMetadata);

			int readCount = -1;
			char[] cbuf = null;
			StringBuffer sb = new StringBuffer(0);
			do {
				cbuf = new char[1024 * 1024];
				readCount = reader.read(cbuf, 0, cbuf.length);

				if (sb.length() + readCount < Integer.MAX_VALUE && readCount > -1) {
					sb.append(new String(cbuf, 0, readCount));
				}

			} while (readCount > -1);
			mContents = sb.toString();
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
	}

	private void readerSmallFile(File file) throws Exception {
		ContentHandlerDecorator handler = null;
		InputStream inputstream = null;
		ParseContext pcontext = null;
		Parser parser = null;
		String prefix = StringUtils.EMPTY;

		try {
			if (mGfsFile != null) {
				handler = new BodyExtendContentHandler(-1);
				inputstream = mGfsFile.getInputStream();
				if (file.getName().contains(CODE_SPOT)) {
					prefix = mGfsFile.getFilename().substring(mGfsFile.getFilename().lastIndexOf(CODE_SPOT) + 1)
							.toLowerCase();
				}
			} else {
				if (file.length() < 100 * 1000) {
					handler = new BodyExtendContentHandler();
				} else {
					handler = new BodyExtendContentHandler(-1);
				}
				inputstream = new FileInputStream(file);
				if (file.getName().contains(CODE_SPOT)) {
					prefix = file.getName().substring(file.getName().lastIndexOf(CODE_SPOT) + 1).toLowerCase();
				}
			}

			mMetadata = new Metadata();
			pcontext = new ParseContext();

			((BodyExtendContentHandler) handler).setFileId(MD5.getFileMD5String(file));

			if (APPLICATION_XML.equals(mFileType) && PREFIX_XSD.equals(prefix)) {
				parser = new TXTParser();
			} else if (APPLICATION_XML.equals(mFileType) && PREFIX_XML.equals(prefix)) {
				parser = new XmlPageParser();
			} else {
				parser = new AutoDetectParser();
			}

			parser.parse(inputstream, handler, mMetadata, pcontext);

			mFileId = ((BodyExtendContentHandler) handler).getFileId();
			mContents = ((BodyExtendContentHandler) handler).toString();
		} finally {
			if (inputstream != null) {
				inputstream.close();
			}
		}
	}
}