package com.trawind.caas.utils;

import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapper;

public class DozerConvertFactory
  implements ConvertFactory
{
  public <T> T convert(Object source, Class<T> destinationClasss)
  {
    ConvertValidator.validateConvertRequest(source, destinationClasss);
    DozerBeanMapper mapper = new DozerBeanMapper();
    return mapper.map(source, destinationClasss);
  }

  public <T> List<T> convertList(Object source, Class<T> destinationClasss)
  {
    List destList = new ArrayList();
    if (isList(source.getClass())) {
      List list = (List)source;
      for (Object obj : list) {
        Object temp = convert(obj, destinationClasss);
        destList.add(temp);
      }
    }
    return destList;
  }

  private boolean isList(Class<?> aClass) {
    return List.class.isAssignableFrom(aClass);
  }
}
