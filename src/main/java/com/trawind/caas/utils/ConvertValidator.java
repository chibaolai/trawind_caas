package com.trawind.caas.utils;

import net.minidev.asm.ex.ConvertException;

public class ConvertValidator
{
  public static void validateConvertRequest(Object srcObj)
  {
    if (srcObj == null)
      throw new ConvertException("srcObjNULL");
  }

  public static void validateConvertRequest(Object srcObj, Object destObj)
  {
    validateConvertRequest(srcObj);
    if (destObj == null)
      throw new ConvertException("destObjNULL");
  }

  public static void validateConvertRequest(Object srcObj, Class<?> destClass)
  {
    validateConvertRequest(srcObj);
    if (destClass == null)
      throw new ConvertException("destClassNULL");
  }
}
