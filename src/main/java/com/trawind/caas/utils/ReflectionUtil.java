package com.trawind.caas.utils;

import java.lang.reflect.Method;

public class ReflectionUtil {

	public static Object getInstance(String className) throws Exception {
		Object obj = null;

		Class<?> clazz = Class.forName(className);
		obj = clazz.newInstance();

		return obj;
	}

	public static Method getMethod(Object obj, String methodName) {
		Method method = null;

		Method[] methods = obj.getClass().getMethods();
		for (Method m : methods) {
			if (methodName.equals(m.getName())) {
				method = m;
				break;
			}
		}

		return method;
	}

}
