package com.trawind.caas.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;

import javax.xml.transform.TransformerException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import com.trawind.caas.xml.util.DomUtil;

/**
 * 文件处理工具类
 * @author PC
 *
 */

public class FileUtil {

	private static Logger logger = Logger.getLogger(FileUtil.class);

	public static void deleteLocalFile(String filePath) throws Exception {
		File file = new File(filePath);
		try {
			if (file.delete()) {
				logger.info("File : " + filePath + " has been deleted.");
			} else {
				logger.error("Delete operation failed. Path: " + filePath);
			}

		} catch (Exception e) {
			logger.error("Error on deleting XML. Path: " + filePath, e);
		}
	}

	public static void writeXMLInFile(String filePath, String content) throws Exception {
		File file = new File(filePath);
		Writer fileWriter = null;
		try {
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			fileWriter = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
			fileWriter.write(content);
			fileWriter.flush();
		} catch (Exception e) {
			logger.error("Error on writing XML to file. Path: " + filePath, e);
			throw e;
		} finally {
			if (null != fileWriter) {
				try {
					fileWriter.close();
				} catch (Exception e) {
					logger.error("Error on closing file writer.", e);
				}
			}
		}
		logger.info("Write file to: " + filePath);
	}

	public static File writeXMLInFile(String filePath, Document document) throws Exception {
		File file = new File(filePath);
		Writer writer = null;
		try {
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");

			DomUtil.writeXMLInPretty(document, writer);
		} catch (FileNotFoundException e) {
			logger.error("File or path not found! Path: " + filePath, e);
			throw e;
		} catch (IOException e) {
			logger.error("Error on writing XML to file. Path: " + filePath, e);
			throw e;
		} finally {
			if (null != writer) {
				try {
					writer.close();
				} catch (Exception e) {
					logger.error("Error on closing file writer.", e);
				}
			}
		}
		logger.info("Write file to: " + filePath);
		return file;
	}
	
	public static void writeXMLInFile(File file, Document document) throws Exception {
		Writer writer = null;
		try {
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");

			DomUtil.writeXMLInPretty(document, writer);
		} catch (IOException e) {
			throw e;
		} finally {
			if (null != writer) {
				try {
					writer.close();
				} catch (Exception e) {
					logger.error("Error on closing file writer.", e);
				}
			}
		}
	}

	public static InputStream readFileOnFtpAsInputStream(String ip, String port, String username, String password, String path, String fileName)
			throws Exception {
		InputStream inputStream = null;

		FTPClient ftpClient = new FTPClient();
		try {
			ftpClient.connect(ip, Integer.valueOf(port));
			ftpClient.login(username, password);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			String remoteFile = path + fileName;
			inputStream = ftpClient.retrieveFileStream(remoteFile);
		} catch (NumberFormatException | IOException e) {
			logger.error("Error on reading file from FTP: " + path + fileName);
			throw e;
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return inputStream;
	}

	public static void writeXMLOnFTP(String ip, String port, String username, String password, String path, String fileName, Document document)
			throws Exception {
		if (!path.endsWith("/")) {
			path += "/";
		}

		StringBuilder pathUrl = new StringBuilder();
		pathUrl.append("ftp://").append(username).append(":").append(password).append("@").append(ip).append(":").append(port).append(path)
				.append(fileName);

		Writer writer = null;
		URL url;

		try {
			url = new URL(pathUrl.toString());
			writer = new OutputStreamWriter(url.openConnection().getOutputStream(), "UTF-8");

			DomUtil.writeXMLInPretty(document, writer);
		} catch (IOException | TransformerException e) {
			logger.error("Error on writing file to FTP. Path: " + pathUrl, e);
			throw e;
		} finally {
			if (null != writer) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.error("Error on closing writer", e);
				}
			}
		}
		logger.info("Write file to: " + path + fileName);
	}

	public static void writeXMLOnFTP(String ip, String port, String username, String password, String path, String fileName, String content)
			throws Exception {
		if (!path.endsWith("/")) {
			path += "/";
		}

		StringBuilder pathUrl = new StringBuilder();
		pathUrl.append("ftp://").append(username).append(":").append(password).append("@").append(ip).append(":").append(port).append(path)
				.append(fileName);

		Writer writer = null;
		URL url;

		try {
			url = new URL(pathUrl.toString());
			writer = new OutputStreamWriter(url.openConnection().getOutputStream(), "UTF-8");
			writer.write(content);
			writer.flush();
		} catch (IOException e) {
			logger.error("Error on writing file to FTP. Path: " + pathUrl, e);
			throw e;
		} finally {
			if (null != writer) {
				try {
					writer.close();
				} catch (IOException e) {
					logger.error("Error on closing writer", e);
				}
			}
		}
		logger.info("Write file to: " + path + fileName);
	}

	public static void mkdirOnFTP(String ip, String port, String username, String password, String path) throws Exception {
		if (!path.endsWith("/")) {
			path += "/";
		}

		FTPClient ftpClient = null;
		try {
			ftpClient = new FTPClient();
			ftpClient.connect(ip, Integer.valueOf(port));
			ftpClient.login(username, password);

			ftpClient.changeWorkingDirectory(path);
			int returnCode = ftpClient.getReplyCode();
			if (returnCode == 550) {
				ftpClient.makeDirectory(path);
			}
		} catch (NumberFormatException | IOException e) {
			StringBuilder toPathUrl = new StringBuilder();
			toPathUrl.append("ftp://").append(username).append(":").append(password).append("@").append(ip).append(":").append(port).append(path);
			logger.error("Error on mkdir. Path: " + toPathUrl, e);
			throw e;
		} finally {
			if (null != ftpClient && ftpClient.isConnected()) {
				try {
					ftpClient.logout();
					ftpClient.disconnect();
				} catch (IOException e) {
					logger.error("Error on disconnecting FTP.", e);
				}
			}
		}
	}

	public static void uploadFileOnFTP(String ip, String port, String username, String password, String localPath, String remotePath, String fileName)
			throws Exception {
		if (!localPath.endsWith("/")) {
			localPath += "/";
		}
		if (!remotePath.endsWith("/")) {
			remotePath += "/";
		}

		FTPClient ftpClient = null;
		File localFile = null;
		InputStream localFileIs = null;
		try {
			ftpClient = new FTPClient();
			ftpClient.connect(ip, Integer.valueOf(port));
			ftpClient.login(username, password);
			ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);

			localFile = new File(localPath + fileName);
			localFileIs = new FileInputStream(localFile);
			ftpClient.storeFile(remotePath + fileName, localFileIs);
		} catch (NumberFormatException | IOException e) {
			StringBuilder toPathUrl = new StringBuilder();
			toPathUrl.append("ftp://").append(username).append(":").append(password).append("@").append(ip).append(":").append(port)
					.append(localPath).append(fileName);
			logger.error("Error on uploading file. Path: " + toPathUrl, e);
			throw e;
		} finally {
			if (null != localFileIs) {
				try {
					localFileIs.close();
				} catch (IOException e) {
				}
			}
			if (null != ftpClient && ftpClient.isConnected()) {
				try {
					ftpClient.logout();
					ftpClient.disconnect();
				} catch (IOException e) {
					logger.error("Error on disconnecting FTP.", e);
				}
			}
		}
		logger.info("Upload file from " + localPath + fileName + " to " + remotePath + fileName);
	}

	public static void uploadFileOnFTP(String ip, String port, String username, String password, String localPath, String remotePath,
			String fileName, int ftpTransferMode) throws Exception {
		if (!localPath.endsWith("/")) {
			localPath += "/";
		}
		if (!remotePath.endsWith("/")) {
			remotePath += "/";
		}

		FTPClient ftpClient = null;
		File localFile = null;
		InputStream localFileIs = null;
		try {
			ftpClient = new FTPClient();
			ftpClient.connect(ip, Integer.valueOf(port));
			ftpClient.login(username, password);
			ftpClient.setFileType(ftpTransferMode);
			ftpClient.setFileTransferMode(ftpTransferMode);
			ftpClient.enterLocalPassiveMode();

			localFile = new File(localPath + fileName);
			localFileIs = new FileInputStream(localFile);
			ftpClient.storeFile(remotePath + fileName, localFileIs);
		} catch (NumberFormatException | IOException e) {
			StringBuilder toPathUrl = new StringBuilder();
			toPathUrl.append("ftp://").append(username).append(":").append(password).append("@").append(ip).append(":").append(port)
					.append(localPath).append(fileName);
			logger.error("Error on uploading file. Path: " + toPathUrl, e);
			throw e;
		} finally {
			if (null != localFileIs) {
				try {
					localFileIs.close();
				} catch (IOException e) {
				}
			}
			if (null != ftpClient && ftpClient.isConnected()) {
				try {
					ftpClient.logout();
					ftpClient.disconnect();
				} catch (IOException e) {
					logger.error("Error on disconnecting FTP.", e);
				}
			}
		}
		logger.info("Upload file from " + localPath + fileName + " to " + remotePath + fileName);
	}

	public static void moveFileOnFTP(String ip, String port, String username, String password, String fromPath, String toPath, String fileName)
			throws Exception {
		if (!fromPath.endsWith("/")) {
			fromPath += "/";
		}
		if (!toPath.endsWith("/")) {
			toPath += "/";
		}

		FTPClient ftpClient = null;
		try {
			ftpClient = new FTPClient();
			ftpClient.connect(ip, Integer.valueOf(port));
			ftpClient.login(username, password);
			ftpClient.rename(fromPath + fileName, toPath + fileName);
		} catch (NumberFormatException | IOException e) {
			StringBuilder toPathUrl = new StringBuilder();
			toPathUrl.append("ftp://").append(username).append(":").append(password).append("@").append(ip).append(":").append(port).append(toPath)
					.append(fileName);
			logger.error("Error on moving file. Path: " + toPathUrl, e);
			throw e;
		} finally {
			if (null != ftpClient && ftpClient.isConnected()) {
				try {
					ftpClient.logout();
					ftpClient.disconnect();
				} catch (IOException e) {
					logger.error("Error on disconnecting FTP.", e);
				}
			}
		}
		logger.info("Move file from " + fromPath + fileName + " to " + toPath + fileName);
	}
	
	public static File creatTempFile() throws IOException {
		return File.createTempFile(Constant.file_prefix_temp, Constant.file_suffix_xml);// 生成临时文件
	}
	
	public static File creatMappingFile() throws IOException {
		return File.createTempFile(Constant.file_prefix_mapping, Constant.file_suffix_xml);// 生成Mapping文件
	}
	
	public static File creatOutputFile() throws IOException {
		return File.createTempFile(Constant.file_prefix_output, Constant.file_suffix_xml);// 生成Output文件
	}

}
