package com.trawind.caas.common;

import javax.servlet.http.HttpSession;

import com.trawind.caas.web.LoginUser;
/**
 * 获取登录信息的上下文
 * 从session中取
 * @author PC
 */
public class WebContext {
	
	static String LOGIN_USER_ID_DEF = "-";
	static String LOGIN_USER_KEY = "loginUser";
	/**
	 * 从HttpSession中,取得LoginUserId
	 * 
	 * @param httpSession
	 * @return
	 */
	public static String getLoginUserIdFromHttpSession(HttpSession httpSession) {


		String loginUserId = LOGIN_USER_ID_DEF;
		if (httpSession != null && httpSession.getAttribute(LOGIN_USER_KEY) != null) {
			LoginUser loginUser = (LoginUser) httpSession.getAttribute(LOGIN_USER_KEY);
			loginUserId = loginUser.getUserId();
		}

		return loginUserId;
	}
	
	/**
	 * 从HttpSession中,取得LoginUser
	 * 
	 * @param httpSession
	 * @return
	 */
	public static LoginUser getLoginUserFromHttpSession(HttpSession httpSession) {

		LoginUser loginUser = null;
		if (httpSession != null && httpSession.getAttribute(LOGIN_USER_KEY) != null) {
			loginUser = (LoginUser) httpSession.getAttribute(LOGIN_USER_KEY);
		}

		return loginUser;
	}
	
	/**
	 * 向HttpSession中,设定LoginUser
	 * 
	 * @param httpSession
	 * @param loginUser
	 * @return
	 */
	public static LoginUser setLoginUserToHttpSession(HttpSession httpSession,LoginUser loginUser) {

		if (httpSession == null) {
			return null;
		}
		httpSession.setAttribute(LOGIN_USER_KEY, loginUser);

		return getLoginUserFromHttpSession(httpSession);
	}
}
