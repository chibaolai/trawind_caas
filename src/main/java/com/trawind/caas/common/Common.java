package com.trawind.caas.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.trawind.caas.utils.UUID;

public class Common {
	public final static String STRING_EMPTY = "";
	public final static String DATE_FORMAT_YYYYMMDDHHMMSS_SSS = "yyyy-MM-dd HH:mm:ss SSS";

	public enum LanguageType {
		English("en"), Chinese("cn");

		private String mDbValue = STRING_EMPTY;

		private LanguageType(String dbValue) {
			mDbValue = dbValue;
		}

		public String getDbValue() {
			return mDbValue;
		}
	}

	public enum VisitShortlistType {
		Form(1), Product(2);

		private int mDbCode = 0;

		private VisitShortlistType(int code) {
			mDbCode = code;
		}

		public int getDbCode() {
			return mDbCode;
		}
	}

	/**
	 * 从DB中取得的String数据进行Null和空判断,Null和空的场合,返回缺省值
	 * 
	 * @param input
	 *            从DB中取得的String数据
	 * @param defValue
	 *            缺省值
	 * @return
	 */
	public static String getStringValue(Object input, String defValue) {
		if (input == null) {
			return defValue;
		}
		if (input instanceof String) {
			if (StringUtils.EMPTY.equals(input.toString().trim())) {
				return defValue;
			}

			return input.toString().trim();
		} else {
			return String.valueOf(input);
		}
	}

	public static String getStringValue(Object input) {
		return getStringValue(input, StringUtils.EMPTY);
	}

	/**
	 * 从DB中取得的Integer数据进行Null判断,Null的场合,返回缺省值
	 * 
	 * @param input
	 *            从DB中取得的Integer数据
	 * @param defValue
	 *            缺省值
	 * @return
	 */
	public static Integer getIntegerValue(String input, Integer defValue) {
		String tmp = getStringValue(input, StringUtils.EMPTY);
		if (StringUtils.EMPTY.equals(tmp)) {
			return defValue;
		}

		return Integer.parseInt(input);
	}

	public static Long getLongValue(String input, Long defValue) {
		String tmp = getStringValue(input, StringUtils.EMPTY);
		if (StringUtils.EMPTY.equals(tmp)) {
			return defValue;
		}

		return Long.parseLong(tmp);
	}

	/**
	 * 去除String数组中的数据前后空格
	 * 
	 * @param arrayStr
	 *            String数组
	 * @return
	 */
	public static List<String> trimArrayStrToList(String[] arrayStr) {
		List<String> strList = new ArrayList<>();

		for (String str : arrayStr) {
			strList.add(str.trim());
		}

		return strList;
	}

	/**
	 * 随机生成自定义属性ID（格式：四位字母+四位数字）
	 * 
	 * @return
	 */
	public static String randomAttrId() {
		return (UUID.randomUpperUUID(4) + UUID.randomNumberUUID(4));
	}

	public static boolean isNullOrEmpty(String input) {
		return (input == null || input.trim().isEmpty());
	}
	/**
	 * 生成随机数
	 * @param numLength 随机数长度
	 */
	public static String generateNumber(int numLength) {
		String no = "";
		int num[] = new int[numLength];
		int c = 0;
		for (int i = 0; i < numLength; i++) {
			num[i] = new Random().nextInt(10);
			c = num[i];
			for (int j = 0; j < i; j++) {
				if (num[j] == c) {
					i--;
					break;
				}
			}
		}
		if (num.length > 0) {
			for (int i = 0; i < num.length; i++) {
				no += num[i];
			}
		}
		return no;
	}
	/**
	 * 密码加密
	 * @param password
	 */
	public static String encodePassword(String password){
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		return bcrypt.encode(password);
	}
	/**
	 * 产生随机字符串
	 * @param length
	 * */
	public static String randomString(int length) {
		Random randGen = null;
		char[] numbersAndLetters = null;
		Object initLock = new Object(); 
	         if (length < 1) {
	             return null;
	         }
	         if (randGen == null) {
	             synchronized (initLock) {
	                 if (randGen == null) {
	                     randGen = new Random();
	                     numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz" +
	                     "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
	                     //numbersAndLetters = ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
	                 }
	             }
	         }
	         char [] randBuffer = new char[length];
	         for (int i=0; i<randBuffer.length; i++) {
	             randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
	             //randBuffer[i] = numbersAndLetters[randGen.nextInt(35)];
	         }
	         return new String(randBuffer);
	}
	/**
	 * 获取客户端IP
	 * @param request
	 * @return
	 */
	public static String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}
}
