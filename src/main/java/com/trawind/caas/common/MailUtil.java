package com.trawind.caas.common;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.trawind.caas.security.SysUser;

/**
 * 基于javaMail邮件发送 
 * @author PC
 */
public class MailUtil {

    /**
     * 邮件发送 
     * @param sysUser
     * @return
     */
    public static boolean sendMail(SysUser sysUser){
    	
        Properties prop = new Properties();
        prop.setProperty("mail.host", Constant.MAIL_HOST);
        prop.setProperty("mail.transport.protocol", Constant.MAIL_TRANSPORT_PROTOCOL);
        prop.setProperty("mail.smtp.auth",Constant.MAIL_SMTP_AUTH);
        //使用JavaMail发送邮件的5个步骤
        //1、创建session
        Session session = Session.getInstance(prop);
        //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        session.setDebug(true);
        //2、通过session得到transport对象
        Transport ts;
		try {
			ts = session.getTransport();
	        //3、使用邮箱的用户名和密码连上邮件服务器，发送邮件时，发件人需要提交邮箱的用户名和密码给smtp服务器，用户名和密码都通过验证之后才能够正常发送邮件给收件人。
	        ts.connect(Constant.MAIL_HOST, Constant.MAIL_SEND_HOST_NAME, Constant.MAIL_SEND_PASSWORD);
	        //4、创建邮件
	        Message message = createSimpleMail(session,sysUser);
	        //5、发送邮件
	        ts.sendMessage(message, message.getAllRecipients());
	        //
	        ts.close();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    	return true;
    }
    
    /**
    * @Method: createSimpleMail
    * @Description: 创建一封只包含文本的邮件
    * @param session
    * @return
    * @throws Exception
    */ 
    public static MimeMessage createSimpleMail(Session session,SysUser sysUser)
            throws Exception {
        //创建邮件对象
        MimeMessage message = new MimeMessage(session);
        //指明邮件的发件人
        message.setFrom(new InternetAddress(Constant.MAIL_SEND_ACCOUND));
        //指明邮件的收件人，现在发件人和收件人是一样的，那就是自己给自己发
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(sysUser.getEmail()));
        //邮件的标题
        message.setSubject("CAAS系统通知");
        //邮件的文本内容
        StringBuffer sb = new StringBuffer("");
        sb.append("<p style='padding-left:1%;'>").append(sysUser.getRealname()).append("先生/小姐您好！您已经成功注册CAAS系统账号！</p>");
        sb.append("<p style='padding-left:1%;'>您的登陆账号为：").append(sysUser.getUsername()).append("</p>");
        sb.append("<p style='padding-left:1%;'>您的登陆密码为：").append(sysUser.getPassword()).append("</p>");
        sb.append("<p style='color:red;padding-left:1%;'>注：登录帐号和密码为随机生成，请即时登录系统进行修改！</p>");
        
        message.setContent(sb.toString(), "text/html;charset=UTF-8");
        //返回创建好的邮件对象
        return message;
    }
	

}
