package com.trawind.caas.common;

public class Constant {
	/**
	 * 产品mongo存储
	 */
	public static final String mongo_widget = "widget";
	public static final String mongo_key = "key";
	public static final String mongo_value = "value";

	/**
	 * 控件Type
	 */
	public static final String type_input = "input";
	public static final String type_select = "select";
	public static final String type_image = "image";
	public static final String type_grid = "grid";
	public static final String type_chk = "chk";
	public static final String type_textarea = "textarea";
	public static final String type_date = "date";

	/**
	 * 日期格式
	 */
	public static final String date_format = "yyyy-MM-dd HH:mm:ss";
	public static final String date_format_stamp = "yyyyMMddHHmmssSSS";

	/**
	 * 产品状态（创建,初步发布，修改，审批,发布给交易伙伴，退出市场）
	 */
	public static final String product_status_create = "create";
	public static final String product_status_release = "release";
	public static final String product_status_update = "update";
	public static final String product_status_approve = "approve";
	public static final String product_status_2partner = "2partner;";
	public static final String product_status_close = "close;";

	/**
	 * home页产品变更履历 格式
	 */
	public static final String FORMAT_CONTENT = "%1$s ：%2$s -> %3$s";

	/**
	 * 分隔符(逗号)
	 */
	public static final String symbol_comma = ",";
	public static final String symbol_slash = "/";
	public static final String symbol_at = "@";

	/**
	 * 产品数据变更 源头
	 */
	public static final int mode_selft = 1;
	public static final int mode_parnter = 2;
	/**
	 * 邮件发送配置
	 */
	public static final String MAIL_HOST = "smtp.163.com";
	public static final String MAIL_TRANSPORT_PROTOCOL = "smtp";
	public static final String MAIL_SMTP_AUTH = "false";
	public static final String MAIL_SEND_ACCOUND = "winxiaomaolv@163.com";
	public static final String MAIL_SEND_PASSWORD = "t85181012";
	public static final String MAIL_SEND_HOST_NAME = "winxiaomaolv";

	/**
	 * 自定义属性
	 */
	public static final String widgetLabel = "widgetLabel";// 属性名
	public static final String describe = "describe";// 描述
	public static final String grid = "grid";//列表
	public static final String columns = "columns";//列表的列

	/**
	 * Xml
	 */
	public static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	
	
	/**
	 * 文件后缀
	 */
	public static final String file_suffix_xml = "xml";
	public static final String file_suffix_xsd = "xsd";
	public static final String file_suffix_csv = "csv";
	public static final String file_suffix_txt = "txt";
	
	/**
	 * 文件前缀
	 */
	public static final String file_prefix_mapping = "mapping";
	public static final String file_prefix_temp = "temp";
	public static final String file_prefix_output = "output";
	
	/**
	 * 渠道路径
	 */
	public static final String channel_route = "fda/diform/";
	
	/**
	 * Mapping 关键词
	 */
	public static final String mapping_node = "node";
	
	public static final String mapping_eleName = "xpath";
	
	public static final String mapping_eleTxtVal = "textValue";
	
	public static final String mapping_attrName_path = "path";
	
	public static final String mapping_attrName_name = "name";
	
	public static final String mapping_attrName_suggest = "suggestAttrs";
	
	public static final String mapping_matching_degree = "matchDegree";
	
	public static final String mapping_attrName_field = "field";
	
	public static final String mapping_attr = "attribute";
	
	public static final String mapping_configDao = "dao";
	
	public static final String mapping_configMethod = "method";
	
	public static final String mapping_configMethodValue = "getProductData";
	
	public static final String mapping_configClass = "class";
	
	public static final String mapping_configClassValue = "TEFormMongoService";
	
}
