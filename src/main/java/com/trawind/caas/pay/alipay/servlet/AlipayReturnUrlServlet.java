package com.trawind.caas.pay.alipay.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.MailUtil;
import com.trawind.caas.entity.TETrade;
import com.trawind.caas.pay.alipay.util.AlipayNotify;
import com.trawind.caas.security.SysUser;
import com.trawind.caas.service.TETradeService;
import com.trawind.caas.service.TEUserService;

/* *
功能：支付宝页面跳转同步通知页面
版本：3.2
日期：2011-03-17
说明：
以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

//***********页面功能说明***********
该页面可在本机电脑测试
可放入HTML等美化页面的代码、商户业务逻辑程序代码
TRADE_FINISHED(表示交易已经成功结束，并不能再对该交易做后续操作);
TRADE_SUCCESS(表示交易已经成功结束，可以对该交易做后续操作，如：分润、退款等);
//********************************
* */

@RequestMapping(value = "alipayReturnUrlServlet")
public class AlipayReturnUrlServlet extends HttpServlet {
	
	@Autowired
	private TETradeService tradeService;
	@Autowired
	private TEUserService userService;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8437920150504280965L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//获取支付宝GET过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}
		
		//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		//商户订单号

		String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");

		//支付宝交易号

		String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");

		//交易状态
		String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

		//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//
		
		//计算得出通知验证结果
		boolean verify_result = AlipayNotify.verify(params);
		
		//验证成功
		if(verify_result){
			if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
				
				TETrade trade = tradeService.findByOutTradeNo(out_trade_no);
				if(trade!=null){
					SysUser sysuser = userService.findByCorpid(trade.getCompanyId());
					String password = Common.generateNumber(8);
					sysuser.setPassword(Common.encodePassword(password));
					sysuser.setUsername(Common.randomString(8));
					if(userService.saveSysUser(sysuser)!=null){
						sysuser.setPassword(password);
						if(MailUtil.sendMail(sysuser)){
							//发送成功
						}else{
							//发送失败
						}
						trade.setTradeStatus("1");
						tradeService.saveTETrade(trade);
						
						//跳转到首页 弹出提示
					}
				}
			}
			
			//该页面可做页面美工编辑
			System.out.println("验证成功<br />");
			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

			//////////////////////////////////////////////////////////////////////////////////////////
		}else{
			//该页面可做页面美工编辑
			System.out.println("验证失败");
		}
	}

}
