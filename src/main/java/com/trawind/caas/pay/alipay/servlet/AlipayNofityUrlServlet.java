package com.trawind.caas.pay.alipay.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.MailUtil;
import com.trawind.caas.entity.TETrade;
import com.trawind.caas.pay.alipay.util.AlipayNotify;
import com.trawind.caas.security.SysUser;
import com.trawind.caas.service.TETradeService;
import com.trawind.caas.service.TEUserService;

/* *
功能：支付宝服务器异步通知页面
版本：3.3
日期：2012-08-17
说明：
以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
该代码仅供学习和研究支付宝接口使用，只是提供一个参考。

//***********页面功能说明***********
创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
该页面调试工具请使用写文本函数logResult，该函数在com.alipay.util文件夹的AlipayNotify.java类文件中
如果没有收到该页面返回的 success 信息，支付宝会在24小时内按一定的时间策略重发通知
//********************************
* */
@RequestMapping(value = "alipayNofityUrlServlet")
public class AlipayNofityUrlServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6582377167395971774L;
	
	@Autowired
	private TETradeService tradeService;
	@Autowired
	private TEUserService userService;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//获取支付宝POST过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}
		
		//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以下仅供参考)//
		//商户订单号

		String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
		//支付宝交易号
		String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
		//交易状态
		String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");

		//获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表(以上仅供参考)//

		if(AlipayNotify.verify(params)){//验证成功
			if(trade_status.equals("TRADE_FINISHED")){
				//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
					//如果有做过处理，不执行商户的业务程序
				if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
					
					TETrade trade = tradeService.findByOutTradeNo(out_trade_no);
					if(trade!=null){
						SysUser sysuser = userService.findByCorpid(trade.getCompanyId());
						String password = Common.generateNumber(8);
						sysuser.setPassword(Common.encodePassword(password));
						sysuser.setUsername(Common.randomString(8));
						if(userService.saveSysUser(sysuser)!=null){
							sysuser.setPassword(password);
							if(MailUtil.sendMail(sysuser)){
								//发送成功
							}else{
								//发送失败
							}
							trade.setTradeStatus("1");
							tradeService.saveTETrade(trade);
							
							//跳转到首页 弹出提示
						}
					}
				}
					
				//注意：
				//退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
			} else if (trade_status.equals("TRADE_SUCCESS")){
				//判断该笔订单是否在商户网站中已经做过处理
					//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
					//请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
					//如果有做过处理，不执行商户的业务程序
				//根据订单号 到trade 更新订单状态
				
				if(trade_status.equals("TRADE_FINISHED") || trade_status.equals("TRADE_SUCCESS")){
					
					TETrade trade = tradeService.findByOutTradeNo(out_trade_no);
					if(trade!=null){
						SysUser sysuser = userService.findByCorpid(trade.getCompanyId());
						String password = Common.generateNumber(8);
						sysuser.setPassword(Common.encodePassword(password));
						sysuser.setUsername(Common.randomString(8));
						if(userService.saveSysUser(sysuser)!=null){
							sysuser.setPassword(password);
							if(MailUtil.sendMail(sysuser)){
								//发送成功
							}else{
								//发送失败
							}
							trade.setTradeStatus("1");
							tradeService.saveTETrade(trade);
							
							//跳转到首页 弹出提示
						}
					}
				}
				
				
				
					
				//注意：
				//付款完成后，支付宝系统发送该交易状态通知
			}

			//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
				
			System.out.print("success");	//请不要修改或删除

			//////////////////////////////////////////////////////////////////////////////////////////
		}else{//验证失败
			System.out.print("fail");
		}
	}

}
