package com.trawind.caas.xml.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class DomUtil {

	private static DocumentBuilderFactory resuseFactory;
	private static final Object LOCK = new Object();
	private static XPathFactory reuseXPathFactory;
	private static final Object LOCK2 = new Object();

	public static Document newEmptyDocument() throws Exception {
		DocumentBuilderFactory dbf = getReuseFactory();
		DocumentBuilder db;
		db = dbf.newDocumentBuilder();
		return db.newDocument();
	}

	public static Document parseXMLFile(String filePath) throws Exception {
		Document document = null;

		try {
			DocumentBuilderFactory dbf = getReuseFactory();
			DocumentBuilder db;
			db = dbf.newDocumentBuilder();
			document = db.parse(DomUtil.class.getResourceAsStream(filePath));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw e;
		}

		return document;
	}
	
	public static Document parseExtendXMLFile(String filePath) throws Exception{
		Document document = null;
		File xmlFile = new File(filePath);
		if(xmlFile == null || !xmlFile.exists()){
			throw new IllegalArgumentException(xmlFile + " not existed, please check the file path");
		}
		try {			
			DocumentBuilderFactory dbf = getReuseFactory();
			DocumentBuilder db;
			db = dbf.newDocumentBuilder();
			document = db.parse(new FileInputStream(xmlFile));
		}catch (ParserConfigurationException | SAXException | IOException e) {
			throw e;
		}
		return document;
	}

	public static Document parseXMLFile(InputStream inputStream) throws Exception {
		Document document = null;

		try {
			DocumentBuilderFactory dbf = getReuseFactory();
			DocumentBuilder db;
			db = dbf.newDocumentBuilder();
			document = db.parse(inputStream);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw e;
		}

		return document;
	}

	public static Document parseXMLFileInString(String xml) throws Exception {
		Document document = null;

		try {
			DocumentBuilderFactory dbf = getReuseFactory();
			DocumentBuilder db;
			db = dbf.newDocumentBuilder();
			document = db.parse(new InputSource(new StringReader(xml)));
		} catch (ParserConfigurationException | SAXException | IOException e) {
			throw e;
		}

		return document;
	}

	public static void writeXMLInPretty(Document document, Writer writer) throws Exception {
		try {
			DOMImplementationRegistry domRegistry = DOMImplementationRegistry.newInstance();
			DOMImplementationLS impl = (DOMImplementationLS) domRegistry.getDOMImplementation("LS");
			LSSerializer lss = impl.createLSSerializer();
			LSOutput lso = impl.createLSOutput();
			lso.setCharacterStream(writer);

			lss.getDomConfig().setParameter("format-pretty-print", true);
			lss.getDomConfig().setParameter("xml-declaration", true);

			lss.write(document, lso);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String convertDocumentToString(Document document) throws Exception {
		StringWriter writer = new StringWriter();
		writeXMLInPretty(document, writer);
		return writer.toString();
	}

	public static Node getNodeByXpath(Document document, String xPath) throws XPathExpressionException {
		Node resultNode = null;

		try {
			XPathFactory factory = getReuseXPathFactory();
			XPath xpath = factory.newXPath();
			XPathExpression expression = xpath.compile(xPath);
			resultNode = (Node) expression.evaluate(document, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			throw e;
		}

		return resultNode;
	}

	public static void setValueByXPath(Node node, String xPath, String value) throws XPathExpressionException {
		try {
			XPathFactory factory = getReuseXPathFactory();
			XPath xpath = factory.newXPath();
			XPathExpression expression = xpath.compile(xPath);
			Node xPathNode = (Node) expression.evaluate(node, XPathConstants.NODE);
			if (null != xPathNode) {
				xPathNode.setTextContent(value);
			}
		} catch (XPathExpressionException e) {
			throw e;
		}
	}

	public static String getXPath(Node node) {
		Node parent = node.getParentNode();
		if (parent == null) {
			return "/" + node.getNodeName();
		}
		return getXPath(parent) + "/" + node.getNodeName();
	}

	public static boolean isEmptyNode(Node node) {
		boolean flag = false;

		NodeList childNodeList = node.getChildNodes();
		for (int i = 0; i < childNodeList.getLength(); i++) {
			Node childNode = childNodeList.item(i);
			if (Node.ELEMENT_NODE == childNode.getNodeType()) {
				return false;
			}
		}

		String content = node.getTextContent();
		if (StringUtils.isEmpty(content)) {
			flag = true;
		} else if (StringUtils.isEmpty(content.trim().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t", ""))) {
			flag = true;
		} else if ("null".equals(content.trim().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t", "").toLowerCase())) {
			flag = true;
		}
		return flag;
	}

	public static boolean isEmptyNodeRecursively(Node node) {
		boolean flag = false;

		NodeList childNodeList = node.getChildNodes();
		for (int i = 0; i < childNodeList.getLength(); i++) {
			Node childNode = childNodeList.item(i);
			if (Node.ELEMENT_NODE == childNode.getNodeType()) {
				flag = isEmptyNodeRecursively(childNode);
				if (!flag) {
					return flag;
				}
			}
		}

		String content = node.getTextContent();
		if (StringUtils.isEmpty(content)) {
			flag = true;
		} else if (StringUtils.isEmpty(content.trim().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t", ""))) {
			flag = true;
		} else if ("null".equals(content.trim().replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t", "").toLowerCase())) {
			flag = true;
		}
		return flag;
	}

	public static boolean isAttrsEmpty(Node node) {
		boolean isEmpty = true;

		if (node.getNodeName().equals("lowes:lowesTradeItemExtension")) {
			return true;
		}

		if (node.getNodeName().equals("food_beverage_tobacco:foodAndBeverageTradeItemExtension")) {
			return true;
		}

		if (node.getNodeName().equals("healthcare:healthcareExtension")) {
			return true;
		}

		if (node.getNodeName().equals("gdsn:attributeValuePairExtension")) {
			return true;
		}

		NamedNodeMap attrs = node.getAttributes();
		List<String> attrsNeedToRemove = new ArrayList<String>();

		for (int i = 0; null != attrs && i < attrs.getLength(); i++) {
			Node attrNode = attrs.item(i);
			if (Node.ATTRIBUTE_NODE == attrNode.getNodeType() && !StringUtils.isEmpty(attrNode.getNodeValue())) {
				isEmpty = false;
			} else if (Node.ATTRIBUTE_NODE == attrNode.getNodeType()
					&& (StringUtils.isEmpty(attrNode.getNodeValue()) || "null".equals(attrNode.getNodeValue()))) {
				attrsNeedToRemove.add(attrNode.getNodeName());
			}
		}
		for (String attrToRemove : attrsNeedToRemove) {
			attrs.removeNamedItem(attrToRemove);
		}

		return isEmpty;
	}

	public static String getValueByXPath(Node node, String xPath) throws XPathExpressionException {
		String result = null;

		try {
			XPathFactory factory = getReuseXPathFactory();
			XPath xpath = factory.newXPath();
			XPathExpression expression = xpath.compile(xPath);
			result = expression.evaluate(node);
		} catch (XPathExpressionException e) {
			throw e;
		}

		return StringUtils.defaultString(result);
	}

	public static DocumentBuilderFactory getReuseFactory() {
		DocumentBuilderFactory factory = resuseFactory;
		if (factory == null) {
			synchronized (LOCK) {
				if (factory == null) {
					try {
						factory = DocumentBuilderFactory.newInstance();
						resuseFactory = factory;
					} catch (FactoryConfigurationError e) {
						e.printStackTrace();
						throw (e);
					}
				}
			}
		}
		return factory;
	}

	public static XPathFactory getReuseXPathFactory() {
		XPathFactory factory = reuseXPathFactory;

		if (factory == null) {
			synchronized (LOCK2) {
				if (factory == null) {
					try {
						factory = XPathFactory.newInstance();
						reuseXPathFactory = factory;
					} catch (FactoryConfigurationError e) {
						e.printStackTrace();
						throw (e);
					}
				}
			}
		}
		return factory;
	}

	public static String convertXpathToLocalNamePattern(String xpath) {
		StringBuffer result = new StringBuffer();
		String[] xpathSpiceArray = xpath.split("/");
		for (String splice : xpathSpiceArray) {
			if (splice.length() == 0) {
				continue;
			} else if (splice.startsWith("@")) {
				result.append("/").append(splice);
			} else {
				result.append("/*").append("[local-name()='").append(splice).append("']");
			}
		}
		return result.toString();
	}

}
