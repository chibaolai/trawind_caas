package com.trawind.caas.xml.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import com.google.common.collect.Lists;
import com.trawind.caas.common.Constant;
import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.xml.entity.XmlAttr;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class XmlUtil {
	
	private static Logger logger = Logger.getLogger(XmlUtil.class);
	
	private static String MULTIAYER_SEP = "/";

	private static String ENCODING_DEFAULT = "UTF-8";
	
	/**
	 * 过滤Namespace
	 * @param path
	 * @return
	 */
	public static String cleanNamespace(String path){
		if(path.indexOf(":")>-1){
			return path.split(":")[1];
		}else{
			return path;
		}
	}
	
	/**
	 * 是否包含子元素
	 * @param node
	 * @return
	 */
	public static boolean hasChildElementNode(Node node){
		if(!node.hasChildNodes()){
			return false;
		}
		
		for(int i=0 ;i<node.getChildNodes().getLength();i++){
			Node children = node.getChildNodes().item(i);
			if(children.getNodeType() == Node.ELEMENT_NODE){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 获取DaoNode
	 * @param parent
	 * @param type
	 * @return
	 */
	public static Node getFirstChildElementNode(Node parent) {
		if (!parent.hasChildNodes()) {
			return null;
		}

		for (int i = 0; i < parent.getChildNodes().getLength(); i++) {
			Node children = parent.getChildNodes().item(i);
			if (children.getNodeType() == Node.ELEMENT_NODE) {
				return children;
			}
		}
		return null;
	}
	
	
	/**
	 * 获取Mapping文件名字
	 * @param file
	 * @return
	 */
	public static String getConfigFileName(File file) {
		if (file != null) {
			return file.getName().substring(0, file.getName().indexOf(".")) + "Config.xml";
		}
		return null;
	}
	
	/**
	 * 获取Mapping文件名字
	 * @param file
	 * @return
	 */
	public static String getConfigFileName(String fileName) {
		if (fileName != null) {
			return fileName.substring(0, fileName.indexOf(".")) + "Config.xml";
		}
		return null;
	}
	
	/**
	 * 转为 临时xml文件名字
	 * @param file
	 * @return
	 */
	public static String getTemplateFileName(File file) {
		if (file != null) {
			return file.getName().substring(0, file.getName().indexOf(".")) + "Tmpl.xml";
		}
		return null;
	}
	
	/**
	 * 转为 临时xml文件名字
	 * @param file
	 * @return
	 */
	public static String getTemplateFileName(String fileName) {
		if (fileName != null) {
			return fileName.substring(0, fileName.indexOf(".")) + "Tmpl.xml";
		}
		return null;
	}
	
	/**
	 * 转为 临时xml文件名字
	 * @param file
	 * @return
	 */
	public static String getTemplateFileNameFilterConfig(String fileName) {
		if (fileName != null) {
			String fileNameConfig = fileName.substring(0, fileName.indexOf("."));
			return fileNameConfig.substring(0, fileNameConfig.length() - 6) + "Tmpl.xml";
		}
		return null;
	}
	
	/**
	 * 添加属性(xpath，field)
	 * @param doc
	 * @param node
	 * @param name
	 * @param value
	 * @return
	 */
	public static Node addAttribute(Document doc, Node node, String name, String value){
		NamedNodeMap atts = node.getAttributes();
		Attr att = doc.createAttribute(name);
		att.setValue(value);	
		atts.setNamedItem(att);
		return node;
	}
	
	/**
	 * 获取产品所有属性
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public static List<XmlAttr> getAttrList(List<TEAttr> attrData,String lang) {
		String _lang = "en";
		if(lang != null) {
			_lang = lang;
		}
		List<XmlAttr> retAttrList = Lists.newArrayList();
		if(attrData != null) {
			for(TEAttr attr:attrData) {
				if(attr != null) {	
					XmlAttr xmlAttr = new XmlAttr();
					xmlAttr.setId(attr.getId());
					xmlAttr.setType(attr.getWidgetType());
					String directJson = attr.getDirecJson();
					if(directJson != null && !"".equals(directJson)) {
						JSONObject jsonObject = JSONObject.fromObject(directJson);
						if(jsonObject.get(_lang) != null) {						
							JSONObject widgetJson = (JSONObject) jsonObject.get(_lang);
							if(widgetJson.get(Constant.widgetLabel) != null) {						
								String label = (String) widgetJson.get(Constant.widgetLabel);
								xmlAttr.setLabel(label);
							}
							if(widgetJson.get(Constant.describe) != null) {
								String describe = (String) widgetJson.get(Constant.describe);
								xmlAttr.setDescribe(describe);
							}
							if(widgetJson.get(Constant.grid) != null) {
								JSONObject grid = (JSONObject)widgetJson.get(Constant.grid);
								if(grid.get(Constant.columns) != null) {
								 JSONArray columns = (JSONArray) grid.get(Constant.columns);
								 xmlAttr.setColumns(columns);
								}
							}
						}
					}
					retAttrList.add(xmlAttr);
				}
			}
		}
		return retAttrList;
	}
	
	/**
	 * 获取文件前缀
	 * @param file
	 * @return
	 */
	public static String getPrefix(String fileName) {
		if (fileName != null) {
			return fileName.substring(0, fileName.indexOf("."));
		}
		return null;
	}
	
	/**
	 * 获取文件前缀
	 * @param file
	 * @return
	 */
	public static String getSuffix(String fileName) {
		if (fileName != null) {
			return fileName.substring(fileName.indexOf("."));
		}
		return null;
	}
	
	/**
	 * getRootElement
	 *@param document
	 *@return
	 */
	public static Element getRootElement(Document document){
		if( document == null){
			return null ;
		}
		return document.getDocumentElement() ;
	}
	
	/**
	 * createDocument
	 *@param name
	 *@param namespace
	 *@return
	 *@throws ParserConfigurationException
	 */
	public static Document createDocument(String name,String namespace) throws ParserConfigurationException{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		DOMImplementation impl = builder.getDOMImplementation();
		Document document = impl.createDocument(namespace, name, null);
		return document ;
	}
	
	

	
	/**
	 * 设置xml到指定Node下
	 * @param document
	 * @param nd
	 * @param xml
	 * @return
	 * @throws Exception 
	 * @throws  
	 */
	public static void setXmlToNode(Document document,Node nd, String xml) throws Exception{
		DOMParser parser=new DOMParser();
		ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes(ENCODING_DEFAULT));
		InputSource ins = new InputSource(bais);
		ins.setEncoding(ENCODING_DEFAULT);
		parser.parse(ins);
		Document doc=parser.getDocument();
		Node root=doc.getDocumentElement();
		if(root!=null)
			nd.appendChild(document.importNode(root, true));
	}
	
	/**
	 * 将此xml文档转换成字节
	 * @param document
	 * @return
	 * @throws Exception
	 */
	public static byte[] write2Bytes(Document document) throws Exception {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		OutputFormat format = new OutputFormat(document);
		format.setLineWidth(65);
		format.setIndenting(true);
		format.setEncoding(ENCODING_DEFAULT);
		format.setIndent(2);
		XMLSerializer serializer = new XMLSerializer(os, format);
		serializer.serialize(document);
		byte[] outbytes = os.toByteArray();
		format = null;
		os.close();
		return outbytes;
	}

	/**
	 * 根据xpath，返回多层子节点的值
	 * @param nd
	 * @param name
	 * @return
	 */
	public static String getValueByXpath(Node nd,String name){
		
		String[] nameArr = getMultiayerArr(name);
		String Attrname="";
		if(nameArr[nameArr.length-1].startsWith("@")){
			Attrname=nameArr[nameArr.length-1].substring(1);
			name=name.substring(0, name.indexOf("/"+nameArr[nameArr.length-1]));
		}

		Node node = getMultilayerChildByName(nd, name);
		if (node == null)
			return null;
		
//		if (node.getFirstChild() == null){
//			return null;
//		}
		
		if(Attrname.equals("")){
			if(null==node.getFirstChild())
				return null;
			else
				return node.getFirstChild().getNodeValue();
		}
		else{
			return getAttriByName(node, Attrname);
			
		}
	}
	
	
	
	/**
	 * 根据名称带/的分隔符返回的子节点
	 * @param nd
	 * @param name
	 * @return
	 */
	private static Node getMultilayerChildByName(Node nd,String name){
		String[] nameArr = getMultiayerArr(name);
		Properties attr=new Properties();
		int start=nameArr[0].indexOf("[");
		int end=nameArr[0].indexOf("]");
		String nodename=nameArr[0];
		if(start<end&&start!=-1){
			nodename=nameArr[0].substring(0,start);
			String[] attrs=nameArr[0].substring(start+1, end).split(",");
			for(String temp:attrs){
				String attrname=(String) temp.subSequence(1, temp.indexOf("="));
				String attrvalue=(String) temp.subSequence(temp.indexOf("=")+2, temp.length()-1);
				attr.setProperty(attrname,attrvalue);
			}
		}
		
		Node node=null;
		if(attr.isEmpty())
			node = getChildByName(nd, nodename);
		else node=getChildByNameAndAttr(nd,  nodename, attr);
		
		if (node == null)
			return null;
		
//		if (node.getFirstChild() == null){
//			return null;
//		}
		
		if (nameArr.length > 1){
			StringBuffer strBuff = new StringBuffer();
			for (int i = 1; i < nameArr.length; i++) {
				strBuff.append(nameArr[i] + MULTIAYER_SEP);
			}
			return getMultilayerChildByName(node,strBuff.toString());
		}else{
			return node;
		}
	}
	
	
	
	/**
	 * 通过属性名称取得节点属性
	 *@param nd
	 *@param name
	 *@return
	 */
	private static String getAttriByName(Node nd, String name) {
		String value = null;
		if (null == nd || null == name || "".equals(name.trim()))
			return null;
		NamedNodeMap attr = nd.getAttributes();
		if (null != attr) {
			Node n = attr.getNamedItem(name);
			if (null != n) {
				value = n.getNodeValue();
			}
		}
		return value;
	}
	
	
	
	/**
	 * 将xml文档2字符串
	 * @param document
	 * @return
	 * @throws Excepiton
	 */
	public static String document2String(Document document) throws Exception {
		return new String(write2Bytes(document));
	}

	private static String[] getMultiayerArr(String name) {
		if(name.startsWith("/")) {
			name = name.substring(1);
		}
		return name.split(MULTIAYER_SEP);
	}

	/**
	 * 根据xpath生成xml
	 * @param document xml文档
	 * @param nd  在doucment的那个元素下
	 * @param name 需要生成的xpath 
	 * @param value path value
	 * @param attrs  xml属性
	 * @return
	 */
	public static Node createXMLByXPath(Document document,
			Node nd,  String name, String value,Properties attrs) {
		if (null == nd || null == name || "".equals(name.trim()) || name.startsWith("@"))
			return null;

		String[] nameArr = getMultiayerArr(name);
//		Properties midAttr = new Properties();
//		int start = nameArr[0].indexOf("[");
//		int end = nameArr[0].indexOf("]");
		String nodename = nameArr[0];
//		if (start < end && start != -1) {
//			nodename = nameArr[0].substring(0, start);
//			String[] as = nameArr[0].substring(start + 1, end).split(",");
//			for (String temp : as) {
//				String attrname = (String) temp.subSequence(1,
//						temp.indexOf("="));
//				String attrvalue = (String) temp.subSequence(
//						temp.indexOf("=") + 2, temp.length() - 1);
//				if (nameArr.length > 1)
//					midAttr.setProperty(attrname, attrvalue);
//				else
//					attrs.setProperty(attrname, attrvalue);
//			}
//		}

		Node nnd = null;
		if (attrs.isEmpty())
			nnd = getChildByName(nd, nodename);
		else
			nnd = getChildByNameAndAttr(nd, nodename, attrs);
		if (null != nnd) {
			if (nameArr.length > 1) {
				StringBuffer strBuff = new StringBuffer();
				for (int i = 1; i < nameArr.length; i++) {
					strBuff.append(nameArr[i] + MULTIAYER_SEP);
				}
				return createXMLByXPath(document, nnd, strBuff.toString(),value,
						 attrs);
			}

			boolean judge = true;// 属性值相等
			Properties props = getNodeProperties(nnd);
			if (attrs != null) {
				Iterator<Map.Entry<Object, Object>> it = attrs.entrySet()
						.iterator();
				while (it.hasNext()) {
					Map.Entry<Object, Object> pairs = (Map.Entry<Object, Object>) it
							.next();
					if (!((String) pairs.getValue()).equals(props
							.getProperty((String) pairs.getKey()))) {
						judge = false;
						break;
					}
				}
			}
			if (attrs == null && !props.isEmpty())
				judge = false;

			if (value != null) {
				if (null != nnd.getFirstChild() && judge) {// 原节点有值且属性相等，直接覆盖原值
					if (nnd.getFirstChild().hasChildNodes()) {
						logger.error("set value error!");
					}

					nnd.getFirstChild().setNodeValue(value);
				} else if (!judge) {// 属性不等，新建节点
					nnd = document.createElement(nodename);
					Text text = document.createTextNode(value);
					nnd.appendChild(text);
					nd.appendChild(nnd);
				} else {// 原节点没值，属性相等，直接把value赋到节点下
					Text text = document.createTextNode(value);
					nnd.appendChild(text);
				}
			}
			if (attrs != null && !judge) {
				String ktmp = null;
				Enumeration<Object> keys = attrs.keys();
				while (keys.hasMoreElements()) {
					ktmp = keys.nextElement().toString();
					((Element) nnd).setAttribute(ktmp, attrs.getProperty(ktmp));
				}
			}
		} else {
			if (nd.getFirstChild() != null
					&& nd.getFirstChild().getNodeValue() != null) {
				if (!nd.getFirstChild().getNodeValue().trim().equals("")) {
					logger.error("set value error!");
				}
			}

			nnd = document.createElement(nodename);
			if (nameArr.length > 1) {
				StringBuffer strBuff = new StringBuffer();
				for (int i = 1; i < nameArr.length; i++) {
					strBuff.append(nameArr[i] + MULTIAYER_SEP);
				}

				if (attrs != null) {
					String ktmp = null;
					Enumeration<Object> keys = attrs.keys();
					while (keys.hasMoreElements()) {
						ktmp = keys.nextElement().toString();
						((Element) nnd).setAttribute(ktmp,
								attrs.getProperty(ktmp));
					}
				}
				nd.appendChild(nnd);
				return createXMLByXPath(document, nnd,strBuff.toString(), value,
						 attrs);
			}

			if (value != null) {
				Text text = document.createTextNode(value);
				nnd.appendChild(text);
			}
			if (attrs != null) {
				String ktmp = null;
				Enumeration<Object> keys = attrs.keys();
				while (keys.hasMoreElements()) {
					ktmp = keys.nextElement().toString();
					((Element) nnd).setAttribute(ktmp, attrs.getProperty(ktmp));
				}
			}
			nd.appendChild(nnd);
		}

		return nnd;
	}
	/**
	 * 根据节点获取子节点
	 *@param nd
	 *@param name
	 *@return
	 */
	private static Node getChildByName(Node nd, String name) {
		if (null == nd || null == name || "".equals(name.trim()))
			return null;
		NodeList temp = null;
		if (null != nd)
			temp = nd.getChildNodes();
		if (null == temp) {
			return null;
		}
		String tempName;
		for (int i = 0; i < temp.getLength(); i++) {
			tempName = temp.item(i).getNodeName();
			if (tempName.equals(name))
				return temp.item(i);
		}
		return null;
	}
	/**
	 * getChildByNameAndAttr
	 *@param nd
	 *@param name
	 *@param attrs
	 *@return
	 */
	private static Node getChildByNameAndAttr(Node nd, String name,
			Properties attrs) {
		if (null == nd || null == name || "".equals(name.trim()))
			return null;
		NodeList temp = null;
		if (null != nd)
			temp = nd.getChildNodes();
		if (null == temp) {
			return null;
		}

		for (int i = 0; i < temp.getLength(); i++) {
			boolean judge = false;
			if (name.equals(temp.item(i).getNodeName())) {
				if (attrs == null)
					return temp.item(i);
				else if (attrs.size() == 0) {// 获取不带属性的节点
					if (!temp.item(i).hasAttributes())
						return temp.item(i);
				} else {
					judge = true;
					Properties props = getNodeProperties(temp.item(i));
					Iterator<Map.Entry<Object, Object>> it = attrs.entrySet()
							.iterator();
					while (it.hasNext()) {
						Map.Entry<Object, Object> pairs = (Map.Entry<Object, Object>) it
								.next();
						if (!((String) pairs.getValue()).equals(props
								.getProperty((String) pairs.getKey()))) {
							judge = false;
							break;
						}
					}
					if (judge)
						return temp.item(i);
				}
			}
		}
		return null;
	}
	/**
	 * getNodeProperties
	 *@param nd
	 *@return
	 */
	private static Properties getNodeProperties(Node nd) {
		Properties rp = new Properties();
		if (null == nd)
			return rp;
		NamedNodeMap attr = nd.getAttributes();
		if (null != attr) {
			for (int i = 0; i < attr.getLength(); i++) {
				Node nnd = attr.item(i);
				String attrName = nnd.getNodeName();
				String attrValue = nnd.getNodeValue();
				rp.put(attrName, attrValue);
			}

		}
		return rp;
	}
	
	public static void main(String[] args) {
		String name = "/a/b/@c";
		String[] nameArr = getMultiayerArr(name);
		System.out.println("-==========");
	}
}
