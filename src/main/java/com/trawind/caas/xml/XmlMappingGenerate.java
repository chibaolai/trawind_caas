package com.trawind.caas.xml;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.trawind.caas.common.Constant;
import com.trawind.caas.res.Convert2XmlMappingRes;
import com.trawind.caas.web.LoginUser;
import com.trawind.caas.xml.schema.Convert2XmlMapping;

@Component
public class XmlMappingGenerate {

	@Autowired
	private Convert2XmlMapping convert2XmlMapping;

	public Convert2XmlMappingRes convert2XmlMapping(File file, LoginUser loginUser) {
		Convert2XmlMappingRes retXml = null;
		try {
			if (file.getName().endsWith(Constant.file_suffix_xsd)) {
				retXml = convert2XmlMapping.parseSchema2Xml(file, loginUser);
			} else if (file.getName().endsWith(Constant.file_suffix_xml)) {
				retXml = convert2XmlMapping.parseSample2Xml(file, loginUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retXml;
	}
}
