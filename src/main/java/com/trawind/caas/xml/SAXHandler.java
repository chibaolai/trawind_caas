package com.trawind.caas.xml;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler{
	
	private Document document;
	
	private Element rootGen;
	
	private String preTag;
	
	private StringBuffer xpath;
	
	private HashMap attrMap;//索引
	
	private static final String slash = "/";

	//处理文档解析开始事件
	@Override
	public void startDocument() throws SAXException {
		document = DocumentHelper.createDocument();
		rootGen = document.addElement("root");
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		preTag = qName;
		if(xpath == null) {
			xpath = new StringBuffer();
		}else {
			xpath.append(slash);
		}
		xpath.append(qName);
		//索引
		if(attributes.getLength() > 0) {
			attrMap = new HashMap();
			for(int i=0;i<attributes.getLength();i++) {
				attrMap.put(attributes.getQName(i), attributes.getValue(i));
			}
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
//		System.out.print("</"+qName+">");
		if(xpath.length() == qName.length()) {			
			xpath = null;
		}else {
			xpath.setLength(xpath.length()-qName.length()-1);			
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String cha = new String(ch,start,length);
		if(cha.contains("\n") || cha.contains("\t")) {
			System.out.println(" ");//换行
		}else{			
			Element nameGen = rootGen.addElement("element").addText(cha)
					.addAttribute("name", preTag).addAttribute("xpath", xpath.toString());
		}
	}
	
	@Override
	 public void endDocument ()throws SAXException {
		createNewXML();
	}
	
//	/**
//	 * 生成索引
//	 * @throws FileNotFoundException 
//	 */
//	private void builIndex(Document document) throws FileNotFoundException{
//		Directory directory = new SimpleFSDirectory(new File("D:/newXMLIndex.xml"));
//		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_36, new StandardAnalyzer(Version.LUCENE_36));
//		IndexWriter indexWriter = new IndexWriter(directory,config);
//		indexWriter.addDocument(document);
//		 try {
//			   writer.close();
//			 } finally {
//			   if (IndexWriter.isLocked(directory)) {
//			     IndexWriter.unlock(directory);
//			   }
//			 }
//	}
	
	/**
	 * 创建新XML
	 */
	private void createNewXML() {
		OutputFormat format = null;
		XMLWriter xmlwriter = null;
		// 将定义好的内容写入xml文件中
		try {
			// 进行格式化
			format = OutputFormat.createPrettyPrint();
			// 设定编码
			format.setEncoding("UTF-8");
			xmlwriter = new XMLWriter(new FileOutputStream("D:/newXML.xml"), format);
			xmlwriter.write(document);
			System.out.println(document);
			xmlwriter.flush();
			xmlwriter.close();
			System.out.println("==========successfully===========");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("==========Exception=============");
		}
	}
	
	/**
	 * 获取新XML
	 */
	public Document getNewXML() {
        SAXParserFactory spf=SAXParserFactory.newInstance();  
        try {  
            SAXParser sp=spf.newSAXParser();  //创建SAX解析器  
            sp.parse(new File("D:/NewFile.xml"),new SAXHandler()); //开始解析文档  
        } catch (Exception e) {  
            e.printStackTrace();  
        }
        return document;
	}
	
}