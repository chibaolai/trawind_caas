package com.trawind.caas.xml.entity;

import net.sf.json.JSONArray;

public class XmlAttr {

	private String id;
	private String type;
	private String label;
	private String describe;
	private JSONArray columns;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public JSONArray getColumns() {
		return columns;
	}
	public void setColumns(JSONArray columns) {
		this.columns = columns;
	}
}
