package com.trawind.caas.xml.schema;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.trawind.caas.xml.util.DomUtil;


public class SchemaParser {

	Document document = null;
	
	Node elementNode = null;
	
	public void parse(File xsdFile) throws Exception {
		elementNode = null;
		document = DomUtil.parseXMLFile(new FileInputStream(xsdFile));
		Node rootNode = document.getFirstChild();
		if (rootNode != null) {
			NodeList nodeList = rootNode.getChildNodes();
			for (int i=0; i<nodeList.getLength(); i++) {
				if (nodeList.item(i).getNodeName() == "xsd:element") {
					elementNode = nodeList.item(i);
					break;
				}
			}
		}
	}
	
	public String getNamespaceURI() {
		Node rootNode = document.getFirstChild();
		if (rootNode != null 
				&& rootNode.hasAttributes()
				&& rootNode.getAttributes().getNamedItem("targetNamespace") != null) {
			return rootNode.getAttributes().getNamedItem("targetNamespace").getNodeValue();
		}
		return null;
	}
	
	public String getNamespace() throws XPathExpressionException {
//		Node elementNode = DomUtil.getNodeByXpath(document, "schema/element");
		if (elementNode != null 
				&& elementNode.hasAttributes()
				&& elementNode.getAttributes().getNamedItem("name") != null) {
			return elementNode.getAttributes().getNamedItem("name").getNodeValue();
		}
		return null;
	}
	
	public String getNamespacePrefix() throws XPathExpressionException {
//		Node elementNode = DomUtil.getNodeByXpath(document, "schema/element");
		if (elementNode != null 
				&& elementNode.hasAttributes()
				&& elementNode.getAttributes().getNamedItem("type") != null) {
			String type = elementNode.getAttributes().getNamedItem("type").getNodeValue();
			if (!StringUtils.isEmpty(type)) {
				return type.split(":")[0];
			} 
		}
		return null;
	}
	
}
