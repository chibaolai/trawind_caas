package com.trawind.caas.xml.schema;

import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.xerces.xs.XSModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.trawind.caas.base.TradeCode;
import com.trawind.caas.common.Constant;
import com.trawind.caas.res.Convert2XmlMappingRes;
import com.trawind.caas.service.TEAttrsService;
import com.trawind.caas.service.TRChannelService;
import com.trawind.caas.utils.UUID;
import com.trawind.caas.web.LoginUser;
import com.trawind.caas.xml.entity.XmlAttr;
import com.trawind.caas.xml.util.DomUtil;
import com.trawind.caas.xml.util.XmlUtil;

import jlibs.xml.sax.XMLDocument;
import jlibs.xml.xsd.XSInstance;
import jlibs.xml.xsd.XSParser;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Component
public class Convert2XmlMapping {
	@Autowired
	private TEAttrsService attrService;
	
//	@Autowired
//	private TRChannelService channelService;
	
//	@Autowired
//	private UploadSettings uploadSettings;
	
	@Autowired
	private TRChannelService channelService;
	
	private List<XmlAttr> _attrList = null;
	
	private Map<String,String> xpathMap = null;

	/**
	 * 将Schema转换为Xml
	 * @param path
	 * @return
	 */
	public Convert2XmlMappingRes parseSchema2Xml(File xsdFile,LoginUser loginUser) throws Exception{
		String tmplStr = parseSchema2XmlStr(xsdFile);
		Document tmplDoc = DomUtil.parseXMLFileInString(tmplStr);
		return parse2Xml(tmplDoc,xsdFile.getName(),loginUser);
	}
	
	/**
	 * 将Sample xml转换为XmlMapping
	 * @param path
	 * @return
	 */
	public Convert2XmlMappingRes parseSample2Xml(File xmlFile,LoginUser loginUser) throws Exception{
		Document tmplDoc = DomUtil.parseXMLFile(new FileInputStream(xmlFile));
		return parse2Xml(tmplDoc,xmlFile.getName(),loginUser);
	}
	
	/**
	 * 解析上传文件
	 * @param tmplDoc
	 * @param fileName
	 * @param lang
	 * @return
	 */
	private Convert2XmlMappingRes parse2Xml(Document tmplDoc, String fileName, LoginUser loginUser) {
		Convert2XmlMappingRes retRes = new Convert2XmlMappingRes();
		Node rootTmplNode = tmplDoc.getFirstChild();

		/**
		 * Mapping校验是否存在
		 */
		String mappingHashCode = channelService.getMappingHashCode(Constant.channel_route);
		try {
//			if (exitTempFile != null) {// 文件已存在
//				File file = FileUtil.creatTempFile();// 生成临时文件
//				mgridFSTempService.writeToFile(file, exitTempFile);// DB中file写入临时File
//				Document dbDoc = DomUtil.parseXMLFile(new FileInputStream(file));// 将xml文件转换为document
//				boolean isChanged = isChanged(dbDoc.getChildNodes(), tmplDoc.getChildNodes());
//				if (isChanged) {
//					retRes.setResultCode(TradeCode.file_content_changed);
//					retRes.setResultMessage(RespMsgUtils.respCodeForMsg(TradeCode.file_content_changed));
//					return retRes;
//				}
//			}
			String currentParentPath = Constant.symbol_slash + XmlUtil.cleanNamespace(rootTmplNode.getNodeName());
//			String daoPath = this.getClass().getResource(_defaultConfig).getFile();
			Document configDocument = DomUtil.newEmptyDocument();
			Node root = configDocument.importNode(rootTmplNode, false);
			configDocument.appendChild(root);
			
			Element dao = configDocument.createElement(Constant.mapping_configDao);
			dao.setAttribute(Constant.mapping_configClass, Constant.mapping_configClassValue);
			dao.setAttribute(Constant.mapping_configMethod,Constant.mapping_configMethodValue);
			root.appendChild(dao);
//			Document configDocument = DomUtil.getReuseFactory().newDocumentBuilder().parse(new InputSource(daoPath));
			Node daoNode = XmlUtil.getFirstChildElementNode(configDocument.getFirstChild());
			NamedNodeMap attrs = daoNode.getAttributes();
			if (null != attrs) {
				_attrList = XmlUtil.getAttrList(attrService.findAllAttrList(), loginUser.getLanCode());
			}
			cleanTemplateNodeAndGenerateConfigNode(rootTmplNode.getChildNodes(), configDocument, daoNode, currentParentPath);

//			String templateFilePath = uploadSettings.getXmlTempUrl() + XmlUtil.getTemplateFileName(fileName);
//			File tempFile = FileUtil.writeXMLInFile(templateFilePath, tmplDoc);
//			File tempFile = FileUtil.creatTempFile();
//			FileUtil.writeXMLInFile(tempFile, tmplDoc);
//			String tempId = this.mgridFSTempService.saveTempDocument(tempFile);
//			TikaUtils tikaUtils = new TikaUtils(tempFile);
//			tikaUtils.doTikaParse();
//			String contentsHashCode = generateTempHashCode(tikaUtils.getContents());
//			channelService.getTempHashCode(channelRout);

			String configStr = DomUtil.convertDocumentToString(configDocument);
			retRes.setResultCode(TradeCode.SUCCESS);
			retRes.setResultValue(configStr);
			if(xpathMap != null) {
				xpathMap = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retRes;

	}
	
	private String generateTempHashCode(String contents) {
		return DigestUtils.md5Hex(getContentBytes(contents, "UTF-8"));
	}
	
	/**
	 * @param content
	 * @param charset
	 * @return
	 * @throws SignatureException
	 * @throws UnsupportedEncodingException
	 */
	private static byte[] getContentBytes(String content, String charset) {
		if (charset == null || "".equals(charset)) {
			return content.getBytes();
		}
		try {
			return content.getBytes(charset);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("MD5签名过程中出现错误,指定的编码集不对,您目前指定的编码集是:" + charset);
		}
	}
	
	/**
	 * 判断上传文件与数据库中Mapping是否有改变
	 * @param dbNodes
	 * @param fileNodes
	 * @return
	 */
	private boolean isChanged(NodeList dbNodes,NodeList fileNodes) {
		for (int i = 0; null != dbNodes && i < dbNodes.getLength(); i++) {
			Node dbNode = dbNodes.item(i);
			Node fileNode = fileNodes.item(i);
			if(!dbNode.getNodeName().equals(fileNode.getNodeName())) {
				return true;
			}
			isChanged(dbNode.getChildNodes(),fileNode.getChildNodes());
		}
		return false;
	}
	
	/**
	 * 解析生成xpath
	 * @param nodeList
	 * @param configDocument
	 * @param daoNode
	 * @param currentParentPath
	 */
	private void cleanTemplateNodeAndGenerateConfigNode(NodeList nodeList, Document configDocument, Node daoNode,
			String currentParentPath) {
		if (nodeList == null || nodeList.getLength() == 0) {
			return;
		}
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			node.setNodeValue("");
			NamedNodeMap attrs = node.getAttributes();
			if (null != attrs) {
				for (int attrIndex = 0; attrIndex < attrs.getLength(); attrIndex++) {
					attrs.item(attrIndex).setNodeValue("");
				}
			}

			boolean hasChildElementNode = XmlUtil.hasChildElementNode(node);
			if (node.getNodeType() == Node.ELEMENT_NODE && !hasChildElementNode) {
				if(xpathMap == null) {
					xpathMap = new HashMap<String,String>();
				}

				Node xpathNode = null;
				if (null != attrs && attrs.getLength() > 0) {//复合类型
					String nodeName = XmlUtil.cleanNamespace(node.getNodeName());
					String xpath4Config = currentParentPath + Constant.symbol_slash + nodeName;
					if (xpathMap.containsKey(xpath4Config)) {
						continue;
					}
					xpathMap.put(xpath4Config, nodeName);
//					String suggestAttrs = getSuggestAttr(nodeName);
					xpathNode = configDocument.createElement(Constant.mapping_node);
					XmlUtil.addAttribute(configDocument, xpathNode, Constant.mapping_attrName_path, xpath4Config);
					XmlUtil.addAttribute(configDocument, xpathNode, Constant.mapping_attrName_name,
							XmlUtil.cleanNamespace(node.getNodeName()));
					
					/** 属性 xpath做成 */
					for (int attrIndex = 0; attrIndex < attrs.getLength(); attrIndex++) {
						Node attrXpathNode = configDocument.createElement(Constant.mapping_eleName);
						String attrName = attrs.item(attrIndex).getNodeName();
						String xpathOfAttr4Config = currentParentPath + Constant.symbol_slash
								+ XmlUtil.cleanNamespace(node.getNodeName()) + Constant.symbol_slash
								+ Constant.symbol_at + XmlUtil.cleanNamespace(attrs.item(attrIndex).getNodeName());
						if (xpathMap.containsKey(xpathOfAttr4Config)) {
							continue;
						}
						xpathMap.put(xpathOfAttr4Config, xpathOfAttr4Config);
						String suggestAttrs = getSuggestAttr(attrName);
						XmlUtil.addAttribute(configDocument, attrXpathNode, Constant.mapping_attrName_path, xpathOfAttr4Config);
						XmlUtil.addAttribute(configDocument, attrXpathNode, Constant.mapping_attrName_name, attrName);
						XmlUtil.addAttribute(configDocument, attrXpathNode, Constant.mapping_attrName_suggest, suggestAttrs);
						XmlUtil.addAttribute(configDocument, attrXpathNode, Constant.mapping_matching_degree, UUID.randomNumberUUID(2) + "%");
						xpathNode.appendChild(attrXpathNode);
					}
				
				} else {//简单类型
					String nodeName = XmlUtil.cleanNamespace(node.getNodeName());
					String xpath4Config = currentParentPath + Constant.symbol_slash + nodeName;
					if (xpathMap.containsKey(xpath4Config)) {
						continue;
					}
					xpathMap.put(xpath4Config, nodeName);
					String suggestAttrs = getSuggestAttr(nodeName);
					xpathNode = configDocument.createElement(Constant.mapping_eleName);
					XmlUtil.addAttribute(configDocument, xpathNode, Constant.mapping_attrName_path, xpath4Config);
					XmlUtil.addAttribute(configDocument, xpathNode, Constant.mapping_attrName_name,
							XmlUtil.cleanNamespace(node.getNodeName()));
					XmlUtil.addAttribute(configDocument, xpathNode, Constant.mapping_attrName_suggest, suggestAttrs);
					XmlUtil.addAttribute(configDocument, xpathNode, Constant.mapping_matching_degree, UUID.randomNumberUUID(2) + "%");
				}
				daoNode.appendChild(xpathNode);
			} else if (node.getNodeType() == Node.ELEMENT_NODE && hasChildElementNode) {
				cleanTemplateNodeAndGenerateConfigNode(node.getChildNodes(), configDocument, daoNode,
						currentParentPath + Constant.symbol_slash + XmlUtil.cleanNamespace(node.getNodeName()));
			}
		}
	}
	
	/**
	 * 获取推荐属性
	 * @return
	 */
	private String getSuggestAttr(String nodeLeaf) {
		JSONArray retSuggestAttrArray = new JSONArray();
		if(_attrList != null) {
			Iterator<XmlAttr> ite = _attrList.iterator();
			while(ite.hasNext()) {
				XmlAttr attr = ite.next();
				if(attr.getLabel() != null && nodeLeaf.contains(attr.getLabel())) {
					JSONObject suggestAttrObj = new JSONObject();
					suggestAttrObj.put("id", attr.getId());
					suggestAttrObj.put("label", attr.getLabel());
					retSuggestAttrArray.add(suggestAttrObj);
					break;
				}
//				todo 分词 匹配
				
			}
		}
		if(retSuggestAttrArray != null) {
			return retSuggestAttrArray.toString();
		}
		return null;
	}
	
	/**
	 * 将Schema转换为Xml字符串
	 * @param xsdFile
	 * @return
	 * @throws Exception
	 */
	private String parseSchema2XmlStr(File xsdFile) throws Exception{
		XSModel xsModel = new XSParser().parse(xsdFile.getAbsolutePath());//输入
		QName root = getSchemaRoot(xsdFile);//获取schema根元素
		StreamResult streamResult = new StreamResult();
		streamResult.setWriter(new StringWriter());
		XMLDocument templateDocument = new XMLDocument(streamResult, false, 4, null);//输出
		
		XSInstance instance = new XSInstance();
		instance.minimumElementsGenerated = 1;
		instance.maximumElementsGenerated = 1;		
		instance.generateDefaultAttributes = false;
		instance.generateOptionalAttributes = false;
		instance.maximumRecursionDepth = 1;
		instance.generateOptionalElements = true;
		instance.generate(xsModel, root, templateDocument);//解析
		String tmplStr = streamResult.getWriter().toString();
		//remove all comments
		tmplStr = tmplStr.replaceAll("<\\!--.*-->", "");
		return tmplStr;
	}
	
	/**
	 * 获取Schema根节点
	 * @param path
	 * @return
	 * @throws Exception
	 */
	private QName getSchemaRoot(File xsdFile) throws Exception{	
		SchemaParser schemaParser = new SchemaParser();		
		schemaParser.parse(xsdFile);
		String namespaceURI = schemaParser.getNamespaceURI();
		String namespace = schemaParser.getNamespace();
		String namespacePrefix = schemaParser.getNamespacePrefix();
		QName elName = new QName(namespaceURI, namespace,namespacePrefix);
		return elName;
	}
}
