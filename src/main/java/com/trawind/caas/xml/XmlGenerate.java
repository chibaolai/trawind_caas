package com.trawind.caas.xml;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.trawind.caas.common.Constant;
import com.trawind.caas.common.FileUtil;
import com.trawind.caas.common.WebContext;
import com.trawind.caas.config.UploadSettings;
import com.trawind.caas.entity.MProductData;
import com.trawind.caas.entity.MWidget;
import com.trawind.caas.service.TEFormMongoService;
import com.trawind.caas.utils.DateUtils;
import com.trawind.caas.web.LoginUser;
import com.trawind.caas.xml.util.DomUtil;
import com.trawind.caas.xml.util.XmlUtil;

@Component
public class XmlGenerate {
	private static String DATE = DateUtils.format(new Date(), "yyyyMMdd");
	
	@Autowired
	private UploadSettings uploadSettings;
	
	@Autowired
	private TEFormMongoService formMongoService;
	
	private Collection<String> _attrKeyWord = null;
	
	public File convert2Xml(File configFile, String productId, Long corpId) throws Exception {
		if(_attrKeyWord == null) {
			_attrKeyWord = new ArrayList<String>();
		}
		_attrKeyWord.add("root");
		_attrKeyWord.add("code");
		_attrKeyWord.add("codeSystem");
		_attrKeyWord.add("type");
//		XmlConfig xmlConfig = xmlConfigParser.parseXmlConfig(configFile);
//		Document document = xmlConverter.convert(xmlConfig, null, productId, corpId);
//		Document targetDocument = mapping2XmlHandler.getTargetFile(configFile);
		String FILE_PATH = uploadSettings.getXmlUrl() + DATE + "/CIN_XML_"
				+ DateUtils.format(new Date(), "yyyyMMddHHmmss") + ".xml";
//		File file = FileUtil.creatOutputFile();
//		File file = FileUtil.writeXMLInFile(FILE_PATH, document);
		/**
		 * get Mapping Document
		 */
		Document mappingDoc = DomUtil.parseXMLFile(new FileInputStream(configFile));
		
		/**
		 * clone mapping document root
		 */
		Document outputDoc = DomUtil.newEmptyDocument();
		Node root = outputDoc.importNode(mappingDoc.getFirstChild(), false);
		outputDoc.appendChild(root);
		
		NodeList nodeList = mappingDoc.getChildNodes();
		/**
		 * get DB value
		 */
		MProductData mProductData = formMongoService.getProductData(productId, corpId);
		Map<String, Object> daoObj = new HashMap<String, Object>();
		if(mProductData != null) {					
			List<MWidget> widgets = mProductData.getWidget();
			if(widgets != null) {
				for(MWidget widget:widgets) {
					daoObj.put(widget.getKey(), widget.getValue());
				}
			}
		}
		createXMLByXPath(nodeList,outputDoc,root,daoObj);

		
		/**
		 * document to file
		 */
		File outputFile = File.createTempFile(Constant.file_prefix_output, Constant.file_suffix_xml);
		FileUtil.writeXMLInFile(outputFile, outputDoc);
		return outputFile;
	}
	
	private void createXMLByXPath(NodeList nodeList,Document outputDoc,Node root,Map<String, Object> daoObj) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node node = nodeList.item(i);
			if (node.getNodeName().equals(Constant.mapping_eleName)) {
				Properties attrs = new Properties();
				NamedNodeMap attrsMap = node.getAttributes();
				if (null != attrsMap) {
					Object value = null;
					String path = null;
					for (int attrIndex = 0; attrIndex < attrsMap.getLength(); attrIndex++) {
						Node attrNode = attrsMap.item(attrIndex);
						
						if(_attrKeyWord.contains(attrNode.getNodeName())) {
							attrs.setProperty(attrNode.getNodeName(), attrNode.getNodeValue());
						}
						if(attrNode.getNodeName().equals(Constant.mapping_attrName_field)) {
							String field = attrNode.getNodeValue();
							value = daoObj.get(field);
						}
						if(attrNode.getNodeName().equals(Constant.mapping_attrName_path)) {
							if(!StringUtils.isEmpty(attrNode.getNodeValue())) {		
								path = attrNode.getNodeValue();
							}
						}
					}
					if(path.startsWith("/document")) {
						path = path.substring(9);
					}
					XmlUtil.createXMLByXPath(outputDoc, root, path, value!=null?value.toString():"", attrs);
				}
			}else {
				createXMLByXPath(node.getChildNodes(),outputDoc,root,daoObj);
			}
		}
	}
}
