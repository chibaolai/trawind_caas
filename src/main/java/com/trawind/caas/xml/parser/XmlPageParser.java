package com.trawind.caas.xml.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.AbstractParser;
import org.apache.tika.parser.ParseContext;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

import com.trawind.caas.utils.TikaUtils;
import com.trawind.caas.xml.handler.BodyExtendContentHandler;

public class XmlPageParser extends AbstractParser {
	private static final long serialVersionUID = -1656197033781181324L;

	private static final Set<MediaType> SUPPORTED_TYPES = Collections.unmodifiableSet(new HashSet<MediaType>(
			Arrays.asList(MediaType.application(TikaUtils.PREFIX_XML), MediaType.image(TikaUtils.PREFIX_IMAGE))));

	@Override
	public Set<MediaType> getSupportedTypes(ParseContext context) {
		return SUPPORTED_TYPES;
	}

	@Override
	public void parse(InputStream stream, ContentHandler handler, Metadata metadata, ParseContext context)
			throws IOException, SAXException, TikaException {

		if (metadata.get(Metadata.CONTENT_TYPE) == null) {
			metadata.set(Metadata.CONTENT_TYPE, TikaUtils.APPLICATION_XML);
		}
		BodyExtendContentHandler bech = (BodyExtendContentHandler) handler;
		bech.setContentType(metadata.get(Metadata.CONTENT_TYPE));

		SAXReader sr = null;
		Document doc = null;
		Element root = null;
		Element cloneRoot = null;
		Element father = null;
		try {
			boolean isExist = bech.hasItemExist(bech.getFileId());
			if (isExist) {
				return;
			}

			sr = new SAXReader();
			doc = sr.read(stream);
			root = doc.getRootElement();
			cloneRoot = (Element) root.clone();
			cloneRoot.elements().clear();
			String id = bech.getFileId();
			String rootContent = cloneRoot.asXML();
			String childContent = StringUtils.EMPTY;
			cloneRoot = null;
			bech.setContents(doc.asXML());

			for (Iterator<?> fathers = root.elementIterator(); fathers.hasNext();) {
				father = (Element) fathers.next();
				childContent = father.asXML();

				bech.saveNodeContentToDB(id, rootContent, childContent);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
