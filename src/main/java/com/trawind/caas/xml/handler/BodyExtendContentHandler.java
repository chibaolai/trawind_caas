package com.trawind.caas.xml.handler;

import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.sax.WriteOutContentHandler;
import org.springframework.stereotype.Component;

import com.trawind.caas.service.TETempXmlElementService;
import com.trawind.caas.utils.SpringContextUtil;
import com.trawind.caas.utils.TikaUtils;

@Component
public class BodyExtendContentHandler extends BodyContentHandler {

	private String mContentType = null;
	private String mFileId = null;
	private String mContents = null;

	private TETempXmlElementService tempXmlElementService = (TETempXmlElementService) SpringContextUtil
			.getApplicationContext().getBean("TETempXmlElementService");

	public void setContents(String mContents) {
		this.mContents = mContents;
	}

	public BodyExtendContentHandler() {
		super(new WriteOutContentHandler());
	}

	public BodyExtendContentHandler(int writeLimit) {
		super(new WriteOutContentHandler(writeLimit));
	}

	public void setContentType(String mContentType) {
		this.mContentType = mContentType;
	}

	public void saveNodeContentToDB(String id, String rootContent, String childContent) {
		if (TikaUtils.APPLICATION_XML.equals(mContentType)) {
			tempXmlElementService.saveNodeContentToDB(id, rootContent, childContent);
		}
	}

	public boolean hasItemExist(String id) {
		if (TikaUtils.APPLICATION_XML.equals(mContentType)) {
			return tempXmlElementService.hasItemExist(id);
		}

		return false;
	}

	public String getFileId() {
		return mFileId;
	}

	public void setFileId(String mFileId) {
		this.mFileId = mFileId;
	}

	public String nextXml(String fileId, int currentPageIndex, int pageSize) {
		return tempXmlElementService.nextXml(fileId, currentPageIndex, pageSize);
	}

	@Override
	public String toString() {
		if (TikaUtils.APPLICATION_XML.equals(mContentType)) {
			return mContents;
		} else {
			return super.toString();
		}
	}
}
