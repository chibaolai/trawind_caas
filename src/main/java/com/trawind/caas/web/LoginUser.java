package com.trawind.caas.web;

import java.util.List;

import com.trawind.caas.security.SysRole;

public class LoginUser {
	/** 用户编号 主键 **/
	private String userId;
	/** 姓名 **/
	private String username;
	/** 所属公司 **/
	private Long corpId;

	private String realname;
	
	private String lanCode;
	
	private List<SysRole> roleList = null;

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLanCode() {
		return lanCode;
	}

	public void setLanCode(String lanCode) {
		this.lanCode = lanCode;
	}

	public List<SysRole> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<SysRole> roleList) {
		this.roleList = roleList;
	}
	
}
