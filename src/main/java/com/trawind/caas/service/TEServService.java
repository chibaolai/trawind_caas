package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEService;
import com.trawind.caas.repository.TEServiceRepository;

@Service
public class TEServService {
	@Autowired
	private TEServiceRepository serviceRepository;
	
	public List<TEService> findServiceList() {
		return serviceRepository.findAll();
	}
}
