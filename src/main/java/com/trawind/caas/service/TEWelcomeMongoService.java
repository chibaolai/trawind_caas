package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.MWelcomeData;
import com.trawind.caas.repository.TEWelcomeMongoRepository;

@Service
public class TEWelcomeMongoService {
	@Autowired
	private TEWelcomeMongoRepository welcomeMongoRepository;

	public MWelcomeData save(MWelcomeData welcomeData) {
		return welcomeMongoRepository.save(welcomeData);
	}

	public MWelcomeData getWelcomeData(String languageCode) {
		return welcomeMongoRepository.findByLanguageCode(languageCode);
	}

}
