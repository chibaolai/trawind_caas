package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TContact;
import com.trawind.caas.repository.TContactRepository;

@Service
public class TContactService {
	@Autowired
	private TContactRepository contactRepository;

	public TContact save(TContact contact) {
		return contactRepository.save(contact);
	}
}
