package com.trawind.caas.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.common.Common;
import com.trawind.caas.entity.TEInformation;
import com.trawind.caas.repository.TEInformationRepository;
import com.trawind.caas.utils.DateUtils;
import com.trawind.caas.utils.UUID;

import net.sf.json.JSONObject;

@Service
public class TEInformationService {

	public enum JournalType {
		Add("add"), Update("update"), Delete("delete");

		private String mDbValue = Common.STRING_EMPTY;

		private JournalType(String dbValue) {
			mDbValue = dbValue;
		}

		public String getDbValue() {
			return mDbValue;
		}
	}

	@Autowired
	private TEInformationRepository informationRepository;

	public TEInformation saveFormOne(TEInformation form) {
		return informationRepository.save(form);
	}

	public TEInformation getForm(String newFormId) {
		return informationRepository.findOne(newFormId);
	}

	public List<TEInformation> getFormListByUserId(String userId) {
		return informationRepository.findByUserIdOrderByUTime(userId);
	}

	public List<TEInformation> getFormListByUserIdAndClassifyCodeAndLanDefAndCorpId(String userId, String classifyCode,
			String landef, Long corpId) {
		return informationRepository.findByUserIdAndClassifyCodeAndLanDefAndCorpIdOrderByUTimeDesc(userId, classifyCode,
				landef, corpId);
	}

	public TEInformation getOneLinkInfoFormById(String id, Long corpId) {
		return informationRepository.findByIdAndCorpIdOrderByUTime(id, corpId);
	}

	public TEInformation saveJournalNumberLog(JournalType journalType, Object dataObj) {
		JSONObject json = JSONObject.fromObject(dataObj);
		json.put("journalType", journalType.getDbValue());

		TEInformation info = new TEInformation();
		info.setId(UUID.randomUUID());
		info.setDelFlag(0);
		info.setLanDef(Common.LanguageType.English.getDbValue());
		info.setCorpId(Long.valueOf(json.getString("corpid").toString()));
		String now = DateUtils.format(new Date(), Common.DATE_FORMAT_YYYYMMDDHHMMSS_SSS);
		info.setUserId(json.getString("userId"));
		info.setDelFlag(0);
		info.setcTime(now);
		info.setcUser(json.getString("userId"));
		info.setuTime(now);
		info.setuUser(json.getString("userId"));
		info.setClassifyCode("syslog");
		info.setContent(json.toString());
		info.setName(json.getString("name"));

		return saveFormOne(info);
	}
}
