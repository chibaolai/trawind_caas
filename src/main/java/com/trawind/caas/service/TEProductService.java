package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEProduct;
import com.trawind.caas.repository.TEProductRepository;

@Service
public class TEProductService {
	@Autowired
	private TEProductRepository productRepository;

	public List<TEProduct> getProductListByCorpid(Long corpid) {
		return productRepository.findByCorpidOrderByUTimeDesc(corpid);
	}

	public TEProduct getProductById(String id) {
		return productRepository.findById(id);
	}

	public TEProduct findOne(String id) {
		return productRepository.findById(id);
	}

	public TEProduct saveProduct(TEProduct product) {
		return productRepository.save(product);
	}

	public List<TEProduct> getProductList() {
		Sort uTimeSort = new Sort(Sort.Direction.DESC, "uTime");
		return productRepository.findAll(uTimeSort);
	}

	// public TEProduct getProductByPrimaryId(String primaryId) {
	// return productRepository.findByPrimaryId(primaryId);
	// }
}
