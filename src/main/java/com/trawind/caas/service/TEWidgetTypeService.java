package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEWidgetType;
import com.trawind.caas.repository.TEWidgetTypeRepository;

@Service
public class TEWidgetTypeService {
	@Autowired
	private TEWidgetTypeRepository widgetTypeRepository;
	
	public List<TEWidgetType> getWidgetTypes() {
		return widgetTypeRepository.findAll();
	}
}
