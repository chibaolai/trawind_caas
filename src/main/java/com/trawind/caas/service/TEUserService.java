package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.repository.TEUserRepository;
import com.trawind.caas.security.SysUser;

@Service
public class TEUserService {
	@Autowired
	private TEUserRepository userRepository;

	public SysUser saveSysUser(SysUser user) {
		return userRepository.save(user);
	}
	public SysUser findByCorpid(Long corpid){
		return userRepository.findByCorpid(corpid);
	}
	public SysUser findByUsername(String username){
		return userRepository.findByUsername(username);
	}
	public SysUser findByPassword(String password){
		return userRepository.findByPassword(password);
	}
}
