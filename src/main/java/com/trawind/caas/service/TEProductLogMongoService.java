package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.common.Constant;
import com.trawind.caas.entity.MProductDataLog;
import com.trawind.caas.repository.TEProductLogMongoRepository;
import com.trawind.caas.utils.ConvertFactory;
import com.trawind.caas.utils.DozerConvertFactory;

@Service
public class TEProductLogMongoService {
	@Autowired
	private TEProductLogMongoRepository productLogmongoRepository;

	public MProductDataLog saveProductDataLog(MProductDataLog productDataLog) {
		return productLogmongoRepository.save(productDataLog);
	}

	/**
	 * 获取产品log
	 * 
	 * @param productId
	 * @return
	 */
	public List<MProductDataLog> getAllProductData(String productId, Long corpId) {
		return productLogmongoRepository.findListByProductIdAndCorpIdOrderByDatetimeDesc(productId, corpId);
	}

	/**
	 * 获取最新产品log
	 * 
	 * @param productId
	 * @return
	 */
	public MProductDataLog getLatestProductData(String productId, Long corpId) {
		return productLogmongoRepository.findLatestByProductIdAndCorpIdOrderByDatetimeDesc(productId, corpId);
	}

	/**
	 * 通过id获取产品变更履历
	 * 
	 * @param productDataLogId
	 * @return
	 */
	public MProductDataLog getMProductDataLog(String productDataLogId) {
		return productLogmongoRepository.findOne(productDataLogId);
	}

	/**
	 * 发布修改的数据log给合作伙伴
	 * 
	 * @param widget
	 * @param corpId
	 * @return
	 */
	public MProductDataLog sendUpdateDataLogToParnter(MProductDataLog productDataLog, Long corpId) {
		ConvertFactory cf = new DozerConvertFactory();
		MProductDataLog cloneProductDataLog = cf.convert(productDataLog, MProductDataLog.class);
		cloneProductDataLog.setId(null);
		cloneProductDataLog.setCorpId(corpId);
		cloneProductDataLog.setMode(Constant.mode_parnter);
		productLogmongoRepository.save(cloneProductDataLog);
		return cloneProductDataLog;
	}
}
