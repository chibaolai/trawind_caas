package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEChartData;
import com.trawind.caas.repository.TEChartDataRepository;

@Service
public class TEChartDataService {
	@Autowired
	TEChartDataRepository chartDataRepository;

	public List<TEChartData> getChartData(Integer styleId, Integer typeId, String userId, Long corpId) {
		return chartDataRepository.findByStyleIdAndTypeIdAndUserIdAndCorpId(styleId, typeId, userId, corpId);
	}
}
