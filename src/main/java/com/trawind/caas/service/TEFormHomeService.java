package com.trawind.caas.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEFormHome;
import com.trawind.caas.repository.TEFormHomeRepository;

@Service
public class TEFormHomeService {
	public static final String NAME_HOMEPAGE = "homePage";
	public static final String CODE_SHARE = "share";
	public static final String CODE_TRAWIND = "Trawind";

	@Autowired
	private TEFormHomeRepository formHomeRepository;

	public TEFormHome saveFormOne(TEFormHome form) {
		return formHomeRepository.save(form);
	}

	public TEFormHome getForm(String newFormId) {
		return formHomeRepository.findById(newFormId);
	}

	public List<TEFormHome> getFormListByUserId(String userId) {
		return formHomeRepository.findByUserIdOrderByUTime(userId);
	}

	public List<TEFormHome> getFormListByCorpidAndUserIdAndName(Long corpid, String userId) {
		return formHomeRepository.findByCorpidAndUserIdOrderByUTime(corpid, userId);
	}

	public TEFormHome getDefaultForm() {
		List<TEFormHome> list = formHomeRepository.findByCorpidAndUserIdOrderByUTime(0l, StringUtils.EMPTY);

		if (list.isEmpty()) {
			return null;
		}

		return list.get(0);
	}
}
