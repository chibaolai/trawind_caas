package com.trawind.caas.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TIssuedRelations;
import com.trawind.caas.repository.TIssuedRelationsRepository;

@Service
public class TIssuedRelationsService {
	@Autowired
	private TIssuedRelationsRepository issuedRelationsRepository;

	public List<TIssuedRelations> getSelectedDistributor(String productId, Long manufacturerId) {
		return issuedRelationsRepository.findByProductIdAndManufacturerId(productId, manufacturerId);
	}

	public List<TIssuedRelations> saveList(Iterable<TIssuedRelations> entities) {
		return issuedRelationsRepository.save(entities);
	}
	
	@Transactional
	public void deleteByProductIdAndManufacturerId(String productId, Long manufacturerId) {
		issuedRelationsRepository.deleteByProductIdAndManufacturerId(productId, manufacturerId);
	}
}
