package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.common.Common;
import com.trawind.caas.entity.TEConfig;
import com.trawind.caas.repository.TEConfigRepository;
import com.trawind.caas.utils.UUID;

import net.sf.json.JSONArray;

@Service
public class TEConfigService {
	@Autowired
	private TEConfigRepository configRepository;

	public TEConfig getLanguageCode(String key, String userId, Long corpid) {
		List<TEConfig> langList = configRepository.findByKeyAndUserIdAndCorpId(key, userId, corpid);
		if (langList.isEmpty()) {
			TEConfig lang = new TEConfig();
			lang.setId(UUID.randomUUID());
			lang.setKey(key);
			lang.setUserId(userId);
			lang.setCorpId(corpid);
			lang.setValue(Common.LanguageType.English.getDbValue());

			return setLanguageCode(lang);
		}

		return langList.get(0);
	}

	public TEConfig setLanguageCode(TEConfig config) {
		return configRepository.save(config);
	}

	public TEConfig getCommandCode(String key, Long corpid) {
		List<TEConfig> langList = configRepository.findByKeyAndCorpId(key, corpid);
		if (langList.isEmpty()) {
			TEConfig lang = new TEConfig();
			lang.setId(UUID.randomUUID());
			lang.setKey(key);
			lang.setUserId(null);
			lang.setCorpId(corpid);
			lang.setValue((new JSONArray()).toString());

			return setCommandCode(lang);
		}

		return langList.get(0);
	}

	public TEConfig setCommandCode(TEConfig config) {
		return configRepository.save(config);
	}
}
