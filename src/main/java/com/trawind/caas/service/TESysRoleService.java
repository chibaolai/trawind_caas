package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.repository.TESysRoleRepository;
import com.trawind.caas.security.SysRole;

@Service
public class TESysRoleService {
	@Autowired
	private TESysRoleRepository sysRoleRepository;

	public SysRole findSysRoleByName(String roleName) {
		return sysRoleRepository.findByName(roleName);
	}
}
