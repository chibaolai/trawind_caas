package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEIssuedRequest;
import com.trawind.caas.repository.TEIssuedRequestRepository;

@Service
public class TEIssuedRequestService {
	@Autowired
	private TEIssuedRequestRepository issuedRequestRepository;

	public TEIssuedRequest save(TEIssuedRequest issuedRequest) {
		return issuedRequestRepository.save(issuedRequest);
	}

	public List<TEIssuedRequest> save(List<TEIssuedRequest> issuedRequestList) {
		return issuedRequestRepository.save(issuedRequestList);
	}

	public List<TEIssuedRequest> getAllIssuedInfo(Long corpId, int state, String productId, String productLogId) {
		return issuedRequestRepository.findByToCorpIdAndStateAndProductIdAndProductLogId(corpId, state, productId,
				productLogId);
	}

	public List<TEIssuedRequest> getAllIssuedInfo(Long corpId, int state) {
		return issuedRequestRepository.findByToCorpIdAndState(corpId, state);
	}
}
