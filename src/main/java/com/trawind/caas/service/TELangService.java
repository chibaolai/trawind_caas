package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.trawind.caas.entity.TELang;
import com.trawind.caas.repository.TELangRepository;

@Service
public class TELangService {
	@Autowired
	private TELangRepository langRepository;
	
	public List<TELang> getLangList() {
		return langRepository.findAll();
	}
}
