package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TRCategory;
import com.trawind.caas.repository.TRCategoryRepository;

import net.sf.json.JSONObject;

@Service
public class TRCategoryService {
	@Autowired
	private TRCategoryRepository categoryRepository;

	public JSONObject getAll() {
		List<TRCategory> categoryList = categoryRepository.findAll();

		StringBuffer sb = new StringBuffer();
		sb.append("[");
		sb.append("    {");
		sb.append("        \"item\": \"服装\",");
		sb.append("        \"id\": 5,");
		sb.append("        \"data\": [");
		sb.append("            {");
		sb.append("                \"item\": \"男装\",");
		sb.append("                \"id\": 18,");
		sb.append("                \"data\": [");
		sb.append("                    {");
		sb.append("                        \"item\": \"T恤\",");
		sb.append("                        \"id\": 22,");
		sb.append("                        \"data\": []");
		sb.append("                    },");
		sb.append("                    {");
		sb.append("                        \"item\": \"西服\",");
		sb.append("                        \"id\": 21,");
		sb.append("                        \"data\": []");
		sb.append("                    }");
		sb.append("                ]");
		sb.append("            },");
		sb.append("            {");
		sb.append("                \"item\": \"女装\",");
		sb.append("                \"id\": 19,");
		sb.append("                \"data\": [");
		sb.append("                    {");
		sb.append("                        \"item\": \"短裙\",");
		sb.append("                        \"id\": 23,");
		sb.append("                        \"data\": []");
		sb.append("                    }");
		sb.append("                ]");
		sb.append("            }");
		sb.append("        ]");
		sb.append("    }");
		sb.append("]");
		String jsonStr = sb.toString();

		return JSONObject.fromObject(jsonStr);
	}
}
