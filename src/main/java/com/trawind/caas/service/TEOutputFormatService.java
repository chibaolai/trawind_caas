package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEOutputFormat;
import com.trawind.caas.repository.TEOutputFormatRepository;

@Service
public class TEOutputFormatService {

	@Autowired
	TEOutputFormatRepository outputFormatRepository;
	
	public String getOutputFormat(Long corpId) {
		String retOutputFormat = null;
		TEOutputFormat outputFormat = outputFormatRepository.findByCorpId(corpId);
		if(outputFormat != null) {
			retOutputFormat = outputFormat.getOutputFormat();
		}
		return retOutputFormat;
	}
}
