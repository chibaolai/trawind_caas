package com.trawind.caas.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Properties;

import javax.activation.MimetypesFileTypeMap;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.trawind.caas.config.MongoDBSettings;
import com.trawind.caas.xml.util.XmlUtil;

/**
 * 控件上传 img 保存到mongo
 * @author chibaolai
 *
 */
@Service
public class MGridFSImgService implements InitializingBean{
	@Autowired
	private MongoDbFactory mongoDbFactory;
	@Autowired
	private MongoDBSettings mongoDBSettings;
	
	private DB _db;
	
	private String _fsName;
	
	/**
	 * 保存文件（其中FS_NAME相当于gridfs文件系统的表名，可以有多个）
	 * @param file
	 * @param fileName
	 * @param contentType
	 * @param metaData
	 * @return
	 */
	public String saveProductImg(MultipartFile file, String fileName, String contentType, DBObject metaData) {
	    GridFSInputFile gfsInput;    
	    try {       
	      gfsInput = new GridFS(_db, _fsName).createFile(file.getInputStream());    
	      gfsInput.setFilename(fileName);    
	      gfsInput.setContentType(contentType);    
	      gfsInput.setMetaData(metaData);    
	      gfsInput.save();    
	      return gfsInput.getId().toString();
	    } catch (IOException e) {    
	    	
	    }    
	    return null; 
	}
	
	/**
	 * 通过文件名读取文件
	 * @param fileName
	 * @return
	 */
	public GridFSDBFile findFileByName(String fileName){    
	    GridFSDBFile gfsFile ;  
	    try {          
	      gfsFile = new GridFS(_db, _fsName).findOne(fileName);    
	    } catch (Exception e) {    
	      return null;    
	    }    
	    return gfsFile;    
	  } 
	
	/**
	 * 通过id读取文件
	 * @param id
	 * @return
	 */
	public GridFSDBFile findFileById(String id){    
	    GridFSDBFile gfsFile ;    
	    try {          
	      gfsFile = new GridFS(_db, _fsName).find(new ObjectId(id));    
	    } catch (Exception e) {    
	      return null;    
	    }    
	    return gfsFile;    
	  }
	
	/**
	 * 输出文件到OutputStream
	 * @param out
	 * @param gfsFile
	 * @return
	 */
	private boolean writeToOutputStream(OutputStream out, GridFSDBFile gfsFile) {    
	    try {         
	      gfsFile.writeTo(out);    
	    } catch (IOException e) {    
	      return false;    
	    }    
	    return true;    
	  } 
	
	@Override
	public void afterPropertiesSet() throws Exception {
		_db = mongoDbFactory.getDb(mongoDBSettings.getDbName());
		_fsName = mongoDBSettings.getFsNameImg();
	}
}
