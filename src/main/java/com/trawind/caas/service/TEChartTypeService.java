package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEChartType;
import com.trawind.caas.repository.TEChartTypeRepository;

@Service
public class TEChartTypeService {

	@Autowired
	TEChartTypeRepository chartTypeRepository;

	public TEChartType getChartType(Integer styleId, Integer typeId, String userId, Long corpId) {
		return chartTypeRepository.findByStyleIdAndTypeIdAndUserIdAndCorpId(styleId, typeId, userId, corpId);
	}

	public List<TEChartType> getChartTypeList(String userId, Long corpId) {
		return chartTypeRepository.findByUserIdAndCorpId(userId, corpId);
	}
}
