package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.MProfileData;
import com.trawind.caas.repository.TEProfileMongoRepository;

@Service
public class TEProfileMongoService {
	
	@Autowired
	private TEProfileMongoRepository profileMongoRepository;
	
	public void saveProfile(MProfileData profile){
		profileMongoRepository.save(profile);
	}
	
	public MProfileData findByUserId(String userId){
		List<MProfileData> profiles = profileMongoRepository.findByUserId(userId);
		if (profiles.size() > 0) {
			return profiles.get(0);
		}
		return null;
	}
}
