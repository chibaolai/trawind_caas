package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.MCorporationData;
import com.trawind.caas.repository.TECorporationMongoRepository;

@Service
public class TECorporationMongoService {
	
	@Autowired
	private TECorporationMongoRepository corporationMongoRepository;
	
	public void saveCorporation(MCorporationData data){
		corporationMongoRepository.save(data);
	}
	
	public MCorporationData findByCorporationId(String corId) {
		List<MCorporationData> corps = corporationMongoRepository.findByCorporationId(corId);
		if (corps.size() > 0) {
			return corps.get(0);
		}
		return null;
	}
}
