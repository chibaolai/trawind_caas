package com.trawind.caas.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.drools.core.spi.Activation;
import org.kie.api.KieBase;
import org.kie.api.builder.Message;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.ObjectFilter;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.runtime.rule.StatefulRuleSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TERules;

@Service
public class CAAS24Service {
    @Autowired
    private KieBase kbase;
//	private final DroolsDemoRepository fraudRepository;
	
//  @Autowired
//  public DroolsService(KieBase kbase, DroolsDemoRepository fraudRepository) {
//      this.kbase = kbase;
//      this.fraudRepository = fraudRepository;
//  }

	/* (non-Javadoc)
	 * @see com.drools.demo.point.PointRuleEngine#executeRuleEngine(com.drools.demo.point.PointDomain)
	 */
	public TERules executeRuleEngine(TERules rule) {
//		if(null == kbase.getKiePackages() || 0 == kbase.getKiePackages().size()) {
//			return null;
//		}
		
		KieSession statefulSession = kbase.newKieSession();
		statefulSession.insert(rule);
//		statefulSession.insert(attrDomainPrimary);
//		statefulSession.insert(attrDomainAssociate);
//		// fire
//		statefulSession.fireAllRules(new AgendaFilter() {
//			public boolean accept(Activation activation) {
//				return !activation.getRule().getName().contains("_test");
//			}
//		});
		statefulSession.fireAllRules();
		statefulSession.dispose();
		return rule;
	}
}
