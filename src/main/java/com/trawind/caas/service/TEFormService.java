package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEForm;
import com.trawind.caas.repository.TEFormRepository;
import com.trawind.caas.service.TEInformationService.JournalType;

@Service
public class TEFormService {

	@Autowired
	private TEFormRepository formRepository;

	@Autowired
	private TEInformationService informationService;

	public TEForm saveFormOne(TEForm form, boolean isNewFlag) {
		TEForm savedForm = formRepository.save(form);
		if (savedForm != null) {
			if (isNewFlag) {
				informationService.saveJournalNumberLog(JournalType.Add, form);
			} else {
				informationService.saveJournalNumberLog(JournalType.Update, form);
			}
		}
		return savedForm;
	}

	public TEForm getForm(Long corpid, String newFormId) {
		return formRepository.findOneByCorpidAndId(corpid, newFormId);
	}

	public List<TEForm> getFormListByUserId(String userId) {
		return formRepository.findByUserIdOrderByUTimeDesc(userId);
	}

	public List<TEForm> getFormListByUserIdAndName(String userId, String name) {
		return formRepository.findByUserIdAndNameOrderByUTime(userId, name);
	}

	public List<TEForm> getFormListByCorpid(Long corpid) {
		return formRepository.findByCorpidOrderByUTimeDesc(corpid);
	}

	public List<TEForm> getFormListByCategoryId(String categoryId) {
		return formRepository.findByCategoryIdOrderByUTimeDesc(categoryId);
	}

	public List<TEForm> getFormListByCategoryIdNotNull() {
		return formRepository.findByCategoryIdNotNullOrderByUTimeDesc();
	}

	public TEForm getForm(String newFormId, String categoryId) {
		return formRepository.findOneByIdAndCategoryId(newFormId, categoryId);
	}

	public TEForm getFormById(String formId) {
		return formRepository.findOneById(formId);
	}

	public TEForm getForm(String categoryId) {
		List<TEForm> formList = formRepository.findOneByCategoryIdOrderByUTimeDesc(categoryId);
		if (formList.isEmpty()) {
			return null;
		}
		return formList.get(0);
	}

	/**
	 * 获取最新表单
	 * 
	 * @param corpid
	 * @return
	 */
	public TEForm getLastFormByCorpid(Long corpid) {
		List<TEForm> formList = formRepository.findByCorpidOrderByUTimeDesc(corpid);
		if (formList != null && formList.size() > 0) {
			return formList.get(0);
		}
		return null;
	}
	
	public TEForm geTeFormsByFormType(String formType) {
		TEForm form = new TEForm();
		List<TEForm> formList = formRepository.findByFormTypeOrderByCTimeDesc(formType);
		if (formList != null && formList.size() > 0) {
			form = formList.get(0);
		}
		return form;
	}
}
