package com.trawind.caas.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TEImgCategory;
//import com.trawind.caas.repository.TEAttrWidgetRepository;
import com.trawind.caas.repository.TEAttrsRepository;
import com.trawind.caas.repository.TEImgCategoryRepository;

@Service
public class TEAttrsService {
	@Autowired
	private TEAttrsRepository attrsRepository;
//	@Autowired
//	private TEAttrWidgetRepository attrWidgetRepository;
	@Autowired
	private TEImgCategoryRepository imgCategoryRepository;
	public List<TEAttr> getAttrListByCorpCode(String corpCode) {
		List<TEAttr> attrList = attrsRepository.findByCorpCode(corpCode);
		if(attrList != null) {
			for(TEAttr attr:attrList) {
				if(null != attr.getImgCategoryId()) {
					TEImgCategory imgCategory = imgCategoryRepository.findById(attr.getImgCategoryId());
					if(imgCategory != null) {
						String attrsIds = imgCategory.getInnerAttrsId();
						if(!StringUtils.isEmpty(attrsIds)) {
							List<TEAttr> innerAttrList = new ArrayList<TEAttr>();
							String[] attrIdArray = attrsIds.split(",");
							for(String attrId:attrIdArray) {
								TEAttr innerAttr = attrsRepository.findById(attrId);
								if(innerAttr != null) 
								innerAttrList.add(innerAttr);
							}
							imgCategory.setInnerAttrs(innerAttrList);
						}
						attr.setImgCategory(imgCategory);
					}
				}
			}
		}
		return attrList;
	}
	public TEAttr getAttrById(String id) {
		return attrsRepository.findById(id);
	}
	public TEAttr saveAttr(TEAttr attr) {
		return attrsRepository.save(attr);
	}
	
	public List<TEAttr> findBaseAttrList() {
		return attrsRepository.findByBaseFlag(1);
	}
	
	public List<TEAttr> findAllAttrList() {
		return attrsRepository.findAll();
	}
}
