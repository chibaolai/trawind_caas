package com.trawind.caas.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TEAttr;
import com.trawind.caas.entity.TEImgCategory;
import com.trawind.caas.repository.TEAttrsRepository;
import com.trawind.caas.repository.TEImgCategoryRepository;

@Service
public class TEImgCategoryService {

	@Autowired
	private TEImgCategoryRepository categoryRepository;
	@Autowired
	private TEAttrsRepository attrsRepository;
	
	public List<TEImgCategory> getImgCategoryList() {
		List<TEImgCategory> imgCategoryList = categoryRepository.findAll();
		if(imgCategoryList != null) {
			for(TEImgCategory imgCategory:imgCategoryList) {
				String attrsIds = imgCategory.getInnerAttrsId();
				if(!StringUtils.isEmpty(attrsIds)) {
					List<TEAttr> innerAttrList = new ArrayList<TEAttr>();
					String[] attrIdArray = attrsIds.split(",");
					for(String attrId:attrIdArray) {
						TEAttr innerAttr = attrsRepository.findById(attrId);
						if(innerAttr != null) 
						innerAttrList.add(innerAttr);
					}
					imgCategory.setInnerAttrs(innerAttrList);
				}
			}
		}
		return imgCategoryList;
	}
}
