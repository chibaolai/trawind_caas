package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TETrade;
import com.trawind.caas.repository.TETradeRepository;

@Service
public class TETradeService {
	@Autowired
	private TETradeRepository tradeRepository;

	public TETrade saveTETrade(TETrade trade) {
		return tradeRepository.save(trade);
	}
	public TETrade findByOutTradeNo(String tradeNo){
		return tradeRepository.findByOutTradeNo(tradeNo);
	}
}
