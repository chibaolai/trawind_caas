package com.trawind.caas.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Service;

import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.trawind.caas.entity.MProductData;
import com.trawind.caas.entity.MProductDataLog;
import com.trawind.caas.entity.MWidget;
import com.trawind.caas.repository.TEProductMongoRepository;
import com.trawind.caas.utils.ConvertFactory;
import com.trawind.caas.utils.DozerConvertFactory;
import com.trawind.caas.utils.SpringContextUtil;

import net.sf.json.JSONObject;

@Service
public class TEFormMongoService implements ApplicationContextAware{
	@Autowired
	private TEProductMongoRepository mongoRepository;
	
	public MProductData saveProductData(MProductData productData) {
		return mongoRepository.save(productData);
	}
	
	/**
	 * 获取表单数据
	 * @param productId
	 * @return
	 */
	public MProductData getProductData(String productId,Long corpId) {
		return mongoRepository.findByProductIdAndCorpId(productId,corpId);
	}
	
	/**
	 * 发布初始数据给合作伙伴
	 * @param widget
	 * @param corpId
	 * @return
	 */
	public MProductData sendInitDataToParnter(MProductData productData,Long corpId) {
		ConvertFactory cf = new DozerConvertFactory();
		MProductData cloneProductData = cf.convert(productData, MProductData.class);
		cloneProductData.setCorpId(corpId);
		mongoRepository.save(cloneProductData);
		return cloneProductData;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContextUtil.setAppCtx(applicationContext);
	}
	
}
