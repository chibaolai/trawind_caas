package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TECompHome;
import com.trawind.caas.repository.TECompHomeRepository;

@Service
public class TECompHomeService {
	public enum GroupType {
		Layout, CustomComponent;
	}

	@Autowired
	private TECompHomeRepository compHomeRepository;

	/**
	 * 取得所有组件
	 * 
	 * @return
	 */
	public List<TECompHome> getList() {
		return compHomeRepository.findAll();
	}

	/**
	 * 保存组件到DB
	 * 
	 * @param compHome
	 *            组件信息
	 * @return
	 */
	public TECompHome saveTECompHome(TECompHome compHome) {
		return compHomeRepository.save(compHome);
	}

	public List<TECompHome> saveTECompHomeList(List<TECompHome> compHomeList) {
		return compHomeRepository.save(compHomeList);
	}

	/**
	 * 取得所有Layout组件
	 * 
	 * @return
	 */
	public List<TECompHome> getLayoutCompList(Long corpId) {
		return compHomeRepository.findByCompGroupAndCorpIdOrderBySort(GroupType.Layout.toString(),corpId);
	}

	/**
	 * 取得所有自定义组件
	 * 
	 * @return
	 */
	public List<TECompHome> getCustomCompList(Long corpId) {
		return compHomeRepository.findByCompGroupAndCorpIdOrderBySort(GroupType.CustomComponent.toString(),corpId);
	}
}
