package com.trawind.caas.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.trawind.caas.common.Common;
import com.trawind.caas.common.Constant;
import com.trawind.caas.entity.TETempXmlElement;
import com.trawind.caas.repository.TETempXmlElementRepository;
import com.trawind.caas.utils.SpringContextUtil;

@Service
public class TETempXmlElementService implements ApplicationContextAware {
	@Autowired
	private EntityManager entityManager;

	@Autowired
	private TETempXmlElementRepository tempXmlElementRepository;

	public void saveNodeContentToDB(String id, String rootContent, String childContent) {
		TETempXmlElement entity = new TETempXmlElement();
		entity.setSeqNo(null);
		entity.setId(id);
		entity.setRootContent(rootContent);
		entity.setChildContent(childContent);
		entity.setTime(StringUtils.leftPad(String.valueOf((new Date()).getTime()), 36, '0'));

		tempXmlElementRepository.save(entity);
	}

	public boolean hasItemExist(String id) {
		List<TETempXmlElement> elementList = tempXmlElementRepository.findById(id);
		return !elementList.isEmpty();
	}

	public String nextXml(String fileId, int currentPageIndex, int pageSize) {

		Query query = entityManager.createNativeQuery("SELECT count(*) FROM t_e_temp_xml_element WHERE ID = :id ");
		query.setParameter("id", fileId);
		Object o = query.getSingleResult();

		if (Long.valueOf(String.valueOf(o)) <= currentPageIndex * pageSize) {
			return StringUtils.EMPTY;
		}

		query = entityManager.createNativeQuery(
				"SELECT * FROM t_e_temp_xml_element WHERE ID = :id ORDER BY \"time\" ASC", TETempXmlElement.class);
		query.setFirstResult(currentPageIndex * pageSize);
		query.setMaxResults(pageSize);
		query.setParameter("id", fileId);
		@SuppressWarnings("unchecked")
		List<TETempXmlElement> result = query.getResultList();

		Document outputDoc = null;
		Document elementDoc = null;
		StringBuffer outXmlStr = new StringBuffer(Constant.XML_HEADER);
		StringBuffer elementXmlStr = null;
		boolean isSetRoot = false;
		Element parent = null;
		for (TETempXmlElement item : result) {
			if (!isSetRoot && !Common.isNullOrEmpty(item.getRootContent())) {
				try {
					isSetRoot = true;
					outXmlStr.append(item.getRootContent());
					outputDoc = DocumentHelper.parseText(outXmlStr.toString());
				} catch (Exception e) {
					isSetRoot = false;
					outputDoc = null;
				}
			}

			if (outputDoc != null) {
				elementXmlStr = new StringBuffer(Constant.XML_HEADER);
				elementXmlStr.append(item.getChildContent());

				try {
					elementDoc = DocumentHelper.parseText(elementXmlStr.toString());
					parent = (Element) outputDoc.getRootElement();
					parent.add(elementDoc.getDocument().getRootElement());
				} catch (Exception e) {
					elementXmlStr = new StringBuffer(Constant.XML_HEADER);
				}
			}
		}

		if (!result.isEmpty() && outputDoc != null) {
			parent = (Element) outputDoc.getRootElement();
			Element pageInfo = parent.addElement("pageInfo");
			pageInfo.addAttribute("fileId", fileId);
			pageInfo.addAttribute("totalPage", String.valueOf(o));
			pageInfo.addAttribute("currentPage", String.valueOf(currentPageIndex + 1));
			pageInfo.addAttribute("pageSize", String.valueOf(pageSize));

			return outputDoc.asXML();
		}

		return new StringBuffer(Constant.XML_HEADER).toString();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		SpringContextUtil.setAppCtx(applicationContext);
	}
}
