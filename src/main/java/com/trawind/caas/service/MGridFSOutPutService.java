package com.trawind.caas.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;

import javax.activation.MimetypesFileTypeMap;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.trawind.caas.config.MongoDBSettings;
import com.trawind.caas.xml.util.XmlUtil;

/**
 * outPut document file
 * @author chibaolai
 *
 */
@Service
public class MGridFSOutPutService implements InitializingBean{
	@Autowired
	private MongoDbFactory mongoDbFactory;
	@Autowired
	private MongoDBSettings mongoDBSettings;
	
	private DB _db;
	
	private String _fsName;
	
	/**
	 * 保存xml格式文件
	 * @return
	 */
	public String saveXmlFormatDocument(File file) {
	    GridFSInputFile gfsInput;    
	    try {       
	      gfsInput = new GridFS(_db, _fsName).createFile(new FileInputStream(file));    
	      gfsInput.setFilename(file.getName());  
	      String contentType = new MimetypesFileTypeMap().getContentType(file);
	      gfsInput.setContentType(contentType);   
		  DBObject metaData = new BasicDBObject();
		  metaData.put("datasource", "this is a Xml Format File");
	      gfsInput.setMetaData(metaData);    
	      gfsInput.save();    
	      return gfsInput.getId().toString();
	    } catch (IOException e) {    
	    	
	    }    
		return null;
	}
	
	/**
	 * 保存xml格式文件
	 * @return
	 */
	public String saveOrUpdateXmlFormatDocument(File file) {
		GridFS myGridFS = new GridFS(_db, _fsName);
		List<GridFSDBFile> exitFiles = myGridFS.find(file.getName());
		if(exitFiles != null) {
			DBObject query  = new BasicDBObject("_id", exitFiles.get(0).getId());
			GridFSDBFile gridFSDBFile = myGridFS.findOne(query);
			if(gridFSDBFile != null) {
				myGridFS.remove(query);
			}
		}
		saveXmlFormatDocument(file);
		return null;
	}
	
	/**
	 * 通过文件名读取文件
	 * @param fileName
	 * @return
	 */
	public GridFSDBFile findFileByName(String fileName){    
	    GridFSDBFile gfsFile ;  
	    try {          
	      gfsFile = new GridFS(_db, _fsName).findOne(fileName);    
	    } catch (Exception e) {    
	      return null;    
	    }    
	    return gfsFile;    
	  } 
	
	/**
	 * 通过id读取文件
	 * @param id
	 * @return
	 */
	public GridFSDBFile findFileById(String id){    
	    GridFSDBFile gfsFile ;    
	    try {          
	      gfsFile = new GridFS(_db, _fsName).find(new ObjectId(id));    
	    } catch (Exception e) {    
	      return null;    
	    }    
	    return gfsFile;    
	  }
	
	/**
	 * 输出文件到OutputStream
	 * @param out
	 * @param gfsFile
	 * @return
	 */
	public boolean writeToOutputStream(OutputStream out, GridFSDBFile gfsFile) {    
	    try {         
	      gfsFile.writeTo(out);    
	    } catch (IOException e) {    
	      return false;    
	    }    
	    return true;    
	  }
	
	/**
	 * 输出文件到File
	 * @param out
	 * @param gfsFile
	 * @return
	 */
	public boolean writeToFile(File file, GridFSDBFile gfsFile) {    
	    try {         
	      gfsFile.writeTo(file);    
	    } catch (IOException e) {    
	      return false;    
	    }    
	    return true;    
	  }

	@Override
	public void afterPropertiesSet() throws Exception {
		_db = mongoDbFactory.getDb(mongoDBSettings.getDbName());
		_fsName = mongoDBSettings.getFsNameOutput();
	}
}
