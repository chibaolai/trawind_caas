package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TECorp;
import com.trawind.caas.repository.TECorpRepository;

@Service
public class TECorpService {
	@Autowired
	private TECorpRepository corpRepository;

	public TECorp saveTECorp(TECorp corp) {
		return corpRepository.save(corp);
	}

	public TECorp findByName(String companyname) {
		return corpRepository.findByName(companyname);
	}

	public List<TECorp> getAllOutOwn(Long companyId) {
		return corpRepository.findByIdNotAndHasDistributorRoleTrue(companyId);
	}
	public List<TECorp> getAllCorpInfo() {
		return corpRepository.findByHasDistributorRoleTrue();
	}
}
