package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TETemp;
import com.trawind.caas.repository.TETempRepository;

@Service
public class TETempService {
	@Autowired
	private TETempRepository tempRepository;
	
	public List<TETemp> getList() {
		return tempRepository.findAllByOrderByNoDesc();
	}
	
	public TETemp saveTETemp(TETemp temp) {
		return tempRepository.save(temp);
	}
}
