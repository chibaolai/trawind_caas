package com.trawind.caas.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.lang.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;
import com.trawind.caas.config.MongoDBSettings;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Mapping document file
 * 
 * @author chibaolai
 *
 */
@Service
public class MGridFSMappingService implements InitializingBean{
	@Autowired
	private MongoDbFactory mongoDbFactory;
	@Autowired
	private MongoDBSettings mongoDBSettings;

	private DB _db;
	
	private String _fsName;
	
	/**
	 * 保存xml配置文件
	 * 
	 * @return
	 */
	public String saveConfigDocument(File file) {
		GridFSInputFile gfsInput;
		try {
			gfsInput = new GridFS(_db, _fsName).createFile(new FileInputStream(file));
			gfsInput.setFilename(file.getName());
			String contentType = new MimetypesFileTypeMap().getContentType(file);
			gfsInput.setContentType(contentType);
			DBObject metaData = new BasicDBObject();
			metaData.put("datasource", "this is a Mapping Xml");
			gfsInput.setMetaData(metaData);
			gfsInput.save();
			return gfsInput.getId().toString();
		} catch (IOException e) {

		}
		return null;
	}

	/**
	 * 通过文件名读取文件
	 * 
	 * @param fileName
	 * @return
	 */
	public GridFSDBFile findFileByName(String fileName) {
		GridFSDBFile gfsFile;
		try {
			gfsFile = new GridFS(_db, _fsName).findOne(fileName);
		} catch (Exception e) {
			return null;
		}
		return gfsFile;
	}

	/**
	 * 通过id读取文件
	 * 
	 * @param id
	 * @return
	 */
	public GridFSDBFile findFileById(String id) {
		GridFSDBFile gfsFile;
		try {
			gfsFile = new GridFS(_db, _fsName).find(new ObjectId(id));
		} catch (Exception e) {
			return null;
		}
		return gfsFile;
	}

	/**
	 * 输出文件到OutputStream
	 * 
	 * @param out
	 * @param gfsFile
	 * @return
	 */
	public boolean writeToOutputStream(OutputStream out, GridFSDBFile gfsFile) {
		try {
			gfsFile.writeTo(out);
		} catch (IOException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 输出文件到File
	 * @param out
	 * @param gfsFile
	 * @return
	 */
	public boolean writeToFile(File file, GridFSDBFile gfsFile) {    
	    try {         
	      gfsFile.writeTo(file);    
	    } catch (IOException e) {    
	      return false;    
	    }    
	    return true;    
	  }

	/**
	 * 取得文件列表
	 * 
	 * @return
	 */
	public JSONArray getFileList() {
		DBCursor dbCursor = null;
		JSONArray arr = new JSONArray();
		try {
			dbCursor = new GridFS(_db, _fsName).getFileList();

			while (dbCursor.hasNext()) {
				DBObject obj = dbCursor.next();
				arr.add(JSONObject.fromObject(obj.toString()));
			}

		} catch (Exception e) {
			return new JSONArray();
		} finally {
			if (dbCursor != null) {
				dbCursor.close();
			}
		}

		return arr;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		_db = mongoDbFactory.getDb(mongoDBSettings.getDbName());
		_fsName = mongoDBSettings.getFsNameMapping();
	}
}
