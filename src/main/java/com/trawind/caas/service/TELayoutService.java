package com.trawind.caas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TELayout;
import com.trawind.caas.repository.TELayoutRepository;

@Service
public class TELayoutService {
	@Autowired
	private TELayoutRepository layoutRepository;

	public List<TELayout> getLayoutCompList(Long corpId) {
		return layoutRepository.findByCorpid(corpId);
	}
}
	