package com.trawind.caas.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.trawind.caas.entity.TRChannel;
import com.trawind.caas.repository.TRChannelRepository;

@Service
public class TRChannelService {

	@Autowired
	private TRChannelRepository channelRepository;
	
	public String getMappingHashCode(String channelRout) {
		TRChannel channel = channelRepository.findByChannelRoute(channelRout);
		if(channel != null) {
			return channel.getMappingHashCode();
		}
		return null;
	}
	
	public void saveChannel(TRChannel channel) {
		channelRepository.save(channel);
	}
}
