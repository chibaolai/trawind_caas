package com.trawind.caas.req;

import net.sf.json.JSONArray;

public class SaveAttrReq {
	private JSONArray nameLang;
	private String corpCode;
	private String widgetId;
	private String widgetType;
	private String widgetLayout;
	private String defaultVal;
	private String attrName;
	private String layoutJson;
	private String imgCategoryId;
	public JSONArray getNameLang() {
		return nameLang;
	}
	public void setNameLang(JSONArray nameLang) {
		this.nameLang = nameLang;
	}
	public String getCorpCode() {
		return corpCode;
	}
	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}
	public String getWidgetId() {
		return widgetId;
	}
	public void setWidgetId(String widgetId) {
		this.widgetId = widgetId;
	}
	public String getWidgetType() {
		return widgetType;
	}
	public void setWidgetType(String widgetType) {
		this.widgetType = widgetType;
	}
	public String getWidgetLayout() {
		return widgetLayout;
	}
	public void setWidgetLayout(String widgetLayout) {
		this.widgetLayout = widgetLayout;
	}
	public String getDefaultVal() {
		return defaultVal;
	}
	public void setDefaultVal(String defaultVal) {
		this.defaultVal = defaultVal;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getLayoutJson() {
		return layoutJson;
	}
	public void setLayoutJson(String layoutJson) {
		this.layoutJson = layoutJson;
	}
	public String getImgCategoryId() {
		return imgCategoryId;
	}
	public void setImgCategoryId(String imgCategoryId) {
		this.imgCategoryId = imgCategoryId;
	}
}
