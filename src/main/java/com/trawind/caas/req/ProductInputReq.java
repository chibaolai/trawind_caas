package com.trawind.caas.req;

import java.util.List;

import com.trawind.caas.entity.MWidget;

import net.sf.json.JSONObject;

/**
 * 产品入力请求
 * 
 * @author chibaolai
 *
 */
public class ProductInputReq {
	private String productId;
	// 输入的数据(json格式)
	private List<MWidget> productDataJson;

	private String formId;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public List<MWidget> getProductDataJson() {
		return productDataJson;
	}

	public void setProductDataJson(List<MWidget> productDataJson) {
		this.productDataJson = productDataJson;
	}

}
