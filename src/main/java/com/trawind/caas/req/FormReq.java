package com.trawind.caas.req;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class FormReq {
	// 所属公司ID
	private String corpId;
	// 所属公司Code
	private String corpCode;
	// 表单ID
	private String formId;
	// 表单Name
	private String formName;
	// 包含的所有组件ID
	private String widgetIds;
	// html布局
	private String layout;
	// 输入的数据(json格式)
	private JSONObject formDataJson;
	// 指令对应的json
	private JSONArray directJson;
	// 产品类别
	private String categoryId;
	// 命令list
	private JSONArray selCommandList;
	//表单类型
	private String formType;

	public JSONArray getSelCommandList() {
		return selCommandList;
	}

	public void setSelCommandList(JSONArray selCommandList) {
		this.selCommandList = selCommandList;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getWidgetIds() {
		return widgetIds;
	}

	public void setWidgetIds(String widgetIds) {
		this.widgetIds = widgetIds;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public JSONObject getFormDataJson() {
		return formDataJson;
	}

	public void setFormDataJson(JSONObject formDataJson) {
		this.formDataJson = formDataJson;
	}

	public JSONArray getFormLayoutJson() {
		return directJson;
	}

	public void setFormLayoutJson(JSONArray formLayoutJson) {
		this.directJson = formLayoutJson;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}
	
}
