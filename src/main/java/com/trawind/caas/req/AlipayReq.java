package com.trawind.caas.req;

public class AlipayReq {
	String payType;// 产品购买种类
	
	String tradeNo;//订单号

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

}
