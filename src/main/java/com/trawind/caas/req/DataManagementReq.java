package com.trawind.caas.req;

public class DataManagementReq {
	private String jsonStr;// 导出XML json String

	private String param;// 模糊查询keyword

//	private String productId;// 表单ID
	
	private String fileName;//上传xsd文件名

	public String getJsonStr() {
		return jsonStr;
	}

	public void setJsonStr(String jsonStr) {
		this.jsonStr = jsonStr;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

//	public String getProductId() {
//		return productId;
//	}
//
//	public void setProductId(String productId) {
//		this.productId = productId;
//	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
