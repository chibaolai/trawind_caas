package com.trawind.caas.req;

import java.util.List;

import com.trawind.caas.entity.MWidget;

import net.sf.json.JSONObject;

/**
 * 保存产品以及数据请求
 * 
 * @author chibaolai
 *
 */
public class SaveProductAndDataReq {
	//产品ID
	private String productId;
	//表单ID
	private String formId;
	// 输入的数据(json格式)
	private List<MWidget> productDataJson;
	// 时间戳
	private String timeStamp;
	// 覆盖更新Flag
	private boolean overWriteFlag;
	public String getFormId() {
		return formId;
	}
	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public List<MWidget> getProductDataJson() {
		return productDataJson;
	}
	public void setProductDataJson(List<MWidget> productDataJson) {
		this.productDataJson = productDataJson;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public boolean isOverWriteFlag() {
		return overWriteFlag;
	}
	public void setOverWriteFlag(boolean overWriteFlag) {
		this.overWriteFlag = overWriteFlag;
	}

}
