package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_item_chart_data database table.
 * 
 */
@Entity
@Table(name = "t_e_item_chart_data")
@NamedQuery(name = "TEChartData.findAll", query = "SELECT t FROM TEChartData t")
public class TEChartData implements Serializable {
	private static final long serialVersionUID = 8886172793839121681L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "style_id")
	private Integer styleId;

	@Column(name = "type_id")
	private Integer typeId;

	@Column(name = "item_name")
	private String itemName;

	@Column(name = "item_type")
	private String itemType;

	@Column(name = "item_stack")
	private String itemStack;

	@Column(name = "item_data")
	private String itemData;

	@Column(name = "lan_def")
	private String lanDef;

	@Column(name = "del_flag")
	private String delFlag;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "c_user")
	private String cUser;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "corpid")
	private Long corpId;

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public Integer getStyleId() {
		return styleId;
	}

	public void setStyleId(Integer styleId) {
		this.styleId = styleId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemStack() {
		return itemStack;
	}

	public void setItemStack(String itemStack) {
		this.itemStack = itemStack;
	}

	public String getItemData() {
		return itemData;
	}

	public void setItemData(String itemData) {
		this.itemData = itemData;
	}

	public String getLanDef() {
		return lanDef;
	}

	public void setLanDef(String lanDef) {
		this.lanDef = lanDef;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
