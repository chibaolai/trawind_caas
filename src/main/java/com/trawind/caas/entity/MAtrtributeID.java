package com.trawind.caas.entity;

public class MAtrtributeID {
	//修改前属性值
	private Object previousValue;
	//修改后属性值
	private Object currentValue;

	public void setPreviousValue(Object previousValue) {
		this.previousValue = previousValue;
	}

	public Object getPreviousValue() {
		return this.previousValue;
	}

	public void setCurrentValue(Object currentValue) {
		this.currentValue = currentValue;
	}

	public Object getCurrentValue() {
		return this.currentValue;
	}

}