package com.trawind.caas.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MProductData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String productId;
	private Long corpId;
	private List<MWidget> widget;
	private String timeStamp;
	private List<MWidget> preWidget;
	private List<MWidget> nextWidget;

	public void setWidget(List<MWidget> widget) {
		this.widget = widget;
	}

	public List<MWidget> getWidget() {
		return this.widget;
	}

	public static MProductData fill(JSONObject jo) {
		MProductData o = new MProductData();
		if (jo.containsKey("widget")) {
			o.setWidget(jo.getJSONArray("widget"));
		}
		return o;
	}

	public static List<MProductData> fillList(JSONArray ja) {
		if (ja == null || ja.size() == 0)
			return null;
		List<MProductData> sqs = new ArrayList<MProductData>();
		for (int i = 0; i < ja.size(); i++) {
			sqs.add(fill(ja.getJSONObject(i)));
		}
		return sqs;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public List<MWidget> getPreWidget() {
		return preWidget;
	}

	public void setPreWidget(List<MWidget> preWidget) {
		this.preWidget = preWidget;
	}

	public List<MWidget> getNextWidget() {
		return nextWidget;
	}

	public void setNextWidget(List<MWidget> nextWidget) {
		this.nextWidget = nextWidget;
	}
}
