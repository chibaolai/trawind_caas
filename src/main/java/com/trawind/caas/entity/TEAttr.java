package com.trawind.caas.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.repository.Query;


/**
 * 属性实体
 * 
 */
@Entity
@Table(name="t_e_attrs")
public class TEAttr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long seqNo;
	// 属性id(字母+数字 uuid)
	private String id;
	// 属性名
	private String name;
	// 登录的机构Code
	@Column(name="corp_code")
	private String corpCode;
	// 登录的机构ID
	@Column(name="corp_id")
	private Long corpId;
	// 图像管理组件 内部属性类别ID
	@Column(name="imgCategory_id")
	private String imgCategoryId;
	// 控件Type
	@Column(name="widget_type")
	private String widgetType;
	// 属性指令json数据
	@Column(name="direc_json")
	private String direcJson;
	// 基本属性标示
	@Column(name="base_flag")
	private Integer baseFlag;
	// 删除标示
	@Column(name="del_flag")
	private Integer delFlag;
	// 创建用户
	@Column(name="c_user")
	private String cUser;
	// 创建时间
	@Column(name="c_time")
	private String cTime;
	// 更新用户
	@Column(name="u_user")
	private String uUser;
	// 更新时间
	@Column(name="u_time")
	private String uTime;
	// 当属性为图像管理组件：获取其内部属性
	@Transient
	private TEImgCategory imgCategory;
	public Long getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCorpCode() {
		return corpCode;
	}
	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}
	public Long getCorpId() {
		return corpId;
	}
	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}
	public String getWidgetType() {
		return widgetType;
	}
	public void setWidgetType(String widgetType) {
		this.widgetType = widgetType;
	}
	public String getDirecJson() {
		return direcJson;
	}
	public void setDirecJson(String direcJson) {
		this.direcJson = direcJson;
	}
	public Integer getBaseFlag() {
		return baseFlag;
	}
	public void setBaseFlag(Integer baseFlag) {
		this.baseFlag = baseFlag;
	}
	public Integer getDelFlag() {
		return delFlag;
	}
	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}
	public String getcUser() {
		return cUser;
	}
	public void setcUser(String cUser) {
		this.cUser = cUser;
	}
	public String getcTime() {
		return cTime;
	}
	public void setcTime(String cTime) {
		this.cTime = cTime;
	}
	public String getuUser() {
		return uUser;
	}
	public void setuUser(String uUser) {
		this.uUser = uUser;
	}
	public String getuTime() {
		return uTime;
	}
	public void setuTime(String uTime) {
		this.uTime = uTime;
	}
	public String getImgCategoryId() {
		return imgCategoryId;
	}
	public void setImgCategoryId(String imgCategoryId) {
		this.imgCategoryId = imgCategoryId;
	}
	public TEImgCategory getImgCategory() {
		return imgCategory;
	}
	public void setImgCategory(TEImgCategory imgCategory) {
		this.imgCategory = imgCategory;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}