package com.trawind.caas.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_r_channel")
public class TRChannel  implements java.io.Serializable{
	private static final long serialVersionUID = 5796974748070359403L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String sourceId;
	
	private String tagetId;
	
	private String channelRoute;
	
	private String mappingHashCode;
	
	private String protocol;
	
	private String mappingId;
	
	private Long corpId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getTagetId() {
		return tagetId;
	}

	public void setTagetId(String tagetId) {
		this.tagetId = tagetId;
	}

	public String getChannelRoute() {
		return channelRoute;
	}

	public void setChannelRoute(String channelRoute) {
		this.channelRoute = channelRoute;
	}

	public String getMappingHashCode() {
		return mappingHashCode;
	}

	public void setMappingHashCode(String mappingHashCode) {
		this.mappingHashCode = mappingHashCode;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getMappingId() {
		return mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}
}
