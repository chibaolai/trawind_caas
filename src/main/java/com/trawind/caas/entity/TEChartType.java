package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_item_chart_type database table.
 * 
 */
@Entity
@Table(name = "t_e_item_chart_type")
@NamedQuery(name = "TEChartType.findAll", query = "SELECT t FROM TEChartType t")
public class TEChartType implements Serializable {
	private static final long serialVersionUID = -4404130012678405705L;

	@Id
	@Column(name = "style_id")
	private Integer styleId;

	@Column(name = "type_id")
	private Integer typeId;

	@Column(name = "option_item")
	private String optionItem;

	@Column(name = "lan_def")
	private String lanDef;

	@Column(name = "del_flag")
	private String delFlag;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "c_user")
	private String cUser;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "name")
	private String name;

	@Column(name = "corpid")
	private Long corpId;

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public Integer getStyleId() {
		return styleId;
	}

	public void setStyleId(Integer styleId) {
		this.styleId = styleId;
	}

	public Integer getTypeId() {
		return typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	public String getOptionItem() {
		return optionItem;
	}

	public void setOptionItem(String optionItem) {
		this.optionItem = optionItem;
	}

	public String getLanDef() {
		return lanDef;
	}

	public void setLanDef(String lanDef) {
		this.lanDef = lanDef;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
