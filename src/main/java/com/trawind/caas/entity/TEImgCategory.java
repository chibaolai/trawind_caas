package com.trawind.caas.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

//图像管理组件 
@Entity
@Table(name="t_e_imgCategory")
public class TEImgCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long seqNo;
	// id(字母+数字 uuid)
	private String id;
	// 名称
	private String name;
	// 类别
	private String type;
	// 属性id列表
	private String innerAttrsId;
	// 小属性列表
	@Transient
	private List<TEAttr> innerAttrs;
	public Long getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getInnerAttrsId() {
		return innerAttrsId;
	}
	public void setInnerAttrsId(String innerAttrsId) {
		this.innerAttrsId = innerAttrsId;
	}
	public List<TEAttr> getInnerAttrs() {
		return innerAttrs;
	}
	public void setInnerAttrs(List<TEAttr> innerAttrs) {
		this.innerAttrs = innerAttrs;
	}
	
}
