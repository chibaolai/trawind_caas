package com.trawind.caas.entity;

import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MCorporationData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	
	private String corporationId;
	
	private List<MWidget> corporationDataList;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getCorporationId() {
		return corporationId;
	}

	public void setCorporationId(String corporationId) {
		this.corporationId = corporationId;
	}

	public List<MWidget> getCorporationDataList() {
		return corporationDataList;
	}

	public void setCorporationDataList(List<MWidget> corporationDataList) {
		this.corporationDataList = corporationDataList;
	}

	
	
}
