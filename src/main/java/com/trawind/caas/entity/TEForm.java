package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_form database table.
 * 
 */
@Entity
@Table(name = "t_e_form")
public class TEForm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long seqNo;
	// 属性id(字母+数字 uuid)
	private String id;
	// 表单Name
	private String name;
	// html布局
	private String layout;
	// 指令对应的json
	private String directJson;
	// 所有组件ID
	private String widgetIds;
	// 所属公司ID
	private Long corpid;
	// 所属公司Code
	private String corpCode;
	// 所属用户
	private String userId;
	// 删除标示
	private Integer delFlag;
	// 创建用户
	private String cUser;
	// 创建时间
	private String cTime;
	// 更新用户
	private String uUser;
	// 更新时间
	private String uTime;
	// 产品类别
	private String categoryId;
	// 命令list
	private String command;
	//表单类型
	private String formType;

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public Long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getDirectJson() {
		return directJson;
	}

	public void setDirectJson(String directJson) {
		this.directJson = directJson;
	}

	public String getWidgetIds() {
		return widgetIds;
	}

	public void setWidgetIds(String widgetIds) {
		this.widgetIds = widgetIds;
	}

	public Long getCorpid() {
		return corpid;
	}

	public void setCorpid(Long corpid) {
		this.corpid = corpid;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

}