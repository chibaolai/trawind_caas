package com.trawind.caas.entity;

import java.util.List;

public class MImageData {
	private String id;
	private String fileName;
	private List<MWidget> innerAttrs;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<MWidget> getInnerAttrs() {
		return innerAttrs;
	}
	public void setInnerAttrs(List<MWidget> innerAttrs) {
		this.innerAttrs = innerAttrs;
	}
}
