package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_form_home database table.
 * 
 */
@Entity
@Table(name = "t_e_form_home")
@NamedQuery(name = "TEFormHome.findAll", query = "SELECT t FROM TEFormHome t")
public class TEFormHome implements Serializable {
	private static final long serialVersionUID = -820712975312978944L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer no;

	private String id;

	private String code;

//	@Column(name = "comp_ids")
//	private String compIds;

	@Column(name = "layout")
	private String layout;

//	private String name;
	
	private Long corpid;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "del_flag")
	private Integer delFlag;

	@Column(name = "c_user")
	private String cUser;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	public TEFormHome() {
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

//	public String getCompIds() {
//		return compIds;
//	}
//
//	public void setCompIds(String compIds) {
//		this.compIds = compIds;
//	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}

	public Long getCorpid() {
		return corpid;
	}

	public void setCorpid(Long corpid) {
		this.corpid = corpid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

}