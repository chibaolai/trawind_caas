package com.trawind.caas.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the t_e_temp database table.
 * 
 */
@Entity
@Table(name = "t_e_temp")
@NamedQuery(name = "TETemp.findAll", query = "SELECT t FROM TETemp t")
public class TETemp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer no;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "c_user")
	private String cUser;

	private String layout;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "del_flag")
	private Integer delFlag;

	public TETemp() {
	}

	public Integer getNo() {
		return this.no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getCTime() {
		return this.cTime;
	}

	public void setCTime(String cTime) {
		this.cTime = cTime;
	}

	public String getCUser() {
		return this.cUser;
	}

	public void setCUser(String cUser) {
		this.cUser = cUser;
	}

	public String getLayout() {
		return this.layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getUTime() {
		return this.uTime;
	}

	public void setUTime(String uTime) {
		this.uTime = uTime;
	}

	public String getUUser() {
		return this.uUser;
	}

	public void setUUser(String uUser) {
		this.uUser = uUser;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}