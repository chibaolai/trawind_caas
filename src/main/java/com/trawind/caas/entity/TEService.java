package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_service database table.
 * 
 */
@Entity
@Table(name = "t_e_service")
public class TEService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// 服务名称
	@Column(name = "service_content")
	private String serviceContent;
	// 服务价格
	@Column(name = "service_price")
	private int servicePrice;
	// //服务路径
	// @Column(name = "service_path")
	// private String servicePath;
	// 周期
	@Column(name = "service_cycle")
	private String serviceCycle;
	// 是否删除
	@Column(name = "del_flag")
	private String delFlag;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getServiceContent() {
		return serviceContent;
	}

	public void setServiceContent(String serviceContent) {
		this.serviceContent = serviceContent;
	}

	public int getServicePrice() {
		return servicePrice;
	}

	public void setServicePrice(int servicePrice) {
		this.servicePrice = servicePrice;
	}

	// public String getServicePath() {
	// return servicePath;
	// }
	//
	// public void setServicePath(String servicePath) {
	// this.servicePath = servicePath;
	// }

	public String getServiceCycle() {
		return serviceCycle;
	}

	public void setServiceCycle(String serviceCycle) {
		this.serviceCycle = serviceCycle;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

}