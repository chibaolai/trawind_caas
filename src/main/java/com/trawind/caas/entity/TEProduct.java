package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_form database table.
 * 
 */
@Entity
@Table(name = "t_e_Product")
public class TEProduct implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long seqNo;
	// 属性id(字母+数字 uuid)
	private String id;
	// 产品状态(创建,初步发布，修改，审批,发布给交易伙伴，退出市场)
	private String status;
	// 表单Id
	private String formId;
	// 所属公司ID
	private Long corpid;
	// 所属公司Code
	private String corpCode;
	// 所属用户
	private String userId;
	// 删除标示
	private Integer delFlag;
	// 创建用户
	private String cUser;
	// 创建时间
	private String cTime;
	// 更新用户
	private String uUser;
	// 更新时间
	private String uTime;
	// 产品类别
	private String categoryId;

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	// 修改的属性
	@Column(name = "show_attrs")
	private String showAttrs;

	public String getShowAttrs() {
		return showAttrs;
	}

	public void setShowAttrs(String showAttrs) {
		this.showAttrs = showAttrs;
	}

	public Long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public Long getCorpid() {
		return corpid;
	}

	public void setCorpid(Long corpid) {
		this.corpid = corpid;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

}