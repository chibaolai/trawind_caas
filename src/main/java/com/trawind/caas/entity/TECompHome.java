package com.trawind.caas.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the t_e_comp_home database table.
 * 
 */
@Entity
@Table(name = "t_e_comp_home")
@NamedQuery(name = "TECompHome.findAll", query = "SELECT t FROM TECompHome t")
public class TECompHome implements Serializable {
	private static final long serialVersionUID = 3075864793552480498L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer no;

	private Long corpId;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "c_user")
	private String cUser;

	@Column(name = "del_flag")
	private Integer delFlag;

	private String layout;

	@Column(name = "name_def")
	private String nameDef;

	private String type;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	@Column(name = "comp_group")
	private String compGroup;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "sort")
	private Integer sort;

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getNo() {
		return this.no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public String getCTime() {
		return this.cTime;
	}

	public void setCTime(String cTime) {
		this.cTime = cTime;
	}

	public String getCUser() {
		return this.cUser;
	}

	public void setCUser(String cUser) {
		this.cUser = cUser;
	}

	public Integer getDelFlag() {
		return this.delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getLayout() {
		return this.layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getNameDef() {
		return this.nameDef;
	}

	public void setNameDef(String nameDef) {
		this.nameDef = nameDef;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUTime() {
		return this.uTime;
	}

	public void setUTime(String uTime) {
		this.uTime = uTime;
	}

	public String getUUser() {
		return this.uUser;
	}

	public void setUUser(String uUser) {
		this.uUser = uUser;
	}

	public String getCompGroup() {
		return compGroup;
	}

	public void setCompGroup(String compGroup) {
		this.compGroup = compGroup;
	}

}