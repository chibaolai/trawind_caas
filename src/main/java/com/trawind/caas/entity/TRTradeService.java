package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_trade database table.
 * 
 */
@Entity
@Table(name = "t_r_trade_service")
public class TRTradeService implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	// 公司ID
	@Column(name = "company_id")
	private Long companyId;
	//订单号
	@Column(name = "trad_no")
	private String tradNo;
	//服务ID
	@Column(name = "service_id")
	private String serviceId;
	//开始时间
	@Column(name = "service_start_time")
	private String serviceStartTime;
	//周期
	@Column(name = "service_cycle")
	private String serviceCycle;
	//是否自动续费
	@Column(name = "automatic_renewa")
	private String automaticRenewa;
	//续费周期
	@Column(name = "renewa_cycle")
	private String renewaCycle;
	//到期时间
	@Column(name = "service_end_time")
	private String serviceEndTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getTradNo() {
		return tradNo;
	}

	public void setTradNo(String tradNo) {
		this.tradNo = tradNo;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceStartTime() {
		return serviceStartTime;
	}

	public void setServiceStartTime(String serviceStartTime) {
		this.serviceStartTime = serviceStartTime;
	}

	public String getServiceCycle() {
		return serviceCycle;
	}

	public void setServiceCycle(String serviceCycle) {
		this.serviceCycle = serviceCycle;
	}

	public String getAutomaticRenewa() {
		return automaticRenewa;
	}

	public void setAutomaticRenewa(String automaticRenewa) {
		this.automaticRenewa = automaticRenewa;
	}

	public String getRenewaCycle() {
		return renewaCycle;
	}

	public void setRenewaCycle(String renewaCycle) {
		this.renewaCycle = renewaCycle;
	}

	public String getServiceEndTime() {
		return serviceEndTime;
	}

	public void setServiceEndTime(String serviceEndTime) {
		this.serviceEndTime = serviceEndTime;
	}

}