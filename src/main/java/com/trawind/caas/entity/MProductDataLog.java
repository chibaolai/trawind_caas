package com.trawind.caas.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MProductDataLog {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	// 产品ID
	private String productId;
	// 更新时间
	private String datetime;
	// 是否是回滚
	private boolean rollbackFlg;
	// 机构Id
	private Long corpId;
	// 源头(self：1,Parnter：2)
	private int mode;

	private List<MChangedAttrs> MChangedAttrs;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getDatetime() {
		return this.datetime;
	}

	public boolean isRollbackFlg() {
		return rollbackFlg;
	}

	public void setRollbackFlg(boolean rollbackFlg) {
		this.rollbackFlg = rollbackFlg;
	}

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public int getMode() {
		return mode;
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

	public void setMChangedAttrs(List<MChangedAttrs> MChangedAttrs) {
		this.MChangedAttrs = MChangedAttrs;
	}

	public List<MChangedAttrs> getMChangedAttrs() {
		return this.MChangedAttrs;
	}

	public static MProductDataLog fill(JSONObject jo) {
		MProductDataLog o = new MProductDataLog();
		if (jo.containsKey("productID")) {
			o.setProductId(jo.getString("productID"));
		}
		if (jo.containsKey("datetime")) {
			o.setDatetime(jo.getString("datetime"));
		}
		if (jo.containsKey("MChangedAttrs")) {
			o.setMChangedAttrs(jo.getJSONArray("MChangedAttrs"));
		}
		return o;
	}

	public static List<MProductDataLog> fillList(JSONArray ja) {
		if (ja == null || ja.size() == 0)
			return null;
		List<MProductDataLog> sqs = new ArrayList<MProductDataLog>();
		for (int i = 0; i < ja.size(); i++) {
			sqs.add(fill(ja.getJSONObject(i)));
		}
		return sqs;
	}
}
