package com.trawind.caas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 机构
 * 
 * @author chibaolai
 *
 */
@Entity
@Table(name = "t_e_corp")
public class TECorp {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// 机构名
	private String name;
	// 登录的机构Code
	@Column(name = "corp_code")
	private String corpCode;
	private String licenseCode;
	private String contray;
	private String regAdd;
	private String trade;
	private String descrip;
	// 删除标示
	@Column(name = "del_flag")
	private Integer delFlag;
	// 创建用户
	@Column(name = "c_user")
	private String cUser;
	// 创建时间
	@Column(name = "c_time")
	private String cTime;
	// 更新用户
	@Column(name = "u_user")
	private String uUser;
	// 更新时间
	@Column(name = "u_time")
	private String uTime;

	// 是否有分销商角色
	@Column(name = "has_distributor_role")
	private boolean hasDistributorRole;

	public boolean isHasDistributorRole() {
		return hasDistributorRole;
	}

	public void setHasDistributorRole(boolean hasDistributorRole) {
		this.hasDistributorRole = hasDistributorRole;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCorpCode() {
		return corpCode;
	}

	public void setCorpCode(String corpCode) {
		this.corpCode = corpCode;
	}

	public String getLicenseCode() {
		return licenseCode;
	}

	public void setLicenseCode(String licenseCode) {
		this.licenseCode = licenseCode;
	}

	public String getContray() {
		return contray;
	}

	public void setContray(String contray) {
		this.contray = contray;
	}

	public String getRegAdd() {
		return regAdd;
	}

	public void setRegAdd(String regAdd) {
		this.regAdd = regAdd;
	}

	public String getTrade() {
		return trade;
	}

	public void setTrade(String trade) {
		this.trade = trade;
	}

	public String getDescrip() {
		return descrip;
	}

	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}
}
