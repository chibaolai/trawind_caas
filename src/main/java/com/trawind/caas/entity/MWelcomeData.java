package com.trawind.caas.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MWelcomeData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private String languageCode;
	private String data;

	public String getId() {
		return id;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
