package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_layout database table.
 * 
 */
@Entity
@Table(name = "t_e_layout")
@NamedQuery(name = "TELayout.findAll", query = "SELECT t FROM TELayout t")
public class TELayout implements Serializable {
	private static final long serialVersionUID = 1165349097974720151L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "type")
	private String type;

	@Column(name = "layout")
	private String layout;

	@Column(name = "lan_def")
	private String lanDef;

	@Column(name = "name_def")
	private String nameDef;

	@Column(name = "comp_group")
	private String compGroup;

	@Column(name = "del_flag")
	private int delFlag;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "c_user")
	private String cUser;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	@Column(name = "corpid")
	private Long corpid;

	public Long getCorpid() {
		return corpid;
	}

	public void setCorpid(Long corpid) {
		this.corpid = corpid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getLanDef() {
		return lanDef;
	}

	public void setLanDef(String lanDef) {
		this.lanDef = lanDef;
	}

	public String getNameDef() {
		return nameDef;
	}

	public void setNameDef(String nameDef) {
		this.nameDef = nameDef;
	}

	public String getCompGroup() {
		return compGroup;
	}

	public void setCompGroup(String compGroup) {
		this.compGroup = compGroup;
	}

	public int getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(int delFlag) {
		this.delFlag = delFlag;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

}
