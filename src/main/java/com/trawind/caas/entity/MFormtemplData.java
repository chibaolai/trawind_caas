package com.trawind.caas.entity;

import java.util.List;

public class MFormtemplData {

	
	private List<MWidget> widgets;
	
	private String category;

	public List<MWidget> getWidgets() {
		return widgets;
	}

	public void setWidgets(List<MWidget> widgets) {
		this.widgets = widgets;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	
}
