package com.trawind.caas.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "t_e_temp_xml_element")
@NamedQuery(name = "TETempXmlElement.findAll", query = "SELECT t FROM TETempXmlElement t")
public class TETempXmlElement implements Serializable {
	private static final long serialVersionUID = 3500646083571044533L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "seq_no")
	private Long seqNo;

	@Column(name = "id")
	private String id;

	@Column(name = "root_content")
	private String rootContent;

	@Column(name = "child_content")
	private String childContent;

	@Column(name = "time")
	private String time;

	public Long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(Long seqNo) {
		this.seqNo = seqNo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRootContent() {
		return rootContent;
	}

	public void setRootContent(String rootContent) {
		this.rootContent = rootContent;
	}

	public String getChildContent() {
		return childContent;
	}

	public void setChildContent(String childContent) {
		this.childContent = childContent;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}