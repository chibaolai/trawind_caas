package com.trawind.caas.entity;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.repository.Query;


/**
 * 校验规则
 * 
 */
@Entity
@Table(name="t_e_rules")
public class TERules implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long TP_AUDIT_RULE_ID;//规则主键
	private Long TP_AUDIT_RULE_GRP_ID;//所属机构
	private String AUDIT_RULE_NAME;//规则名称
	private String AUDIT_RULE_TYPE;//规则类型(LOGICAL,REQUIRED,UNIQUE_SINGLE_LEVEL,GTIN_DUPLICATE,GTIN_ALLOC,GLN_CHECK_DIGIT,CUMULATIVE_WEIGHT,CHECK_EACH_GTIN_STARTS_WITH_ZERO,CHECK_MAX_LENGTH,CUMULATIVE_WEIGHT,SAME_ACROSS_LEVELS,GTIN_DUPLICATE_ROOT_GTIN,INTEGER)
	private String AUDIT_RULE_LOGICAL_OP;//逻辑参数(GT,EQ,GT_EQ,LT_EQ,RANGE_20)
	private boolean AUDIT_RULE_IS_HIERARCHICAL;//是否分层
	private Long AUDIT_RULE_ATTR_ID;//校验属性ID
	private String AUDIT_RULE_ATTR_NAME;//规则属性名字
	private String AUDIT_RULE_ATTR_VALUE;//属性值
	private Long AUDIT_RULE_CHECK_ATTR_ID;//CHECK属性ID
	private String AUDIT_RULE_CHECK_ATTR_NAME;//CHECK属性名字
	private String AUDIT_RULE_CHECK_ATTR_VALUE;//CHECK属性值
	private String AUDIT_RULE_CHECK_HIER_LEVEL;//校验等级(ALL,ANY,SINGLE,LOWEST,EXCEPT_EACH,EACH,HIGHEST_BELOW_PALLET,PALLET,HIGHEST)
	private String AUDIT_RULE_ERR_MSG;//错误信息
	private String AUDIT_RULE_APPLY_TO_NEW;
	private boolean AUDIT_ENABLE;//是否激活
	private String AUDIT_RULE_COND_ATTR_NAME;//规则条件属性名
	private String AUDIT_RULE_COND_ATTR_VALUE;//规则条件属性值
	public Long getTP_AUDIT_RULE_ID() {
		return TP_AUDIT_RULE_ID;
	}
	public void setTP_AUDIT_RULE_ID(Long tP_AUDIT_RULE_ID) {
		TP_AUDIT_RULE_ID = tP_AUDIT_RULE_ID;
	}
	public Long getTP_AUDIT_RULE_GRP_ID() {
		return TP_AUDIT_RULE_GRP_ID;
	}
	public void setTP_AUDIT_RULE_GRP_ID(Long tP_AUDIT_RULE_GRP_ID) {
		TP_AUDIT_RULE_GRP_ID = tP_AUDIT_RULE_GRP_ID;
	}
	public String getAUDIT_RULE_NAME() {
		return AUDIT_RULE_NAME;
	}
	public void setAUDIT_RULE_NAME(String aUDIT_RULE_NAME) {
		AUDIT_RULE_NAME = aUDIT_RULE_NAME;
	}
	public String getAUDIT_RULE_TYPE() {
		return AUDIT_RULE_TYPE;
	}
	public void setAUDIT_RULE_TYPE(String aUDIT_RULE_TYPE) {
		AUDIT_RULE_TYPE = aUDIT_RULE_TYPE;
	}
	public String getAUDIT_RULE_LOGICAL_OP() {
		return AUDIT_RULE_LOGICAL_OP;
	}
	public void setAUDIT_RULE_LOGICAL_OP(String aUDIT_RULE_LOGICAL_OP) {
		AUDIT_RULE_LOGICAL_OP = aUDIT_RULE_LOGICAL_OP;
	}
	public boolean getAUDIT_RULE_IS_HIERARCHICAL() {
		return AUDIT_RULE_IS_HIERARCHICAL;
	}
	public void setAUDIT_RULE_IS_HIERARCHICAL(boolean aUDIT_RULE_IS_HIERARCHICAL) {
		AUDIT_RULE_IS_HIERARCHICAL = aUDIT_RULE_IS_HIERARCHICAL;
	}
	public Long getAUDIT_RULE_ATTR_ID() {
		return AUDIT_RULE_ATTR_ID;
	}
	public void setAUDIT_RULE_ATTR_ID(Long aUDIT_RULE_ATTR_ID) {
		AUDIT_RULE_ATTR_ID = aUDIT_RULE_ATTR_ID;
	}
	public String getAUDIT_RULE_ATTR_NAME() {
		return AUDIT_RULE_ATTR_NAME;
	}
	public void setAUDIT_RULE_ATTR_NAME(String aUDIT_RULE_ATTR_NAME) {
		AUDIT_RULE_ATTR_NAME = aUDIT_RULE_ATTR_NAME;
	}
	public String getAUDIT_RULE_ATTR_VALUE() {
		return AUDIT_RULE_ATTR_VALUE;
	}
	public void setAUDIT_RULE_ATTR_VALUE(String aUDIT_RULE_ATTR_VALUE) {
		AUDIT_RULE_ATTR_VALUE = aUDIT_RULE_ATTR_VALUE;
	}
	public Long getAUDIT_RULE_CHECK_ATTR_ID() {
		return AUDIT_RULE_CHECK_ATTR_ID;
	}
	public void setAUDIT_RULE_CHECK_ATTR_ID(Long aUDIT_RULE_CHECK_ATTR_ID) {
		AUDIT_RULE_CHECK_ATTR_ID = aUDIT_RULE_CHECK_ATTR_ID;
	}
	public String getAUDIT_RULE_CHECK_ATTR_NAME() {
		return AUDIT_RULE_CHECK_ATTR_NAME;
	}
	public void setAUDIT_RULE_CHECK_ATTR_NAME(String aUDIT_RULE_CHECK_ATTR_NAME) {
		AUDIT_RULE_CHECK_ATTR_NAME = aUDIT_RULE_CHECK_ATTR_NAME;
	}
	public String getAUDIT_RULE_CHECK_ATTR_VALUE() {
		return AUDIT_RULE_CHECK_ATTR_VALUE;
	}
	public void setAUDIT_RULE_CHECK_ATTR_VALUE(String aUDIT_RULE_CHECK_ATTR_VALUE) {
		AUDIT_RULE_CHECK_ATTR_VALUE = aUDIT_RULE_CHECK_ATTR_VALUE;
	}
	public String getAUDIT_RULE_CHECK_HIER_LEVEL() {
		return AUDIT_RULE_CHECK_HIER_LEVEL;
	}
	public void setAUDIT_RULE_CHECK_HIER_LEVEL(String aUDIT_RULE_CHECK_HIER_LEVEL) {
		AUDIT_RULE_CHECK_HIER_LEVEL = aUDIT_RULE_CHECK_HIER_LEVEL;
	}
	public String getAUDIT_RULE_ERR_MSG() {
		return AUDIT_RULE_ERR_MSG;
	}
	public void setAUDIT_RULE_ERR_MSG(String aUDIT_RULE_ERR_MSG) {
		AUDIT_RULE_ERR_MSG = aUDIT_RULE_ERR_MSG;
	}
	public String getAUDIT_RULE_APPLY_TO_NEW() {
		return AUDIT_RULE_APPLY_TO_NEW;
	}
	public void setAUDIT_RULE_APPLY_TO_NEW(String aUDIT_RULE_APPLY_TO_NEW) {
		AUDIT_RULE_APPLY_TO_NEW = aUDIT_RULE_APPLY_TO_NEW;
	}
	public boolean getAUDIT_ENABLE() {
		return AUDIT_ENABLE;
	}
	public void setAUDIT_ENABLE(boolean aUDIT_ENABLE) {
		AUDIT_ENABLE = aUDIT_ENABLE;
	}
	public String getAUDIT_RULE_COND_ATTR_NAME() {
		return AUDIT_RULE_COND_ATTR_NAME;
	}
	public void setAUDIT_RULE_COND_ATTR_NAME(String aUDIT_RULE_COND_ATTR_NAME) {
		AUDIT_RULE_COND_ATTR_NAME = aUDIT_RULE_COND_ATTR_NAME;
	}
	public String getAUDIT_RULE_COND_ATTR_VALUE() {
		return AUDIT_RULE_COND_ATTR_VALUE;
	}
	public void setAUDIT_RULE_COND_ATTR_VALUE(String aUDIT_RULE_COND_ATTR_VALUE) {
		AUDIT_RULE_COND_ATTR_VALUE = aUDIT_RULE_COND_ATTR_VALUE;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}