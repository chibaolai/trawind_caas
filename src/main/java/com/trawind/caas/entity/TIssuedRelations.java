package com.trawind.caas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_issued_relations")
public class TIssuedRelations implements java.io.Serializable {
	private static final long serialVersionUID = 4395371687968225229L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private long id;

	// 产品
	@Column(name = "product_id")
	private String productId;

	// 生产商
	@Column(name = "manufacturer_id")
	private Long manufacturerId;

	// 分销商ID
	@Column(name = "distributor_id")
	private Long distributorId;

	public TIssuedRelations() {
	}

	public long getId() {
		return id;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(Long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public Long getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(Long distributorId) {
		this.distributorId = distributorId;
	}

}
