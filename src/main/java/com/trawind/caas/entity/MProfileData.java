package com.trawind.caas.entity;

import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class MProfileData {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	private List<MWidget> profileDataList;
	
	private String userId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<MWidget> getProfileDataList() {
		return profileDataList;
	}

	public void setProfileDataList(List<MWidget> profileDataList) {
		this.profileDataList = profileDataList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
	
}
