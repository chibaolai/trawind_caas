package com.trawind.caas.entity;

import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

/**
 * 属性修改entity
 * @author PC
 *
 */

public class MChangedAttrs {
	private String attributeId;
	private MAtrtributeID MAtrtributeID;

	public void setMAtrtributeID(MAtrtributeID MAtrtributeID) {
		this.MAtrtributeID = MAtrtributeID;
	}

	public MAtrtributeID getMAtrtributeID() {
		return this.MAtrtributeID;
	}

	public String getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}

	public static MChangedAttrs fill(JSONObject jo) {
		MChangedAttrs o = new MChangedAttrs();
		if (jo.containsKey("MAtrtributeID")) {
			o.setMAtrtributeID((MAtrtributeID)jo.get("MAtrtributeID"));
		}
		return o;
	}

	public static List<MChangedAttrs> fillList(JSONArray ja) {
		if (ja == null || ja.size() == 0)
			return null;
		List<MChangedAttrs> sqs = new ArrayList<MChangedAttrs>();
		for (int i = 0; i < ja.size(); i++) {
			sqs.add(fill(ja.getJSONObject(i)));
		}
		return sqs;
	}

}
