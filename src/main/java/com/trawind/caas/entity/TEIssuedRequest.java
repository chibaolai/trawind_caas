package com.trawind.caas.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the t_e_issued_request database table.
 * 
 */
@Entity
@Table(name = "t_e_issued_request")
@NamedQuery(name = "TEIssuedRequest.findAll", query = "SELECT t FROM TEIssuedRequest t")
public class TEIssuedRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int STATE_CODE_NONE = 0;
	public static final int STATE_CODE_ACCEPT = 1;
	public static final int STATE_CODE_REJECT = 2;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "from_corp_id")
	private Long fromCorpId;

	@Column(name = "to_corp_id")
	private Long toCorpId;

	@Column(name = "product_id")
	private String productId;

	@Column(name = "product_log_id")
	private String productLogId;

	@Column(name = "state")
	private int state;

	@Column(name = "send_time")
	private Date sendTime;

	@Column(name = "handle_time")
	private Date handleTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFromCorpId() {
		return fromCorpId;
	}

	public void setFromCorpId(Long fromCorpId) {
		this.fromCorpId = fromCorpId;
	}

	public Long getToCorpId() {
		return toCorpId;
	}

	public void setToCorpId(Long toCorpId) {
		this.toCorpId = toCorpId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductLogId() {
		return productLogId;
	}

	public void setProductLogId(String productLogId) {
		this.productLogId = productLogId;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getSendTime() {
		return sendTime;
	}

	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}

	public Date getHandleTime() {
		return handleTime;
	}

	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}

}
