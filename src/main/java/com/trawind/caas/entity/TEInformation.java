package com.trawind.caas.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the t_e_item_information database table.
 * 
 */
@Entity
@Table(name = "t_e_item_information")
@NamedQuery(name = "TEInformation.findAll", query = "SELECT t FROM TEInformation t")
public class TEInformation implements Serializable {
	private static final long serialVersionUID = 1407565617244986974L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "c_time")
	private String cTime;

	@Column(name = "c_user")
	private String cUser;

	@Column(name = "u_time")
	private String uTime;

	@Column(name = "u_user")
	private String uUser;

	@Column(name = "del_flag")
	private Integer delFlag;

	@Column(name = "lan_def")
	private String lanDef;

	@Column(name = "user_id")
	private String userId;

	@Column(name = "corp_id")
	private Long corpId;

	@Column(name = "classify_code")
	private String classifyCode;

	@Column(name = "content")
	private String content;

	@Column(name = "name")
	private String name;

	public TEInformation() {
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

	public String getLanDef() {
		return lanDef;
	}

	public void setLanDef(String lanDef) {
		this.lanDef = lanDef;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getCorpId() {
		return corpId;
	}

	public void setCorpId(Long corpId) {
		this.corpId = corpId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClassifyCode() {
		return classifyCode;
	}

	public void setClassifyCode(String classifyCode) {
		this.classifyCode = classifyCode;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getcTime() {
		return cTime;
	}

	public void setcTime(String cTime) {
		this.cTime = cTime;
	}

	public String getcUser() {
		return cUser;
	}

	public void setcUser(String cUser) {
		this.cUser = cUser;
	}

	public String getuTime() {
		return uTime;
	}

	public void setuTime(String uTime) {
		this.uTime = uTime;
	}

	public String getuUser() {
		return uUser;
	}

	public void setuUser(String uUser) {
		this.uUser = uUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}