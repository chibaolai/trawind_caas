package com.trawind.caas.entity;

import net.sf.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;

public class MWidget {
	private String key;

	private Object value;

	private String type;

	public void setKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return this.key;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return this.value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static MWidget fill(JSONObject jo) {
		MWidget o = new MWidget();
		if (jo.containsKey("key")) {
			o.setKey(jo.getString("key"));
		}
		if (jo.containsKey("value")) {
			o.setValue(jo.getString("value"));
		}
		if (jo.containsKey("type")) {
			o.setValue(jo.getString("type"));
		}
		return o;
	}

	public static List<MWidget> fillList(JSONArray ja) {
		if (ja == null || ja.size() == 0)
			return null;
		List<MWidget> sqs = new ArrayList<MWidget>();
		for (int i = 0; i < ja.size(); i++) {
			sqs.add(fill(ja.getJSONObject(i)));
		}
		return sqs;
	}

}
