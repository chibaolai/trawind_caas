// The app/scripts/app.js file, which defines our AngularJS app
define([ 'angular', 'controllers/controllers', 'uiRouter', 'services/services',
		'filters/filters', 'directives/directives', 'widget','webuploader','mainHome'], function(
		angular) {
	return angular.module('webapp', [ 'controllers', 'services', 'filters',
			'directives', 'ui.router', 'webapp.widget' ]);
});
