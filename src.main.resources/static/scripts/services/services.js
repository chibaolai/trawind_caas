define(['angular', 'angularTranslate', 'angularTranslateLoader', 'angularCookies'], function(angular) {
  'use strict';
  return angular.module('services', ['pascalprecht.translate', 'ngCookies']);
});
