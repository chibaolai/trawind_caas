define(['services/services'],
  function(services) {
	//拖拽
	services.factory("caasSer", function() {
		return {
			decodeHtml: function (src){
				function cleanHtml(e) {
					$(e).parent().append($(e).children().html());
				};
				var idLs = [];
				var htmlObj = {};
				src.html($(".demo").html());
	        	var t = src.children();
	            t.find("div[name='widget']").each(function (i, n){
	            	idLs.push($(n).find("input[name='widgetId']").val());
	        		$(n).after('<widget-directive widget-options="'+$(n).find("input[name='widgetId']").val()+'"></widget-directive>');
	        		$(n).remove();
	        	});
	            htmlObj.html = src.html(); 
	            htmlObj.idLs = idLs; 
	            return htmlObj;
			}
		};
	});
  });
