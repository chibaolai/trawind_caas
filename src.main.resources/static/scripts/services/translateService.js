define(['services/services', 'angularTranslate'], function(services, $translateProvider) {
      services.factory("transl", function($translate, $http, $cookieStore) {
            var defLanguageCode = "en";
            var languageCode = defLanguageCode;
            var currentLanguageCodeKey = "currentLanguageCode";

            setLanguage = function(langKey) {
              $translate.use(langKey);
              $translate.fallbackLanguage(langKey);
              $translate.preferredLanguage(langKey);
              languageCode = langKey;

              var expireDate = new Date();
              expireDate.setDate(expireDate.getDate() + 999);
              $cookieStore.put(currentLanguageCodeKey, langKey, {
                    'expires' : expireDate
                  });
            }

            getLanguage = function() {
              var langCookie = $cookieStore.get(currentLanguageCodeKey);
              if (langCookie) {
                languageCode = langCookie;
              } else {
                if (navigator.userLanguage) {
                  languageCode = navigator.userLanguage.substring(0, 2).toLowerCase();
                } else {
                  languageCode = navigator.language.substring(0, 2).toLowerCase();
                }

                /* language match */
                switch (languageCode) {
                  case "en" :
                    /* english */
                    break;
                  case "zh" :
                    /* 中文 */
                    break;
                  default :
                    languageCode = defLanguageCode;
                }
              }

              setLanguage(languageCode);
            }

            var ret = {
              currentLanguageCode : function() {
                return languageCode;
              },
              changeLanguage : setLanguage,
              reflashLanguage : function() {
                setLanguage(languageCode);
              }
            };

            getLanguage();

            return ret;
          }).config(function($translateProvider) {
            $translateProvider.useStaticFilesLoader({
                  files : [{
                        prefix : '/scripts/i18n/locale-',
                        suffix : '.json'
                      }]
                });
            $translateProvider.registerAvailableLanguageKeys(['en', 'zh'], {
                  'en_US' : 'en',
                  'en_UK' : 'en',
                  'zh_CN' : 'zh'
                });
            $translateProvider.fallbackLanguage('en');
            $translateProvider.preferredLanguage('en');
          });
    });
