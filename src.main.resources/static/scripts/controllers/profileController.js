define(['controllers/controllers', 'vendor/bootstrap_fileinput'], function(controllers) {
      controllers.controller("profileCtrl", ['$scope', '$location', '$q', '$http', '$compile', function($scope, $location, $q, $http, $compile) {
                $scope.getXmlPageInfo = function() {
                  var data = {};
                  data.inputFileId = $("#inputFileId").val();
                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'profile/getXmlPageInfo',
                        'data' : JSON.stringify(data)
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        if (data) {
                          $(".upload-xml").html("<xmp>" + data.xml + "</xmp>");

                          $(".db-data").empty();
                          $(data.xml).find("pageInfo").each(function(i) {
                                var fileId = $(this).attr("fileId");
                                var totalPage = $(this).attr("totalPage");
                                var currentPage = $(this).attr("currentPage");
                                var pageSize = $(this).attr("pageSize");

                                $(".db-data").append("fileId : <span class=\"view-fileId\">" + fileId + "</span><br />");
                                $(".db-data").append("totalPage : <span class=\"view-totalPage\">" + totalPage + "</span><br />");
                                $(".db-data").append("currentPage : <span class=\"view-currentPage\">" + currentPage + "</span><br />");
                                $(".db-data").append("pageSize : <span class=\"view-pageSize\">" + pageSize + "</span><br />");
                              });
                        }

                        console.log('getXmlPageInfo 请求成功');
                      }, function(data) {
                        console.log('getXmlPageInfo 请求失败');
                      });

                }

                $scope.getNextXmlPageInfo = function() {
                  var data = {};
                  data.inputFileId = $("#inputFileId").val();
                  data.currentPage = $(".view-currentPage").text();

                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'profile/getNextXmlPageInfo',
                        'data' : JSON.stringify(data)
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        if (data) {
                          if (data.xml === "") {
                            return;
                          }

                          $(".upload-xml").html("<xmp>" + data.xml + "</xmp>");

                          $(".db-data").empty();
                          $(data.xml).find("pageInfo").each(function(i) {
                                var fileId = $(this).attr("fileId");
                                var totalPage = $(this).attr("totalPage");
                                var currentPage = $(this).attr("currentPage");
                                var pageSize = $(this).attr("pageSize");

                                $(".db-data").append("fileId : <span class=\"view-fileId\">" + fileId + "</span><br />");
                                $(".db-data").append("totalPage : <span class=\"view-totalPage\">" + totalPage + "</span><br />");
                                $(".db-data").append("currentPage : <span class=\"view-currentPage\">" + currentPage + "</span><br />");
                                $(".db-data").append("pageSize : <span class=\"view-pageSize\">" + pageSize + "</span><br />");
                              });
                        }

                        console.log('getNextXmlPageInfo 请求成功');
                      }, function(data) {
                        console.log('getNextXmlPageInfo 请求失败');
                      });
                }

                $scope.getPreviousXmlPageInfo = function() {
                  var data = {};
                  data.inputFileId = $("#inputFileId").val();
                  data.currentPage = $(".view-currentPage").text();

                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'profile/getPreviousXmlPageInfo',
                        'data' : JSON.stringify(data)
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        if (data) {
                          if (data.xml === "") {
                            return;
                          }

                          $(".upload-xml").html("<xmp>" + data.xml + "</xmp>");

                          $(".db-data").empty();
                          $(data.xml).find("pageInfo").each(function(i) {
                                var fileId = $(this).attr("fileId");
                                var totalPage = $(this).attr("totalPage");
                                var currentPage = $(this).attr("currentPage");
                                var pageSize = $(this).attr("pageSize");

                                $(".db-data").append("fileId : <span class=\"view-fileId\">" + fileId + "</span><br />");
                                $(".db-data").append("totalPage : <span class=\"view-totalPage\">" + totalPage + "</span><br />");
                                $(".db-data").append("currentPage : <span class=\"view-currentPage\">" + currentPage + "</span><br />");
                                $(".db-data").append("pageSize : <span class=\"view-pageSize\">" + pageSize + "</span><br />");
                              });
                        }

                        console.log('getPreviousXmlPageInfo 请求成功');
                      }, function(data) {
                        console.log('getPreviousXmlPageInfo 请求失败');
                      });
                }

                $(document).ready(function() {
                      $scope.setNavStyle(null, "menu_profile");
                    });

              }]);
    });
