define(['controllers/controllers', 'isotope', 'appear', 'backstretch', 'modernizr', 'welcomeCustom', 'easing', 'fullPage'], function(controllers) {
      controllers.controller("welcomeCtrl", ['$scope', '$location', '$q', '$http', '$compile', 'transl', function($scope, $location, $q, $http, $compile, transl) {
                $scope.showLoginWin = function() {
                  $("#downloadModal_login").show();
                }
                angular.element(document).ready(function(){
                	$scope.loadService();
                });
                
                $scope.loadService = function(){
                	$http({
                		method: 'POST',
                		url: '/welcomePage/getServiceList'
                	}).then(function successCallback(response) {
                		if (response.data != undefined) {
                			var regist_service_html = new StringBuffer();
                			var cycle_length = 6;
                			var service_show_th_html = new StringBuffer();
                			var service_show_tbody_html = new StringBuffer();
                			for(var i=0;i<response.data.length;i++){
                				if(response.data[i].delFlag == '0'){
                					//注册
                					var servJsonStr = JSON.parse(response.data[i].serviceContent)[transl.currentLanguageCode()][0];
                					regist_service_html.append("<div class=\"panel panel-default text-center\">");
                					regist_service_html.append("<div class=\"panel-body\">");
                					regist_service_html.append("<h3 class=\"panel-title price\">").append(servJsonStr['title']).append("</h3>");
                					regist_service_html.append("</div>");
                					regist_service_html.append("<ul class=\"list-group\">");
                					regist_service_html.append("<li class=\"list-group-item\">").append(servJsonStr['price']).append("</li>");
                					regist_service_html.append("<li class=\"list-group-item\"><input type=\"checkbox\" name=\"pay_type\" id=\"chk0\" value=\"").append(i).append("\" /></li>");
                					regist_service_html.append("</ul>");
                					regist_service_html.append("</div>");
                					
                					//服务
                					service_show_th_html.append("<th style=\"text-align: center;font-size: 2em;\">").append(servJsonStr['title']).append("</th>");
                				}
                			}
                			$("#service_show_th").html(service_show_th_html.toString());
                			$("#regist_service_choose").html(regist_service_html.toString());
                			
                			var serAttrHtml = new StringBuffer();
                			for(var j=1;j<cycle_length;j++){
                				var name = getSerachName(j);
                				serAttrHtml.append("<tr>");
                				for(var m=0;m<response.data.length;m++){
                					var servJson = JSON.parse(response.data[m].serviceContent)[transl.currentLanguageCode()][0];
                					serAttrHtml.append("<td>").append(servJson[name]).append("</td>");
                				}
                				serAttrHtml.append("</tr>");
                			}
                			$("#service_show_tbody").html(serAttrHtml.toString());
            			} else {
            				console.log("未获取到服务");
            			}
                	}, function errorCallback(response) {
                	});               	
                }
                getSerachName = function(val){
     	    	   switch(val)
    	    	   {
    		    	   case 0:
    		    		   return "title";
    		    	   case 1:
    		    		   return "price";
    		    	   case 2:
    		    		   return "connect";
    		    	   case 3:
    		    		   return "user";
    		    	   case 4:
    		    		   return "product";
    		    	   case 5:
    		    		   return "mode";
    		    	   default:
    		    		   console.log("没找到");
    	    	   }
                }
                
				$compile($("#contactUs").contents())($scope);
                $compile($("#service_id").contents())($scope);
                $compile($("#our_price_comp").contents())($scope);
                
                $(window).resize(function() {
                      setTimeout(function() {
                            $("body").eq(0).css("font-size", "14px");
                          }, 1000);
                    });

                $scope.setSelMenuBc = function(nextIndex) {
                  var pageArr = ["page1", "page2", "page3", "page4", "page5", "page6", "page7", "page8"];
                  angular.forEach(pageArr, function(value, idx) {
                        $("#" + value).find("a").attr("style", "background-color:white");
                        if (idx + 1 == nextIndex) {
                          $("#" + value).find("a").attr("style", "background-color:#f1f1f1");
                        }
                      });

                  $("body").eq(0).css("font-size", "14px");
                }

                $scope.doFullpage = function() {
                  var dowebokHd = $(".dowebok-hd");
                  $('#dowebok').fullpage({
                        navigation : false,
                        onLeave : function(index, nextIndex, direction) {
                          $scope.setSelMenuBc(nextIndex);
                        }
                      });
                }

                $scope.changeLanguage = function(code) {
                  transl.changeLanguage(code);
                  $scope.getPageContent();
                  $scope.loadService();
                }

                $scope.moveTo = function(section, slide) {
                  var fullpage = $("#dowebok").fullpage;
                  if (fullpage) {
                    fullpage.moveTo(section, slide);
                  }
                }

                $scope.sendContactUs = function() {
                  if ($scope.contactForm.$invalid) {
                    if ($scope.contactForm.name2.$error.required) {
                      alert("请输入名字");
                    } else if ($scope.contactForm.corpname2.$error.required) {
                      alert("请输入公司");
                    } else if ($scope.contactForm.email2.$error.required) {
                      alert("请输入email");
                    } else if ($scope.contactForm.email2.$error.email) {
                      alert("email格式不正确");
                    }

                    return;
                  }

                  var deferred = $q.defer();
                  var data = {};
                  data.name = $("#name2").val();
                  data.corpname = $("#corpname2").val();
                  data.phone = $("#phone2").val();
                  data.email = $("#email2").val();
                  data.address = $("#address2").val();
                  data.message = $("#message2").val();

                  $http({
                        method : 'post',
                        url : 'welcomePage/sendContactUs',
                        data : data
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        if (data) {
                          alert("已发送");

                          $("#name2").val("");
                          $("#corpname2").val("");
                          $("#phone2").val("");
                          $("#email2").val("");
                          $("#address2").val("");
                          $("#message2").val("");
                        }

                        console.log('sendContactUs 请求成功');
                      }, function(data) {
                        console.log('sendContactUs 请求失败');
                      });
                }

                //
                $scope.getPageContent = function() {
                  var langCode = transl.currentLanguageCode();

                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'welcomePage/getWelcomeData/' + langCode
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        if (data && data.data != "") {
                          $scope.pageContent = JSON.parse(data.data);
                        }
                        console.log('getPageContent 请求成功');
                      }, function(data) {
                        console.log('getPageContent 请求失败');
                      });

                  $scope.pageContent = {};
                }
                // load服务列表

                // 模拟点击注册事件
                $scope.pro = function(type,flag) {
                  $scope.proType = type;
                  changeTabActive(flag);
                  $("#chk" + type).attr("checked", "checked");
                  $("#downloadModal_login_regist_event").trigger("click");
                }
                
                proJq = function(type){
                    $scope.proType = type;
                    $("#chk" + type).attr("checked", "checked");
                    $("#downloadModal_login_regist_event").trigger("click");
                }
                
                $scope.login = function() {
					var lancode = $("#lanCode :selected").val();
					transl.changeLanguage(lancode);
					//根据用户名密码到数据库查询是否存在
					var loginAttr = {};
					if($("#username").val() == '' || $("#username").val() == null){
						$("#warn_error_login").text("{{\"welcome_page_login.chk_login_name_null\" | translate}}");
						return;
					}else{
						$("#warn_error_login").html("");
					}
					if($("#password").val() == '' || $("#password").val() == null){
						$("#warn_error_login").text("{{\"welcome_page_login.chk_login_password_null\" | translate}}");
						return;
					}else{
						$("#warn_error_login").html("");
					}
					loginAttr.loginname = $("#username").val();
					loginAttr.password = $("#password").val();
					
					var deferred = $q.defer();
					$http({
	                        method : 'POST',
	                        url : 'userCtl/loginCheck',
	                        data : loginAttr
	                      }).success(function(data) {
	                        deferred.resolve(data);
	                      }).error(function(data) {
	                        deferred.reject(data);
	                      });
					deferred.promise.then(function(data) {
                        console.log('doSourceSave 请求成功');
                        if (data) {
                        	if (data.registResult == '0') {
                        		$("#warn_error_login").html("");
                        		$("#loginForm").submit();
                        	}else if(data.registResult == '1'){
                        		$("#warn_error_login").text("{{\"welcome_page_login.chk_login_user_inexistence\" | translate}}");
                        	}else if(data.registResult == '2'){
                        		$("#warn_error_login").text("{{\"welcome_page_login.chk_login_password_incorrect\" | translate}}");
                        	}
                        } else {
                          $("#downloadModal_regist_fail_event").trigger("click");
                        }
                      }, function(data) {
                        console.log('doSourceSave 请求失败');
                      });					
				};
                
                changeTabActive = function(flag){
                	if(flag == '1'){
                		//注册
                		$("#tab_login").removeAttr("class");
                		$("#tab_reg").attr("class","active");
                		$("#tab1").attr("class","tab-pane");
                		$("#tab2").attr("class","tab-pane active");
                	}else{
                		//登录
                		$("#tab_reg").removeAttr("class");
                		$("#tab_login").attr("class","active");
                		$("#tab2").attr("class","tab-pane");
                		$("#tab1").attr("class","tab-pane active");
                		var lancode = transl.currentLanguageCode();
    					if (lancode) {
    						$("#lanCode").find("option").each(function(i, el) {
    							if ($(el).val() == lancode) {
    								$(el).attr("selected", true);
    							}
    						});
    					}
                	}
                }

               // regist
               $scope.regist = function() {
                  var registAttr = {};
                  var chkStr = "";
                  if ($("input[name='pay_type']:checked").length > 0) {
                    $("#service_pro_check").attr("style", "display:none");
                    $("input[name='pay_type']:checked").each(function() {
                          chkStr += $(this).val() + ",";
                        });
                  } else {
                    $("#service_pro_check").attr("style", "display:block");
                    return;
                  }
                  registAttr.companyname = $scope.companyname;
                  registAttr.realname = $scope.realname;
                  registAttr.email = $scope.email;
                  registAttr.mobile = $scope.mobile;
                  registAttr.address = $scope.address;
                  if (chkStr != "") {
                	  chkStr = chkStr.substring(0, chkStr.length - 1);
                  }
                  registAttr.payType = chkStr;
                  var deferred = $q.defer();
                  $http({
                        method : 'POST',
                        url : 'userCtl/regist',
                        data : registAttr
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        console.log('doSourceSave 请求成功');
                        if (data) {
                          if (data.registResult == '0') {
                        	  $("#downloadModal_regist_fail_event").trigger("click");// 失败
                          } else if (data.registResult == '1') {
                        	  $("#sHtmlText").html(data.sHtmlText);// 支付注册
                          } else if (data.registResult == '2') {
                        	  $("#downloadModal_regist_fail_event").trigger("click");// 普通注册成功
                          } else if (data.registResult == '3') {
                        	  // 用户已存在
                        	  $("#downloadModal_regist_fail_event").trigger("click");
                          }
                        } else {
                        		$("#downloadModal_regist_fail_event").trigger("click");
                        }
                      }, function(data) {
                        console.log('doSourceSave 请求失败');
                      });
                };
                

                // 判断字符串是否为空
                function isEmptyStr(str) {
                  if (str == null || str == '') {
                    return false;
                  }
                  return true;
                }
     	       //定义StringBuffer
     	       function StringBuffer() {
     	    	    this.__strings__ = new Array();
     	    	}
     	    	StringBuffer.prototype.append = function (str) {
     	    	    this.__strings__.push(str);
     	    	    return this;    //方便链式操作
     	    	}
     	    	StringBuffer.prototype.toString = function () {
     	    	    return this.__strings__.join("");
     	    	}

                $scope.getPageContent();
                $scope.doFullpage();
                angular.element(document).ready(function() {
                      $scope.setSelMenuBc(1);
                    });
                
                testEmail = function(email) {
                	var reg = /^([a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*[;；]?)+$/i;
                	return reg.test(email);
				}
                checkEmail = function() {
					if (!testEmail($("#contactUsEmail").val())) {
						$("#emailchk").show();
					} else {
						$("#emailchk").hide();
					}
				}
                $scope.saveContact = function() {
                	if ($scope.contactUs && $scope.contactUs.corpname && $scope.contactUs.phone && $scope.contactUs.name && $scope.contactUs.email) {
                		$("#saveContact").modal('show');
                	}
				}
                $scope.addContactUs = function (){
                	if ($scope.contactUs.corpname && $scope.contactUs.phone && $scope.contactUs.name && $scope.contactUs.email) {
                		$http({data:$scope.contactUs, method:"POST", url:"welcomePage/sendContactUs"}).then(function successCallback(response) {
                			$("#contactUs").modal('hide');
                			$scope.contactUs = {};
                		  }, function errorCallback(response) {
                			  console.log("failure");
                		  });
					}
                }
                
              }]);
    });
