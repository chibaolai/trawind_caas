define(['controllers/controllers'], function(controllers) {
  controllers.controller('dataGridCommonCtrl', ['$scope', '$location', '$q', '$http', 'transl', '$state', '$stateParams', '$compile', '$window', '$timeout',
      function($scope, $location, $q, $http, transl, $state, $stateParams, $compile, $window, $timeout) {
        $scope.dataGridOptions = {};
        $scope.dataGridOptions.data = [];

        $scope.selMappingTmp = function(id, $event) {
          var target = $event.currentTarget;
          $(".mapping-templete-items li").attr('class', '');
          $(target).parent().attr('class', 'active');

          if ($scope.mappingTemplete) {
            $scope.mappingTemplete.selected = id;
          } else {
            $scope.mappingTemplete = {
              selected : id
            };
          }
        }

        $scope.productItemView = function(row) {
          if ($stateParams.url != "" && $stateParams.url.length > 0) {
            if ($stateParams.url === "caas") {
              $state.go($stateParams.url, {
                    formId : row.entity.id,
                    searchId : ''
                  });
            } else if ($stateParams.url === "product") {
              $state.go($stateParams.url, {
                    formId : '',
                    productId : row.entity.id,
                    searchId : ''
                  });
            }
          }
        };

        // 非编辑状态时,页面显示
        function viewToHtml() {
          $("body").removeClass("edit");
          $("body").addClass("devpreview sourcepreview");
          $("#menu-layoutit li button").removeClass("active")
          $(this).addClass("active");

          return false;
        }

        $('.demo').on("click", ".issued", function(e) {
              e.preventDefault();

              if ($scope.dataGridOptions.selectedRows.length == 0) {
                $('#resultModal').find(".result-msg").text("{{\"data_grid_common_page.result_modal.msg_error\" | translate}}");
                $compile($('#resultModal').contents())($scope);

                $timeout(function() {
                      $('#resultModal').modal('show');
                    });
                return;
              }

              if ($scope.mappingTemplete == false || $scope.mappingTemplete.selected.length == null || $scope.mappingTemplete.selected.length == 0) {
                $('#resultModal').find(".result-msg").text("{{\"data_grid_common_page.result_modal.msg_error\" | translate}}");
                $compile($('#resultModal').contents())($scope);

                $timeout(function() {
                      $('#resultModal').modal('show');
                    });
                return;
              }

              data = [];
              angular.forEach($scope.dataGridOptions.selectedRows, function(value, idx) {
                    var row = {
                      productLogId : value.productLogId,
                      productId : value.id,
                      mappingId : $scope.mappingTemplete.selected
                    };

                    data.push(row);
                  });

              var deferred = $q.defer();
              $http({
                    method : 'post',
                    url : 'product/issuedDataLogToPartner',
                    'data' : JSON.stringify(data)
                  }).success(function(data) {
                    deferred.resolve(data);
                  }).error(function(data) {
                    deferred.reject(data);
                  });
              deferred.promise.then(function(data) {

                    $('#resultModal').find(".result-msg").text("{{\"data_grid_common_page.result_modal.msg_save_success\" | translate}}");
                    $compile($('#resultModal').contents())($scope);

                    $timeout(function() {
                          $('#resultModal').modal('show');
                        });

                    console.log('issued click 请求成功');
                  }, function(data) {

                    $('#resultModal').find(".result-msg").text("{{\"data_grid_common_page.result_modal.msg_save_fail\" | translate}}");
                    $compile($('#resultModal').contents())($scope);

                    $timeout(function() {
                          $('#resultModal').modal('show');
                        });

                    console.log('issued click 请求失败');
                  });

            });

        $scope.dataGridOptions = {
          enableRowSelection : true,
          enableRowHeaderSelection : true,
          enableFullRowSelection : false,
          noUnselect : false,
          multiSelect : true,
          modifierKeysToMultiSelect : false,
          enableFiltering : true,
          enableColumnResizing : true,
          onRegisterApi : function(gridApi) {
            $scope.gridApi = gridApi,

            // 行选中事件
            $scope.gridApi.selection.on.rowSelectionChanged($scope, function(row, event) {
                  if ($scope.dataGridOptions.selectedRows == null) {
                    $scope.dataGridOptions.selectedRows = [];
                  }

                  if (row) {
                    var isHas = false;
                    angular.forEach($scope.dataGridOptions.selectedRows, function(value, idx) {
                          if (value.id === row.entity.id) {
                            isHas = true;
                            $scope.dataGridOptions.selectedRows.splice(idx, 1);

                            if (row.isSelected) {
                              $scope.dataGridOptions.selectedRows.push(row.entity);
                            }
                          }
                        });

                    if (!isHas && row.isSelected) {
                      $scope.dataGridOptions.selectedRows.push(row.entity);
                    }
                  }
                }),

            $scope.gridApi.selection.on.rowSelectionChangedBatch($scope, function(rows) {
                  if ($scope.dataGridOptions.selectedRows == null) {
                    $scope.dataGridOptions.selectedRows = [];
                  }

                  if (rows) {
                    angular.forEach(rows, function(row, idx) {
                          if (row.isSelected) {
                            $scope.dataGridOptions.selectedRows.push(row.entity);
                          } else {
                            $scope.dataGridOptions.selectedRows = [];
                          }
                        });
                  }
                })

          },
          columnDefs : [],
          data : [],
          selectedRows : []
        }

        $scope.viewUpdateHistory = function(row) {
          if (row.entity.firstContent === "") {
            return;
          }

          var editorModal = $("#editorModal");

          if (editorModal.length > 0) {
            $scope.updateHistoryGridOptions.data = [];
            editorModal.find(".tmpValue").val(row.entity.id);
            editorModal.modal({
                  keyboard : true
                });
            editorModal.modal('show');
          }
        };

        $scope.updateHistoryGridOptions = {
          enableFiltering : true,
          enableColumnResizing : true,
          onRegisterApi : function(gridApi) {
            $scope.gridApi = gridApi
          },
          columnDefs : [],
          data : []
        };

        $scope.rollbackProductData = function(row) {
          var data = {};
          data.productId = row.entity.id;
          data.primaryId = row.entity.primaryId;
          data.updateTime = row.entity.updateTime;
          data.productLogId = row.entity.productLogId;

          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'product/rollbackProductData/',
                data : data
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {
                viewAllUpdateContentInfo(data);

                console.log('rollbackAttrValByHistory 请求成功');
              }, function(data) {
                console.log('rollbackAttrValByHistory 请求失败');
              });
        };

        function viewAllUpdateContentInfo(data) {
          $scope.updateHistoryGridOptions.columnDefs = [{
                field : 'name',
                enableColumnResizing : true,
                enableFiltering : false,
                enableSorting : false,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}">{{row.entity.name}}</div>',
                displayName : "ProductName"
              }, {
                field : 'primaryId',
                width : 150,
                enableColumnResizing : true,
                enableFiltering : false,
                enableSorting : false,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.primaryId}}">{{row.entity.primaryId}}</div>',
                displayName : "PrimaryId"
              }, {
                field : 'updateTime',
                width : 150,
                enableColumnResizing : true,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
                displayName : "LastUpdateTime"
              }, {
                field : 'content',
                width : 250,
                enableColumnResizing : true,
                enableFiltering : true,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" >{{row.entity.firstContent}}</div>',
                displayName : "UpdateContent"
              }, {
                field : 'rollback',
                width : 80,
                enableColumnResizing : false,
                enableFiltering : false,
                enableSorting : false,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="rollback" ><a class="btn btn-xs btn-primary" ng-click="grid.appScope.rollbackProductData(row)">Rollback</a></div>'
              }];

          $scope.updateHistoryGridOptions.data = data;

          $(".updateHistoryGrid").resize();
        }

        $scope.acceptProductData = function(row) {
          var data = {};
          data.productId = row.entity.id;
          data.primaryId = row.entity.primaryId;
          data.updateTime = row.entity.updateTime;
          data.productLogId = row.entity.productLogId;
          data.corpId = row.entity.corpId;

          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'product/acceptProductData/',
                data : data
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {

                console.log('acceptProductData 请求成功');
              }, function(data) {
                console.log('acceptProductData 请求失败');
              });
        };

        $scope.rejectProductData = function(row) {
          var data = {};
          data.productId = row.entity.id;
          data.primaryId = row.entity.primaryId;
          data.updateTime = row.entity.updateTime;
          data.productLogId = row.entity.productLogId;

          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'product/rejectProductData/',
                data : data
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {

                console.log('rejectProductData 请求成功');
              }, function(data) {
                console.log('rejectProductData 请求失败');
              });
        };

        var createUrl = "";

        // 页面加载
        angular.element(document).ready(function() {
          $(".grid").css("height", ($(window).height() - 100) + "px");
          $(".grid").resize();

          $(window).resize(function() {
                $(".grid").css("height", ($(window).height() - 100) + "px");
              });

          $("#return").attr("href", "#/dashboard");

          $("button.create").on("click", function(e) {
                if (createUrl != "") {
                  $state.go(createUrl, {
                        formId : '',
                        productId : '',
                        searchId : ''
                      });
                }
              });

          var state = $window.history.state;
          if (state && state.absUrl == $location.absUrl() && $stateParams.request.length == null) {
            $stateParams.request = state.request;
            $stateParams.url = state.url;
          }

          if ($location.url().indexOf("/dataGridCommon") > -1 && $stateParams.request != "" && $stateParams.request.length > 0) {

            var stateObj = {
              request : $stateParams.request,
              url : $stateParams.url,
              absUrl : $location.absUrl()
            };
            $window.history.pushState(stateObj, 'dataGrid', $location.absUrl());

            setTimeout(function() {
              // 获取grid数据
              $http.get($stateParams.request).success(function(data, status, headers, config) {

                if ($stateParams.request.startsWith("product/")) {
                  createUrl = "product";

                  if ($stateParams.request.indexOf("getProductLinkItemInfo") > -1) {

                    $scope.dataGridOptions.enableRowSelection = true;

                    $scope.dataGridOptions.columnDefs = [{
                      field : 'name',
                      enableFiltering : true,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}"><a style="cursor: pointer" ng-click="grid.appScope.productItemView(row)">{{row.entity.name}}<a></div>',
                      displayName : "ProductName"
                    }, {
                      field : 'primaryId',
                      enableFiltering : true,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.primaryId}}">{{row.entity.primaryId}}</div>',
                      displayName : "PrimaryId"
                    }, {
                      field : 'updateTime',
                      enableFiltering : true,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
                      displayName : "LastUpdateTime"
                    }, {
                      field : 'content',
                      enableFiltering : false,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" style="cursor: pointer" ng-click="grid.appScope.viewUpdateHistory(row)">{{row.entity.firstContent}}</div>',
                      displayName : "UpdateContent"
                    }];
                    $scope.dataGridOptions.data = data;
                    $scope.dataGridOptions.selectedRows = [];
                  } else if ($stateParams.request.indexOf("getProductAuditingItemInfo") > -1) {

                    $scope.dataGridOptions.enableRowSelection = false;

                    $scope.dataGridOptions.columnDefs = [{
                      field : 'name',
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}"><a style="cursor: pointer" ng-click="grid.appScope.productItemView(row)">{{row.entity.name}}<a></div>',
                      displayName : "ProductName"
                    }, {
                      field : 'updateTime',
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
                      displayName : "LastUpdateTime"
                    }, {
                      field : 'content',
                      enableFiltering : false,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" ng-click="">{{row.entity.firstContent}}</div>',
                      displayName : "UpdateContent"
                    }, {
                      field : 'accept',
                      width : 60,
                      enableColumnResizing : false,
                      enableFiltering : false,
                      enableSorting : false,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="Accept" ><a class="btn btn-xs btn-primary" ng-click="grid.appScope.acceptProductData(row)">Accept</a></div>'
                    }, {
                      field : 'reject',
                      width : 60,
                      enableColumnResizing : false,
                      enableFiltering : false,
                      enableSorting : false,
                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="Reject" ><a class="btn btn-xs btn-primary" ng-click="grid.appScope.rejectProductData(row)">Reject</a></div>'
                    }];
                    $scope.dataGridOptions.data = data;
                    $scope.dataGridOptions.selectedRows = [];
                  }

                } else if ($stateParams.request.startsWith("userForms/")) {
                  createUrl = "caas";

                  $scope.dataGridOptions.enableRowSelection = false;
                  $scope.dataGridOptions.enableRowHeaderSelection = false;

                  $scope.dataGridOptions.columnDefs = [{
                    field : 'name',
                    enableFiltering : true,
                    cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}"><a style="cursor: pointer" ng-click="grid.appScope.productItemView(row)">{{row.entity.name}}<a></div>',
                    displayName : "ProductName"
                  }, {
                    field : 'primaryId',
                    enableFiltering : true,
                    cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.primaryId}}">{{row.entity.primaryId}}</div>',
                    displayName : "PrimaryId"
                  }, {
                    field : 'updateTime',
                    enableFiltering : true,
                    cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
                    displayName : "LastUpdateTime"
                  }, {
                    field : 'content',
                    enableFiltering : false,
                    cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" style="cursor: pointer" ng-click="grid.appScope.viewUpdateHistory(row)">{{row.entity.firstContent}}</div>',
                    displayName : "UpdateContent"
                  }];
                  $scope.dataGridOptions.data = data;
                  $scope.dataGridOptions.selectedRows = [];
                }

              }).error(function(data, status, headers, config) {
                    console.log("获取grid数据异常");
                  });
              $(".grid").css("height", ($(window).height() - 100) + "px");
              $(".grid").resize();
            }, 500);
          }

          $("#editorModal").on('shown.bs.modal', function() {
                $("#editorModal").find(".modal-dialog").css("marginLeft", (($(window).width() - $("#editorModal").find(".modal-content").width()) / 2));

                var deferred = $q.defer();

                var productId = $("#editorModal").find(".tmpValue").val();

                $http({
                      method : 'post',
                      url : 'product/getAllUpdateContentInfo/' + productId
                    }).success(function(data) {
                      deferred.resolve(data);
                    }).error(function(data) {
                      deferred.reject(data);
                    });
                deferred.promise.then(function(data) {
                      viewAllUpdateContentInfo(data);

                      console.log('editorModal 请求成功');
                    }, function(data) {
                      console.log('editorModal 请求失败');
                    });
              });

          $("#editorModal").on('hidden.bs.modal', function() {
              });

          viewToHtml();
        });
      }]);
});
