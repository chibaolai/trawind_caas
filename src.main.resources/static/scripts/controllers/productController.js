define(['controllers/controllers', 'jquery', 'jqueryUI', 'datetimepicker'], function(controllers) {
      controllers.controller('productCtrl', ['$scope', '$location', '$q', '$http', 'changeCssSrv', 'caasSer', '$stateParams', '$state', '$compile', '$interval', '$rootScope', 'transl',
              function($scope, $location, $q, $http, changeCssSrv, caasSer, $stateParams, $state, $compile, $interval, $rootScope, transl) {

                if ($stateParams.formId[0] != undefined) {
                  $scope.specifiedFormId = $stateParams.formId;
                } else {
                  $scope.specifiedFormId = "";
                }
                if ($stateParams.productId[0] != undefined) {
                  $scope.specifiedProductId = $stateParams.productId;
                } else {
                  $scope.specifiedProductId = "";
                }
                $scope.specifiedSearchId = $stateParams.searchId;

                // change theme
                $(function() {
                      changeCssSrv.change(".theme", "bootstrapName");
                    });

                // widgetId - >widgetValue
                $scope.$on("$includeContentLoaded", function(event, templateName) {
                      if ($scope.widget != null) {
                        for (var i = 0; i < $scope.widget.length; i++) {
                          var obj = $scope.widget[i];
                          var s = $scope.widgetIds.split(',');
                          for (var j = 0; j < s.length; j++) {
                            if (s[j] === obj.key) {
                              if (obj.type == "grid") {
                                $(".docuNodes").attr("ng-click", "addGridDataInProduct('" + obj.key + "')");
                                $scope[obj.key].data = angular.copy(JSON.parse(obj.value));
                              } else {
                                $scope[obj.key].defaultVal = obj.value;
                              }
                            }
                          }
                        }
                      }
                      $(".grid").each(function(e, t) {
                            $(t).attr("ui-grid-edit", "ui-grid-edit");
                            // $(t).attr("ui-grid-cellNav", "ui-grid-cellNav");
                            $(t).attr("ui-grid-selection", "ui-grid-selection");
                            $(t).attr("ui-grid-row-edit", "ui-grid-row-edit");
                          });
                      if ($scope.gridIds != null && $scope.gridIds.length > 0) {
                        for (var i = 0; i < $scope.gridIds.length; i++) {
                          var m = 0;
                          $scope[$scope.gridIds[i]].onRegisterApi = function(gridApi) {
                            var gridApiParam = 'gridApi' + gridApi.grid.appScope.gridIds[m];
                            $scope[gridApiParam] = gridApi;
                            m = m + 1;
                          };
                          $scope[$scope.gridIds[i]].enableRowHeaderSelection = false;
                        }
                      }
                      var docuNodesNum = 0;
                      $(".docuNodes").each(function(e, t) {
                            docuNodesNum = docuNodesNum + 1;
                            $(t).attr("ng-click", "addGridDataInProduct('" + docuNodesNum + "',$event)");

                          });
                      if ($(".demo .accordion-toggle").length > 0) {
                        $(".demo .accordion-toggle").each(function(e, t) {
                              if ($(t).hasClass("collapsed")) {
                                $(t).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-plus pull-right");
                              } else {
                                $(t).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-minus pull-right");
                              }
                            });
                      }
                      // tooltip初始化
                      $('body').popover({
                            selector : '[data-toggle="popover"]',
                            trigger : 'hover',
                            placement : 'right'
                          });
                    });
                $scope.addGridDataInProduct = function(val, clickEvent) {
                  clickEvent.stopImmediatePropagation();
                  var addGridCom = clickEvent.target.getAttribute("id");
                  var array = {};
                  for (var i = 0; i < $scope[addGridCom].columnDefs.length; i++) {
                    var displayname = $scope[addGridCom].columnDefs[i].name;
                    array[displayname] = "";
                  }
                  $scope[addGridCom].data.push(array);
                  var gridApiParam = 'gridApi' + addGridCom;
                  $scope[gridApiParam].selection.clearSelectedRows();
                  var m = $scope[addGridCom].data.length - 1;
                  $interval(function() {
                        $scope[gridApiParam].selection.selectRow($scope[addGridCom].data[m]);
                      }, 0, 10);

                  $interval(function() {
                        $(".demo").find("div[ui-grid='" + addGridCom + "']").find(".ui-grid-row").find("input").each(function(e, t) {
                              if ($(t).attr("ng-model") == "row.entity['" + $scope[addGridCom].columnDefs[0].name + "']") {
                                $(t).focus();
                              }
                            });
                      }, 0, 20);
                };

                var tempDeleteRowEntity = {};
                $scope.goToDelete = function(row, gridName) {
                  $scope.tempEntity = {};
                  tempDeleteRowEntity = row.entity;
                  for (var i = 0; i < $scope[gridName].data.length; i++) {
                    if ($scope[gridName].data[i] === tempDeleteRowEntity) {
                      $scope[gridName].data.splice(i, 1);
                      break;
                    }
                  }
                  var gridApiParam = 'gridApi' + gridName;
                  $scope[gridApiParam].selection.clearSelectedRows();
                  var m = $scope[gridName].data.length - 1;
                  $interval(function() {
                        $scope[gridApiParam].selection.selectRow($scope[gridName].data[m]);
                      }, 0, 10);
                  $interval(function() {
                        $(".demo").find("div[ui-grid='" + gridName + "']").find(".ui-grid-row").find("input").each(function(e, t) {
                              if ($(t).attr("ng-model") == "row.entity['" + $scope[gridName].columnDefs[0].name + "']") {
                                $(t).focus();
                              }
                            });
                      }, 0, 20);
                };

                // 模版选择
                $scope.goNewDemo = function(formId) {

                  $state.go("product", {
                        formId : formId,
                        productId : $scope.specifiedProductId,
                        searchId : ''
                      });
                }

                $scope.setCommandItems = function(formObj, elem) {

                  if ($scope.command == null) {
                    $scope.command = {
                      all : [],
                      selCommand : [],
                      menu : []
                    };
                  }

                  if (elem.length == 0) {
                    return;
                  }

                  var htmlStr = "";
                  if (formObj && formObj.command) {
                    var all = JSON.parse(formObj.command);
                    $scope.command.menu = [];
                    angular.forEach(all, function(val, idx) {
                          var name = val.name.en;
                          for (var n in val.name) {
                            var lang = transl.currentLanguageCode();
                            if (lang == 'zh') {
                              lang = 'cn'
                            }
                            if (n == lang) {
                              name = val.name[n];
                              break;
                            }
                          }
                          $scope.command.menu.push(val);

                          var checkBoxItem = '<li class=""><a rel="" href="#" ng-click="clickCommandItem(\'' + val.key + '\')">' + name + '</a></li>';
                          htmlStr = htmlStr + checkBoxItem;
                        });

                    elem.html(htmlStr);
                    $compile(elem.contents())($scope);
                  }
                };

                $scope.clickCommandItem = function(key) {
                  if (key) {
                    switch (key) {
                      case "edit" :

                        $("#save").removeAttr("disabled");

                        $("#coverdiv").hide();
                        $(document).off('keydown');
                        break;
                      case "issued" :
                        $("#issuedEdit").modal('show');
                        break;
                    }
                  }
                }

                $('#issuedEdit').on('show.bs.modal', function() {
                      var data = {};
                      var deferred = $q.defer();
                      var distributor = $("#issuedEdit").find(".distributor-items");
                      distributor.empty();

                      data.productId = $scope.specifiedProductId;
                      $http({
                            method : 'post',
                            url : 'product/getAllDistributorAndSelected',
                            data : JSON.stringify(data)
                          }).success(function(data) {
                            deferred.resolve(data);
                          }).error(function(data) {
                            deferred.reject(data);
                          });
                      deferred.promise.then(function(data) {
                            if (data) {
                              angular.forEach(data, function(val, idx) {
                                    var selected = val.selected;
                                    var item = val.data;

                                    var checkBoxItem = '<label style="margin: 10px;"><input type="checkbox" value="' + item.id + '" />' + item.name + '</label>';
                                    if (selected == true) {
                                      checkBoxItem = '<label style="margin: 10px;"><input type="checkbox" value="' + item.id + '" checked="checked"/>' + item.name + '</label>';
                                    }

                                    $(distributor).append(checkBoxItem);
                                  });
                            }

                            console.log('getAllDistributorAndSelected 请求成功');
                          }, function(data) {
                            console.log('getAllDistributorAndSelected 请求失败');
                          });
                    })

                //保存发布信息
                $scope.saveIssuedInfo = function() {
                  var deferred = $q.defer();
                  var distributor = $("#issuedEdit").find(".distributor-items");
                  var checkBoxs = $(distributor).find("[type='checkbox']");
                  var selected = "";
                  angular.forEach(checkBoxs, function(value) {
                        if ($(value).is(":checked")) {
                          selected = selected + $(value).val() + ",";
                        }
                      });

                  var data = {};
                  data.selected = selected;
                  data.productId = $scope.specifiedProductId;

                  $http({
                        method : 'post',
                        url : 'product/setDistributor',
                        data : JSON.stringify(data)
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        distributor.empty();
                        $("#issuedEdit").modal('hide');

                        console.log('saveIssuedInfo 请求成功');
                      }, function(data) {
                        console.log('saveIssuedInfo 请求失败');
                      });
                }

                // 交互组件生成ID
                function handleJsIds() {
                  // 遮盖窗体
                  // handleModalIds();
                  // 手风琴
                  handleAccordionIds();
                }

                // 手风琴
                function handleAccordionIds() {
                  var e = $(".demo #myAccordion");
                  var t = randomNumber();
                  var n = "accordion-" + t;
                  var r;
                  e.attr("id", n);
                  e.find(".accordion-group").each(function(e, t) {
                        r = "accordion-element-" + randomNumber();
                        $(t).find(".accordion-toggle").each(function(e, t) {
                              $(t).attr("data-parent", "#" + n);
                              $(t).attr("href", "#" + r)
                            });
                        $(t).find(".accordion-body").each(function(e, t) {
                              $(t).attr("id", r)
                            })
                      })
                }
                // 判断浏览器是否支持html5本地保存
                function supportstorage() {
                  if (typeof window.localStorage == 'object')
                    return true;
                  else
                    return false;
                }
                // 定时保存页面布局
                function handleSaveLayout() {
                  var e = $(".demo").html();
                  if (!stopsave && e != window.demoHtml) {
                    stopsave++;
                    window.demoHtml = e;
                    // saveLayout();
                    stopsave--;
                  }
                }

                var layouthistory;
                // 保存布局
                // function saveLayout() {
                // var data = layouthistory;
                // if (!data) {
                // data = {};
                // data.count = 0;
                // data.list = [];
                // }
                // if (data.list.length > data.count) {
                // for (i = data.count; i < data.list.length; i++)
                // data.list[i] = null;
                // }
                // data.list[data.count] = window.demoHtml;
                // data.count++;
                // if (supportstorage()) {
                // localStorage.setItem("layoutdata", JSON.stringify(data));
                // }
                // layouthistory = data;
                // }
                // header下载按钮
                function downloadLayout() {

                  $.ajax({
                        type : "POST",
                        url : "/build/downloadLayout",
                        data : {
                          layout : $('#download-layout').html()
                        },
                        success : function(data) {
                          window.location.href = '/build/download';
                        }
                      });
                }

                function downloadHtmlLayout() {
                  $.ajax({
                        type : "POST",
                        url : "/build/downloadLayout",
                        data : {
                          layout : $('#download-layout').html()
                        },
                        success : function(data) {
                          window.location.href = '/build/downloadHtml';
                        }
                      });
                }
                // header撤销按钮
                function undoLayout() {
                  var data = layouthistory;
                  // console.log(data);
                  if (data) {
                    if (data.count < 2)
                      return false;
                    window.demoHtml = data.list[data.count - 2];
                    data.count--;
                    $('.demo').html(window.demoHtml);
                    if (supportstorage()) {
                      localStorage.setItem("layoutdata", JSON.stringify(data));
                    }
                    return true;
                  }
                  return false;
                }
                // header 重做
                function redoLayout() {
                  var data = layouthistory;
                  if (data) {
                    if (data.list[data.count]) {
                      window.demoHtml = data.list[data.count];
                      data.count++;
                      $('.demo').html(window.demoHtml);
                      if (supportstorage()) {
                        localStorage.setItem("layoutdata", JSON.stringify(data));
                      }
                      return true;
                    }
                  }
                  return false;
                }
                // 随机数-》用于生成id
                function randomNumber() {
                  return randomFromInterval(1, 1e6)
                }

                function randomFromInterval(e, t) {
                  return Math.floor(Math.random() * (t - e + 1) + e)
                }

                function gridSystemGenerator() {
                  $(".lyrow .preview input").bind("keyup", function() {
                        var e = 0;
                        var t = "";
                        var n = $(this).val().split(" ", 12);
                        $.each(n, function(n, r) {
                              e = e + parseInt(r);
                              t += '<div class="span' + r + ' column"></div>'
                            });
                        if (e == 12) {
                          $(this).parent().next().children().html(t);
                          $(this).parent().prev().show()
                        } else {
                          $(this).parent().prev().hide()
                        }
                      })
                }
                // 拖拽后内容 编辑的各种事件
                function configurationElm(e, t) {
                  $(".demo").delegate(".configuration > a", "click", function(e) {
                        e.preventDefault();
                        var t = $(this).parent().next().next().children();
                        $(this).toggleClass("active");
                        t.toggleClass($(this).attr("rel"))
                      });
                  $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
                        e.preventDefault();
                        var t = $(this).parent().parent();
                        var n = t.parent().parent().next().next().children();
                        t.find("li").removeClass("active");
                        $(this).parent().addClass("active");
                        var r = "";
                        t.find("a").each(function() {
                              r += $(this).attr("rel") + " "
                            });
                        t.parent().removeClass("open");
                        n.removeClass(r);
                        n.addClass($(this).attr("rel"))
                      })
                }
                // 删除元素
                function removeElm() {
                  $(".demo").delegate(".remove", "click", function(e) {
                        e.preventDefault();
                        $(this).parent().remove();
                        if (!$(".demo .lyrow").length > 0) {
                          clearDemo()
                        }
                        var el = $(this).parent("div").find("input:last");
                        if (el) {
                          idList.remove(el.attr("id"));
                          delete labelValues[el.attr("id")];
                        }
                      })
                }
                // 清空Container
                function clearDemo() {
                  $(".demo").empty();
                  layouthistory = null;
                  if (supportstorage())
                    localStorage.removeItem("layoutdata");
                }
                // 切换header导航 active状态
                function removeMenuClasses() {
                  $("#menu-layoutit li button").removeClass("active")
                }
                // 更改生成html5代码
                function changeStructure(e, t) {
                  $("#download-layout ." + e).removeClass(e).addClass(t)
                }
                // 删除行模板代码
                function cleanHtml(e) {
                  $(e).parent().append($(e).children().html())
                }
                // 下载页面布局
                function downloadLayoutSrc() {
                  var e = "";
                  $("#download-layout").children().html($(".demo").html());
                  var t = $("#download-layout").children();
                  t.find(".preview, .configuration, .drag, .remove").remove();
                  t.find(".lyrow").addClass("removeClean");
                  t.find(".box-element").addClass("removeClean");
                  t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
                        cleanHtml(this)
                      });
                  t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
                        cleanHtml(this)
                      });
                  t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
                        cleanHtml(this)
                      });
                  t.find(".lyrow .lyrow .removeClean").each(function() {
                        cleanHtml(this)
                      });
                  t.find(".lyrow .removeClean").each(function() {
                        cleanHtml(this)
                      });
                  t.find(".removeClean").each(function() {
                        cleanHtml(this)
                      });
                  t.find(".removeClean").remove();
                  $("#download-layout .column").removeClass("ui-sortable");
                  $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
                  if ($("#download-layout .container").length > 0) {
                    changeStructure("row-fluid", "row")
                  }
                  var labelValue = '';
                  for (var i = 0; i < idList.length; i++) {
                    labelValue = $("#" + idList[i]).prev("input").val();
                    $("#" + idList[i]).prev("input").remove();
                    $("#" + idList[i]).before("<label class='control-label' for='" + idList[i] + "'>" + labelValue + "</label>");
                  }

                  formatSrc = $.htmlClean($("#download-layout").html(), {
                        format : true,
                        allowedAttributes : [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"], ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"], ["data-slide-to"],
                            ["data-slide"], ["style"]]
                      });
                }

                // 定时保存生成html5代码，缓存作用
                var timerSave = 1000;
                // 
                var stopsave = 0;
                // 
                var startdrag = 0;
                var demoHtml = $(".demo").html();
                // 
                var currenteditor = null;

                var index = 0;
                var idList = new Array();
                var labelValues = {};
                // 自适应
                $(window).resize(function() {
                      $("body").css("min-height", $(window).height() - 90);
                      $(".demo").css("min-height", $(window).height() - 160)
                    });
                // 从客户端本地获取旧布局
                function restoreData() {
                  if (supportstorage()) {
                    layouthistory = JSON.parse(localStorage.getItem("layoutdata"));
                    if (!layouthistory)
                      return false;
                    window.demoHtml = layouthistory.list[layouthistory.count - 1];
                    if (window.demoHtml)
                      $(".demo").html(window.demoHtml);
                  }
                }

                // Array.prototype.remove = function(val) {
                // var index = this.indexOf(val);
                // if (index > -1) {
                // this.splice(index, 1);
                // }
                // };
                $scope.idList = [];
                function bindEvent(fieldId) {
                  idList.push(fieldId);
                  $scope.idList = idList;
                  if (supportstorage) {
                    window.localStorage.setItem("idList", idList);
                  }
                }

                // 初始化容器
                function initContainer() {
                  // 初始化容器内元素(添加方向，排版等按钮事件)
                  configurationElm();
                }

                $scope.lockKey = function(evt) {
                  var e = evt || window.event, key = e.keyCode;
                  if ((key == 116) || (e.ctrlKey && key == 82) || (e.ctrlKey && key == 65) || (key == 9) || (key == 38) || (key == 40)) {
                    try {
                      e.keyCode = 0;
                    } catch (_) {
                    };
                    e.preventDefault ? e.preventDefault() : e.returnValue = false;
                  };
                }

                // 页面加载
                angular.element(document).ready(function() {
                      $("body").css("min-height", $(window).height() - 90);
                      $(".demo").css("min-height", $(window).height() - 160);
                      $(".sidebar-nav .lyrow").draggable({
                            connectToSortable : ".demo",
                            helper : "clone",
                            handle : ".drag",
                            start : function(e, t) {
                              if (!startdrag)
                                stopsave++;
                              startdrag = 1;
                            },
                            drag : function(e, t) {
                              t.helper.width(400);
                            },
                            stop : function(e, t) {
                              $(".demo .column").sortable({
                                    opacity : .35,
                                    connectWith : ".column",
                                    start : function(e, t) {
                                      if (!startdrag)
                                        stopsave++;
                                      startdrag = 1;
                                    },
                                    stop : function(e, t) {
                                      if (stopsave > 0)
                                        stopsave--;
                                      startdrag = 0;
                                      t.item.find(".view input:last").attr("id", "item" + index);
                                      bindEvent("item" + index);
                                      index++;
                                    }
                                  });
                              if (stopsave > 0)
                                stopsave--;
                              startdrag = 0;
                            }
                          });
                      $(".sidebar-nav .box").draggable({
                            connectToSortable : ".column",
                            helper : "clone",
                            handle : ".drag",
                            start : function(e, t) {
                              if (!startdrag)
                                stopsave++;
                              startdrag = 1;
                            },
                            drag : function(e, t) {
                              t.helper.width(400)
                            },
                            stop : function() {
                              handleJsIds();
                              if (stopsave > 0)
                                stopsave--;
                              startdrag = 0;
                            }
                          });
                      initContainer();

                      // 保存表单
                      $('#body').on("click", "[data-target=#formSaveEditorModal]", function(e) {
                            $("#newFormName").val("Form_" + randomNumber());
                          });
                      $('body.edit .demo').on("click", "[data-target=#editorModal]", function(e) {
                            e.preventDefault();
                            currenteditor = $(this).parent().parent().find('.view');
                            var eText = currenteditor.html();
                            contenthandle.setData(eText);
                          });
                      $("#savecontent").click(function(e) {
                            e.preventDefault();
                            currenteditor.html(contenthandle.getData());
                          });
                      $("[data-target=#downloadModal]").click(function(e) {
                            e.preventDefault();
                            downloadLayoutSrc();
                          });
                      $("[data-target=#shareModal]").click(function(e) {
                            e.preventDefault();
                            handleSaveLayout();
                          });
                      $("#download").click(function() {
                            downloadLayout();
                            return false
                          });
                      $("#downloadhtml").click(function() {
                            downloadHtmlLayout();
                            return false
                          });
                      $("#edit").click(function() {
                            $("body").removeClass("devpreview sourcepreview");
                            $("body").addClass("edit");
                            removeMenuClasses();
                            $(this).addClass("active");
                            return false
                          });
                      $("#clear").click(function(e) {
                            e.preventDefault();
                            clearDemo()
                          });
                      $("#devpreview").click(function() {
                            $("body").removeClass("edit sourcepreview");
                            $("body").addClass("devpreview");
                            removeMenuClasses();
                            $(this).addClass("active");
                            return false
                          });
                      $("#sourcepreview").click(function() {
                            $("body").removeClass("edit");
                            $("body").addClass("devpreview sourcepreview");
                            removeMenuClasses();
                            $(this).addClass("active");
                            for (var p in labelValues) {
                              if (typeof(labelValues[p]) != "function") {
                                if ($("#" + p).prev("label")) {
                                  $("#" + p).prev("label").remove();
                                }
                                $("#" + p).prev("input").remove();
                                $("#" + p).before("<label class='control-label'>" + labelValues[p] + "</label>");
                              }
                            }
                            return false
                          });
                      $("#fluidPage").click(function(e) {
                            e.preventDefault();
                            changeStructure("container", "container-fluid");
                            $("#fixedPage").removeClass("active");
                            $(this).addClass("active");
                            downloadLayoutSrc()
                          });
                      $("#fixedPage").click(function(e) {
                            e.preventDefault();
                            changeStructure("container-fluid", "container");
                            $("#fluidPage").removeClass("active");
                            $(this).addClass("active");
                            downloadLayoutSrc()
                          });
                      $(".nav-header").click(function() {
                            $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
                            $(this).next().slideDown()
                          });
                      $('#undo').click(function() {
                            stopsave++;
                            if (undoLayout())
                              initContainer();
                            stopsave--;
                          });
                      $('#redo').click(function() {
                            stopsave++;
                            if (redoLayout())
                              initContainer();
                            stopsave--;
                          });

                      // 自定义下拉列表 编辑按钮按下 Start
                      $('body.edit .demo').on("click", "[data-target=#dropdownListModifyModal]", function(e) {
                            e.preventDefault();

                            var editor = content.controls[0].custom[0].dropdownList[0].editor;
                            var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
                            var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
                            var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

                            $('#dropdownListModifyModal').empty();
                            $('#dropdownListModifyModal').prepend($("#dropdownListInitModal").html());

                            currenteditor = $(this).parent().parent().find('.view');

                            var currentItemName = currenteditor.find("[class='btn']").find("lable:first");
                            var currentMenus = currenteditor.find("[class='dropdown-menu']");

                            // 项目名称
                            var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='" + itemNamePlaceholder + "']");
                            editorItemName.attr("value", currentItemName.text());

                            // 下拉列表项目
                            var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
                            var editorMenuTmp = $(editorMenuOld).clone();
                            $(editorMenuOld).empty();
                            var itemCount = 0;

                            currentMenus.find("li").each(function(e, t) {
                                  var editorMenuNew = editorMenuTmp.clone();
                                  var textContent = $.trim(t.textContent);
                                  var val = $.trim($(t).find("a").attr("value"));
                                  editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr("value", textContent);
                                  editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']").attr("value", val);

                                  $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);

                                  itemCount++;
                                });

                            if (itemCount == 0) {
                              var editorMenuNew = editorMenuTmp.clone();
                              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr("value", "");
                              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']").attr("value", "");

                              $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);
                            }
                          });

                      if ($scope.specifiedProductId != "") {

                        $("#save").attr("disabled", "disabled");

                        $(document).on('keydown', $scope.lockKey);
                      } else {
                        $("#coverdiv").hide();

                        $("#save").removeAttr("disabled");

                        $(document).off('keydown');
                      }
                      $("#commandMenu").show();

                      $(".command-items").css("z-index", "2000");
                      $("#tempListMenuId").css("z-index", "2000");

                      $scope.setNavStyle(null, "menu_product");

                      removeElm();
                      gridSystemGenerator();
                      setInterval(function() {
                            handleSaveLayout()
                          }, timerSave);

                    });

                // 自定义下拉列表 编辑画面里保存按下的场合 Start
                function doSavecontentForDropdownList() {
                  // if (content.length == 0) {
                  // doUpdateItemFromJSON();
                  // }
                  var view = content.controls[0].custom[0].dropdownList[0].view;
                  var editor = content.controls[0].custom[0].dropdownList[0].editor;
                  var items = view[0].items;
                  var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
                  var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
                  var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

                  var currentItemName = $(currenteditor).find("[class='btn']");
                  var currentMenus = $(currenteditor).find("[class='dropdown-menu']");

                  // 项目名称
                  var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='" + itemNamePlaceholder + "']");
                  if ($.trim(editorItemName.val()) == "") {
                    $(currentItemName).find("lable:first").text("项目名称");
                  } else {
                    $(currentItemName).find("lable:first").text($.trim(editorItemName.val()));
                  }
                  $(currentItemName).find("lable:last").text("");

                  // 下拉列表项目
                  $(currentMenus).empty();
                  var itemCount = 0;

                  var editorMenus = $('#dropdownListModifyModal').find("[class='view clearfix']");
                  $(editorMenus).each(function(e, t) {
                        var text = "";
                        var val = "";

                        var textObj = $(t).find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']");
                        if (textObj.length > 0) {
                          text = $.trim(textObj.val());
                        } else {
                          return true; // false时相当于break, true 就相当于continure。
                        }

                        var valObj = $(t).find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']");
                        if (valObj.length > 0) {
                          val = $.trim(valObj.val());
                        } else {
                          return true; // false时相当于break, true 就相当于continure。
                        }

                        if (text != "") {
                          $(currentMenus).append("<li><a href=\"#\" value=\"" + val + "\">" + text + "</a></li>");
                          itemCount++;
                        }
                      });

                  if (itemCount == 0) {
                    $.each(items, function(idx, item) {
                          $(currentMenus).append("<li><a href=\"#\" value=\"" + item.value + "\">" + item.text + "</a></li>");
                        });
                  }

                  $(currentMenus).find("li").click(function(e) {
                        doClickByAForDropdownList(e);
                      });

                  $("#closeForDropdownListModal").click();
                  return false;
                }
                // 自定义下拉列表 编辑画面里保存按下的场合 End

                // 自定义下拉列表 Li -> a -> Click Start
                function doClickByAForDropdownList(e) {
                  var selText = ":" + e.currentTarget.textContent;
                  $(e.currentTarget).parent().parent().find("button[class='btn'][data-toggle='dropdown']").find("lable:last").text(selText);

                  return false;
                }
                // 自定义下拉列表 Li -> a -> Click End

                // 自定义下拉列表 添加项目 Start
                function doAddItemForDropdownList(t) {
                  // if (content.length == 0) {
                  // doUpdateItemFromJSON();
                  // }
                  var editor = content.controls[0].custom[0].dropdownList[0].editor;
                  var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
                  var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

                  var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
                  var editorMenuTmp = $(editorMenuOld).clone();
                  editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr("value", "");
                  editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']").attr("value", "");

                  $(t).parent().parent().after(editorMenuTmp);

                  return false;
                }
                // 自定义下拉列表 添加项目 End

                // 自定义下拉列表 删除项目 Start
                function doDelItemForDropdownList(t) {
                  if ($(t).parent().parent().parent().find("[class='view clearfix']").length > 4) {
                    $(t).parent().parent().remove();
                  }

                  return false;
                }
                // 自定义下拉列表 删除项目 End

                // 预览按钮按下
                $scope.doSourcePreview = function() {

                  $("body").removeClass("edit");
                  $("body").addClass("devpreview sourcepreview");
                  $("#menu-layoutit li button").removeClass("active");
                  $(this).addClass("active");
                  var t = $(".demo").children();
                  t.each(function(i, e) {
                        $(e).find("div.view").css("padding-top", "0px");
                        $(e).find("div.form-group").css("margin-bottom", "0px");
                        $(e).find(".view .row.clearfix .column").css("padding-top", "0px").css("padding-bottom", "5px");
                        $(e).find("[contenteditable='true']").attr("contenteditable", "false");

                        $(e).find("div.row.clearfix div").find(".box.box-element.ui-draggable.ng-isolate-scope").css("marginLeft", "20px");
                      });

                  return false;
                };

                // 编辑按钮按下
                $scope.doSourceEdit = function() {
                  // if ($("body").hasClass("edit") == false) {
                  $("body").removeClass("devpreview sourcepreview");
                  $("body").addClass("edit");
                  $("#menu-layoutit li button").removeClass("active");
                  $(this).addClass("active");
                  var t = $(".demo").children();
                  t.each(function(i, e) {
                        $(e).find("div.view").css("padding-top", "30px");
                        $(e).find("div.form-group").css("margin-bottom", "15px");
                        $(e).find(".view .row.clearfix .column").css("padding-top", "19px").css("padding-bottom", "19px");
                        $(e).find("[contenteditable='false']").attr("contenteditable", "true");

                        $(e).find(".row-fluid").children().find(".box.box-element.ui-draggable").css("marginLeft", "0px");
                      });
                  // }

                  return false;
                };

                $scope.mcGridOptions = {};
                $scope.mcGridOptions.columnDefs = [{
                      field : 'attributeId',
                      width : 185,
                      displayName : "attributeName"
                    }, {
                      field : 'previousValue',
                      width : 185,
                      displayName : "previousValue"
                    }, {
                      field : 'currentValue',
                      width : 185,
                      displayName : "currentValue"
                    }];

                $("#productCoverSave").click(function() {
                      $scope.doSourceSave(true);
                    });
                $("#productSimpleSave").click(function() {
                      $scope.doSourceSave(false);
                    });
                $('#saveForm').click(function(e) {
                      $scope.doSourceSave(false);
                    });

                // 保存
                $scope.doSourceSave = function(overWriteFlag) {

                  var productAndDataReq = {};
                  var productDataJson = new Object();
                  productAndDataReq.formId = $scope.specifiedFormId;
                  productAndDataReq.productId = $scope.specifiedProductId;
                  productAndDataReq.timeStamp = $scope.timeStamp;// 时间戳 用于二次提交
                  productAndDataReq.overWriteFlag = overWriteFlag;// 是否覆盖更新
                  productAndDataReq.primaryId = $("#DXUQ7278").val();
                  productAndDataReq.productName = $("#SYFF2714").val();

                  // key -> widgetId value -> 属性值 type -> 属性类型
                  var key = "key";
                  var value = "value";
                  var type = "type";

                  var columnDefs = [];
                  $(".demo input").each(function() {
                        var keyName = $(this).attr("id");
                        if (keyName != undefined) {
                          var cul = {};
                          if ($(this).attr("type") == "checkbox") {
                            if ($("#" + keyName).is(":checked")) {
                              cul[key] = $(this).attr("id");
                              cul[value] = true;
                            } else {
                              cul[key] = $(this).attr("id");
                              cul[value] = false;
                            }
                            cul[type] = "checkbox";
                          } else if ($(this).attr("hidden") != "hidden" && $(this).attr("type") != "hidden" && $(this).attr("type") != "button") {
                            cul[key] = $(this).attr("id");
                            cul[value] = $(this).val();
                            cul[type] = "input";
                          }
                          columnDefs.push(cul);
                        }
                      })
                  $(".demo textarea").each(function() {
                        var keyName = $(this).attr("id");
                        if (keyName != undefined) {
                          var cul = {};
                          cul[key] = $(this).attr("id");
                          cul[value] = $(this).val();
                          cul[type] = "textarea";
                          columnDefs.push(cul);
                        }
                      })
                  $(".demo select").each(function() {
                        var keyName = $(this).attr("id");
                        if (keyName != undefined) {
                          var cul = {};
                          cul[key] = $(this).attr("id");
                          cul[value] = $(this).val();
                          if ($(this).val().indexOf("string:") > -1) {
                            cul[value] = $(this).val().substr(7);
                          }
                          cul[type] = "select";
                          columnDefs.push(cul);
                        }
                      })
                  // grid
                  $(".demo .grid").each(function() {
                        var keyName = $(this).attr("ui-grid");
                        if (keyName != undefined) {

                          var cul = {};
                          var gridData = [];
                          cul[key] = $(this).attr("ui-grid");
                          // value
                          var culumArr = [];
                          var gridObj = new Object();
                          var gridDataLength = $scope[keyName].data.length;
                          for (var d = 0; d < gridDataLength; d++) {
                            var cumlVal = $scope[keyName].data[d];
                            var array = {};
                            for (var i = 0; i < $scope[keyName].columnDefs.length; i++) {
                              var displayname = $scope[keyName].columnDefs[i].name;
                              if (displayname != "action") {
                                array[displayname] = cumlVal[displayname];
                              }
                            }
                            culumArr.push(array);
                          }
                          gridObj = culumArr;
                          cul[value] = JSON.stringify(gridObj);
                          cul[type] = "grid";
                          columnDefs.push(cul);
                        }
                      });
                  var form = new FormData();
                  $("div[name='imgCtlDiv']").each(function() {
                        var widgetId = $(this).find("input[name='widgetId']").val();
                        var innerAttrs = [];
                        for (var i = 0; i < $scope[widgetId].fileList.length; i++) {
                          var attr = {};
                          attr.fileName = $scope[widgetId].fileList[i].file.source.source.name;
                          attr.innerAttrs = $scope[widgetId].fileList[i].innerAttrs;
                          innerAttrs.push(attr);
                          form.append("file", $scope[widgetId].fileList[i].file.source.source);
                        }
                        var cul = {};
                        cul[value] = innerAttrs;
                        cul[key] = widgetId;
                        cul[type] = "image";
                        columnDefs.push(cul);
                      });

                  productDataJson.widget = columnDefs;
                  productAndDataReq.productDataJson = columnDefs;
                  form.append("productAndDataReq", JSON.stringify(productAndDataReq));
                  // 直接存一个json串
                  var deferred = $q.defer();

                  $http({
                        method : 'POST',
                        url : 'product/saveProductAndData',
                        data : form,
                        headers : {
                          'Content-Type' : undefined
                        }
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        console.log('doSourceSave 请求成功');
                        if (data) {
                          if (JSON.parse(data).mchangedAttrs) {
                            var mcArrs = [];
                            for (var m = 0; m < JSON.parse(data).mchangedAttrs.length; m++) {
                              var mcArr = {};
                              mcArr.attributeId = JSON.parse(data).mchangedAttrs[m].attributeId;
                              mcArr.previousValue = JSON.parse(data).mchangedAttrs[m].matrtributeID.previousValue;
                              mcArr.currentValue = JSON.parse(data).mchangedAttrs[m].matrtributeID.currentValue;
                              mcArrs.push(mcArr);// ui-grid data push
                            }

                            $scope.mcGridOptions.data = angular.copy(mcArrs);
                          }
                          $("#formReSaveEditorResultModalhh").trigger('click');
                        } else {
                          $('#formSaveEditorResultModalhh').trigger('click');
                        }
                      }, function(data) {
                        console.log('doSourceSave 请求失败');
                      });
                };
                $scope.imgAttr = [];
                $rootScope.addImageAttr = function(file, widgetId) {
                  $("#imgEdit").modal('show');
                  if ($scope[widgetId] && $scope[widgetId].innerAttrs != null) {
                    $scope.imgAttr = [];
                    for (var i = 0; i < $scope[widgetId].innerAttrs.length; i++) {
                      var item = {};
                      item = JSON.parse($scope[widgetId].innerAttrs[i].direcJson);
                      item.widgetId = $scope[widgetId].innerAttrs[i].id;
                      item.type = $scope[widgetId].innerAttrs[i].widgetType;
                      item.isForm = "imgAttr";
                      if ($scope[widgetId].fileList != undefined && $scope[widgetId].fileList != null) {
                        var fileObj;
                        for (var j = 0; j < $scope[widgetId].fileList.length; j++) {
                          fileObj = $scope[widgetId].fileList[j];
                          if (fileObj.file.id == file.id) {
                            for (var k = 0; k < fileObj.innerAttrs.length; k++) {
                              angular.forEach(fileObj.innerAttrs[k], function(value, key) {
                                    if (key == $scope[widgetId].innerAttrs[i].id) {
                                      item.defaultVal = value;
                                    }
                                  });
                            }
                          }
                        }
                      }
                      $scope[$scope[widgetId].innerAttrs[i].id] = item;
                      $scope.imgAttr.push(item);
                      $scope.$apply();
                    }
                  }
                  $scope.imgWidgetId = widgetId;
                  $scope.fileEdit = file;
                };
                $scope.saveImgAttr = function() {
                  if ($scope.imgWidgetId && $scope.imgAttr) {
                    var fileList = [];
                    var fileObj = {};
                    var innerAttrs = [];
                    var attrs = {};
                    fileObj.file = $scope.fileEdit;
                    for (var i = 0; i < $scope.imgAttr.length; i++) {
                      attrs = {};
                      if ($scope.imgAttr[i].type == "input") {
                        attrs[$scope.imgAttr[i].widgetId] = $("#" + $scope.imgAttr[i].widgetId).val();
                        innerAttrs.push(attrs);
                      } else if ($scope.imgAttr[i].type == "select") {

                      }
                    }
                    fileObj.innerAttrs = innerAttrs;
                    fileList.push(fileObj);
                    $scope[$scope.imgWidgetId].fileList = fileList;
                  }
                };
              }]);
    });
