define(['controllers/controllers'], function(controllers) {
      controllers.controller("accountCtrl", ['$scope', '$location', '$q', '$http', '$compile', function($scope, $location, $q, $http, $compile) {
    	  	
    	 //http get 现金金额 充值记录 提现记录   所购项目列表
    	//所购产品grid 产品名称 产品购买时间 产品到期时间 倒计时 自动续费/续费周期 操作 （续费/修改自动续费）  
      	$scope.prodGridOptions = {};
        $scope.prodGridOptions.columnDefs = [
            { field:'productName', width:185, displayName:"产品名称" },
            { field:'proStartTime', width:185, displayName:"产品购买时间" },
            { field:'proEndTime', width:185, displayName:"产品到期时间" },
            { field:'countDown', width:185, displayName:"倒计时" },
            { field:'automaticRenewalOrcycle', width:185, displayName:"自动续费/续费周期" },
            { field:'action', width:185, displayName:"操作" }
        ];
        $scope.prodGridOptions.data = [];
        //充值记录
      	$scope.rechargeGridOptions = {};
        $scope.rechargeGridOptions.columnDefs = [
            { field:'rechargeTime', width:185, displayName:"日期" },
            { field:'rechargeAmount', width:185, displayName:"充值金额" },
            { field:'rechargeArrivalAmount', width:185, displayName:"到账金额" }
        ];
        $scope.rechargeGridOptions.data = [];
        
        //提现记录
        $scope.withdrawGridOptions = {};
        $scope.withdrawGridOptions.columnDefs = [
            { field:'withdrawTime', width:185, displayName:"日期" },
            { field:'withdrawAmount', width:185, displayName:"提现金额" },
            { field:'withdrawArrivalAmount', width:185, displayName:"到账金额" }
        ];
        $scope.withdrawGridOptions.data = [];     

        
        //充值提交
        $scope.rechargeSubmit = function(){
        	var rechargeAmout = $("#inputAmount").val;
        	if(rechargeAmout == null || rechargeAmout == ''){
        		//给出提示
        	}
            var rechargeSeq = {};
            rechargeSeq.rechargeAmout = rechargeAmout;
            var deferred = $q.defer();
            $http({
                  method : 'POST',
                  url : 'accountCtl/getAlipayHtml',
                  data : rechargeSeq
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            deferred.promise.then(function(data) {
                  console.log('doSourceSave 请求成功');
                  if (data) {
                	  //html渲染
                	  $("#sHtmlText").html(data);
                  }
                }, function(data) {
                  console.log('doSourceSave 请求失败');
                });
        	
        };
     }]);
});
