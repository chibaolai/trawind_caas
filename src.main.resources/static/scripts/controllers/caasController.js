define(['controllers/controllers', 'jquery', 'jqueryUI', 'datetimepicker'], function(controllers) {
      controllers.controller('caasCtrl', ['$scope', '$location', '$q', '$http', 'changeCssSrv', 'caasSer', '$stateParams', '$state', '$compile', '$rootScope', 'dateServ',
              function($scope, $location, $q, $http, changeCssSrv, caasSer, $stateParams, $state, $compile, $rootScope, dateServ) {
                $("#col").find(".column").each(function(i, e) {
                      $(e).mousemove(function(event) {
                            if ($scope.mousedown == 1) {
                              $(e).addClass("selected-column");
                            }
                          });
                    });
                $("#col").mouseup(function(event) {
                      $scope.mousedown = 0;
                    });
                $scope.mousedown = 0;
                $("#col").mousedown(function(event) {
                      event.preventDefault();
                      $scope.mousedown = 1;
                    });

                $rootScope.index = 0;
                $scope.mergeColunm = function() {
                  var divide = false;
                  $(".demo").find(".selected-column").not(":last").each(function(i, el) {
                        if ($(el).next(".column").attr("class").indexOf("selected-column") == -1) {
                          divide = true;
                        }
                      });
                  if (divide) {
                    return;
                  } else {
                    var num = 0;
                    var last = false;
                    var nextCol = $(".demo").find(".selected-column:last").next(".column");
                    if (nextCol.length == 0) {
                      nextCol = $(".demo").find(".selected-column:last").parent("div.row");
                      last = true;
                    }
                    $(".demo").find(".selected-column").each(function(i, el) {
                          num += parseInt($(el).attr("data"));
                          $(el).remove();
                        });
                    if (last) {
                      nextCol.append('<div class="col-md-' + num + ' column" data="' + num + '"></div>');
                    } else {
                      nextCol.before('<div class="col-md-' + num + ' column" data="' + num + '"></div>');
                    }

                    function initSortable(e, t) {
                      var widgetId = t.item.find("input[name='widgetId']").val();
                      if ($scope.items != null) {
                        for (var i = 0; i < $scope.items.length; i++) {
                          if ($scope.items[i].widgetId == widgetId) {
                            $scope.items.splice(i, 1);
                            break;
                          }
                        }
                      }
                      if (t.item.find(".grid").length > 0) {
                        t.item.find(".grid").attr("ui-grid", t.item.find("input[name='widgetId']").val());
                        $compile(t.item.contents())($scope);
                      }
                      if (t.item.find("div[name='tabs']").length > 0) {
                        tabs = t.item.find("div[name='tabs']").tabs();
                        var dialog = $("#dialog").dialog({
                              autoOpen : false,
                              modal : true,
                              buttons : {
                                Add : function() {
                                  addTabs(tabs);
                                  tabs.find(".ui-tabs-nav").sortable({
                                        axis : "x",
                                        stop : function() {
                                          tabs.tabs("refresh");
                                        }
                                      });
                                  $(this).dialog("close");
                                },
                                Cancel : function() {
                                  $(this).dialog("close");
                                }
                              },
                              close : function() {
                                $("#dialog").find("form")[0].reset();
                              }
                            });
                        dialog.dialog("open");
                        tabs.delegate("#addTab", "click", function() {
                              tabs = $(this).parents("div[name='tabs']").tabs();
                              dialog.dialog("open");
                            });
                      }
                      t.item.find(".view input:last").attr("id", "item" + $rootScope.index);
                      t.item.find(".view textarea:last").attr("id", "item" + $rootScope.index);
                      t.item.find(".view select:last").attr("id", "item" + $rootScope.index);
                      $scope.idList.push("item" + $rootScope.index);
                      // 图片属性
                      if (t.item.find("div[name='uploader']").length > 0) {
                        t.item.find("div[name='uploader']").attr("id", "uploader" + "_" + widgetId);
                        t.item.find("div[name='filePicker']").attr("id", "filePicker" + "_" + widgetId);
                        t.item.find("div[name='dndArea']").attr("id", "dndArea" + "_" + widgetId);
                        t.item.find(".queueList").attr("id", "queueList" + "_" + widgetId);
                        imgServ.init(widgetId);
                      }
                      // 时间控件触发
                      dateServ.init('.form_date', 'en');

                      $rootScope.index++;
                    }
                    $(".demo, .demo .column").sortable({
                          connectWith : ".column",
                          opacity : .35,
                          handle : ".drag",
                          start : function(e, t) {
                          },
                          stop : function(e, t) {
                            if (t.item.find("span[name='dragBt']").length > 0) {
                              t.item.find("span[name='dragBt']").show();
                            }
                            if (t.item.find(".accordion").length > 0) {
                              $(".demo, .demo .column").sortable({
                                    connectWith : ".column",
                                    opacity : .35,
                                    handle : ".drag",
                                    start : function(e, t) {
                                    },
                                    stop : function(e, t) {
                                      initSortable(e, t);
                                    }
                                  });
                            } else {
                              initSortable(e, t);
                            }
                          }
                        });
                  }

                }
                $scope.specifiedFormId = $stateParams.formId;
                $scope.specifiedSearchId = $stateParams.searchId;

                $scope.categoryParentSelectChange = function() {
                  var id = $scope.category.parent.selected;

                  $scope.category.sub.select = [];
                  angular.forEach($scope.category.sub.all, function(value, key) {
                        if (value.parentId == id) {
                          $scope.category.sub.select.push(value);
                        }
                      });
                }

                $scope.category = {};
                $scope.category.parent = {
                  selected : "",
                  all : [{
                        name : "服装",
                        id : "5"
                      }]
                };

                $scope.category.sub = {
                  selected : "",
                  select : [],
                  all : [{
                        name : "男装",
                        id : "18",
                        parentId : "5"
                      }, {
                        name : "女装",
                        id : "19",
                        parentId : "5"
                      }, {
                        name : "童装",
                        id : "20",
                        parentId : "5"
                      }]
                };

                if ($rootScope.language == undefined) {
                  $rootScope.language = "en";
                }
                $http.get('attrCtl/getAttr').success(function(data, status, headers, config) {
                      var itemsList = [];
                      var items = [];
                      var item = {};
                      for (var d in data) {
                        if (data[d].direcJson) {
                          item = {};
                          if (JSON.parse(data[d].direcJson)[$rootScope.language] != undefined) {
                            item = JSON.parse(data[d].direcJson)[$rootScope.language];
                          } else {
                            item = JSON.parse(data[d].direcJson);
                          }
                          if (data[d].imgCategory != null && data[d].imgCategory.innerAttrs.length > 0) {
                            item.innerAttrs = data[d].imgCategory.innerAttrs;
                          }
                          item.type = data[d].widgetType;
                          // item.id = data[d].id;
                          item.widgetId = data[d].id;
                          item.isForm = "form";
                          item.baseFlag = "";
                          itemsList.push(item);
                        }
                      }
                      for (var d = 0; d < itemsList.length; d++) {
                        if (itemsList[d].type == "grid") {
                          itemsList[d].enableColumnResizing = true;
                          itemsList[d].data = [];
                          itemsList[d].columnDefs = itemsList[d].grid.columns
                        }
                        $scope[itemsList[d].widgetId] = itemsList[d];
                      }
                      if ($scope.dJson != null) {
                        for (var i = 0; i < itemsList.length; i++) {
                          var notExist = true;
                          for (var j = 0; j < $scope.dJson.length; j++) {
                            angular.forEach($scope.dJson[j], function(value, key) {
                                  if (key == itemsList[i].widgetId) {
                                    notExist = false;
                                  }
                                });
                            if (!notExist) {
                              break;
                            }
                          }
                          if (notExist) {
                            items.push(itemsList[i]);
                          }

                        }
                      } else {
                        angular.copy(itemsList, items);
                      }
                      $scope.itemsList = itemsList;
                      $scope.items = items;
                    }).error(function(data, status, headers, config) {

                    });
                // 替换主题
                $(function() {
                      changeCssSrv.change(".theme", "bootstrapName");
                    });
                $scope.edit = function() {
                  $scope.dropArea = false;
                  $(".demo").html($("#srcHtml").html());
                };

                $scope.$on("$includeContentLoaded", function(event, templateName) {
                      // tooltip初始化
                      $('body').popover({
                            selector : '[data-toggle="popover"]',
                            trigger : 'hover',
                            placement : 'right'
                          });
                    });

                // 判断浏览器是否支持html5本地保存
                function supportstorage() {
                  if (typeof window.localStorage == 'object')
                    return true;
                  else
                    return false;
                }
                // 定时保存页面布局
                function handleSaveLayout() {
                  var e = $(".demo").html();
                  if (!stopsave && e != window.demoHtml) {
                    stopsave++;
                    window.demoHtml = e;
                    // saveLayout();
                    stopsave--;
                  }
                }

                var layouthistory;
                // 保存布局
                // function saveLayout() {
                // var data = layouthistory;
                // if (!data) {
                // data = {};
                // data.count = 0;
                // data.list = [];
                // }
                // if (data.list.length > data.count) {
                // for (i = data.count; i < data.list.length; i++)
                // data.list[i] = null;
                // }
                // data.list[data.count] = window.demoHtml;
                // data.count++;
                // if (supportstorage()) {
                // localStorage.setItem("layoutdata", JSON.stringify(data));
                // }
                // layouthistory = data;
                // }
                // header下载按钮
                function downloadLayout() {

                  $.ajax({
                        type : "POST",
                        url : "/build/downloadLayout",
                        data : {
                          layout : $('#download-layout').html()
                        },
                        success : function(data) {
                          window.location.href = '/build/download';
                        }
                      });
                }
                // 交互组件生成ID
                function handleJsIds() {
                  // 遮盖窗体
                  // handleModalIds();
                  // 手风琴
                  handleAccordionIds();
                  // 轮换图
                  // handleCarouselIds();
                  // 切换卡
                  // handleTabsIds()
                  // ？？为什么不是全部组件？

                  // 自定义下拉列表 Start
                  // handleCustomDropdownIds();
                  // 自定义下拉列表 End
                }

                // 手风琴
                function handleAccordionIds() {
                  var e = $(".demo #myAccordion");
                  var t = randomNumber();
                  var n = "accordion-" + t;
                  var r;
                  e.attr("id", n);
                  e.find(".accordion-group").each(function(e, t) {
                        r = "accordion-element-" + randomNumber();
                        $(t).find(".accordion-toggle").each(function(e, t) {
                              $(t).attr("data-parent", "#" + n);
                              $(t).attr("href", "#" + r)
                            });
                        $(t).find(".accordion-body").each(function(e, t) {
                              $(t).attr("id", r)
                            })
                      })
                }

                // function downloadHtmlLayout() {
                // $.ajax({
                // type : "POST",
                // url : "/build/downloadLayout",
                // data : {
                // layout : $('#download-layout').html()
                // },
                // success : function(data) {
                // window.location.href = '/build/downloadHtml';
                // }
                // });
                // }
                // header撤销按钮
                // function undoLayout() {
                // var data = layouthistory;
                // // console.log(data);
                // if (data) {
                // if (data.count < 2)
                // return false;
                // window.demoHtml = data.list[data.count - 2];
                // data.count--;
                // $('.demo').html(window.demoHtml);
                // if (supportstorage()) {
                // localStorage.setItem("layoutdata", JSON.stringify(data));
                // }
                // return true;
                // }
                // return false;
                // }
                // header 重做
                // function redoLayout() {
                // var data = layouthistory;
                // if (data) {
                // if (data.list[data.count]) {
                // window.demoHtml = data.list[data.count];
                // data.count++;
                // $('.demo').html(window.demoHtml);
                // if (supportstorage()) {
                // localStorage.setItem("layoutdata", JSON.stringify(data));
                // }
                // return true;
                // }
                // }
                // return false;
                // }
                // 随机数-》用于生成id
                function randomNumber() {
                  return randomFromInterval(1, 1e6)
                }

                function randomFromInterval(e, t) {
                  return Math.floor(Math.random() * (t - e + 1) + e)
                }

                function gridSystemGenerator() {
                  $(".lyrow .preview input").bind("keyup", function() {
                        var e = 0;
                        var t = "";
                        var n = $(this).val().split(" ", 12);
                        $.each(n, function(n, r) {
                              e = e + parseInt(r);
                              t += '<div class="span' + r + ' column"></div>'
                            });
                        if (e == 12) {
                          $(this).parent().next().children().html(t);
                          $(this).parent().prev().show()
                        } else {
                          $(this).parent().prev().hide()
                        }
                      })
                }
                // 拖拽后内容 编辑的各种事件
                function configurationElm(e, t) {
                  $(".demo").delegate(".configuration > a", "click", function(e) {
                        e.preventDefault();
                        var t = $(this).parent().next().next().children();
                        $(this).toggleClass("active");
                        t.toggleClass($(this).attr("rel"))
                      });
                  $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
                        e.preventDefault();
                        var t = $(this).parent().parent();
                        var n = t.parent().parent().next().next().children();
                        t.find("li").removeClass("active");
                        $(this).parent().addClass("active");
                        var r = "";
                        t.find("a").each(function() {
                              r += $(this).attr("rel") + " ";
                            });
                        t.parent().removeClass("open");
                        n.removeClass(r);
                        n.addClass($(this).attr("rel"))
                      })
                }
                // 删除元素
                function removeElm() {
                  $(".demo").delegate(".remove", "click", function(e) {
                        e.preventDefault();
                        if ($(this).parents("div[name='widget']").length > 0) {
                          $(this).parents("div[name='widget']").remove();
                        } else {
                          $(this).parent().remove();
                        }
                        if (!$(".demo .lyrow").length > 0) {
                          clearDemo()
                        }
                        var widgetId = $(this).parents(".box.box-element").find("input[name='widgetId']").val();
                        if ($scope.items != null && $scope.itemsList != null) {
                          for (var i = 0; i < $scope.itemsList.length; i++) {
                            if ($scope.itemsList[i].widgetId == widgetId) {
                              $scope.items.push($scope.itemsList[i])
                            }
                          }
                          $scope.$apply();
                        }
                      });
                }
                // 清空Container
                function clearDemo() {
                  $(".demo").empty();
                  layouthistory = null;
                  if (supportstorage())
                    localStorage.removeItem("layoutdata");
                }
                // 切换header导航 active状态
                function removeMenuClasses() {
                  $("#menu-layoutit li button").removeClass("active")
                }
                // 更改生成html5代码
                function changeStructure(e, t) {
                  $("#download-layout ." + e).removeClass(e).addClass(t)
                }
                // 删除行模板代码
                function cleanHtml(e) {
                  $(e).parent().append($(e).children().html())
                }
                // 下载页面布局
                // function downloadLayoutSrc() {
                // var e = "";
                // $("#download-layout").children().html($(".demo").html());
                // var t = $("#download-layout").children();
                // t.find(".preview, .configuration, .drag, .remove").remove();
                // t.find(".lyrow").addClass("removeClean");
                // t.find(".box-element").addClass("removeClean");
                // t.find(".lyrow .lyrow .lyrow .lyrow .lyrow
                // .removeClean").each(function() {
                // cleanHtml(this)
                // });
                // t.find(".lyrow .lyrow .lyrow .lyrow
                // .removeClean").each(function() {
                // cleanHtml(this)
                // });
                // t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
                // cleanHtml(this)
                // });
                // t.find(".lyrow .lyrow .removeClean").each(function() {
                // cleanHtml(this)
                // });
                // t.find(".lyrow .removeClean").each(function() {
                // cleanHtml(this)
                // });
                // t.find(".removeClean").each(function() {
                // cleanHtml(this)
                // });
                // t.find(".removeClean").remove();
                // $("#download-layout .column").removeClass("ui-sortable");
                // $("#download-layout
                // .row-fluid").removeClass("clearfix").children().removeClass("column");
                // if ($("#download-layout .container").length > 0) {
                // changeStructure("row-fluid", "row")
                // }
                // var labelValue = '';
                // for (var i = 0; i < idList.length; i++) {
                // labelValue = $("#" + idList[i]).prev("input").val();
                // $("#" + idList[i]).prev("input").remove();
                // $("#" + idList[i])
                // .before("<label class='control-label' for='" + idList[i] +
                // "'>" + labelValue + "</label>");
                // }
                //
                // formatSrc = $.htmlClean($("#download-layout").html(), {
                // format : true,
                // allowedAttributes : [["id"], ["class"], ["data-toggle"],
                // ["data-target"], ["data-parent"], ["role"],
                // ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"],
                // ["data-slide-to"], ["data-slide"],
                // ["style"]]
                // });
                // }

                // 定时保存生成html5代码，缓存作用
                var timerSave = 1000;
                // 
                var stopsave = 0;
                // 
                var startdrag = 0;
                var demoHtml = $(".demo").html();
                // 
                var currenteditor = null;

                var index = 0;
                var idList = new Array();
                var labelValues = {};
                // 自适应
                $(window).resize(function() {
                      $("body").css("min-height", $(window).height() - 90);
                      $(".demo").css("min-height", $(window).height() - 160)
                    });
                // 从客户端本地获取旧布局
                function restoreData() {
                  if (supportstorage()) {
                    layouthistory = JSON.parse(localStorage.getItem("layoutdata"));
                    if (!layouthistory)
                      return false;
                    window.demoHtml = layouthistory.list[layouthistory.count - 1];
                    if (window.demoHtml)
                      $(".demo").html(window.demoHtml);
                  }
                }

                // Array.prototype.remove = function(val) {
                // var index = this.indexOf(val);
                // if (index > -1) {
                // this.splice(index, 1);
                // }
                // };
                $scope.idList = [];
                function bindEvent(fieldId) {
                  idList.push(fieldId);
                  $scope.idList = idList;
                  if (supportstorage) {
                    window.localStorage.setItem("idList", idList);
                  }
                }

                // 初始化容器
                function initContainer() {
                  // 初始化容器内元素(添加方向，排版等按钮事件)
                  configurationElm();
                }
                // 页面加载
                angular.element(document).ready(function() {
                      $("body").css("min-height", $(window).height() - 90);
                      $(".demo").css("min-height", $(window).height() - 160);
                      $(".sidebar-nav .lyrow").draggable({
                            connectToSortable : ".demo",
                            helper : "clone",
                            handle : ".drag",
                            start : function(e, t) {
                              if (!startdrag)
                                stopsave++;
                              startdrag = 1;
                            },
                            drag : function(e, t) {
                              t.helper.width(400);
                            },
                            stop : function(e, t) {
                              $(".demo .column").sortable({
                                    opacity : .35,
                                    connectWith : ".column",
                                    start : function(e, t) {
                                      if (!startdrag)
                                        stopsave++;
                                      startdrag = 1;
                                    },
                                    stop : function(e, t) {
                                      if (stopsave > 0)
                                        stopsave--;
                                      startdrag = 0;
                                      t.item.find(".view input:last").attr("id", "item" + index);
                                      bindEvent("item" + index);
                                      index++;
                                    }
                                  });
                              if (stopsave > 0)
                                stopsave--;
                              startdrag = 0;
                            }
                          });
                      $(".sidebar-nav .box").draggable({
                            connectToSortable : ".column",
                            helper : "clone",
                            handle : ".drag",
                            start : function(e, t) {
                              if (!startdrag)
                                stopsave++;
                              startdrag = 1;
                            },
                            drag : function(e, t) {
                              t.helper.width(400)
                            },
                            stop : function() {
                              handleJsIds();
                              if (stopsave > 0)
                                stopsave--;
                              startdrag = 0;
                              $(".demo, .demo .column").sortable({
                                    connectWith : ".column",
                                    opacity : .35,
                                    handle : ".drag",
                                    start : function(e, t) {
                                    },
                                    stop : function(e, t) {

                                    }
                                  });
                            }
                          });
                      initContainer();

                      // 保存表单
                      $('#body').on("click", "[data-target=#formSaveEditorModal]", function(e) {
                            $("#newFormName").val("Form_" + randomNumber());
                          });

                      $('#saveForm').click(function(e) {
                            $scope.doSourceSave();
                          });

                      $('body.edit .demo').on("click", "[data-target=#editorModal]", function(e) {
                            e.preventDefault();
                            currenteditor = $(this).parent().parent().find('.view');
                            var eText = currenteditor.html();
                            contenthandle.setData(eText);
                          });
                      $("#savecontent").click(function(e) {
                            e.preventDefault();
                            currenteditor.html(contenthandle.getData());
                          });
                      // $("[data-target=#downloadModal]").click(function(e) {
                      // e.preventDefault();
                      // downloadLayoutSrc();
                      // });
                      $("[data-target=#shareModal]").click(function(e) {
                            e.preventDefault();
                            handleSaveLayout();
                          });
                      $("#download").click(function() {
                            downloadLayout();
                            return false
                          });
                      // $("#downloadhtml").click(function() {
                      // downloadHtmlLayout();
                      // return false
                      // });
                      $("#edit").click(function() {
                            $("body").removeClass("devpreview sourcepreview");
                            $("body").addClass("edit");
                            removeMenuClasses();

                            $("#edit").prop("disabled", "disabled");;
                            $("#save").prop("disabled", "");

                            $(this).addClass("active");
                            $("#sourcepreview").prop("disabled", "");;
                            return false
                          });
                      $("#clear").click(function(e) {
                            e.preventDefault();
                            clearDemo()
                          });
                      $("#devpreview").click(function() {
                            $("body").removeClass("edit sourcepreview");
                            $("body").addClass("devpreview");
                            removeMenuClasses();
                            $(this).addClass("active");
                            return false
                          });
                      $("#sourcepreview").click(function() {
                            $("body").removeClass("edit");
                            $("body").addClass("devpreview sourcepreview");
                            removeMenuClasses();
                            $(this).addClass("active");
                            for (var p in labelValues) {
                              if (typeof(labelValues[p]) != "function") {
                                if ($("#" + p).prev("label")) {
                                  $("#" + p).prev("label").remove();
                                }
                                $("#" + p).prev("input").remove();
                                $("#" + p).before("<label class='control-label'>" + labelValues[p] + "</label>");
                              }
                            }

                            $("#edit").prop("disabled", "");

                            $("#sourcepreview").prop("disabled", "disabled");
                            return false
                          });
                      // $("#fluidPage").click(function(e) {
                      // e.preventDefault();
                      // changeStructure("container", "container-fluid");
                      // $("#fixedPage").removeClass("active");
                      // $(this).addClass("active");
                      // downloadLayoutSrc()
                      // });
                      // $("#fixedPage").click(function(e) {
                      // e.preventDefault();
                      // changeStructure("container-fluid", "container");
                      // $("#fluidPage").removeClass("active");
                      // $(this).addClass("active");
                      // downloadLayoutSrc()
                      // });
                      $(".nav-header").click(function() {
                            $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
                            $(this).next().slideDown()
                          });
                      $('#undo').click(function() {
                            stopsave++;
                            if (undoLayout())
                              initContainer();
                            stopsave--;
                          });
                      $('#redo').click(function() {
                            stopsave++;
                            if (redoLayout())
                              initContainer();
                            stopsave--;
                          });

                      // 自定义下拉列表 编辑按钮按下 Start
                      $('body.edit .demo').on("click", "[data-target=#dropdownListModifyModal]", function(e) {
                            e.preventDefault();

                            var editor = content.controls[0].custom[0].dropdownList[0].editor;
                            var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
                            var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
                            var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

                            $('#dropdownListModifyModal').empty();
                            $('#dropdownListModifyModal').prepend($("#dropdownListInitModal").html());

                            currenteditor = $(this).parent().parent().find('.view');

                            var currentItemName = currenteditor.find("[class='btn']").find("lable:first");
                            var currentMenus = currenteditor.find("[class='dropdown-menu']");

                            // 项目名称
                            var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='" + itemNamePlaceholder + "']");
                            editorItemName.attr("value", currentItemName.text());

                            // 下拉列表项目
                            var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
                            var editorMenuTmp = $(editorMenuOld).clone();
                            $(editorMenuOld).empty();
                            var itemCount = 0;

                            currentMenus.find("li").each(function(e, t) {
                                  var editorMenuNew = editorMenuTmp.clone();
                                  var textContent = $.trim(t.textContent);
                                  var val = $.trim($(t).find("a").attr("value"));
                                  editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr("value", textContent);
                                  editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']").attr("value", val);

                                  $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);

                                  itemCount++;
                                });

                            if (itemCount == 0) {
                              var editorMenuNew = editorMenuTmp.clone();
                              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr("value", "");
                              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']").attr("value", "");

                              $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);
                            }
                          });

                      $(".commanddiv").hide();
                      $scope.setNavStyle(null, "menu_form");

                      removeElm();
                      gridSystemGenerator();
                      setInterval(function() {
                            handleSaveLayout()
                          }, timerSave);
                    })

                // 自定义下拉列表 编辑画面里保存按下的场合 Start
                function doSavecontentForDropdownList() {
                  // if (content.length == 0) {
                  // doUpdateItemFromJSON();
                  // }
                  var view = content.controls[0].custom[0].dropdownList[0].view;
                  var editor = content.controls[0].custom[0].dropdownList[0].editor;
                  var items = view[0].items;
                  var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
                  var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
                  var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

                  var currentItemName = $(currenteditor).find("[class='btn']");
                  var currentMenus = $(currenteditor).find("[class='dropdown-menu']");

                  // 项目名称
                  var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='" + itemNamePlaceholder + "']");
                  if ($.trim(editorItemName.val()) == "") {
                    $(currentItemName).find("lable:first").text("项目名称");
                  } else {
                    $(currentItemName).find("lable:first").text($.trim(editorItemName.val()));
                  }
                  $(currentItemName).find("lable:last").text("");

                  // 下拉列表项目
                  $(currentMenus).empty();
                  var itemCount = 0;

                  var editorMenus = $('#dropdownListModifyModal').find("[class='view clearfix']");
                  $(editorMenus).each(function(e, t) {
                        var text = "";
                        var val = "";

                        var textObj = $(t).find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']");
                        if (textObj.length > 0) {
                          text = $.trim(textObj.val());
                        } else {
                          return true; // false时相当于break, true 就相当于continure。
                        }

                        var valObj = $(t).find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']");
                        if (valObj.length > 0) {
                          val = $.trim(valObj.val());
                        } else {
                          return true; // false时相当于break, true 就相当于continure。
                        }

                        if (text != "") {
                          $(currentMenus).append("<li><a href=\"#\" value=\"" + val + "\">" + text + "</a></li>");
                          itemCount++;
                        }
                      });

                  if (itemCount == 0) {
                    $.each(items, function(idx, item) {
                          $(currentMenus).append("<li><a href=\"#\" value=\"" + item.value + "\">" + item.text + "</a></li>");
                        });
                  }

                  $(currentMenus).find("li").click(function(e) {
                        doClickByAForDropdownList(e);
                      });

                  $("#closeForDropdownListModal").click();
                  return false;
                }
                // 自定义下拉列表 编辑画面里保存按下的场合 End

                // 自定义下拉列表 Li -> a -> Click Start
                function doClickByAForDropdownList(e) {
                  var selText = ":" + e.currentTarget.textContent;
                  $(e.currentTarget).parent().parent().find("button[class='btn'][data-toggle='dropdown']").find("lable:last").text(selText);

                  return false;
                }
                // 自定义下拉列表 Li -> a -> Click End

                // 自定义下拉列表 添加项目 Start
                function doAddItemForDropdownList(t) {
                  // if (content.length == 0) {
                  // doUpdateItemFromJSON();
                  // }
                  var editor = content.controls[0].custom[0].dropdownList[0].editor;
                  var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
                  var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

                  var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
                  var editorMenuTmp = $(editorMenuOld).clone();
                  editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr("value", "");
                  editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']").attr("value", "");

                  $(t).parent().parent().after(editorMenuTmp);

                  return false;
                }
                // 自定义下拉列表 添加项目 End

                // 自定义下拉列表 删除项目 Start
                function doDelItemForDropdownList(t) {
                  if ($(t).parent().parent().parent().find("[class='view clearfix']").length > 4) {
                    $(t).parent().parent().remove();
                  }

                  return false;
                }
                // 自定义下拉列表 删除项目 End

                // 预览按钮按下
                $scope.doSourcePreview = function() {

                  $("body").removeClass("edit");
                  $("body").addClass("devpreview sourcepreview");
                  $("#menu-layoutit li button").removeClass("active");
                  $(this).addClass("active");
                  var t = $(".demo").children();
                  t.each(function(i, e) {
                        $(e).find("div.view").css("padding-top", "0px");
                        $(e).find("div.form-group").css("margin-bottom", "0px");
                        $(e).find(".view .row.clearfix .column").css("padding-top", "0px").css("padding-bottom", "5px");
                        $(e).find("[contenteditable='true']").attr("contenteditable", "false");

                        $(e).find("div.row.clearfix div").find(".box.box-element.ui-draggable.ng-isolate-scope").css("marginLeft", "20px");
                      });
                  return false;
                };
                var handleSidebarAndContentHeight = function() {
                  var content = $('.page-content');
                  var sidebar = $('.page-sidebar');
                  var body = $('body');
                  var height;

                  if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
                    var available_height = $(window).height() - $('.footer').height();
                    if (content.height() < available_height) {
                      content.attr('style', 'min-height:' + available_height + 'px !important');
                    }
                  } else {
                    if (body.hasClass('page-sidebar-fixed')) {
                      height = _calculateFixedSidebarViewportHeight();
                    } else {
                      height = sidebar.height() + 20;
                    }
                    if (height >= content.height()) {
                      content.attr('style', 'min-height:' + height + 'px !important');
                    }
                  }
                }

                var handleSidebarMenu = function() {
                  jQuery('.page-sidebar').on('click', 'li > a', function(e) {
                        if ($(this).next().hasClass('sub-menu') == false) {
                          if ($('.btn-navbar').hasClass('collapsed') == false) {
                            $('.btn-navbar').click();
                          }
                          return;
                        }

                        var parent = $(this).parent().parent();

                        parent.children('li.open').children('a').children('.arrow').removeClass('open');
                        parent.children('li.open').children('.sub-menu').slideUp(200);
                        parent.children('li.open').removeClass('open');

                        var sub = jQuery(this).next();
                        if (sub.is(":visible")) {
                          jQuery('.arrow', jQuery(this)).removeClass("open");
                          jQuery(this).parent().removeClass("open");
                          sub.slideUp(200, function() {
                                handleSidebarAndContentHeight();
                              });
                        } else {
                          jQuery('.arrow', jQuery(this)).addClass("open");
                          jQuery(this).parent().addClass("open");
                          sub.slideDown(200, function() {
                                handleSidebarAndContentHeight();
                              });
                        }

                        e.preventDefault();
                      });

                  // handle ajax links
                  jQuery('.page-sidebar').on('click', ' li > a.ajaxify', function(e) {
                        e.preventDefault();
                        App.scrollTop();

                        var url = $(this).attr("href");
                        var menuContainer = jQuery('.page-sidebar ul');
                        var pageContent = $('.page-content');
                        var pageContentBody = $('.page-content .page-content-body');

                        menuContainer.children('li.active').removeClass('active');
                        menuContainer.children('arrow.open').removeClass('open');

                        $(this).parents('li').each(function() {
                              $(this).addClass('active');
                              $(this).children('a > span.arrow').addClass('open');
                            });
                        $(this).parents('li').addClass('active');

                        App.blockUI(pageContent, false);

                        $.post(url, {}, function(res) {
                              App.unblockUI(pageContent);
                              pageContentBody.html(res);
                              App.fixContentHeight(); // fix content height
                              App.initUniform(); // initialize uniform elements
                            });
                      });
                }
                // 编辑按钮按下
                $scope.doSourceEdit = function() {
                  if ($scope.dJson) {
                    for (var i = 0; i < $scope.dJson.length; i++) {
                      angular.forEach($scope.dJson[i], function(value, key) {
                            if ($("#elmBase").find("#" + key).parents(".box.box-element").length > 0) {
                              $("#elmBase").find("#" + key).parents(".box.box-element").hide();
                            }
                          });
                    }
                  }
                  if ($(".demo").find("ul[name='indention']").length > 0) {
                	  elem.find("a[rel='text-left']").attr("ng-click", "indent($event, 'left')");
                	  elem.find("a[rel='text-right']").attr("ng-click", "indent($event, 'right')");
                	  elem.find("a[rel='text-default']").attr("ng-click", "indent($event, 'default')");
					  $compile($(elem.find("a[rel='text-left']").parents("ul")).contents())(scope);
				  }
                  // if ($("body").hasClass("edit") == false) {
                  $("body").removeClass("devpreview sourcepreview");
                  $("body").addClass("edit");
                  $("#menu-layoutit li button").removeClass("active");
                  $(this).addClass("active");
                  var t = $(".demo").children();
                  t.each(function(i, e) {
                        $(e).find("div.form-group").css("margin-bottom", "15px");
                        $(e).find("[contenteditable='false']").attr("contenteditable", "true");
                        $(e).find(".row-fluid").children().find(".box.box-element.ui-draggable").css("marginLeft", "0px");
                      });
                  $(".commanddiv").show();
                  handleSidebarMenu();
                  return false;
                };

                // 保存按钮按下
                $scope.doSourceSave = function() {
                  // if ($("#saveUpdateFrom").is(':checked') == false) {
                  // if ($scope.category.sub.selected == null ||
                  // $scope.category.sub.selected == "") {
                  // return;
                  // }
                  // }

                  $scope.doSourcePreview();

                  var html = "";
                  var formatSrc = "";
                  $("#formatSrc").html($(".demo").html());
                  $("#srcHtml").html($(".demo").html());

                  var data = {};
                  data.corpCode = "Trawind";
                  data.corpId = "100";
                  data.selCommandList = [];

                  if ($("#saveUpdateFrom").is(':checked') == false) {
                    data.categoryId = "18";// $scope.category.sub.selected;
                  }

                  var allCommand = $scope.command.all;
                  var box = $(".commanddiv").find("[command-group-box='']");
                  var allCheckBox = box.find("input[type='checkbox']");
                  angular.forEach(allCheckBox, function(val, idx) {
                        if ($(val).is(':checked') == true) {
                          var key = $(val).val();
                          for (var k in allCommand) {
                            if (allCommand[k].key == key) {
                              data.selCommandList.push(allCommand[k]);
                              break;
                            }
                          }
                        }
                      });

                  if ($("#saveUpdateFrom").is(':checked') == true) {
                    if ($.trim($("#currentFormName").text()) == "") {
                      return false;
                    }
                    data.formName = $.trim($("#currentFormName").text());
                  } else {
                    if ($.trim($("#newFormName").val()) == "") {
                      return false;
                    }
                    data.formName = $.trim($("#newFormName").val());
                  }

                  if ($("#saveUpdateFrom").is(':checked') == true) {
                    if ($.trim($("#currentFormId").text()) == "") {
                      return false;
                    }
                    data.formId = $.trim($("#currentFormId").text());
                  } else {
                    data.formId = "";
                  }

                  // data.layout = $(".demo").html();
                  if ($scope.formType == 'profile') {
                    $(".demo").find(".lyrow:first").remove();
                  }
                  var htmlObj = caasSer.decodeHtml($("#formatSrc"));

                  data.layout = htmlObj.html;
                  var directJson = [];
                  for (var i = 0; i < htmlObj.idLs.length; i++) {
                    var id = htmlObj.idLs[i];
                    var attr = {};
                    var mid = {};
                    angular.copy($scope[id], mid);
                    if ($scope.widget_width && $scope.widget_width[id]) {
                    	mid["viewWidth"] = $scope.widget_width[id];
					}
                    attr[id] = mid;
                    delete attr[id].$$hashKey;
                    directJson.push(attr);
                  }
                  data.widgetIds = htmlObj.idLs.join(",");
                  data.formLayoutJson = directJson;
                  data.formType = $scope.formType;
                  console.log(data);
                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'userForms/saveUserForms',
                        'data' : JSON.stringify(data),
                        transformResponse : function(data, headersGetter, status) {
                          return data;
                        }
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        $("#currentFormId").text(data.formId);
                        $("#currentFormName").text(data.formName);
                        $("#saveCreateFrom").prop("checked", false);
                        $("#saveUpdateFrom").prop("checked", true);

                        $("#edit").removeAttr("disabled");
                        $("#save").attr("disabled", "disabled");

                        $(".commanddiv").hide();

                        console.log('doSourceSave 请求成功');
                      }, function(data) {
                        console.log('doSourceSave 请求失败');
                      });

                };

                $scope.changeTemplate = function() {
                  if ($scope.formType == "profile") {
                    $(".demo").find(".lyrow:first").hide();
                  } else {
                    $(".demo").find(".lyrow:first").show();
                  }
                };

              }]);
    });
