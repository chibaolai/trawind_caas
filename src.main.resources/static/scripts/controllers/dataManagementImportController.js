define(['controllers/controllers', 'jquery', 'jqueryUI'],
  function(controllers) {
	  controllers.controller('dataManagementImportCtrl', ['$scope', '$location', '$q', '$http', '$stateParams','$state','$compile','$interval', function($scope, $location, $q, $http, $stateParams,$state,$compile,$interval) {
	       
	        // 交互组件生成ID
	        function handleJsIds() {
	          // 遮盖窗体
//	          handleModalIds();
	          // 手风琴
	          handleAccordionIds();
	          // 轮换图
//	          handleCarouselIds();
	          // 切换卡
//	          handleTabsIds()
	          // ？？为什么不是全部组件？

	          // 自定义下拉列表 Start
//	          handleCustomDropdownIds();
	          // 自定义下拉列表 End
	        }
	        
	        
	        // 手风琴
	        function handleAccordionIds() {
	          var e = $(".demo #myAccordion");
	          var t = randomNumber();
	          var n = "accordion-" + t;
	          var r;
	          e.attr("id", n);
	          e.find(".accordion-group").each(function(e, t) {
	                r = "accordion-element-" + randomNumber();
	                $(t).find(".accordion-toggle").each(function(e, t) {
	                      $(t).attr("data-parent", "#" + n);
	                      $(t).attr("href", "#" + r)
	                    });
	                $(t).find(".accordion-body").each(function(e, t) {
	                      $(t).attr("id", r)
	                    })
	              })
	        }
	        // 判断浏览器是否支持html5本地保存
	        function supportstorage() {
	          if (typeof window.localStorage == 'object')
	            return true;
	          else
	            return false;
	        }
	        // 定时保存页面布局
	        function handleSaveLayout() {
	          var e = $(".demo").html();
	          if (!stopsave && e != window.demoHtml) {
	            stopsave++;
	            window.demoHtml = e;
//	            saveLayout();
	            stopsave--;
	          }
	        }

	        var layouthistory;
	        // 保存布局
//	        function saveLayout() {
//	          var data = layouthistory;
//	          if (!data) {
//	            data = {};
//	            data.count = 0;
//	            data.list = [];
//	          }
//	          if (data.list.length > data.count) {
//	            for (i = data.count; i < data.list.length; i++)
//	              data.list[i] = null;
//	          }
//	          data.list[data.count] = window.demoHtml;
//	          data.count++;
//	          if (supportstorage()) { 
//	            localStorage.setItem("layoutdata", JSON.stringify(data));
//	          }
//	          layouthistory = data;
//	        }
	        // header下载按钮
	        function downloadLayout() {

	          $.ajax({
	                type : "POST",
	                url : "/build/downloadLayout",
	                data : {
	                  layout : $('#download-layout').html()
	                },
	                success : function(data) {
	                  window.location.href = '/build/download';
	                }
	              });
	        }

	        function downloadHtmlLayout() {
	          $.ajax({
	                type : "POST",
	                url : "/build/downloadLayout",
	                data : {
	                  layout : $('#download-layout').html()
	                },
	                success : function(data) {
	                  window.location.href = '/build/downloadHtml';
	                }
	              });
	        }
	        // header撤销按钮
	        function undoLayout() {
	          var data = layouthistory;
	          // console.log(data);
	          if (data) {
	            if (data.count < 2)
	              return false;
	            window.demoHtml = data.list[data.count - 2];
	            data.count--;
	            $('.demo').html(window.demoHtml);
	            if (supportstorage()) {
	              localStorage.setItem("layoutdata", JSON.stringify(data));
	            }
	            return true;
	          }
	          return false;
	        }
	        // header 重做
	        function redoLayout() {
	          var data = layouthistory;
	          if (data) {
	            if (data.list[data.count]) {
	              window.demoHtml = data.list[data.count];
	              data.count++;
	              $('.demo').html(window.demoHtml);
	              if (supportstorage()) {
	                localStorage.setItem("layoutdata", JSON.stringify(data));
	              }
	              return true;
	            }
	          }
	          return false;
	        }
	        // 随机数-》用于生成id
	        function randomNumber() {
	          return randomFromInterval(1, 1e6)
	        }

	        function randomFromInterval(e, t) {
	          return Math.floor(Math.random() * (t - e + 1) + e)
	        }

	        function gridSystemGenerator() {
	          $(".lyrow .preview input").bind("keyup", function() {
	                var e = 0;
	                var t = "";
	                var n = $(this).val().split(" ", 12);
	                $.each(n, function(n, r) {
	                      e = e + parseInt(r);
	                      t += '<div class="span' + r + ' column"></div>'
	                    });
	                if (e == 12) {
	                  $(this).parent().next().children().html(t);
	                  $(this).parent().prev().show()
	                } else {
	                  $(this).parent().prev().hide()
	                }
	              })
	        }
	        // 拖拽后内容 编辑的各种事件
	        function configurationElm(e, t) {
	          $(".demo").delegate(".configuration > a", "click", function(e) {
	                e.preventDefault();
	                var t = $(this).parent().next().next().children();
	                $(this).toggleClass("active");
	                t.toggleClass($(this).attr("rel"))
	              });
	          $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
	                e.preventDefault();
	                var t = $(this).parent().parent();
	                var n = t.parent().parent().next().next().children();
	                t.find("li").removeClass("active");
	                $(this).parent().addClass("active");
	                var r = "";
	                t.find("a").each(function() {
	                      r += $(this).attr("rel") + " "
	                    });
	                t.parent().removeClass("open");
	                n.removeClass(r);
	                n.addClass($(this).attr("rel"))
	              })
	        }
	        // 删除元素
	        function removeElm() {
	          $(".demo").delegate(".remove", "click", function(e) {
	                e.preventDefault();
	                $(this).parent().remove();
	                if (!$(".demo .lyrow").length > 0) {
	                  clearDemo()
	                }
	                var el = $(this).parent("div").find("input:last");
	                if (el) {
	                  idList.remove(el.attr("id"));
	                  delete labelValues[el.attr("id")];
	                }
	              })
	        }
	        // 清空Container
	        function clearDemo() {
	          $(".demo").empty();
	          layouthistory = null;
	          if (supportstorage())
	            localStorage.removeItem("layoutdata");
	        }
	        // 切换header导航 active状态
	        function removeMenuClasses() {
	          $("#menu-layoutit li button").removeClass("active")
	        }
	        // 更改生成html5代码
	        function changeStructure(e, t) {
	          $("#download-layout ." + e).removeClass(e).addClass(t)
	        }
	        // 删除行模板代码
	        function cleanHtml(e) {
	          $(e).parent().append($(e).children().html())
	        }
	        // 下载页面布局
	        function downloadLayoutSrc() {
	          var e = "";
	          $("#download-layout").children().html($(".demo").html());
	          var t = $("#download-layout").children();
	          t.find(".preview, .configuration, .drag, .remove").remove();
	          t.find(".lyrow").addClass("removeClean");
	          t.find(".box-element").addClass("removeClean");
	          t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".removeClean").remove();
	          $("#download-layout .column").removeClass("ui-sortable");
	          $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
	          if ($("#download-layout .container").length > 0) {
	            changeStructure("row-fluid", "row")
	          }
	          var labelValue = '';
	          for (var i = 0; i < idList.length; i++) {
	            labelValue = $("#" + idList[i]).prev("input").val();
	            $("#" + idList[i]).prev("input").remove();
	            $("#" + idList[i])
	                .before("<label class='control-label' for='" + idList[i] + "'>" + labelValue + "</label>");
	          }

	          formatSrc = $.htmlClean($("#download-layout").html(), {
	                format : true,
	                allowedAttributes : [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"],
	                    ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"], ["data-slide-to"], ["data-slide"],
	                    ["style"]]
	              });
	        }

	        // 定时保存生成html5代码，缓存作用
	        var timerSave = 1000;
	        // 
	        var stopsave = 0;
	        // 
	        var startdrag = 0;
	        var demoHtml = $(".demo").html();
	        // 
	        var currenteditor = null;

	        var index = 0;
	        var idList = new Array();
	        var labelValues = {};
	        // 自适应
	        $(window).resize(function() {
	              $("body").css("min-height", $(window).height() - 90);
	              $(".demo").css("min-height", $(window).height() - 160)
	            });
	        // 从客户端本地获取旧布局
	        function restoreData() {
	          if (supportstorage()) {
	            layouthistory = JSON.parse(localStorage.getItem("layoutdata"));
	            if (!layouthistory)
	              return false;
	            window.demoHtml = layouthistory.list[layouthistory.count - 1];
	            if (window.demoHtml)
	              $(".demo").html(window.demoHtml);
	          }
	        }

//	        Array.prototype.remove = function(val) {
//	          var index = this.indexOf(val);
//	          if (index > -1) {
//	            this.splice(index, 1);
//	          }
//	        };
	        $scope.idList = [];
	        function bindEvent(fieldId) {
	          idList.push(fieldId);
	          $scope.idList = idList;
	          if (supportstorage) {
	            window.localStorage.setItem("idList", idList);
	          }
	        }

	        // 初始化容器
	        function initContainer() {
	          // 初始化容器内元素(添加方向，排版等按钮事件)
	          configurationElm();
	        }
	        // 页面加载
	        angular.element(document).ready(function() {
	          $("body").css("min-height", $(window).height() - 90);
	          $(".demo").css("min-height", $(window).height() - 160);
	          $(".sidebar-nav .lyrow").draggable({
	                connectToSortable : ".demo",
	                helper : "clone",
	                handle : ".drag",
	                start : function(e, t) {
	                  if (!startdrag)
	                    stopsave++;
	                  startdrag = 1;
	                },
	                drag : function(e, t) {
	                  t.helper.width(400);
	                },
	                stop : function(e, t) {
	                  $(".demo .column").sortable({
	                        opacity : .35,
	                        connectWith : ".column",
	                        start : function(e, t) {
	                          if (!startdrag)
	                            stopsave++;
	                          startdrag = 1;
	                        },
	                        stop : function(e, t) {
	                          if (stopsave > 0)
	                            stopsave--;
	                          startdrag = 0;
	                          t.item.find(".view input:last").attr("id", "item" + index);
	                          bindEvent("item" + index);
	                          index++;
	                        }
	                      });
	                  if (stopsave > 0)
	                    stopsave--;
	                  startdrag = 0;
	                }
	              });
	          $(".sidebar-nav .box").draggable({
	                connectToSortable : ".column",
	                helper : "clone",
	                handle : ".drag",
	                start : function(e, t) {
	                  if (!startdrag)
	                    stopsave++;
	                  startdrag = 1;
	                },
	                drag : function(e, t) {
	                  t.helper.width(400)
	                },
	                stop : function() {
	                  handleJsIds();
	                  if (stopsave > 0)
	                    stopsave--;
	                  startdrag = 0;
	                }
	              });
	          initContainer();

	          // 保存表单
	          $('#body').on("click", "[data-target=#formSaveEditorModal]", function(e) {
	                $("#newFormName").val("Form_" + randomNumber());
	              });
	          $('body.edit .demo').on("click", "[data-target=#editorModal]", function(e) {
	                e.preventDefault();
	                currenteditor = $(this).parent().parent().find('.view');
	                var eText = currenteditor.html();
	                contenthandle.setData(eText);
	              });
	          $("#savecontent").click(function(e) {
	                e.preventDefault();
	                currenteditor.html(contenthandle.getData());
	              });
	          $("[data-target=#downloadModal]").click(function(e) {
	                e.preventDefault();
	                downloadLayoutSrc();
	              });
	          $("[data-target=#shareModal]").click(function(e) {
	                e.preventDefault();
	                handleSaveLayout();
	              });
	          $("#download").click(function() {
	                downloadLayout();
	                return false
	              });
	          $("#downloadhtml").click(function() {
	                downloadHtmlLayout();
	                return false
	              });
	          $("#edit").click(function() {
	                $("body").removeClass("devpreview sourcepreview");
	                $("body").addClass("edit");
	                removeMenuClasses();
	                $(this).addClass("active");
	                return false
	              });
	          $("#clear").click(function(e) {
	                e.preventDefault();
	                clearDemo()
	              });
	          $("#devpreview").click(function() {
	                $("body").removeClass("edit sourcepreview");
	                $("body").addClass("devpreview");
	                removeMenuClasses();
	                $(this).addClass("active");
	                return false
	              });
	          $("#sourcepreview").click(function() {
	                $("body").removeClass("edit");
	                $("body").addClass("devpreview sourcepreview");
	                removeMenuClasses();
	                $(this).addClass("active");
	                for (var p in labelValues) {
	                  if (typeof(labelValues[p]) != "function") {
	                    if ($("#" + p).prev("label")) {
	                      $("#" + p).prev("label").remove();
	                    }
	                    $("#" + p).prev("input").remove();
	                    $("#" + p).before("<label class='control-label'>" + labelValues[p] + "</label>");
	                  }
	                }
	                return false
	              });
	          $("#fluidPage").click(function(e) {
	                e.preventDefault();
	                changeStructure("container", "container-fluid");
	                $("#fixedPage").removeClass("active");
	                $(this).addClass("active");
	                downloadLayoutSrc()
	              });
	          $("#fixedPage").click(function(e) {
	                e.preventDefault();
	                changeStructure("container-fluid", "container");
	                $("#fluidPage").removeClass("active");
	                $(this).addClass("active");
	                downloadLayoutSrc()
	              });
	          $(".nav-header").click(function() {
	                $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
	                $(this).next().slideDown()
	              });
	          $('#undo').click(function() {
	                stopsave++;
	                if (undoLayout())
	                  initContainer();
	                stopsave--;
	              });
	          $('#redo').click(function() {
	                stopsave++;
	                if (redoLayout())
	                  initContainer();
	                stopsave--;
	              });

	          // 自定义下拉列表 编辑按钮按下 Start
	          $('body.edit .demo').on("click", "[data-target=#dropdownListModifyModal]", function(e) {
	            e.preventDefault();

	            var editor = content.controls[0].custom[0].dropdownList[0].editor;
	            var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
	            var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
	            var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

	            $('#dropdownListModifyModal').empty();
	            $('#dropdownListModifyModal').prepend($("#dropdownListInitModal").html());

	            currenteditor = $(this).parent().parent().find('.view');

	            var currentItemName = currenteditor.find("[class='btn']").find("lable:first");
	            var currentMenus = currenteditor.find("[class='dropdown-menu']");

	            // 项目名称
	            var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='"
	                + itemNamePlaceholder + "']");
	            editorItemName.attr("value", currentItemName.text());

	            // 下拉列表项目
	            var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
	            var editorMenuTmp = $(editorMenuOld).clone();
	            $(editorMenuOld).empty();
	            var itemCount = 0;

	            currentMenus.find("li").each(function(e, t) {
	              var editorMenuNew = editorMenuTmp.clone();
	              var textContent = $.trim(t.textContent);
	              var val = $.trim($(t).find("a").attr("value"));
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']")
	                  .attr("value", textContent);
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']")
	                  .attr("value", val);

	              $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);

	              itemCount++;
	            });

	            if (itemCount == 0) {
	              var editorMenuNew = editorMenuTmp.clone();
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']")
	                  .attr("value", "");
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']")
	                  .attr("value", "");

	              $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);
	            }
	          });

	          removeElm();
	          gridSystemGenerator();
	          setInterval(function() {
	                handleSaveLayout()
	              }, timerSave);

	        })


	        // 自定义下拉列表 编辑画面里保存按下的场合 Start
	        function doSavecontentForDropdownList() {
	          var view = content.controls[0].custom[0].dropdownList[0].view;
	          var editor = content.controls[0].custom[0].dropdownList[0].editor;
	          var items = view[0].items;
	          var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
	          var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
	          var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

	          var currentItemName = $(currenteditor).find("[class='btn']");
	          var currentMenus = $(currenteditor).find("[class='dropdown-menu']");

	          // 项目名称
	          var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='"
	              + itemNamePlaceholder + "']");
	          if ($.trim(editorItemName.val()) == "") {
	            $(currentItemName).find("lable:first").text("项目名称");
	          } else {
	            $(currentItemName).find("lable:first").text($.trim(editorItemName.val()));
	          }
	          $(currentItemName).find("lable:last").text("");

	          // 下拉列表项目
	          $(currentMenus).empty();
	          var itemCount = 0;

	          var editorMenus = $('#dropdownListModifyModal').find("[class='view clearfix']");
	          $(editorMenus).each(function(e, t) {
	            var text = "";
	            var val = "";

	            var textObj = $(t)
	                .find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']");
	            if (textObj.length > 0) {
	              text = $.trim(textObj.val());
	            } else {
	              return true; // false时相当于break, true 就相当于continure。
	            }

	            var valObj = $(t)
	                .find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']");
	            if (valObj.length > 0) {
	              val = $.trim(valObj.val());
	            } else {
	              return true; // false时相当于break, true 就相当于continure。
	            }

	            if (text != "") {
	              $(currentMenus).append("<li><a href=\"#\" value=\"" + val + "\">" + text + "</a></li>");
	              itemCount++;
	            }
	          });

	          if (itemCount == 0) {
	            $.each(items, function(idx, item) {
	                  $(currentMenus).append("<li><a href=\"#\" value=\"" + item.value + "\">" + item.text + "</a></li>");
	                });
	          }

	          $(currentMenus).find("li").click(function(e) {
	                doClickByAForDropdownList(e);
	              });

	          $("#closeForDropdownListModal").click();
	          return false;
	        }
	        // 自定义下拉列表 编辑画面里保存按下的场合 End

	        // 自定义下拉列表 Li -> a -> Click Start
	        function doClickByAForDropdownList(e) {
	          var selText = ":" + e.currentTarget.textContent;
	          $(e.currentTarget).parent().parent().find("button[class='btn'][data-toggle='dropdown']").find("lable:last")
	              .text(selText);

	          return false;
	        }
	        // 自定义下拉列表 Li -> a -> Click End

	        // 自定义下拉列表 添加项目 Start
	        function doAddItemForDropdownList(t) {
	          var editor = content.controls[0].custom[0].dropdownList[0].editor;
	          var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
	          var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

	          var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
	          var editorMenuTmp = $(editorMenuOld).clone();
	          editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr(
	              "value", "");
	          editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']")
	              .attr("value", "");

	          $(t).parent().parent().after(editorMenuTmp);

	          return false;
	        }
	        function doDelItemForDropdownList(t) {
	          if ($(t).parent().parent().parent().find("[class='view clearfix']").length > 4) {
	            $(t).parent().parent().remove();
	          }

	          return false;
	        }
	        //触发属性选择弹出框事件  $scope.gridApi.grid.refresh();
	        $scope.aliesAttrIndex = 0;
	        $scope.aliesType = "";
	        $scope.attrAliesChange = function(row){//type,indexId
	    	    $scope.aliesAttrIndex = row.entity.indexId; 
	    	    $scope.dislogShow = row.entity.dislogShow; 
	    	    $scope.level = row.entity.level; 
	    	    $scope.childNodeIndex = row.entity.childNodeIndex; 
	    	    $scope.xmlNodeIndex = row.entity.xmlNodeIndex; 
	    	    $scope.aliesType = "xpath";
	    	   //向后台发送请求 把所有属性都列出来 弹出对话框 将匹配属性列出来 如果没有 则显示 暂无匹配数据
	            var attrArr = $scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['suggestAttrs'];
	            $("#aliesAttrs").html("");//clear
	            $("#aliesGridAttrCulum").html("");//clear
	            $("#searchSugges").val("");
	            $("#focusKey").val("");
	            $("#focusValue").val("");
	        	$("#focusType").val("");
	        	$("#focusCellValue").val("");
            	$("#code").val("");
            	$("#uuid").val("");
            	$("#root").removeAttr("checked");
	            $("#aliesAttrVersion").spinner({
	                min: 0,
	                max: 100,
	                step: 1,
	                start: 1000,
	                numberFormat: "C"
	            });
	            $("#addmore").attr("style","display:none");
	            if(!isEmptyObject(attrArr)){
	            	var aliesAttrHtml = new StringBuffer("");
	            	for(var i=0;i<attrArr.length;i++){
  	            		aliesAttrHtml.append("<div class=\"form-group\"><div class=\"col-md-4\">" +
											 "<input type=\"radio\" class=\"pull-right\" name=\"suggestAttrName\" value=\"").append(attrArr[i]["id"]).append(" \" onclick=\"getFocusCell('").append(attrArr[i]["id"]).append("','").append(attrArr[i]["label"]).append("','").append(attrArr[i]["type"]).append("','").append(escapeHtml(JSON.stringify(attrArr[i]["columns"]))).append("')\"/>" +
											 "</div><div class=\"col-md-8\"><label class=\"pull-left\">").append(attrArr[i]["label"]).append("</label>" +
											 "</div></div>");  	            		
	            	}
	            	$("#aliesAttrs").html(aliesAttrHtml.toString());
	            	$compile($("#aliesAttrs").contents())($scope);
	            }else{
	            	$("#aliesAttrs").html("");
	            }
	    	    $("#formAttrsChooseModalhh").trigger("click");
	        }
	        getFocusCell = function(key,value){
	        	$("#focusKey").val(key);
	        	$("#focusValue").val(value);
	        	$("#focusType").val(type);
	        	$("#focusCellValue").val(culmnsArr);
	        }
	        //弹出框属性选择
	        $scope.associatedAttr = function(type){
	        	if(type == '1'){
		        	if($("#focusKey").val()!='' && $("#focusKey").val()!=null){
		        		if($("#focusType").val() == 'grid'){
		        			//进入下个页面
				        	var cellValueAttr = JSON.parse($("#focusCellValue").val());
			            	var aliesGridAttrCulumHtml = new StringBuffer("");
			            	for(var i=0;i<cellValueAttr.length;i++){
			            		var cellKey = $("#focusKey").val() + "@" + cellValueAttr[i]['field'];
			            		var cellValue = $("#focusValue").val() + "@" + cellValueAttr[i]['displayName'];
			            		aliesGridAttrCulumHtml.append("<div class=\"form-group\"><div class=\"col-md-4\">" +
													 "<input type=\"radio\" class=\"pull-right\" name=\"suggestAttrName\" value=\"").append(cellKey).append(" \" onclick=\"getFocusCell('").append(cellKey).append("','").append(cellValue).append("','").append($("#focusType").val()).append("','')\"/>" +
													 "</div><div class=\"col-md-8\"><label class=\"pull-left\">").append(cellValueAttr[i]["displayName"]).append("</label>" +
													 "</div></div>");  	            		
			            	}
			            	$("#aliesGridAttrCulum").html(aliesGridAttrCulumHtml.toString());
			            	$compile($("#aliesGridAttrCulum").contents())($scope);
			            	$("#focusKey").val("");
			            	$("#focusValue").val("");
			            	$("#focusType").val("");
			            	$("#focusCellValue").val("");
			            	$("#gridAliesAttrDialogModalhh").trigger("click");
		        		}else{
			        		$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['aliesAttrs']['id'] = $("#focusKey").val();
			        		$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['aliesAttrs']['label'] = $("#focusValue").val();
			        		$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['matchDegree'] = "100%";

							//重新渲染页面
		        			if($scope.attrJson[0].xpath != undefined){
		        				xmlGridBuild($scope.attrJson,'xpath');
		        			}
		        			//渲染XML
		        			setAliesAttr("2");
		        		}
		        	}else if($("#aliesAttrVersion").val()!='0' || $("#code").val()!= '' || $("#uuid").val()!= '' || $("#root").is(":checked")){
			    			$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['code'] = $("#code").val();
			    			$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['uuid'] = $("#uuid").val();
			    			$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['root'] = $("#root").is(":checked");
		        			if($("#aliesAttrVersion").val()!='0'){
		        				$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['aliesAttrVersion'] = $("#aliesAttrVersion").val();
		        			}
		        			$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['matchDegree'] = "100%";
		        			
							//重新渲染页面
		        			if($scope.attrJson[0].xpath != undefined){
		        				xmlGridBuild($scope.attrJson,'xpath');
		        			}
		        			//渲染XML
		        			setAliesAttr("2");
		        	}
	        	}else if(type == '2'){
	        		if($("#focusKey").val()!='' && $("#focusKey").val()!=null){
			        		$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['aliesAttrs']['id'] = $("#focusKey").val();
			        		$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['aliesAttrs']['label'] = $("#focusValue").val();
			        		$scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['matchDegree'] = "100%";
	
							//重新渲染页面
		        			if($scope.attrJson[0].xpath != undefined){
		        				xmlGridBuild($scope.attrJson,'xpath');
		        			}
		        			//渲染XML
		        			setAliesAttr("2");
	        		}
	        	}
	        }
	        //弹出路径和描述
	        $scope.showPathAndDiscription = function(row){
	        	$scope.aliesAttrIndex = row.entity.indexId; 
	    	    $scope.dislogShow = row.entity.dislogShow; 
	    	    $scope.level = row.entity.level; 
	    	    $scope.childNodeIndex = row.entity.childNodeIndex; 
	    	    $scope.xmlNodeIndex = row.entity.xmlNodeIndex; 
	        	$("#pathLabel").html(row.entity.path);
	        	$("#markLabel").val(row.entity.mark);
	        	$("#formTipResultModalhh").trigger("click");
	        }
	        
	        //记录关键字选中
	        $scope.checkPoint = function(row){
	        	if(row.entity.key){
	        		$scope.attrJson[0]["xpath"][row.entity.indexId]['key'] = false;
	        	}else{
	        		$scope.attrJson[0]["xpath"][row.entity.indexId]['key'] = true;
	        	}
	        	xmlGridBuild($scope.attrJson,"xpath");
    			//渲染XML
    			setAliesAttr("2");
	        }
	        
	        $scope.saveMark = function(){
	        	//向json中添加一个描述
	        	$scope.attrJson[0]['xpath'][$scope.aliesAttrIndex]['mark'] = $("#markLabel").val();
	        	xmlGridBuild($scope.attrJson,"xpath");
	        	//重构XML展示
	        	setAliesAttr("2");
	        }
	        
            $scope.importGridOptions = {};
            $scope.importGridOptions.columnDefs = [{
                field : 'name',
                width : '25%',
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}">{{row.entity.name}}<span ng-if="row.entity.dislogShow == true"><a class="btn btn-xs btn-primary" style="float: right;width: 10%;" ng-click="grid.appScope.showPathAndDiscription(row)"><span class="glyphicon glyphicon-tag"></span></a></div></div>',
                displayName : "name"
              }
            ,{
                  field : 'aliesAttr',
                  width : '25%',
                  cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.aliesAttr}}">{{row.entity.aliesAttr}}</div>',
                  displayName : "aliesAttr"
              }
            ,{
                field : 'matchDegreeegree',
                width : '20%',
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.matchDegree}}"><div class="progress" ng-if="row.entity.dislogShow == true"><div class="progress-bar" role="progessbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:{{row.entity.matchDegree}}"></div></div></div>',
                displayName : "Matching degree"
            }, 
            {
                field : 'action',
                width : '*',
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="attrAliesChange" ><div ng-if="row.entity.dislogShow == true"><a class="btn btn-xs btn-primary" ng-click="grid.appScope.attrAliesChange(row)" style="width: 15%;"><span class="glyphicon glyphicon-random"></span></a></div></div>'
            },
            {
                field : 'key',
                width : '10%',
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.key}}"><div ng-if="row.entity.dislogShow == true"><input type="checkbox" ng-click="grid.appScope.checkPoint(row)"  style="margin-left: 47%;"/></div></div>',
                displayName : "key"
            }];
            $scope.importGridOptions.data = [];
            //构造grid数据
	        xmlGridBuild = function(attrJson,type){
	            //装载数据	     
	        	$scope.importGridOptions.data = [];
	        	for(var m=0;m<attrJson[0][type].length;m++){
	        		var xmlArr = {};
	        		var obj = attrJson[0][type][m];
	        		xmlArr.name = obj['name'];
	        		xmlArr.path = obj['path'];
					if(!isEmptyObject(obj['aliesAttrs'])){
						xmlArr.aliesAttr = obj['aliesAttrs']['label'];
					}else{
						xmlArr.aliesAttr = "";
					}
					xmlArr.indexId = m;
					xmlArr.mark = obj['mark'];
					xmlArr.key = obj['key'];
					xmlArr.matchDegree = obj['matchDegree']; 
            		if(obj['level'] == '0'){
            			xmlArr.$$treeLevel = 0;
            		}
            		xmlArr.level = obj['level'];
            		xmlArr.dislogShow = obj['dislogShow'];//是否有子节点
            		xmlArr.childNodeIndex = obj['childNodeIndex'];
            		xmlArr.xmlNodeIndex = obj['xmlNodeIndex'];
            		$scope.importGridOptions.data.push(xmlArr);// ui-grid data push
	        	}
	        }
            	
	        //xsd文件上传并读取json页面展示
	        $scope.doUpload = function(){
	        	var file=$("#file");
	              if($.trim(file.val())==''){
		    		   $("#post_result").text("{{\"data_manage_page.export_file.file_chk_result_null\" | translate}}");
		    		   $compile($('#formPostResultModal').contents())($scope);
		    		   $("#formPostResultModalhh").trigger("click");
	                   return false;
	               }
                var filepath=$("#file").val();
                //获得上传文件名
                var fileArr=filepath.split("\\");
                var fileTArr=fileArr[fileArr.length-1].toLowerCase().split(".");
                var filetype=fileTArr[fileTArr.length-1];
                //切割出后缀文件名
                if(filetype != "xsd" &&　filetype != "xml"){
		    		$("#post_result").text("{{\"data_manage_page.export_file.file_chk_result_format\" | translate}}");
		    		$compile($('#formPostResultModal').contents())($scope);
		    		$("#formPostResultModalhh").trigger("click");               	
                    return false;
                }
	            var formData = new FormData($( "#uploadForm" )[0]); 
	            formData.append("multiFile", document.getElementById("file").files[0]);
	            
                var deferred = $q.defer();
                $http({
                      method : 'POST',
                      url : 'dataManagementCtl/analyzeFile',
                      data : formData,
                      headers : {
                        'Content-Type' : undefined
                      }
                    }).success(function(data) {
                      deferred.resolve(data);
                    }).error(function(data) {
                      deferred.reject(data);
                    });
                
                deferred.promise.then(function(data) {
                    console.log('doSourceSave 请求成功');
                    if (data) {
                    	if(data.schemaList){
                    		//编译html
                    		$scope.attrJson = [];
                    		$scope.attrXML = data.schemaList;
                    		parseXML();//将XML解析成JSON
                    		setAliesAttr("1");
                    		$("#xmlLayout").html($scope.attrXML);
                    		$scope.fileName = data.fileName;
                    		$("#baseXmlLayout").html(data.baseFileFormatStr);
                    		if($scope.attrJson.length > 0){
                    			if($scope.attrJson[0].xpath != undefined){
                    				//重构leaf
                    				xmlGridBuild($scope.attrJson,'xpath')
                    			}
                    		}
                    	}
                    }
                  }, function(data) {
                    console.log('doSourceSave 请求失败');
                  });              
                
	       };
	       //rebuild XML
	       parseXML = function(){
	    	   
	    	   var xml = $scope.attrXML;
	    	   
	    	   try //Internet Explorer
	    	     {
		    	     xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		    	     xmlDoc.async="false";
		    	     xmlDoc.loadXML(xml);
	    	     }
	    	   catch(e){
		    	   try //Firefox, Mozilla, Opera, etc.
		    	     {
			    	     parser=new DOMParser();
			    	     xmlDoc=parser.parseFromString(xml,"text/xml");
		    	     }
		    	     catch(e){
			    	     alert(e.message);
			    	     return;
		    	     }
	    	   }
	    	   var xmlObj = xmlDoc.getElementsByTagName("dao")[0].childNodes;
	    	   var xmlObjAttr = {};
	    	   var xmlAttrs = [];
	    	   for(var i=0;i<xmlObj.length;i++){
	    		   var xpathCell = {};
	    		   if(xmlObj[i].childNodes.length > 0){
	    			   //有子节点 为一级
	    			   xpathCell['name'] = xmlObj[i].getAttribute("name");
	    			   xpathCell['path'] = xmlObj[i].getAttribute("path");
	    			   xpathCell['level'] = '0';
	    			   xpathCell['xmlNodeIndex'] = i;
	    			   xpathCell['dislogShow'] = false;
	    			   xmlAttrs.push(xpathCell);
	    			   for(var j=0;j<xmlObj[i].childNodes.length;j++){
	    				   var sonArr = {};
	    				   if(xmlObj[i].childNodes[j].nodeName == 'xpath'){
		    				   sonArr['matchDegree'] = xmlObj[i].childNodes[j].getAttribute('matchDegree');
		    				   sonArr['name'] = xmlObj[i].childNodes[j].getAttribute('name');
		    				   sonArr['path'] = xmlObj[i].childNodes[j].getAttribute('path');
		    				   sonArr['suggestAttrs'] = JSON.parse(xmlObj[i].childNodes[j].getAttribute('suggestAttrs'));
		    				   if(isEmptyObject(xmlObj[i].childNodes[j].getAttribute('suggestAttrs'))){
		    					   sonArr["aliesAttrs"]=JSON.parse(xmlObj[i].childNodes[j].getAttribute('suggestAttrs'))[0];
		    				   }else{
		    					   var nullArr = {};
		    					   sonArr["aliesAttrs"] = nullArr;
		    				   }
		    				   sonArr["mark"] = "";
		    				   sonArr["key"] = false;
	    					   sonArr["code"] = "";
	    					   sonArr["uuid"] = "";
	    					   sonArr["root"] = ""; 
	    					   sonArr["aliesAttrVersion"] = "0";
		    				   sonArr['level'] = '1';
		    				   sonArr['dislogShow'] = true;
		    				   sonArr['xmlNodeIndex'] = i;
		    				   sonArr['childNodeIndex'] = j;
		    				   xmlAttrs.push(sonArr);
	    				   }
	    			   }
	    		   }else{
	    			   //二级
					   if(xmlObj[i].nodeName == 'xpath'){
		    			   var xpathSonCell = {};
		    			   xpathSonCell['xmlNodeIndex'] = i;
		    			   xpathSonCell['matchDegree'] = xmlObj[i].getAttribute('matchDegree');
		    			   xpathSonCell['name'] = xmlObj[i].getAttribute('name');
		    			   xpathSonCell['path'] = xmlObj[i].getAttribute('path');
		    			   xpathSonCell['suggestAttrs'] = JSON.parse(xmlObj[i].getAttribute('suggestAttrs'));
		  				   if(isEmptyObject(xmlObj[i].getAttributeNode('suggestAttrs'))){
		  					   xpathSonCell["aliesAttrs"]=JSON.parse(xmlObj[i].getAttribute('suggestAttrs'))[0];
		  				   }else{
		  					   var nullArr = {};
		  					   xpathSonCell["aliesAttrs"] = nullArr;
		  				   }
		  				   xpathSonCell["mark"] = "";
		  				   xpathSonCell["key"] = "";
	  					   xpathSonCell["code"] = "";
	  					   xpathSonCell["uuid"] = "";
	  					   xpathSonCell["root"] = ""; 
	  					   xpathSonCell["aliesAttrVersion"] = "0";
		  				   xpathSonCell['level'] = '0';
		  				   xpathSonCell['dislogShow'] = true;
		  				   xmlAttrs.push(xpathSonCell);
			    		   }
		    		   }
		    	   }
	    	   xmlObjAttr["xpath"] = xmlAttrs;
	    	   $scope.attrJson.push(xmlObjAttr);
	       }	  
	       
	       //属性赋值
	       setAliesAttr = function(type){
	    	   var xmlAttr = $scope.attrXML;
	    	   try //Internet Explorer
	    	     {
		    	     xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
		    	     xmlDoc.async="false";
		    	     xmlDoc.loadXML(xmlAttr);
	    	     }
	    	   catch(e){
		    	   try //Firefox, Mozilla, Opera, etc.
		    	     {
			    	     parser=new DOMParser();
			    	     xmlDoc=parser.parseFromString(xmlAttr,"text/xml");
		    	     }
		    	     catch(e){
			    	     alert(e.message);
			    	     return;
		    	     }
	    	   	 }
	    	   if(type == "1"){
		    	   for(var i=0;i<xmlDoc.getElementsByTagName("xpath").length;i++){
		    		   xmlDoc.getElementsByTagName("xpath")[i].setAttribute("field",""); 
		    		   xmlDoc.getElementsByTagName("xpath")[i].setAttribute("mark",""); 
		    		   xmlDoc.getElementsByTagName("xpath")[i].setAttribute("key",false); 
    				   //去掉推荐元素
    				   xmlDoc.getElementsByTagName("xpath")[i].removeAttribute("suggestAttrs");
		    	   }
	    	   }else if(type == "2"){
	    		   //xml渲染 $scope.level = row.entity.level; 
	    		    var jsonAttrObj = $scope.attrJson[0]["xpath"][$scope.aliesAttrIndex]; 
	    		    var attrName =  jsonAttrObj['name'];
		    	    if($scope.dislogShow && $scope.level == '0'){
		    	    	//xpath
		    	    	if(!isEmptyObject(jsonAttrObj["aliesAttrs"])){
		    	    		xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode("field").nodeValue = jsonAttrObj["aliesAttrs"]["id"];
		    	    	}else{
		    	    		xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode("field").nodeValue = "";
		    	    	}
					   if(jsonAttrObj["uuid"]!=null && jsonAttrObj["uuid"]!=''){
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName).nodeValue = jsonAttrObj['uuid'];
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].setAttribute(attrName,jsonAttrObj['uuid']);   
						   }
					   }
					   if(jsonAttrObj["code"]!=null && jsonAttrObj["code"]!=''){
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName).nodeValue = jsonAttrObj['code'];
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].setAttribute(attrName,jsonAttrObj['code']);   
						   }
					   }
					   if(jsonAttrObj["root"]!=null && jsonAttrObj["root"]!=''){
						   var rootValue = "";
						   if(jsonAttrObj["root"]){
							   rootValue = "UUID.randomUUID()";
						   }
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName).nodeValue = rootValue;
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].setAttribute(attrName,rootValue);   
						   }
					   }
					   if(jsonAttrObj["aliesAttrVersion"]!=null && jsonAttrObj["aliesAttrVersion"]!=''&& jsonAttrObj["aliesAttrVersion"]!='0'){
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode(attrName).nodeValue = jsonAttrObj['aliesAttrVersion'];
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].setAttribute(attrName,jsonAttrObj["aliesAttrVersion"]);   
						   }
					   }
					   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode("matchDegree").nodeValue = jsonAttrObj["matchDegree"];	
					   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode("mark").nodeValue = jsonAttrObj["mark"];
					   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].getAttributeNode("key").nodeValue = jsonAttrObj["key"];
		    	    }else if($scope.dislogShow && $scope.level == '1'){
		    	    	//node
		    	    	if(!isEmptyObject(jsonAttrObj["aliesAttrs"])){
		    	    		xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode("field").nodeValue = jsonAttrObj["aliesAttrs"]["id"];
		    	    	}else{
		    	    		xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode("field").nodeValue = "";
		    	    	}
					   if(jsonAttrObj["uuid"]!=null && jsonAttrObj["uuid"]!=''){
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName).nodeValue = jsonAttrObj['uuid'];
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].setAttribute(attrName,jsonAttrObj['uuid']);   
						   }
					   }
					   if(jsonAttrObj["code"]!=null && jsonAttrObj["code"]!=''){
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName).nodeValue = jsonAttrObj['code'];
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].setAttribute(attrName,jsonAttrObj['code']);   
						   }
					   }
					   if(jsonAttrObj["root"]!=null && jsonAttrObj["root"]!=''){
						   var rootValue = "";
						   if(jsonAttrObj["root"]){
							   rootValue = "UUID.randomUUID()";
						   }
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName).nodeValue = rootValue;
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].setAttribute(attrName,rootValue);   
						   }
					   }
					   if(jsonAttrObj["aliesAttrVersion"]!=null && jsonAttrObj["aliesAttrVersion"]!='' && jsonAttrObj["aliesAttrVersion"]!='0'){
						   if(xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName) != undefined){
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode(attrName).nodeValue = jsonAttrObj['aliesAttrVersion'];
						   }else{
							   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].setAttribute(attrName,jsonAttrObj["aliesAttrVersion"]);   
						   }
					   }
					   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode("matchDegree").nodeValue = jsonAttrObj["matchDegree"];	
					   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode("mark").nodeValue = jsonAttrObj["mark"];
					   xmlDoc.getElementsByTagName("dao")[0].childNodes[$scope.xmlNodeIndex].childNodes[$scope.childNodeIndex].getAttributeNode("key").nodeValue = jsonAttrObj["key"];
		    	    }
	    	   }

	    	   $scope.attrXML = XMLtoString(xmlDoc);//xml转化为字符串
	       }
	       
	       //保存映射关系
	       $("#exportXML").on("click",function(){
	    	   if(isEmptyObject($scope.attrJson)){
	    		   $("#post_result").text("{{\"data_manage_page.export_file.file_post_result_null\" | translate}}");
	    		   $compile($('#formPostResultModal').contents())($scope);
	    		   $("#formPostResultModalhh").trigger("click");
	    		   return;
	    	   }
	    	   //判断是否选择关键词
	    	   var keyBs = "";
	    	   label_key_chk:for(var i=0;i<$scope.attrJson[0]["xpath"].length;i++){
	    		   if($scope.attrJson[0]["xpath"][i]["key"] == true){
	    			   keyBs = "1";
	    			   break label_key_chk;
	    		   }
	    	   }
	    	   if(keyBs == ""){
	    		   $("#post_result").text("{{\"data_manage_page.export_file.file_chk_result_key_null\" | translate}}");
	    		   $compile($('#formPostResultModal').contents())($scope);
	    		   $("#formPostResultModalhh").trigger("click");
	    		   return;
	    	   }
	    	   //给XML赋值
	    	   var xmlData = {};
	    	   xmlData.jsonStr = $scope.attrXML;
	    	   xmlData.fileName = $scope.fileName;
               var deferred = $q.defer();
               $http({
                     method : 'POST',
                     url : 'dataManagementCtl/saveMappingFile',
                     data : xmlData
                   }).success(function(data) {
                     deferred.resolve(data);
                   }).error(function(data) {
                     deferred.reject(data);
                   });
               
               deferred.promise.then(function(data) {
                   console.log('exportXML 请求成功');
                   if (data) {
	                   	if(data.resuleStr == '0000'){
	                   		console.log("导出成功");
	                   		$("#exportResult").text("{{\"data_manage_page.import_file.file_import_success\" | translate}}");
	                   		$compile($('#formExportResultModal').contents())($scope);
	                   		$("#formExportResultModalhh").trigger("click");
	                   	}else{
	                   		//缺少mapping已经存在是否覆盖的过程
	                   		console.log("导出失败");
	                   		$("#exportResult").text("{{\"data_manage_page.import_file.file_import_fail\" | translate}}");
	                   		$compile($('#formExportResultModal').contents())($scope);
	                   		$("#formExportResultModalhh").trigger("click");
	                   	}
                   }
                 }, function(data) {
                	 $("#exportResult").text("{{\"data_manage_page.import_file.file_import_fail\" | translate}}");
                	 $compile($('#formExportResultModal').contents())($scope);
               		 $("#formExportResultModalhh").trigger("click");
                     console.log('exportXML 请求失败');
                 });
	       });
	       //属性关联模糊查询
	       searchSuggest = function(event){
	    	  if(isEmptyStr(event.value)){
              	//渲染默认属性
  	            var attrArr = $scope.attrJson[0][$scope.aliesType][$scope.aliesAttrIndex]['suggestAttrs'];
  	            $("#aliesAttrs").html("");
  	            $("#addmore").attr("style","display:none");
  	            if(!isEmptyObject(attrArr)){
	  	            	var aliesAttrHtml = new StringBuffer("");
	  	            	for(var i=0;i<attrArr.length;i++){
	  	            		aliesAttrHtml.append("<div class=\"form-group\"><div class=\"col-md-4\">" +
												 "<input type=\"radio\" class=\"pull-right\" name=\"suggestAttrName\" value=\"").append(attrArr[i]["id"]).append(" \" onclick=\"getFocusCell('").append(attrArr[i]["id"]).append("','").append(attrArr[i]["label"]).append("','").append(attrArr[i]["type"]).append("','").append(escapeHtml(JSON.stringify(attrArr[i]["columns"]))).append("')\"/>" +
												 "</div><div class=\"col-md-8\"><label class=\"pull-left\">").append(attrArr[i]["label"]).append("</label>" +
												 "</div></div>");  
	  	            	}
	  	            	$("#aliesAttrs").html(aliesAttrHtml.toString());
	  	            	$compile($("#aliesAttrs").contents())($scope);
	  	            }else{
	  	            	$("#aliesAttrs").html("");
	  	            }
  	            	return false;  
	    	  }else{
	    		  $("#addmore").attr("style","display:none");
	    	  }
	    	  var postParam = {};
	    	  postParam.param = event.value;
	    	  var deferred = $q.defer();
              $http({
                  method : 'POST',
                  url : 'dataManagementCtl/searchAliesAttrsByKeyWord',
                  data : postParam
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            
	            deferred.promise.then(function(data) {
	                console.log('关联属性模糊查询 请求成功');
	                if (data) {
	    	            $("#aliesAttrs").html("");
	    	            $scope.length = 5;
	    	            $scope.aliesMoreData = data;//用于加载更多
		            	var aliesAttrHtml = new StringBuffer();
		            	var dataLength = $scope.length;
		            	if(data.length <= $scope.length){
		            		dataLength = data.length;
		            		$("#addmore").attr("style","display:none");
		            	}else{
		            		$("#addmore").attr("style","display:inline");
		            	}
		            	for(var i=0;i<dataLength;i++){
		            		if(data[i]["describe"] == null){
		            			data[i]["describe"] = "";
		            		}
	  	            		aliesAttrHtml.append("<div class=\"form-group\"><div class=\"col-md-4\">" +
												 "<input type=\"radio\" class=\"pull-right\" name=\"suggestAttrName\" value=\"").append(data[i]["id"]).append(" \" onclick=\"getFocusCell('").append(data[i]["id"]).append("','").append(data[i]["label"]).append("','").append(data[i]["type"]).append("','").append(escapeHtml(JSON.stringify(data[i]["columns"]))).append("')\"/>" +
												 "</div><div class=\"col-md-8\"><label class=\"pull-left\">").append(data[i]["label"]).append("</label>" +
												 "</div></div>");	            	
		            	}
		            	$("#aliesAttrs").html(aliesAttrHtml.toString());
		            	$compile($("#aliesAttrs").contents())($scope);
	                }else{
	                	$("#addmore").attr("style","display:none");
	                }
	              }, function(data) {
	                console.log('关联属性模糊查询 请求失败');
	              });
	       }
	       $("#addmore").on("click",function(){
	    	    $scope.length = $scope.length + 2;
	    	    if($scope.length >= $scope.aliesMoreData.length){
	    	    	$("#addmore").attr("style","display:none");
	    	    }else{
		    	    $("#aliesAttrs").html("");
		    	    var aliesAttrHtml = new StringBuffer();
		    	    var data = $scope.aliesMoreData
		           	for(var i=0;i<$scope.length;i++){
  	            		aliesAttrHtml.append("<div class=\"form-group\"><div class=\"col-md-4\">" +
											 "<input type=\"radio\" class=\"pull-right\" name=\"suggestAttrName\" value=\"").append(data[i]["id"]).append(" \" onclick=\"getFocusCell('").append(data[i]["id"]).append("','").append(data[i]["label"]).append("','").append(data[i]["type"]).append("','").append(escapeHtml(JSON.stringify(data[i]["columns"]))).append("')\"/>" +
											 "</div><div class=\"col-md-8\"><label class=\"pull-left\">").append(data[i]["label"]).append("</label>" +
											 "</div></div>");			        	
  	            		}
		        	$("#aliesAttrs").html(aliesAttrHtml.toString());
		        	$compile($("#aliesAttrs").contents())($scope);	
	    	    }
	       });
	       //清空其他输入框
	       clearOthers = function(val){
	    	   switch(val)
	    	   {
		    	   case 'code':
		            	 $("#uuid").val("");
		            	 $("#root").removeAttr("checked");
			             $("#aliesAttrVersion").val("0");
			    	     break;
		    	   case 'root':
		            	 $("#uuid").val("");
		            	 $("#code").val("");
		            	 $("#aliesAttrVersion").val("0");
			    	     break;
		    	   case 'aliesAttrVersion':
		            	 $("#code").val("");
		            	 $("#uuid").val("");
		            	 $("#root").removeAttr("checked");
		    		   	 break;
		    	   default:
		    		   console.log("没找到");
	    	   }
	       }
	        //生成UUID
	       $scope.getUUIDEvent = function(){
	        	 $("#uuid").val(getUUID());
	           	 $("#code").val("");
	        	 $("#root").removeAttr("checked");
	        	 $("#aliesAttrVersion").val("0");
	       }
	       //-----------------------工具类部分-----------------------------
	       //定义StringBuffer
	       function StringBuffer() {
	    	    this.__strings__ = new Array();
	    	}
	    	StringBuffer.prototype.append = function (str) {
	    	    this.__strings__.push(str);
	    	    return this;    //方便链式操作
	    	}
	    	StringBuffer.prototype.toString = function () {
	    	    return this.__strings__.join("");
	    	}
	        //判断是否为空
	        function isEmptyObject(e) {  
	            var t;  
	            for (t in e)  
	                return !1;  
	            return !0  
	        }
	        function isEmptyStr(str){
	        	return (str == null || str == '')
	        }
	       //document xml对象转化为String
	       function XMLtoString(elem){  
	    	    var serialized;  
	    	    try {  
	    	        // XMLSerializer exists in current Mozilla browsers                                                                              
	    	        serializer = new XMLSerializer();                                                                                                
	    	        serialized = serializer.serializeToString(elem);                                                                                 
	    	    }                                                                                                                                    
	    	    catch (e) {  
	    	        // Internet Explorer has a different approach to serializing XML                                                                 
	    	        serialized = elem.xml;                                                                                                           
	    	    }      
	    	    return serialized;                                                                                                                   
	    	}  
	       //获取UUID
	       getUUID = function() {
	    	   var s = [];
	    	   var hexDigits = "0123456789abcdef";
	    	   for (var i = 0; i < 36; i++) {
	    	   s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
	    	   }
	    	   s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
	    	   s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
	    	   s[8] = s[13] = s[18] = s[23] = "-";

	    	   var uuid = s.join("");
	    	   return uuid;
	       }
	       function escapeHtml(string) {
	    	    var entityMap = {
	    	        "&": "&amp;",
	    	        "<": "&lt;",
	    	        ">": "&gt;",
	    	        '"': '&quot;',
	    	        "'": '&#39;',
	    	        "/": '&#x2F;'
	    	    };
	    	    return String(string).replace(/[&<>"'\/]/g, function (s) {
	    	        return entityMap[s];
	    	    });
	    	}
   	       //获取UUID
   	       getUUID = function() {
   	    	   var s = [];
   	    	   var hexDigits = "0123456789abcdef";
   	    	   for (var i = 0; i < 36; i++) {
   	    	   s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
   	    	   }
   	    	   s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
   	    	   s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
   	    	   s[8] = s[13] = s[18] = s[23] = "-";
   
   	    	   var uuid = s.join("");
   	    	   return uuid;
   	       }	 
	 }]);
});
