define(['controllers/controllers'], function(controllers) {
  controllers.controller('dashBoardCtrl', ['$scope', '$location', '$q', '$http', 'transl', 'gridUtil', '$timeout', '$rootScope',
      function($scope, $location, $q, $http, transl, gridUtil, $timeout, $rootScope) {

        if ($(".modal-backdrop")) {
          $(".modal-backdrop").remove();
        }
        var gridData = [];
        // 定时保存生成html5代码，缓存作用
        var timerSave = 1000;
        var stopsave = 0;
        var startdrag = 0;
        var demoHtml = $(".demo").html();
        var currenteditor = null;

        var index = 0;
        var idList = new Array();
        var labelValues = {};

        $scope.adjustIndent = function(type, direction, $event) {
          if ($event) {
            var targetObj = $($event.currentTarget);
            var ownObj = $("#" + targetObj.attr("own_id"));
            if (ownObj == false) {
              return;
            }

            var margin_left = ownObj.css("margin-left").replace("px", "");
            var margin_right = ownObj.css("margin-right").replace("px", "");
            if (margin_left === "") {
              margin_left = 0;
            }
            if (margin_right === "") {
              margin_right = 0;
            }

            var defNum = 5;
            var minNum = 0;
            var maxNum = 50;

            margin_left = Number(margin_left);
            margin_right = Number(margin_right);

            if (type === 'add' && direction === 'left') {
              margin_left = margin_left + defNum;
              if (margin_left > maxNum) {
                margin_left = maxNum;
              }
            } else if (type === 'add' && direction === 'right') {
              margin_right = margin_right + defNum;
              if (margin_right > maxNum) {
                margin_right = maxNum;
              }
            } else if (type === 'subtra' && direction === 'left') {
              margin_left = margin_left - defNum;
              if (margin_left < minNum) {
                margin_left = minNum;
              }
            } else if (type === 'subtra' && direction === 'right') {
              margin_right = margin_right - defNum;
              if (margin_right < minNum) {
                margin_right = minNum;
              }
            }

            ownObj.css("margin-left", margin_left + "px");
            ownObj.css("margin-right", margin_right + "px");

            $timeout(function() {
                  $scope.gridResize();
                  $(window).resize();
                });
          }
        }

        // 编辑按钮按下
        $scope.layoutEdit = function() {
          $scope.gridResize();

          if ($('body').hasClass("page-sidebar-closed")) {
            if ($('.sidebar-search').hasClass('open') == false) {
              $('.page-sidebar .sidebar-toggler').click();
            }
          }

          $("body").removeClass("devpreview sourcepreview");
          $("body").addClass("edit");
          $("#menu-layoutit li button").removeClass("active");
          $(this).addClass("active");

          var t = $(".demo").children();
          t.each(function(i, e) {
                // $(e).find("div.row.clearfix
                // div").find(".box.box-element.ui-draggable.ng-isolate-scope").css("marginLeft",
                // "0px");
              });

          $timeout(function() {

                $("#edit").attr("disabled", "disabled");
                $("#save").removeAttr("disabled");
                $("#undo").removeAttr("disabled");

                $(window).resize();
              });

          return false;
        }

        $scope.gridResize = function() {
          setTimeout(function() {
                // $scope.gridApi.grid.gridWidth = 300;
                // $scope.gridApi.grid.refreshCanvas(true);
                $(window).resize();
              }, 500);
        }

        // 非编辑状态时,页面显示
        function viewToHtml() {
          $("body").removeClass("edit");
          $("body").addClass("devpreview sourcepreview");
          $("#menu-layoutit li button").removeClass("active")
          $(this).addClass("active");

          var t = $(".demo").children();
          t.each(function(i, e) {
                var items = $(e).find("div.row.clearfix div").find(".box.box-element.ui-draggable.ng-isolate-scope");
                angular.forEach(items, function(value, key) {
                      var item = $(value);

                      var margin_left = item.css("margin-left").replace("px", "");
                      var margin_right = item.css("margin-right").replace("px", "");

                      if (margin_left === "") {
                        margin_left = 0;
                      }
                      if (margin_right === "") {
                        margin_right = 0;
                      }

                      margin_left = Number(margin_left);
                      margin_right = Number(margin_right);

                      var defNum = 10;

                      if (margin_left === 0 && margin_right === 0) {
                        margin_left = defNum;
                        margin_right = defNum;

                        item.css("margin-left", margin_left + "px");
                        item.css("margin-right", margin_right + "px");
                      }
                    });
              });

          return false;
        }

        // 保存按钮按下
        $scope.layoutSaveToDB = function() {
          if ($("body").hasClass("edit")) {
            viewToHtml();
            saveToDb();
            $scope.layoutReload();
          }
        }

        // 取消按钮按下
        $scope.layoutReload = function() {
          window.top.location.href = "index";
        }

        // 保存到DB
        function saveToDb() {
          var data = {};
          data.corpCode = "Trawind";
          data.formId = "homePage";
          data.formName = "homePage";

          $(".demo").find("div.syslogGrid").parent().empty();
          $(".demo").find("div.productGrid").parent().empty();
          $(".demo").find("div.productAuditingGrid").parent().empty();

          data.layout = $(".demo").html();
          // data.directJson = {"todo":"todo"};
          data.widgetIds = $scope.idList.join(",");
          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'homeForms/saveHomeForms',
                'data' : JSON.stringify(data)
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {
                console.log('请求成功');
              }, function(data) {
                console.log('请求失败');
              });

          return false;
        }

        // 判断浏览器是否支持html5本地保存
        function supportstorage() {
          if (typeof window.localStorage == 'object')
            return true;
          else
            return false;
        }
        // 定时保存页面布局
        function handleSaveLayout() {
          var e = $(".demo").html();
          if (!stopsave && e != window.demoHtml) {
            stopsave++;
            window.demoHtml = e;
            // saveLayout();
            stopsave--;
          }
        }

        var layouthistory;
        // 保存布局
        // function saveLayout() {
        // var data = layouthistory;
        // if (!data) {
        // data = {};
        // data.count = 0;
        // data.list = [];
        // }
        // if (data.list.length > data.count) {
        // for (i = data.count; i < data.list.length; i++)
        // data.list[i] = null;
        // }
        // data.list[data.count] = window.demoHtml;
        // data.count++;
        // if (supportstorage()) {
        // localStorage.setItem("layoutdata", JSON.stringify(data));
        // }
        // layouthistory = data;
        // }

        // 自适应
        $(window).resize(function() {
              $("body").css("min-height", $(window).height() - 90);
              $(".demo").css("min-height", $(window).height() - 160);

              $("#editorModal").find(".modal-dialog").css("marginLeft", (($(window).width() - $("#editorModal").find(".modal-content").width()) / 2));
            });

        $scope.idList = [];
        function bindEvent(fieldId) {
          idList.push(fieldId);
          $scope.idList = idList;
          if (supportstorage) {
            window.localStorage.setItem("idList", idList);
          }
        }

        // 删除元素
        function removeElm() {
          $(".demo").delegate(".remove", "click", function(e) {
                e.preventDefault();
                $(this).parent().remove();
                if (!$(".demo .lyrow").length > 0) {
                  clearDemo()
                }
              })
        }

        // 删除行模板代码
        function cleanHtml(e) {
          $(e).parent().append($(e).children().html())
        }

        // 初始化容器
        function initContainer() {
          /*
           * $(".demo, .demo .column").sortable({ connectWith : ".column",
           * opacity : .35, handle : ".drag", start : function(e, t) { if
           * (!startdrag) stopsave++; startdrag = 1; }, stop : function(e, t) {
           * if (stopsave > 0) stopsave--; startdrag = 0; t.item.find(".view
           * input:last").attr("id", "item" + index); bindEvent("item" + index);
           * index++; } });
           */
          // 初始化容器内元素(添加方向，排版等按钮事件)
          configurationElm();
        }

        // 清空Container
        function clearDemo() {
          $(".demo").empty();
          layouthistory = null;
          if (supportstorage())
            localStorage.removeItem("layoutdata");
        }

        // 拖拽后内容 编辑的各种事件
        function configurationElm(e, t) {
          $(".demo").delegate(".configuration > a", "click", function(e) {
                e.preventDefault();
                var t = $(this).parent().next().next().children();
                $(this).toggleClass("active");
                t.toggleClass($(this).attr("rel"))
              });
          $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
                e.preventDefault();
                var t = $(this).parent().parent();
                var n = t.parent().parent().next().next().children();
                t.find("li").removeClass("active");
                $(this).parent().addClass("active");
                var r = "";
                t.find("a").each(function() {
                      r += $(this).attr("rel") + " "
                    });
                t.parent().removeClass("open");
                n.removeClass(r);
                n.addClass($(this).attr("rel"))
              })
        }

        function gridSystemGenerator() {
          $(".lyrow .preview input").bind("keyup", function() {
                var e = 0;
                var t = "";
                var n = $(this).val().split(" ", 12);
                $.each(n, function(n, r) {
                      e = e + parseInt(r);
                      t += '<div class="span' + r + ' column"></div>'
                    });
                if (e == 12) {
                  $(this).parent().next().children().html(t);
                  $(this).parent().prev().show()
                } else {
                  $(this).parent().prev().hide()
                }
              })
        }

        // 交互组件生成ID
        function handleJsIds() {
        }

        // 切换header导航 active状态
        function removeMenuClasses() {
          $("#menu-layoutit li button").removeClass("active")
        }

        $scope.viewUpdateHistory = function(row) {
          if (row.entity.firstContent === "") {
            return;
          }

          var editorModal = $("#editorModal");

          if (editorModal.length > 0) {
            $scope.updateHistoryGridOptions.data = [];
            editorModal.find(".tmpValue").val(row.entity.id);
            editorModal.modal({
                  keyboard : true
                });
            editorModal.modal('show');
          }
        };

        $scope.updateHistoryGridOptions = {
          enableFiltering : true,
          enableColumnResizing : true,
          onRegisterApi : function(gridApi) {
            $scope.gridApi = gridApi
          },
          columnDefs : [],
          data : []
        };

        $scope.rollbackProductData = function(row) {
          var data = {};
          data.productId = row.entity.id;
          data.primaryId = row.entity.primaryId;
          data.updateTime = row.entity.updateTime;
          data.productLogId = row.entity.productLogId;

          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'product/rollbackProductData/',
                data : data
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {
                viewAllUpdateContentInfo(data);

                console.log('rollbackAttrValByHistory 请求成功');
              }, function(data) {
                console.log('rollbackAttrValByHistory 请求失败');
              });
        };

        $scope.acceptProductData = function(row) {
          var data = {};
          data.productId = row.entity.id;
          data.primaryId = row.entity.primaryId;
          data.updateTime = row.entity.updateTime;
          data.productLogId = row.entity.productLogId;
          data.corpId = row.entity.corpId;

          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'product/acceptProductData/',
                data : data
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {
                // product
                $http({
                      method : 'post',
                      url : 'product/getProductLinkItemInfo/8'
                    }).success(function(data) {
                      $scope.productGridOptions.data = data;
                      console.log('acceptProductData getProductLinkItemInfo 请求成功');
                    }).error(function(data) {
                      console.log('acceptProductData getProductLinkItemInfo 请求失败');
                    });

                // productAuditing
                $http({
                      method : 'post',
                      url : 'product/getProductAuditingItemInfo/8'
                    }).success(function(data) {
                      $scope.productAuditingGridOptions.data = data;
                      console.log('acceptProductData getProductAuditingItemInfo 请求成功');
                    }).error(function(data) {
                      console.log('acceptProductData getProductAuditingItemInfo 请求失败');
                    });

                console.log('acceptProductData 请求成功');
              }, function(data) {
                console.log('acceptProductData 请求失败');
              });
        };

        $scope.rejectProductData = function(row) {
          var data = {};
          data.productId = row.entity.id;
          data.primaryId = row.entity.primaryId;
          data.updateTime = row.entity.updateTime;
          data.productLogId = row.entity.productLogId;

          var deferred = $q.defer();
          $http({
                method : 'post',
                url : 'product/rejectProductData/',
                data : data
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });
          deferred.promise.then(function(data) {
                // product
                $http({
                      method : 'post',
                      url : 'product/getProductLinkItemInfo/8'
                    }).success(function(data) {
                      $scope.productGridOptions.data = data;
                      console.log('acceptProductData getProductLinkItemInfo 请求成功');
                    }).error(function(data) {
                      console.log('acceptProductData getProductLinkItemInfo 请求失败');
                    });

                // productAuditing
                $http({
                      method : 'post',
                      url : 'product/getProductAuditingItemInfo/8'
                    }).success(function(data) {
                      $scope.productAuditingGridOptions.data = data;
                      console.log('acceptProductData getProductAuditingItemInfo 请求成功');
                    }).error(function(data) {
                      console.log('acceptProductData getProductAuditingItemInfo 请求失败');
                    });

                console.log('rejectProductData 请求成功');
              }, function(data) {
                console.log('rejectProductData 请求失败');
              });
        };

        function viewAllUpdateContentInfo(data) {
          $scope.updateHistoryGridOptions.columnDefs = [{
                field : 'name',
                enableColumnResizing : true,
                enableFiltering : false,
                enableSorting : false,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}">{{row.entity.name}}</div>',
                displayName : "ProductName"
              }, {
                field : 'primaryId',
                width : 150,
                enableColumnResizing : true,
                enableFiltering : false,
                enableSorting : false,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.primaryId}}">{{row.entity.primaryId}}</div>',
                displayName : "PrimaryId"
              }, {
                field : 'updateTime',
                width : 150,
                enableColumnResizing : true,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
                displayName : "LastUpdateTime"
              }, {
                field : 'content',
                width : 250,
                enableColumnResizing : true,
                enableFiltering : true,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" >{{row.entity.firstContent}}</div>',
                displayName : "UpdateContent"
              }, {
                field : 'rollback',
                width : 80,
                enableColumnResizing : false,
                enableFiltering : false,
                enableSorting : false,
                cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="rollback" ><a class="btn btn-xs btn-primary" ng-click="grid.appScope.rollbackProductData(row)">Rollback</a></div>'
              }];

          $scope.updateHistoryGridOptions.data = data;

          $(".updateHistoryGrid").resize();
        }

        // 页面加载
        angular.element(document).ready(function() {
              $("#language_menu-layoutit").find(".dropdown-menu a").click(function(e) {
                    var id = $(this).attr("id");
                    var idArr = id.split("_");
                    if (idArr.length == 2) {
                      transl.changeLanguage(idArr[1]);
                      $rootScope.language = idArr[1];
                    }
                  });

              $("body").css("min-height", $(window).height() - 90);
              $(".demo").css("min-height", $(window).height() - 160);

              initContainer();
              $('body.edit .demo').on("click", "[data-target=#editorModal]", function(e) {
                  });
              $("#savecontent").click(function(e) {
                e.preventDefault();
                  // currenteditor.html(contenthandle.getData());
                });
              $("[data-target=#downloadModal]").click(function(e) {
                    e.preventDefault();
                    downloadLayoutSrc();
                  });
              $("[data-target=#shareModal]").click(function(e) {
                    e.preventDefault();
                    handleSaveLayout();
                  });
              $("#edit").click(function() {
                    $("body").removeClass("devpreview sourcepreview");
                    $("body").addClass("edit");
                    removeMenuClasses();

                    $("#edit").attr("disabled", "disabled");
                    $("#save").removeAttr("disabled");
                    $("#undo").removeAttr("disabled");

                    $(this).addClass("active");
                    return false
                  });
              $("#clear").click(function(e) {
                    e.preventDefault();
                    clearDemo()
                  });
              $("#devpreview").click(function() {
                    $("body").removeClass("edit sourcepreview");
                    $("body").addClass("devpreview");
                    removeMenuClasses();
                    $(this).addClass("active");
                    return false
                  });
              $("#sourcepreview").click(function() {
                    $("body").removeClass("edit");
                    $("body").addClass("devpreview sourcepreview");
                    removeMenuClasses();
                    $(this).addClass("active");
                    for (var p in labelValues) {
                      if (typeof(labelValues[p]) != "function") {
                        if ($("#" + p).prev("label")) {
                          $("#" + p).prev("label").remove();
                        }
                        $("#" + p).prev("input").remove();
                        $("#" + p).before("<label class='control-label'>" + labelValues[p] + "</label>");
                      }
                    }
                    return false
                  });
              $("#fluidPage").click(function(e) {
                    e.preventDefault();
                    changeStructure("container", "container-fluid");
                    $("#fixedPage").removeClass("active");
                    $(this).addClass("active");
                    downloadLayoutSrc()
                  });
              $("#fixedPage").click(function(e) {
                    e.preventDefault();
                    changeStructure("container-fluid", "container");
                    $("#fluidPage").removeClass("active");
                    $(this).addClass("active");
                    downloadLayoutSrc()
                  });
              $(".nav-header").click(function() {
                    $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
                    $(this).next().slideDown()
                  });
              $('#undo').click(function() {
                    $scope.layoutReload();
                  });
              $('#redo').click(function() {
                    $scope.layoutReload();
                  });
              $('#save').click(function() {
                    $scope.layoutSaveToDB();
                  });

              $("#editorModal").on('shown.bs.modal', function() {
                    $("#editorModal").find(".modal-dialog").css("marginLeft", (($(window).width() - $("#editorModal").find(".modal-content").width()) / 2));

                    var deferred = $q.defer();
                    var productId = $("#editorModal").find(".tmpValue").val();

                    $http({
                          method : 'post',
                          url : 'product/getAllUpdateContentInfo/' + productId
                        }).success(function(data) {
                          deferred.resolve(data);
                        }).error(function(data) {
                          deferred.reject(data);
                        });
                    deferred.promise.then(function(data) {
                          viewAllUpdateContentInfo(data);

                          console.log('editorModal 请求成功');
                        }, function(data) {
                          console.log('editorModal 请求失败');
                        });
                  });

              $("#editorModal").on('hidden.bs.modal', function() {
                  });

              $scope.setNavStyle(null, "menu_dashboard");

              removeElm();
              gridSystemGenerator();
              setInterval(function() {
                    handleSaveLayout()
                  }, timerSave);

              viewToHtml();
              App.menu();

              $timeout(function() {
                    $("#edit").removeAttr("disabled");
                    $("#save").attr("disabled", "disabled");
                    $("#undo").attr("disabled", "disabled");

                    $(window).resize();

                    $(".leftmenu .sub-menu").css("display", "none");
                  });
            });
      }]);
});
