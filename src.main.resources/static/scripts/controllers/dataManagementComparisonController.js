define(['controllers/controllers', 'jquery', 'jqueryUI'],
  function(controllers) {
	  controllers.controller('dataManagementComparisonCtrl', ['$scope', '$location', '$q', '$http', '$stateParams','$state','$compile','$interval','uiGridTreeViewConstants', function($scope, $location, $q, $http, $stateParams,$state,$compile,$interval,uiGridTreeViewConstants) {
	       
	        // 交互组件生成ID
	        function handleJsIds() {
	          // 遮盖窗体
//	          handleModalIds();
	          // 手风琴
	          handleAccordionIds();
	          // 轮换图
//	          handleCarouselIds();
	          // 切换卡
//	          handleTabsIds()
	          // ？？为什么不是全部组件？

	          // 自定义下拉列表 Start
//	          handleCustomDropdownIds();
	          // 自定义下拉列表 End
	        }
	        
	        
	        // 手风琴
	        function handleAccordionIds() {
	          var e = $(".demo #myAccordion");
	          var t = randomNumber();
	          var n = "accordion-" + t;
	          var r;
	          e.attr("id", n);
	          e.find(".accordion-group").each(function(e, t) {
	                r = "accordion-element-" + randomNumber();
	                $(t).find(".accordion-toggle").each(function(e, t) {
	                      $(t).attr("data-parent", "#" + n);
	                      $(t).attr("href", "#" + r)
	                    });
	                $(t).find(".accordion-body").each(function(e, t) {
	                      $(t).attr("id", r)
	                    })
	              })
	        }
	        // 判断浏览器是否支持html5本地保存
	        function supportstorage() {
	          if (typeof window.localStorage == 'object')
	            return true;
	          else
	            return false;
	        }
	        // 定时保存页面布局
	        function handleSaveLayout() {
	          var e = $(".demo").html();
	          if (!stopsave && e != window.demoHtml) {
	            stopsave++;
	            window.demoHtml = e;
//	            saveLayout();
	            stopsave--;
	          }
	        }

	        var layouthistory;
	        // 保存布局
//	        function saveLayout() {
//	          var data = layouthistory;
//	          if (!data) {
//	            data = {};
//	            data.count = 0;
//	            data.list = [];
//	          }
//	          if (data.list.length > data.count) {
//	            for (i = data.count; i < data.list.length; i++)
//	              data.list[i] = null;
//	          }
//	          data.list[data.count] = window.demoHtml;
//	          data.count++;
//	          if (supportstorage()) { 
//	            localStorage.setItem("layoutdata", JSON.stringify(data));
//	          }
//	          layouthistory = data;
//	        }
	        // header下载按钮
	        function downloadLayout() {

	          $.ajax({
	                type : "POST",
	                url : "/build/downloadLayout",
	                data : {
	                  layout : $('#download-layout').html()
	                },
	                success : function(data) {
	                  window.location.href = '/build/download';
	                }
	              });
	        }

	        function downloadHtmlLayout() {
	          $.ajax({
	                type : "POST",
	                url : "/build/downloadLayout",
	                data : {
	                  layout : $('#download-layout').html()
	                },
	                success : function(data) {
	                  window.location.href = '/build/downloadHtml';
	                }
	              });
	        }
	        // header撤销按钮
	        function undoLayout() {
	          var data = layouthistory;
	          // console.log(data);
	          if (data) {
	            if (data.count < 2)
	              return false;
	            window.demoHtml = data.list[data.count - 2];
	            data.count--;
	            $('.demo').html(window.demoHtml);
	            if (supportstorage()) {
	              localStorage.setItem("layoutdata", JSON.stringify(data));
	            }
	            return true;
	          }
	          return false;
	        }
	        // header 重做
	        function redoLayout() {
	          var data = layouthistory;
	          if (data) {
	            if (data.list[data.count]) {
	              window.demoHtml = data.list[data.count];
	              data.count++;
	              $('.demo').html(window.demoHtml);
	              if (supportstorage()) {
	                localStorage.setItem("layoutdata", JSON.stringify(data));
	              }
	              return true;
	            }
	          }
	          return false;
	        }
	        // 随机数-》用于生成id
	        function randomNumber() {
	          return randomFromInterval(1, 1e6)
	        }

	        function randomFromInterval(e, t) {
	          return Math.floor(Math.random() * (t - e + 1) + e)
	        }

	        function gridSystemGenerator() {
	          $(".lyrow .preview input").bind("keyup", function() {
	                var e = 0;
	                var t = "";
	                var n = $(this).val().split(" ", 12);
	                $.each(n, function(n, r) {
	                      e = e + parseInt(r);
	                      t += '<div class="span' + r + ' column"></div>'
	                    });
	                if (e == 12) {
	                  $(this).parent().next().children().html(t);
	                  $(this).parent().prev().show()
	                } else {
	                  $(this).parent().prev().hide()
	                }
	              })
	        }
	        // 拖拽后内容 编辑的各种事件
	        function configurationElm(e, t) {
	          $(".demo").delegate(".configuration > a", "click", function(e) {
	                e.preventDefault();
	                var t = $(this).parent().next().next().children();
	                $(this).toggleClass("active");
	                t.toggleClass($(this).attr("rel"))
	              });
	          $(".demo").delegate(".configuration .dropdown-menu a", "click", function(e) {
	                e.preventDefault();
	                var t = $(this).parent().parent();
	                var n = t.parent().parent().next().next().children();
	                t.find("li").removeClass("active");
	                $(this).parent().addClass("active");
	                var r = "";
	                t.find("a").each(function() {
	                      r += $(this).attr("rel") + " "
	                    });
	                t.parent().removeClass("open");
	                n.removeClass(r);
	                n.addClass($(this).attr("rel"))
	              })
	        }
	        // 删除元素
	        function removeElm() {
	          $(".demo").delegate(".remove", "click", function(e) {
	                e.preventDefault();
	                $(this).parent().remove();
	                if (!$(".demo .lyrow").length > 0) {
	                  clearDemo()
	                }
	                var el = $(this).parent("div").find("input:last");
	                if (el) {
	                  idList.remove(el.attr("id"));
	                  delete labelValues[el.attr("id")];
	                }
	              })
	        }
	        // 清空Container
	        function clearDemo() {
	          $(".demo").empty();
	          layouthistory = null;
	          if (supportstorage())
	            localStorage.removeItem("layoutdata");
	        }
	        // 切换header导航 active状态
	        function removeMenuClasses() {
	          $("#menu-layoutit li button").removeClass("active")
	        }
	        // 更改生成html5代码
	        function changeStructure(e, t) {
	          $("#download-layout ." + e).removeClass(e).addClass(t)
	        }
	        // 删除行模板代码
	        function cleanHtml(e) {
	          $(e).parent().append($(e).children().html())
	        }
	        // 下载页面布局
	        function downloadLayoutSrc() {
	          var e = "";
	          $("#download-layout").children().html($(".demo").html());
	          var t = $("#download-layout").children();
	          t.find(".preview, .configuration, .drag, .remove").remove();
	          t.find(".lyrow").addClass("removeClean");
	          t.find(".box-element").addClass("removeClean");
	          t.find(".lyrow .lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .lyrow .lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".lyrow .removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".removeClean").each(function() {
	                cleanHtml(this)
	              });
	          t.find(".removeClean").remove();
	          $("#download-layout .column").removeClass("ui-sortable");
	          $("#download-layout .row-fluid").removeClass("clearfix").children().removeClass("column");
	          if ($("#download-layout .container").length > 0) {
	            changeStructure("row-fluid", "row")
	          }
	          var labelValue = '';
	          for (var i = 0; i < idList.length; i++) {
	            labelValue = $("#" + idList[i]).prev("input").val();
	            $("#" + idList[i]).prev("input").remove();
	            $("#" + idList[i])
	                .before("<label class='control-label' for='" + idList[i] + "'>" + labelValue + "</label>");
	          }

	          formatSrc = $.htmlClean($("#download-layout").html(), {
	                format : true,
	                allowedAttributes : [["id"], ["class"], ["data-toggle"], ["data-target"], ["data-parent"], ["role"],
	                    ["data-dismiss"], ["aria-labelledby"], ["aria-hidden"], ["data-slide-to"], ["data-slide"],
	                    ["style"]]
	              });
	        }

	        // 定时保存生成html5代码，缓存作用
	        var timerSave = 1000;
	        // 
	        var stopsave = 0;
	        // 
	        var startdrag = 0;
	        var demoHtml = $(".demo").html();
	        // 
	        var currenteditor = null;

	        var index = 0;
	        var idList = new Array();
	        var labelValues = {};
	        // 自适应
	        $(window).resize(function() {
	              $("body").css("min-height", $(window).height() - 90);
	              $(".demo").css("min-height", $(window).height() - 160)
	            });
	        // 从客户端本地获取旧布局
	        function restoreData() {
	          if (supportstorage()) {
	            layouthistory = JSON.parse(localStorage.getItem("layoutdata"));
	            if (!layouthistory)
	              return false;
	            window.demoHtml = layouthistory.list[layouthistory.count - 1];
	            if (window.demoHtml)
	              $(".demo").html(window.demoHtml);
	          }
	        }

//	        Array.prototype.remove = function(val) {
//	          var index = this.indexOf(val);
//	          if (index > -1) {
//	            this.splice(index, 1);
//	          }
//	        };
	        $scope.idList = [];
	        function bindEvent(fieldId) {
	          idList.push(fieldId);
	          $scope.idList = idList;
	          if (supportstorage) {
	            window.localStorage.setItem("idList", idList);
	          }
	        }

	        // 初始化容器
	        function initContainer() {
	          // 初始化容器内元素(添加方向，排版等按钮事件)
	          configurationElm();
	        }
	        // 页面加载
	        angular.element(document).ready(function() {
	          $("body").css("min-height", $(window).height() - 90);
	          $(".demo").css("min-height", $(window).height() - 160);
	          $(".sidebar-nav .lyrow").draggable({
	                connectToSortable : ".demo",
	                helper : "clone",
	                handle : ".drag",
	                start : function(e, t) {
	                  if (!startdrag)
	                    stopsave++;
	                  startdrag = 1;
	                },
	                drag : function(e, t) {
	                  t.helper.width(400);
	                },
	                stop : function(e, t) {
	                  $(".demo .column").sortable({
	                        opacity : .35,
	                        connectWith : ".column",
	                        start : function(e, t) {
	                          if (!startdrag)
	                            stopsave++;
	                          startdrag = 1;
	                        },
	                        stop : function(e, t) {
	                          if (stopsave > 0)
	                            stopsave--;
	                          startdrag = 0;
	                          t.item.find(".view input:last").attr("id", "item" + index);
	                          bindEvent("item" + index);
	                          index++;
	                        }
	                      });
	                  if (stopsave > 0)
	                    stopsave--;
	                  startdrag = 0;
	                }
	              });
	          $(".sidebar-nav .box").draggable({
	                connectToSortable : ".column",
	                helper : "clone",
	                handle : ".drag",
	                start : function(e, t) {
	                  if (!startdrag)
	                    stopsave++;
	                  startdrag = 1;
	                },
	                drag : function(e, t) {
	                  t.helper.width(400)
	                },
	                stop : function() {
	                  handleJsIds();
	                  if (stopsave > 0)
	                    stopsave--;
	                  startdrag = 0;
	                }
	              });
	          initContainer();

	          // 保存表单
	          $('#body').on("click", "[data-target=#formSaveEditorModal]", function(e) {
	                $("#newFormName").val("Form_" + randomNumber());
	              });
	          $('body.edit .demo').on("click", "[data-target=#editorModal]", function(e) {
	                e.preventDefault();
	                currenteditor = $(this).parent().parent().find('.view');
	                var eText = currenteditor.html();
	                contenthandle.setData(eText);
	              });
	          $("#savecontent").click(function(e) {
	                e.preventDefault();
	                currenteditor.html(contenthandle.getData());
	              });
	          $("[data-target=#downloadModal]").click(function(e) {
	                e.preventDefault();
	                downloadLayoutSrc();
	              });
	          $("[data-target=#shareModal]").click(function(e) {
	                e.preventDefault();
	                handleSaveLayout();
	              });
	          $("#download").click(function() {
	                downloadLayout();
	                return false
	              });
	          $("#downloadhtml").click(function() {
	                downloadHtmlLayout();
	                return false
	              });
	          $("#edit").click(function() {
	                $("body").removeClass("devpreview sourcepreview");
	                $("body").addClass("edit");
	                removeMenuClasses();
	                $(this).addClass("active");
	                return false
	              });
	          $("#clear").click(function(e) {
	                e.preventDefault();
	                clearDemo()
	              });
	          $("#devpreview").click(function() {
	                $("body").removeClass("edit sourcepreview");
	                $("body").addClass("devpreview");
	                removeMenuClasses();
	                $(this).addClass("active");
	                return false
	              });
	          $("#sourcepreview").click(function() {
	                $("body").removeClass("edit");
	                $("body").addClass("devpreview sourcepreview");
	                removeMenuClasses();
	                $(this).addClass("active");
	                for (var p in labelValues) {
	                  if (typeof(labelValues[p]) != "function") {
	                    if ($("#" + p).prev("label")) {
	                      $("#" + p).prev("label").remove();
	                    }
	                    $("#" + p).prev("input").remove();
	                    $("#" + p).before("<label class='control-label'>" + labelValues[p] + "</label>");
	                  }
	                }
	                return false
	              });
	          $("#fluidPage").click(function(e) {
	                e.preventDefault();
	                changeStructure("container", "container-fluid");
	                $("#fixedPage").removeClass("active");
	                $(this).addClass("active");
	                downloadLayoutSrc()
	              });
	          $("#fixedPage").click(function(e) {
	                e.preventDefault();
	                changeStructure("container-fluid", "container");
	                $("#fluidPage").removeClass("active");
	                $(this).addClass("active");
	                downloadLayoutSrc()
	              });
	          $(".nav-header").click(function() {
	                $(".sidebar-nav .boxes, .sidebar-nav .rows").hide();
	                $(this).next().slideDown()
	              });
	          $('#undo').click(function() {
	                stopsave++;
	                if (undoLayout())
	                  initContainer();
	                stopsave--;
	              });
	          $('#redo').click(function() {
	                stopsave++;
	                if (redoLayout())
	                  initContainer();
	                stopsave--;
	              });

	          // 自定义下拉列表 编辑按钮按下 Start
	          $('body.edit .demo').on("click", "[data-target=#dropdownListModifyModal]", function(e) {
	            e.preventDefault();

	            var editor = content.controls[0].custom[0].dropdownList[0].editor;
	            var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
	            var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
	            var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

	            $('#dropdownListModifyModal').empty();
	            $('#dropdownListModifyModal').prepend($("#dropdownListInitModal").html());

	            currenteditor = $(this).parent().parent().find('.view');

	            var currentItemName = currenteditor.find("[class='btn']").find("lable:first");
	            var currentMenus = currenteditor.find("[class='dropdown-menu']");

	            // 项目名称
	            var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='"
	                + itemNamePlaceholder + "']");
	            editorItemName.attr("value", currentItemName.text());

	            // 下拉列表项目
	            var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
	            var editorMenuTmp = $(editorMenuOld).clone();
	            $(editorMenuOld).empty();
	            var itemCount = 0;

	            currentMenus.find("li").each(function(e, t) {
	              var editorMenuNew = editorMenuTmp.clone();
	              var textContent = $.trim(t.textContent);
	              var val = $.trim($(t).find("a").attr("value"));
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']")
	                  .attr("value", textContent);
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']")
	                  .attr("value", val);

	              $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);

	              itemCount++;
	            });

	            if (itemCount == 0) {
	              var editorMenuNew = editorMenuTmp.clone();
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']")
	                  .attr("value", "");
	              editorMenuNew.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']")
	                  .attr("value", "");

	              $('#dropdownListModifyModal').find("[class='input-group']:last").append(editorMenuNew);
	            }
	          });

	          removeElm();
	          gridSystemGenerator();
	          setInterval(function() {
	                handleSaveLayout()
	              }, timerSave);

	        })


	        // 自定义下拉列表 编辑画面里保存按下的场合 Start
	        function doSavecontentForDropdownList() {
	          var view = content.controls[0].custom[0].dropdownList[0].view;
	          var editor = content.controls[0].custom[0].dropdownList[0].editor;
	          var items = view[0].items;
	          var itemNamePlaceholder = editor[0].lable[0].itemNamePlaceholder;
	          var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
	          var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

	          var currentItemName = $(currenteditor).find("[class='btn']");
	          var currentMenus = $(currenteditor).find("[class='dropdown-menu']");

	          // 项目名称
	          var editorItemName = $('#dropdownListModifyModal').find("input[type='text'][placeholder='"
	              + itemNamePlaceholder + "']");
	          if ($.trim(editorItemName.val()) == "") {
	            $(currentItemName).find("lable:first").text("项目名称");
	          } else {
	            $(currentItemName).find("lable:first").text($.trim(editorItemName.val()));
	          }
	          $(currentItemName).find("lable:last").text("");

	          // 下拉列表项目
	          $(currentMenus).empty();
	          var itemCount = 0;

	          var editorMenus = $('#dropdownListModifyModal').find("[class='view clearfix']");
	          $(editorMenus).each(function(e, t) {
	            var text = "";
	            var val = "";

	            var textObj = $(t)
	                .find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']");
	            if (textObj.length > 0) {
	              text = $.trim(textObj.val());
	            } else {
	              return true; // false时相当于break, true 就相当于continure。
	            }

	            var valObj = $(t)
	                .find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']");
	            if (valObj.length > 0) {
	              val = $.trim(valObj.val());
	            } else {
	              return true; // false时相当于break, true 就相当于continure。
	            }

	            if (text != "") {
	              $(currentMenus).append("<li><a href=\"#\" value=\"" + val + "\">" + text + "</a></li>");
	              itemCount++;
	            }
	          });

	          if (itemCount == 0) {
	            $.each(items, function(idx, item) {
	                  $(currentMenus).append("<li><a href=\"#\" value=\"" + item.value + "\">" + item.text + "</a></li>");
	                });
	          }

	          $(currentMenus).find("li").click(function(e) {
	                doClickByAForDropdownList(e);
	              });

	          $("#closeForDropdownListModal").click();
	          return false;
	        }
	        // 自定义下拉列表 编辑画面里保存按下的场合 End

	        // 自定义下拉列表 Li -> a -> Click Start
	        function doClickByAForDropdownList(e) {
	          var selText = ":" + e.currentTarget.textContent;
	          $(e.currentTarget).parent().parent().find("button[class='btn'][data-toggle='dropdown']").find("lable:last")
	              .text(selText);

	          return false;
	        }
	        // 自定义下拉列表 Li -> a -> Click End

	        // 自定义下拉列表 添加项目 Start
	        function doAddItemForDropdownList(t) {
	          var editor = content.controls[0].custom[0].dropdownList[0].editor;
	          var itemTextPlaceholder = editor[0].lable[0].itemTextPlaceholder;
	          var itemValuePlaceholder = editor[0].lable[0].itemValuePlaceholder;

	          var editorMenuOld = $('#dropdownListModifyModal').find("[class='view clearfix']:last");
	          var editorMenuTmp = $(editorMenuOld).clone();
	          editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemTextPlaceholder + "']").attr(
	              "value", "");
	          editorMenuTmp.find("input[type='text'][class='input-mini'][placeholder='" + itemValuePlaceholder + "']")
	              .attr("value", "");

	          $(t).parent().parent().after(editorMenuTmp);

	          return false;
	        }
	        function doDelItemForDropdownList(t) {
	          if ($(t).parent().parent().parent().find("[class='view clearfix']").length > 4) {
	            $(t).parent().parent().remove();
	          }

	          return false;
	        }
            
            //构造grid数据
            
            //数据装载
//        	$http({
//        		method: 'POST',
//        		url: '/attrCtl/getWidgetTypes'
//        	}).then(function successCallback(response) {
//        		if (response.data != undefined) {
//        			$scope.widgetTypes = response.data;
//    			} else {
//    				$scope.widgetTypes = [];
//    			}
//        	}, function errorCallback(response) {
//        	});
            
            var attrJsonArrs = [
                {"name":"nodeProduct0","aliesProductId":"123456",
		            	"attr":[{"name":"attrName0","value":"attrValue0","aliesAttr":"aliesAttrName0","aliesValue":"aliesAttrValue0"},
		            	        {"name":"attrName1","value":"attrValue1","aliesAttr":"aliesAttrName1","aliesValue":"aliesAttrValue1"}],
		            	"aliesProduct":[{"name":"aliesProductName0","id":"123456",
      					  				"attr":[{"name":"aliesAttrName0","value":"aliesAttrValue0"},
      					  				        	{"name":"aliesAttrName1","value":"aliesAttrValue1"}]}],
		            	"suggestProduct":[{"name":"aliesProductName0","id":"2255487",
		            					  "attr":[{"name":"aliesAttrName0","value":"aliesAttrValue0"},
		            					          {"name":"aliesAttrName1","value":"aliesAttrValue1"}]},
		            					{"name":"aliesProductName1","id":"123456",
		            					   "attr":[{"name":"aliesAttrName0","value":"aliesAttrValue3"},
		            					           {"name":"aliesAttrName1","value":"aliesAttrValue1"}]}]},
                {"name":"nodeProduct1",
		                "attr":[{"name":"attrName0","value":"attrValue0","aliesAttr":"aliesAttr0","aliesValue":"aliesValue0"},
		                        {"name":"attrName1","value":"attrValue1","aliesAttr":"aliesAttr1","aliesValue":"aliesValue1"}],
		                "aliesProduct":[{"name":"aliesProductName0","id":"123456",
  					  				"attr":[{"name":"aliesAttrName0","value":"aliesAttrValue0"},
  					  				        	{"name":"aliesAttrName1","value":"aliesAttrValue1"}]}],
		                "suggestProduct":[{"name":"aliesProductName0","id":"123456",
		                					"attr":[{"name":"aliesAttrName0","value":"aliesAttrValue0"},
		                					        {"name":"aliesAttrName1","value":"aliesAttrValue1"}]},
		                			    {"name":"aliesProductName1","id":"123456",
		                					"attr":[{"name":"aliesAttrName0","value":"aliesAttrValue0"},
		                					        {"name":"aliesAttrName1","value":"aliesAttrValue1"}]}]}];
            
            
             //product name value aliesProduct aliesAttr aliesValue action
             $scope.importGridOptions = {
            	showTreeExpandNoChildren: true,
             };
             $scope.importGridOptions.columnDefs = [{
                 field : 'product',
                 width : '13%',
                 cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.product}}">{{row.entity.product}}</div>',
                 displayName : "product"
               }
             ,{
                   field : 'name',
                   width : '13%',
                   cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}">{{row.entity.name}}</div>',
                   displayName : "name"
               }
             ,{
             	field : 'value',
             	width : '13%',
             	cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.value}}">{{row.entity.value}}</div>',
             	displayName : "value"
             },
             {
                 field : 'aliesProduct',
                 width : '13%',
                 cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.aliesProduct}}">{{row.entity.aliesProduct}}</div>',
                 displayName : "aliesProduct"
             }            
             ,{
                 field : 'aliesAttr',
                 width : '13%',
                 cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.aliesAttr}}">{{row.entity.aliesAttr}}</div>',
                 displayName : "aliesAttr"
             }
             ,{
         	   field : 'aliesValue',
 	          	width : '13%',
 	          	cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.aliesValue}}">{{row.entity.aliesValue}}</div>',
 	          	displayName : "aliesValue"
            	}
             , 
             {
                 field : 'action',
                 width : '*',
                 cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" >'
                	 			+'<div ng-if="row.entity.level == 0"><div ng-if="row.entity.aliesProductFlag == true"><a class="btn btn-xs btn-primary" ng-click="grid.appScope.attrAliesChange(row)" style="width: 33%;">更多关联产品</a></div></div>'
                	 			+'<div ng-if="row.entity.level == 1">'
                	 			+'<div ng-if="row.entity.ifRecognized == true"><a class="btn btn-xs btn-primary" ng-click="grid.appScope.recognizedAliesValue(row)" style="width: 33%;">认可</a></div>'
                	 			+'<div ng-if="row.entity.ifRecognized == false"><a class="btn btn-xs btn-primary" style="width: 33%;" disabled="true">已认可</a></div>'
                	 			+'</div>',
                 displayName : "Action"
             }];
             $scope.importGridOptions.data = [];
            
             //重构json attrJsonArrs
             $scope.rebuildAttrsJson = function(){
            	 $scope.newJson = [];
            	 for(var i=0;i<attrJsonArrs.length;i++){
            		 //level 1 公司 选择更多关联公司按钮
            		 //level 属性值对应关系
            		 var fatherArr = {};
            		 //标记点
            		 fatherArr['attrIndex']=i;
            		 fatherArr['product'] = attrJsonArrs[i]['name'];
            		 fatherArr['aliesProduct'] = attrJsonArrs[i]['aliesProduct'][0]['name'];
            		 fatherArr['name'] = "";
            		 fatherArr['value'] = "";
            		 fatherArr['aliesAttr'] = "";
            		 fatherArr['aliesValue'] = "";
            		 if(attrJsonArrs[i]['suggestProduct'].length > 1){
            			 fatherArr['aliesProductFlag'] = true; 
            		 }else{
            			 fatherArr['aliesProductFlag'] = false; 
            		 }
            		 fatherArr['ifRecognized'] = false; 
            		 fatherArr['level'] = 0;
            		 fatherArr['productId'] = attrJsonArrs[i]['aliesProductId'];
            		 $scope.newJson.push(fatherArr);
            		 for(var j=0;j<attrJsonArrs[i]['attr'].length;j++){
            			 var sonArr = {};
            			 sonArr['product'] = "";
            			 sonArr['name'] = attrJsonArrs[i]['attr'][j]['name'];
           				 sonArr['value'] = attrJsonArrs[i]['attr'][j]['value'];
       					 sonArr['aliesAttr'] = attrJsonArrs[i]['attr'][j]['aliesAttr'];
     					 sonArr['aliesValue'] = attrJsonArrs[i]['attr'][j]['aliesValue'];
     					 sonArr['aliesProduct'] = ""
            			 if(attrJsonArrs[i]['attr'][j]['value'] == attrJsonArrs[i]['attr'][j]['aliesValue']){
            				 //按钮禁用
            				 sonArr['ifRecognized'] = false;
            			 }else{
            				 //按钮可用
            				 sonArr['ifRecognized'] = true;
            			 }
     					 sonArr['aliesProductFlag'] = true;
            			 sonArr['level'] = 1;
            			 sonArr['productId'] = attrJsonArrs[i]['aliesProductId'];
            			 sonArr['sonIndex'] = j;
            			 sonArr['attrIndex'] = i;
            			 $scope.newJson.push(sonArr);
            		 }
            	 }
            	 //grid赋值
            	 var gridArrs = [];
            	 for(var m = 0;m<$scope.newJson.length;m++){
            		 var gridAttr = {};
            		 gridAttr['product'] = $scope.newJson[m]['product'];
            		 gridAttr['name'] = $scope.newJson[m]['name'];
            		 gridAttr['value'] = $scope.newJson[m]['value'];
            		 gridAttr['aliesAttr'] = $scope.newJson[m]['aliesAttr'];
            		 gridAttr['aliesValue'] = $scope.newJson[m]['aliesValue'];
            		 gridAttr['aliesProduct'] = $scope.newJson[m]['aliesProduct'];
            		 gridAttr['ifRecognized'] = $scope.newJson[m]['ifRecognized'];
            		 gridAttr['level'] = $scope.newJson[m]['level'];
            		 gridAttr['aliesProductFlag'] = $scope.newJson[m]['aliesProductFlag'];
            		 gridAttr['attrIndex'] = $scope.newJson[m]['attrIndex'];
            		 gridAttr['sonIndex'] = $scope.newJson[m]['sonIndex'];
            		 gridAttr['productId'] = $scope.newJson[m]['productId'];
            		 if($scope.newJson[m]['level'] == '0'){
            			 gridAttr.$$treeLevel = 0;
            		 }
            		 gridArrs.push(gridAttr);
            	 }
            	 $scope.importGridOptions.data = angular.copy(gridArrs);
             }
             
             //关联产品弹窗grid
//             $scope.aliesProductGridOptions = {
//                 	//showTreeExpandNoChildren: true,
//                  };
//                  $scope.aliesProductGridOptions.columnDefs = [{
//                      field : 'productName',
//                      width : '25%',
//                      cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.productName}}">{{row.entity.productName}}</div>',
//                      displayName : "productName"
//                    }
//                  ,{
//                        field : 'attrName',
//                        width : '30%',
//                        cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.attrName}}">{{row.entity.attrName}}</div>',
//                        displayName : "attrName"
//                    }
//                  ,{
//                  	field : 'attrValue',
//                  	width : '*',
//                  	cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.attrValue}}">{{row.entity.attrValue}}</div>',
//                  	displayName : "attrValue"
//                  }];
//                  $scope.aliesProductGridOptions.data = [];
//             
//             //更换产品按钮
//             $scope.attrAliesChange = function(row){
//            	 var aliesProAttrs = [];
//            	 for(var i=0;i<attrJsonArrs[row.entity.attrIndex]['aliesProduct'].length;i++){
//            		 var aliesProAttr = {};
//            		 aliesProAttr['productName'] = attrJsonArrs[row.entity.attrIndex]['aliesProduct'][i]['name'];
//            		 aliesProAttr['productId'] = attrJsonArrs[row.entity.attrIndex]['aliesProduct'][i]['id'];
////            		 aliesProAttr['attrName'] = attrJsonArrs[row.entity.attrIndex]['aliesProduct'][i]['name'];
//            		 aliesProAttrs.push(aliesProAttr);
//            	 }
//            	 $scope.aliesProductGridOptions.data = angular.copy(aliesProAttrs);
//            	 $("#aliesProductChooseModalhh").trigger("click");
//             }
             //添加弹出grid选中事件
             
             //更换产品按钮
             $scope.attrAliesChange = function(row){
            	 var aliesProductListHtml = new StringBuffer("");
          	   	 for(var i=0;i<attrJsonArrs[row.entity.attrIndex]['suggestProduct'].length;i++){
          	   		aliesProductListHtml.append("<div class=\"form-group\"><div class=\"col-md-4\">" +
											   "<input type=\"radio\" class=\"pull-right\" name=\"suggestAttrName\" value=\"").append(attrJsonArrs[row.entity.attrIndex]['suggestProduct'][i]['id']).append(" \" onclick=\"getFocusCell('").append(row.entity.attrIndex).append("','").append(i).append("')\"/>" +
											   "</div><div class=\"col-md-8\"><label class=\"pull-left\">").append(attrJsonArrs[row.entity.attrIndex]['suggestProduct'][i]['name']).append("</label>" +
											   "</div></div>");  
          	   	 }
          	   	 $("#aliesProductList").html(aliesProductListHtml.toString());
          	   	 $compile($("#aliesProductList").contents())($scope);
          	   	 $("#aliesProductChooseModalhh").trigger("click");
             }
             
             //关联产品选择
             getFocusCell = function(jsonIndex,productIndex){
        	    $("#jsonIndex").val(jsonIndex);
        	    $("#productIndex").val(productIndex);
             }
             //关联产品选择确认
             $scope.aliseProductCom = function(){
            	 if($("#jsonIndex").val()!='' && $("#jsonIndex").val()!=null){
            		 attrJsonArrs[$("#jsonIndex").val()]['aliesProductId'] = attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['id'];
                     attrJsonArrs[$("#jsonIndex").val()]['aliesProduct'][0]['name'] = attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['name'];
                     attrJsonArrs[$("#jsonIndex").val()]['aliesProduct'][0]['id'] = attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['id'];
                     attrJsonArrs[$("#jsonIndex").val()]['aliesProduct'][0]['attr'].length = 0;
                     for(var i=0;i<attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['attr'].length;i++){
                    	 attrJsonArrs[$("#jsonIndex").val()]['aliesProduct'][0]['attr'].push(attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['attr'][i]);
                    	 for(var j=0;j<attrJsonArrs[$("#jsonIndex").val()]['attr'].length;j++){
                    		 if(attrJsonArrs[$("#jsonIndex").val()]['attr'][j]['aliesAttr'] == attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['attr'][i]['name']){
                    			 attrJsonArrs[$("#jsonIndex").val()]['attr'][j]['aliesAttr'] = attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['attr'][i]['name'];
                    			 attrJsonArrs[$("#jsonIndex").val()]['attr'][j]['aliesValue'] = attrJsonArrs[$("#jsonIndex").val()]['suggestProduct'][$("#productIndex").val()]['attr'][i]['value'];
                    			 break;
                    		 }
                    	 }
                     }
                     console.log(attrJsonArrs[$("#jsonIndex").val()]['aliesProduct'][0]['attr']);
                     console.log(attrJsonArrs);
                     $scope.rebuildAttrsJson();
            	 }
             }
             
             //关联属性值认可
             $scope.recognizedAliesValue = function(row){
            	 //直接更新数据库
            	 console.log(row.entity.productId);
            	 console.log(row.entity.aliesAttr);
            	 console.log(row.entity.aliesValue);
            	 console.log(row.entity.sonIndex);
            	 
            	 //后台处理成功  
            	 attrJsonArrs[row.entity.attrIndex]['attr'][row.entity.sonIndex]['aliesValue'] = attrJsonArrs[row.entity.attrIndex]['attr'][row.entity.sonIndex]['value']
            	 $scope.rebuildAttrsJson();
             }
            	//$scope.attrJson
//            	var attrJson = [];
//            	var attrJsonArr = {};
//            	for(var i=0;i<2;i++){
//            		attrJsonArr['name']="nodeProduct"+i;
//            		attrJsonArr['attr'] = [];
//            		for(var j=0;j<2;j++){
//            			var attrArr = {};
//            			attrArr['name'] = "attrName"+j;
//            			attrArr['value'] = "attrValue"+j;
//            			attrArr['aliesAttr'] = "aliesAttr"+j;
//            			attrArr['aliesValue'] = "aliesValue"+j;
//            			attrJsonArr['attr'].push(attrArr);
//            		}
//            		attrJsonArr['aliesProduct'] = [];
//            		for(var k=0;k<2;k++){
//            			var aliesProduct = {};
//            			aliesProduct['name']="aliesProductName"+k;
//            			aliesProduct['attr']=[];
//            			for(var m=0;m<2;m++){
//            				var aliesAttr = {};
//            				aliesAttr['name'] = "aliesAttrName"+m;
//            				aliesAttr['value'] = "aliesAttrValue"+m;
//            				aliesProduct['attr'].push(aliesAttr);
//            			}
//            			attrJsonArr['aliesProduct'].push(aliesProduct);
//            		}
//            		attrJson.push(attrJsonArr);
//            	}
//            	console.log(JSON.stringify(attrJson));
            
            
            
//            product:{"productName":"productname","level":"0",
//            "attr"[{"name":"name1","value":"value1","aliesAttr":"name1","aliesValue":"value1"},                                                                          
//            	     {"name":"name2","value":"value2","aliesAttr":"name2","aliesValue":"value2"}]
//            aliesProduct:[{"name":"product1","attr":[{"name":"name1","value":"value3"},						     						     					             
//                                                     {"name":"name2","value":"value4"},
//                          {"name":"product2","attr":[{"name":"name1","value":"value5"},					             				                     						     
//                                                     {"name":"name2","value":"value6"}]}]} 
            
//	        xmlGridBuild = function(attrJson,type){
//	            //装载数据	     
//	        	$scope.importGridOptions.data = [];
//	   			for(var i=0;i<attrJson[0][type].length;i++){
//					var obj = attrJson[0][type][i];
//	                var xmlArr = {};
//	                xmlArr.name = obj['@name'];
//	                xmlArr.path = obj['@path'];
//					if(!isEmptyObject(obj['aliesAttrs'])){
//						xmlArr.aliesAttr = obj['aliesAttrs']['label'];
//					}else{
//						xmlArr.aliesAttr = "";
//					}
//					xmlArr.indexId = i;
//					xmlArr.mark = obj['mark'];
//					xmlArr.key = obj['key'];
//					xmlArr.matchDegree = obj['matchDegree'];
//					$scope.importGridOptions.data.push(xmlArr);// ui-grid data push
//                }
//	        }
            
//           {"product":"product1","attr"[{"name":"name1","value":"value1"},{"name":"name2","value":"value2"}],
//            aliesProduct:[{"product":"product1","attr":[{"name":"name1","value":"value1"},{"name":"name2","value":"value2"},{"product":"product1","attr":[{"name":"name3","value":"value3"},{"name":"name4","value":"value4"}]}]} 
            
            //
            	
  
	       //保存映射关系
	       $("#exportXML").on("click",function(){
	    	   if(isEmptyObject($scope.attrJson)){
	    		   return;
	    	   }
	    	   //给XML赋值
	    	   setAliesAttr("2");
	    	   var xmlData = {};
	    	   xmlData.jsonStr = $scope.attrXML;
	    	   xmlData.fileName = $scope.fileName;
               var deferred = $q.defer();
               $http({
                     method : 'POST',
                     url : 'dataManagementCtl/saveMappingFile',
                     data : xmlData
                   }).success(function(data) {
                     deferred.resolve(data);
                   }).error(function(data) {
                     deferred.reject(data);
                   });
               
               deferred.promise.then(function(data) {
                   console.log('exportXML 请求成功');
                   if (data) {
	                   	if(data.resuleStr == '0'){
	                   		$("#exportResult").text("{{\"data_manage_page.export_file.file_export_fail\" | translate}}");
	                   		$("#formExportResultModalhh").trigger("click");
	                   		console.log("导出失败");
	                   	}else if(data.resuleStr == '1'){
	                   		console.log("导出成功");
	                   		$("#exportResult").text("{{\"data_manage_page.export_file.file_export_success\" | translate}}");
	                   		$("#formExportResultModalhh").trigger("click");
	                   	}else if(data.resuleStr == '2'){
	                   		console.log("该产品已经存入");
	                   		$("#exportResult").text("{{\"data_manage_page.export_file.file_export_fail\" | translate}}");
	                   		$("#formExportResultModalhh").trigger("click");
	                   	}
                   }
                 }, function(data) {
                	 $("#exportResult").text("{{\"data_manage_page.export_file.file_export_fail\" | translate}}");
               		$("#formExportResultModalhh").trigger("click");
                    console.log('exportXML 请求失败');
                 });
	       });
	       //-----------------------工具类部分-----------------------------
	       //定义StringBuffer
	       function StringBuffer() {
	    	    this.__strings__ = new Array();
	    	}
	    	StringBuffer.prototype.append = function (str) {
	    	    this.__strings__.push(str);
	    	    return this;    //方便链式操作
	    	}
	    	StringBuffer.prototype.toString = function () {
	    	    return this.__strings__.join("");
	    	}
	        //判断是否为空
	        function isEmptyObject(e) {  
	            var t;  
	            for (t in e)  
	                return !1;  
	            return !0  
	        }
	        function isEmptyStr(str){
	        	return (str == null || str == '')
	        }
	       //document xml对象转化为String
	       function XMLtoString(elem){  
	    	    var serialized;  
	    	    try {  
	    	        // XMLSerializer exists in current Mozilla browsers                                                                              
	    	        serializer = new XMLSerializer();                                                                                                
	    	        serialized = serializer.serializeToString(elem);                                                                                 
	    	    }                                                                                                                                    
	    	    catch (e) {  
	    	        // Internet Explorer has a different approach to serializing XML                                                                 
	    	        serialized = elem.xml;                                                                                                           
	    	    }      
	    	    return serialized;                                                                                                                   
	    	}  
	 }]);
});
