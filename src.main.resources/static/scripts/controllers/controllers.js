define(['angular', 'i18n','webuploader','mainHome'], function(angular) {
  'use strict';
  return angular.module('controllers', ['ui.grid.i18n','ui.grid', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.saveState', 'ui.grid.selection',
                                        'ui.grid.cellNav', 'ui.grid.rowEdit', 'ui.grid.pagination', 'ui.grid.edit','ui.grid.treeView']);
});
