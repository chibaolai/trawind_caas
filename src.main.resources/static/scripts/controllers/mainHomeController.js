define(['controllers/controllers', 'jquery', 'mainHome'], function(controllers) {
      controllers.controller("mainHomeCtrl", ['$scope', '$location', '$q', '$http', '$compile', function($scope, $location, $q, $http, $compile) {
                $scope.logout = function() {
                  $("#id_logout").click();
                }

                $scope.setNavStyle = function($event, objId) {
                  var lis = $(".page-sidebar-menu li");
                  lis.removeClass("active");

                  var tar = null;
                  if ($event) {
                    tar = $event.currentTarget;
                  } else if (objId) {
                    tar = $("#" + objId);
                  }

                  $(tar).parent().addClass("active");
                  var selected = $(tar).find("span.selected");
                  if (selected.length == 0) {
                    $(tar).append('<span class="selected"></span>');
                  }
                  setTimeout(function() {
                        $("body").addClass("page-header-fixed");
                      }, 100);
                }
                
                $('.page-sidebar').on('click', 'li > a', function(e) {

                    var parent = $(this).parent().parent();
                    
                    parent.children('li.open').children('a').children('.arrow').removeClass('open');
                    parent.children('li.open').children('.sub-menu').slideUp(200);
                    parent.children('li.open').removeClass('open');

                    var sub = $(this).next();
                    if (parent.children('li.open').length > 0) {
                      $('.arrow', $(this)).removeClass("open");
                      $(this).parent().removeClass("open");
                      sub.slideUp(200, function() {
                            handleSidebarAndContentHeight();
                          });
                    } else {
                      $('.arrow', $(this)).addClass("open");
                      $(this).parent().addClass("open");
                      sub.slideDown(200, function() {
                            handleSidebarAndContentHeight();
                          });
                    }

                    e.preventDefault();
                  });
                var handleSidebarAndContentHeight = function() {
                    var content = $('.page-content');
                    var sidebar = $('.page-sidebar');
                    var body = $('body');
                    var height;

                    if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
                      var available_height = $(window).height() - $('.footer').height();
                      if (content.height() < available_height) {
                        content.attr('style', 'min-height:' + available_height + 'px !important');
                      }
                    } else {
                      if (body.hasClass('page-sidebar-fixed')) {
                        height = _calculateFixedSidebarViewportHeight();
                      } else {
                        height = sidebar.height() + 20;
                      }
                      if (height >= content.height()) {
                        content.attr('style', 'min-height:' + height + 'px !important');
                      }
                    }
                  }               

                $scope.userInfo = {};
                $scope.getUserInfo = function() {
                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'userCtl/getUserInfo'
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  deferred.promise.then(function(data) {
                        if (data) {
                          $scope.userInfo = data;
                        }

                        console.log('getUserInfo 请求成功');
                      }, function(data) {
                        console.log('getUserInfo 请求失败');
                      });
                }

                $scope.getUserInfo();
                $(document).ready(function() {
                      App.init(); // initlayout and core plugins
                    });
              }]);
    });
