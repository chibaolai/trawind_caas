define(['controllers/controllers', 'jquery', 'jqueryUI', 'bootstrap', 'datetimepicker'],
  function(controllers) {
    controllers.controller('addAttrCtrl', ['$scope', '$location', '$q', '$http', 'i18nService', '$compile','dateServ', '$rootScope', '$translate',  function ($scope, $location, $q, $http, i18nService, $compile,dateServ, $rootScope, $translate) {
    	$(".container-fluid").parents("body").attr("class", "devpreview sourcepreview");
    	var data = {
			"zh" :[
		            {"id":"en", "name":"英语"},
		            {"id":"fr", "name":"法语"},
		            {"id":"ge", "name":"德语"},
		            {"id":"ja", "name":"日语"},
		            {"id":"zh", "name":"中文"},
		          ],
	        "en" :[
		            {"id":"en", "name":"English"},
		            {"id":"fr", "name":"France"},
		            {"id":"ge", "name":"German"},
		            {"id":"ja", "name":"Japanesew"},
		            {"id":"zh", "name":"Chinese"}
		          ]      
    	};
    	if ($translate.use() == "en") {
    		$("#preview_area").text("Preview");
		} else if ($translate.use() == "zh") {
			$("#preview_area").text("预览");
		}
    	$scope.languages = data[$translate.use()]
		$scope.languageList = [];
		angular.copy(data[$translate.use()], $scope.languageList);
		
    	$http({
    		method: 'POST',
    		url: '/attrCtl/getWidgetTypes'
    	}).then(function successCallback(response) {
    		if (response.data != undefined) {
    			$scope.widgetTypes = response.data;
			} else {
				$scope.widgetTypes = [];
			}
    	}, function errorCallback(response) {
    	});
    	
    	$scope.gridOpts = {};
    	$scope.gridOpts.data = [];
    	$scope.gridOpts.columnDefs = [];
    	$scope.index = 0;
    	
    	initModal = function (){
    		$scope.language = "";
    		$scope.codeList = [];
    		if ($scope.languageList.length != $scope.languages.length) {
    			angular.copy($scope.languageList, $scope.languages);
    			$compile($("#addTab").contents())($scope);
    			$scope.language = "";
			}
	    	$(".nav-tabs").children("li").each(function(i, el) {
	    		if ($(el).attr("id") != "addTab") {
					$(el).remove();
				}
	    	});
    		$("#tabContent").children(".tab-pane").each(function(i, el) {
				$(el).remove();
    		});
    		$scope.widget = {};
    		$scope.widget.required = false;
    		$("#preAttrName").text("Attribute Name");
    		$("#preDefaultVal").val("");
    		$scope.index = 0;
    		$("#gridWidget").hide();
    		$scope.gridOpts = {};
        	$scope.gridOpts.data = [];
        	$scope.gridOpts.columnDefs = [];
        	$scope.index = 0;
        	$scope.widgetType = "";
    	}
    	
    	setPreviewAttrName = function(el) {
			$("#preAttrName").text($(el).val());
		} 
    	setPreviewDefaultVal = function(el) {
    		$("#preDefaultVal").val($(el).val());
    	} 
    	
    	$scope.codeList = [];
    	
    	$scope.addTabPane = function() {
    		if ($scope.language == null) {
				return;
			}
    		var code = $scope.language.id;
    		var text = $scope.language.name;
    		var preview = $scope.language.preview;
    		$scope.codeList.push(code);
    		
    		if (code != undefined && code.trim() == "") {
    			return;
    		}
    		for (var i = 0; i < $scope.languages.length; i++) {
				if ($scope.languages[i].id == code) {
					$scope.languages.splice(i, 1);
				}
			}
    		$scope.language = "";
    		$("#preAttrName").text("Attribute Name");
    		
    		$("#baseLi").find("a").attr("href", "#tab_" + code);
    		$("#baseLi").find("a").text(text);
    		$("#addTab").before($("#baseLi").html());
    		$("#addTab").prev("li").show();
    		$(".nav-tabs").find("li").each(function(i, element) {
    			if ($(element).attr("class") != undefined && $(element).attr("class").indexOf("active") > -1) {
    				$(element).attr("class", "");
				}
    		});
    		$("#addTab").prev("li").attr("class", "active");
    		$("#addTab").prev("li").find('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    			var href = $(e.target).attr("href");
    			var attrName = $(href).find("input[name='attrName']").val();
    			var defaultVal = $(href).find("input[name='defaultVal']").val();
    			if (attrName != undefined && attrName.trim() != "") {
					$("#preAttrName").text(attrName);
					if (defaultVal != undefined && defaultVal.trim() != "") {
						$("#preDefaultVal").val(defaultVal.trim());
					}
				}
    			if ($scope.widgetType != "select" && $(href).find("textarea[name='selectVals']").length > 0) {
    				$(href).find("textarea[name='selectVals']").parents("div.form-group").remove();
				} else if($scope.widgetType == "select"){
					var code = href.slice(5);
	    			if ($(href).find("textarea[name='selectVals']").length == 0) {
	    				$("#baseTextarea").find("textarea").attr("ng-model", "widget." + code + ".selectVals");
	    				$(href).find("form").append($("#baseTextarea").html());
	    				$compile($("#tab_" + code).contents())($scope);
					} else {
						if ($scope.widget[code].selectVals.trim() != "") {
							var options = []; 
							options = $scope.widget[code].selectVals.split("\n");
							$("#preview").find($scope.widgetType).html("");
							for (var i = 0; i < options.length; i++) {
								$("#preview").find($scope.widgetType).append("<option>" +options[i]+ "</option>")
							}
						}
					} 
				}
			});
    		
    		$("#baseTab").find(".tab-pane").attr("id", "tab_" + code);
    		$("#baseTab").find("input[name='attrName']").attr("ng-model", "widget." + code + ".widgetLabel");
    		$("#baseTab").find("input[name='attrName']").attr("placeholder", text);
    		$("#baseTab").find("input[name='defaultVal']").attr("ng-model", "widget." + code + ".defaultVal");
    		$("#baseTab").find("input[name='description']").attr("ng-model", "widget." + code + ".description");
    		$("#baseTab").find("input[name='required']").attr("ng-model", "widget." + code + ".required");
    		$("#tabContent").append($("#baseTab").html());
    		$("#baseTab").find(".tab-pane").attr("id", "");
    		$("#baseTab").find("input[name='attrName']").attr("ng-model", "");
    		$("#baseTab").find("input[name='attrName']").attr("placeholder", "");
    		$("#baseTab").find("input[name='defaultVal']").attr("ng-model", "");
    		$("#baseTab").find("input[name='required']").attr("ng-model", "");
    		if ($scope.widgetType == "select") {
    			$("#baseTextarea").find("textarea").attr("ng-model", "widget." + code + ".selectVals");
    			$("#tab_" + code).find("form").append($("#baseTextarea").html());
			}
    		if ($("#tabContent").find(".active").length > 0) {
    			$("#tabContent").find(".active").attr("class", "tab-pane");
			}
    		$("#tabContent").find(".tab-pane:last").attr("class", "tab-pane active");
    		$("#tabContent").find(".tab-pane:last").show();
    		$compile($("#tabContent").find(".tab-pane:last"))($scope);
		};
		
		
		$scope.addOption = function (e) {
			if (e.keyCode != 13) {
				return;
			}
			$(".nav-tabs").find("li").each(function(i, el) {
				if ($(el).attr("class") != undefined && $(el).attr("class").indexOf("active") > -1) {
					var code = $(el).find("a").attr("href").slice(5);
					if ($scope.widget[code].selectVals.trim() != "") {
						var options = []; 
						options = $scope.widget[code].selectVals.split("\n");
						$("#preview").find($scope.widgetType).html("");
						for (var i = 0; i < options.length; i++) {
							$("#preview").find($scope.widgetType).append("<option>" +options[i]+ "</option>")
						}
					}
					
				}
			});
		}
		
		$scope.saveWidget = function (){
			if ($scope.widgetType == "select") {
				for (var i = 0; i < $scope.codeList.length; i++) {
					var options = []; 
					options = $scope.widget[$scope.codeList[i]].selectVals.split("\n");
					$scope.widget[$scope.codeList[i]].options = options;
					delete $scope.widget[$scope.codeList[i]].selectVals;
				}
			}
			var attr = {};
			attr.widgetType = $scope.widgetType;
			attr.corpCode = "Trawind";
			attr.layoutJson = JSON.stringify($scope.widget);
			console.log(attr);
			$http({
	    		  method: 'POST',
	    		  url: '/attrCtl/saveAttr',
	    		  data: attr
			}).then(function successCallback(response) {
				gridData.push(response.data);
				$scope.gridOptions.data = gridData;
				console.log("Success!!!");
			}, function errorCallback(response) {
			});
		}
		
		$scope.changeWidgetType = function () {
			var code = "";
			if ($("#baseTextarea").next("div.tab-pane").length > 0) {
				$(".nav-tabs").children("li").each(function(i, el) {
					if ($(el).attr("class") != undefined && $(el).attr("class").indexOf("active") > -1) {
						var href = $(el).find("a").attr("href");
						code = href.slice(5);
						if ($scope.widgetType == 'select') {
							if ($(href).find("textarea[name='selectVals']").length == 0) {
								$("#baseTextarea").find("textarea").attr("ng-model", "widget." + code + ".selectVals");
								$(href).find("form").append($("#baseTextarea").html());
								$compile($("#tab_" + code).contents())($scope);
							}
						} else {
							if ($(href).find("textarea[name='selectVals']").length > 0) {
								$(href).find("textarea[name='selectVals']").parents("div.form-group").remove();
							}
							
						}
					}
				});
			}
			if (code == "" ||code == undefined) {
				$scope.widgetType = "";
				return;
			}
			if (code != "" && code != undefined && $scope.widgetType != "grid") {
				if ($scope.widget[code] != "" && $scope.widget[code] != undefined && $scope.widget[code].grid != undefined) {
					$scope.widget[code].grid.columns = [];
					$scope.gridOpts.columnDefs = [];
					$scope.index = 0;
				}
			}
			$("#preview").children().each(function(i, el) {
				$(el).hide();
			});
			$("#gridWidget").hide();
			if ($scope.widgetType == "input") {
				$("#preview").find($scope.widgetType + "[type='text']").show();
			} else if ($scope.widgetType == "date") {
				$("#preview").find("input[type='date']").show();
			} else if ($scope.widgetType == "checkbox") {
				$("#preview").find("input[type='checkbox']").show();
			} else if ($scope.widgetType == "grid") {    
				$("#gridWidget").show();
				if ($("#grid").attr("ui-grid") == undefined) {
					$("#grid").attr("ui-grid", "gridOpts");
					$compile($("#grid"))($scope);
				}
			} else {
				$("#preview").find($scope.widgetType).show();
			}
		}
		$scope.addColunm = function() {
			if ($scope.columnName.trim() != "") {
				var column = {};
				column.field = "name" + $scope.index;
				column.width = 200;
				column.displayName = $scope.columnName.trim();
				$scope.gridOpts.columnDefs.push(column);
				$scope.index++;
				
				var code = "";
				if ($("#baseTextarea").next("div.tab-pane").length > 0) {
					$(".nav-tabs").children("li").each(function(i, el) {
						if ($(el).attr("class") != undefined && $(el).attr("class").indexOf("active") > -1) {
							var href = $(el).find("a").attr("href");
							code = href.slice(5);
						}
					});
				}
				if ($scope.widget[code] == undefined) {
					$scope.widget[code] = {};
				}
				if ($scope.widget[code].grid == undefined) {
					$scope.widget[code].grid = {};
					$scope.widget[code].grid.columns = [];
				}
				$scope.widget[code].grid.columns.push(column);
				$scope.columnName = "";
			}
		};
		
        var gridData = [];	
		$scope.addWidget = function (type){
			switch (type) {
			case "input":
				d.dialog("open");
				break;

			default:
				break;
			}
		}
		i18nService.setCurrentLang('zh-cn');
    	$scope.gridOptions = {};
    	$scope.gridOptions.enableFiltering = true; 
    	$scope.gridOptions.enableColumnResizing = true; 
        $scope.gridOptions.columnDefs = [
            { field:'name', width:'*', displayName:"属性名" },
            { field:'defaultVal', width:'25%', displayName:"默认值" },
            { field:'require', width:'25%', displayName:"是否必填" },
            { field:'disabled', width:'25%', displayName:"是否可编辑" },
        ];
        
    	//获取属性别表数据
        $http.get('attrCtl/getAttr').success(function(data, status, headers, config) {
        	for(var i = 0; i < data.length; i++){
                data[i].require = transBoolean(JSON.parse(data[i].direcJson).require);
                data[i].disabled = transBoolean(JSON.parse(data[i].direcJson).disabled);
                if (JSON.parse(data[i].direcJson).en) {
                	data[i].name = JSON.parse(data[i].direcJson).en.widgetLabel;
                }
        		gridData.push(data[i]);
        	}//18310861792
			$scope.gridOptions.data = gridData;
        }).error(function(data, status, headers, config) {
        	console.log("获取属性数据异常");
        });       
        
       function transBoolean(bool){
    	   if(bool == true){
    		   return 'N';
    	   }
    	   return 'Y';
       }
       
//       // 默认显示左边菜单
//       angular.element(document).ready(function() {
//           $("body").removeClass("devpreview sourcepreview");
//           $("body").addClass("edit");
//           $("#menu-layoutit li button").removeClass("active");
//           $(this).addClass("active");
//
//           var t = $(".demo").children();
//           t.each(function(i, e) {
//                 $(e).find(".row-fluid").children().find(".box.box-element.ui-draggable").css("marginLeft", "0px");
//               });
//           $(window).resize();
//       });
       
        $scope.saveRow = function( rowEntity ) {
            var defer = $q.defer();
            $http({
//            	url
            })
            $scope.gridApi.rowEdit.setSavePromise( rowEntity, defer.promise );
          };
         
        $scope.gridOptions.onRegisterApi = function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
        };
        
        $scope.gridOptions.data = gridData;
        
        $scope.columns = [];
        $scope.gridOptions_grid = {
        		columnDefs: $scope.columns,
        		data:[]
        };
        $scope.saveGrid = function(){
        	if ($scope.newCol != "") {
        		var row = {};
        		row.field = "name"+$scope.columns.length;
        		row.width = 133;
        		row.displayName = $scope.newCol;
        		//row.enableCellEditOnFocus = true;
        		$scope.columns.push(row);
        		$scope.newCol = "";
			}
        };
        
        
        //属性添加
        $scope.add = function (w_type){
	        	var attr = $scope[w_type];
	        	
	        	if (attr == null){
	        		return false;
	        	}
        		var attrObject = new Object();
        	
				attrObject.widgetLabel = attr.attrName_en;
				if (w_type == "grid") {
					attrObject.columnDefs = []; 
					$scope.columns.push({
						name:'action',enableCellEdit:false,enableCellEditOnFocus:false,cellTemplate: '<div class="container-fluid"><div class="row cell-action-style"><div class="col-xs-3 text-center" ><div class="div-click"  ng-click="grid.appScope.goToDelete(row,ctrl.options.widgetId)"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></div></div><div></div></div></div>'
					});
					attrObject.columnDefs = $scope.columns;
				}else if(w_type === 'textarea'){
					attrObject.require = attr.require;
					attrObject.disabled = attr.disabled;
					attrObject.defaultVal = attr.defaultVal;
				}else if(w_type === 'date'){
					attrObject.require = attr.require;
					attrObject.disabled = attr.disabled;
					attrObject.defaultVal = attr.defaultVal;
				}else if(w_type === 'chk'){
					attrObject.require = attr.require;
					attrObject.disabled = attr.disabled;
					attrObject.defaultVal = attr.defaultVal;
				}else if (w_type === 'select' && $scope.selList != null) {
					attrObject.selList = $scope.selList;
					attrObject.disabled = attr.disabled;
					attrObject.defaultVal = attr.defaultVal;
				}else if(w_type === 'input'){
					attrObject.require = attr.require;
					attrObject.disabled = attr.disabled;
					attrObject.defaultVal = attr.defaultVal;
				} else if (w_type === 'image') {
					attr.imgCategoryId = $scope.imgCategory;
				}
				
//				attr.widgetLayout = t.prop("outerHTML");
//				attr.widgetId = $scope.temp.id;
				attr.widgetType = w_type;
				attr.corpCode = "Trawind";
				attr.attrName = attr.attrName_en;
				var nameLang = [];
				var lang = {};
				lang["en"] = attr["attrName_en"];
				angular.forEach($scope.addedLanguage, function(value, key){
					lang = {};
					lang[value] = attr["attrName_"+value];
					nameLang.push(lang);
				});
				attr.nameLang = nameLang;
				attr.layoutJson = JSON.stringify(attrObject); 
				attr.direcJson = JSON.stringify(attrObject);
				$http.post("/attrCtl/saveAttr",JSON.stringify(attr))
					.success(function(data, status, headers, config) {
						gridData.push(data);
						//console.log(gridData);
						$scope.gridOptions.data = gridData;
						$scope[w_type] = {};
					}).error(function(data, status, headers, config) {
						console.log("fail to save attribute");
					});
        };

        $scope.languages = [{"id":"ge", "name":"German"},{"id":"fr", "name":"French"}];
        $scope.select_language = "ge";
        var attr_index = 1;
        $scope.addedLanguage = [];
        $scope.addIn = function (){
        	if (!checkAdded($scope.addedLanguage, $scope.select_language)) {
        		var label = "";
        		angular.forEach($scope.languages, function(value, key) {
        			if (value.id == $scope.select_language) {
						label = value.name;
					}
        		})
        		var html = '<div class="form-group" id="attrName'+attr_index+'">'
				      	   +'	<label for="input.attrName_'+$scope.select_language+'" class="col-md-4 control-label">Attribute Name('+label+')</label>'
				      	   +'	<div class="col-md-6">'
				           +'		<input type="text" ng-model="input.attrName_'+$scope.select_language+'" name="input.attrName_'+$scope.select_language+'" class="form-control" placeholder="widget name" aria-describedby="basic-addon1"/>'
				           +'	</div>'
				           +'	<div class="col-md-2" style="margin-top: 6px;">'
				           +'		<span title="Delete this line" class="glyphicon glyphicon-minus" ng-click="delIn(\'attrName'+attr_index+'\')" aria-hidden="true"></span>'
				           +'	</div>'
				           +'</div>';
        			
        		$("#inputFirst").after($compile(html)($scope));
        		$scope.addedLanguage.push($scope.select_language);
        		attr_index++;
			} else {
				alert("已添加该语言");
			}
        }
        function checkAdded(list, el){
        	if (list && list.length > 0) {
				for (var i = 0; i < list.length; i++) {
					var l_el = list[i].val;
					if (l_el == el) {
						return true;
					}
				}
			} else {
				return false;
			}
        	return false;
        }
        $scope.delIn = function (index){
        	var attr_index = index.substr(9);
        	var val;
        	angular.forEach($scope.languages, function(value, key) {
        		if (value.id == attr_index) {
					val = value.name;
				}
        	})
        	$scope.addedLanguage.splice($scope.addedLanguage.indexOf(val), 1);
        	$("#"+index).remove();
        	
        }
        angular.element(document).ready(function(){
        	$(".boxes").slideDown();
        });
        $scope.updateVal = function (){
        	$scope.selected_val = angular.element("#sel").find(":selected").text();
        }
        $scope.select = "";
        $scope.selected_val = $("#sel").find(":selected").text();
        $scope.selList = [];
        $scope.clear = function (){
        	$scope.select_en = "";
            $scope.selected_val = "";
        }
        $scope.addEle = function (){
        	if ($scope.selected_val != "" && $scope.selected_val != undefined && $scope.selected_val != "---请选择---") {
        		if (!checkAdded($scope.selList, $scope.selected_val)) {
        			var sel = {};
        			sel.val = $scope.selected_val;
        			sel.text = $scope.selected_val;
        			$scope.selList.push(sel);
        			angular.element("#sel").append('<option value="'+$scope.selected_val+'">'+$scope.selected_val+'</option>');
        			$scope.select_en = "";
        			$scope.selected_val = "---请选择---";
        		} else {
        			alert("已添加");
        		}
			}
        }
        $scope.delEle = function (){
        	angular.forEach(angular.element("#sel").children(), function(value, key) {
        		if (angular.element(value).val() == $scope.selected_val) {
					value.remove();
					$scope.select = "";
					$scope.selected_val = "";
				}
        	});
        }
        
        //时间控件触发
        dateServ.init('.form_date','en');  
        
//        $scope.AB = {"widgetLabel": "image angle", "widgetId": "AB", "type": "input"};
//        $scope.AC = {"widgetLabel": "image shape", "widgetId": "AC", "type": "input"};
//        $scope.AD = {"widgetLabel": "image shape11", "widgetId": "AD", "type": "input"};
        $scope.initImage = function() {
        	var deferred = $q.defer();
            $http({
                  method : 'post',
                  url : 'attrCtl/getImgCategoryList',
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            var html = "";
            deferred.promise.then(function(data) {
            	
            	$scope.imageCategory = data;
            	for ( var d in data) {
            		$scope[data[d].id] = data[d];
				}
              console.log(data);
            }, function(data) {
              console.log('linkSearch 请求失败');
            });
        };
        
        $scope.imgAttr  = [];
        $scope.updateAttr = function() {
        	var imgCg = $scope[$scope.imgCategory];
        	if (imgCg != null && imgCg.innerAttrsJson != "") {
        		$scope.imgAttr  = [];
        		if (imgCg.innerAttrs) {
					for ( var d in imgCg.innerAttrs) {
						var item = {};
						if (imgCg.innerAttrs[d].direcJson) {
							item = JSON.parse(imgCg.innerAttrs[d].direcJson);
						}
						item.widgetId = imgCg.innerAttrs[d].id;
						item.type = imgCg.innerAttrs[d].widgetType;
						item.isForm = "imgAttr";
						item.disabled = true;
						$scope[imgCg.innerAttrs[d].id] = item;
						$scope.imgAttr.push(item);  
					}
				}
			}
		};
	}]);
});
