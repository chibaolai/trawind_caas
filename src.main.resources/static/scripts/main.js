// the app/scripts/main.js file, which defines our RequireJS config
require.config({
      paths : {
        'angular' : 'vendor/angular',
        'angularCookies' : 'vendor/angular-cookies',
        'jquery' : 'vendor/jquery-2.0.0.min',
        'bootstrap' : 'vendor/bootstrap.min',
        'jqueryUI' : 'vendor/jquery-ui',
        'htmlClean' : 'vendor/jquery.htmlClean',
        'webuploader' : 'vendor/webuploader',
        'uiGrid' : 'vendor/ui-grid',
        'angularAria' : 'vendor/angular-aria',
        'i18n' : 'i18n/ui-i18n',
        'zhcn' : 'i18n/zh-cn',
        'angularTranslate' : 'vendor/angular-translate',
        'angularTranslateLoader' : 'vendor/angular-translate-loader-static-files',
        'widget' : 'vendor/widget',
        'datetimepicker' : 'vendor/bootstrap-datetimepicker',
        'uiRouter' : 'vendor/angular-ui-router',
        'domReady' : 'vendor/domReady',
        'angularAMD' : 'vendor/angularAMD',
        'isotope' : 'vendor/isotope.pkgd.min',
        'appear' : 'vendor/jquery.appear',
        'backstretch' : 'vendor/jquery.backstretch.min',
        'modernizr' : 'vendor/modernizr',
        'welcomeCustom' : 'vendor/custom',
        'easing' : 'vendor/jquery.easing.min',
        'fullPage' : 'vendor/jquery.fullPage.min',
        'mainHome' : 'vendor/mainHome'
      },
      waitSeconds : 0,
      shim : {
        'angular' : {
          exports : 'angular'
        },
        'angularCookies' : {
          exports : 'angularCookies',
          deps : ["angular"]
        },
        'uiRouter' : {
          exports : 'uiRouter',
          deps : ["angular"]
        },
        'angularAMD' : {
          exports : 'angularAMD',
          deps : ["angular"]
        },
        'jquery' : {
          exports : 'jquery'
        },
        'bootstrap' : {
          exports : 'bootstrap',
          deps : ["jquery"]
        },
        'jqueryUI' : {
          exports : 'jqueryUI',
          deps : ["jquery"]
        },
        'htmlClean' : {
          exports : 'htmlClean',
          deps : ["jquery"]
        },
        'webuploader' : {
          deps : ["jquery"]
        },
        'uiGrid' : {
          exports : 'uiGrid',
          deps : ["angular"]
        },
        'angularAria' : {
          exports : 'angularAria',
          deps : ["angular"]
        },
        'i18n' : {
          exports : 'i18n',
          deps : ['angular', 'uiGrid']
        },
        'zhcn' : {
          exports : 'zhcn',
          deps : ['angular', 'uiGrid']
        },
        'angularTranslate' : {
          exports : 'angularTranslate',
          deps : ['angular']
        },
        'angularTranslateLoader' : {
          exports : 'angularTranslateLoader',
          deps : ['angular', 'angularTranslate']
        },
        'ckconfig' : {
          exports : 'ckconfig',
          deps : ['ckeditor']
        },
        'widget' : {
          exports : 'widget',
          deps : ['angular']
        },
        'datetimepicker' : {
          exports : 'datetimepicker',
          deps : ['bootstrap']
        },
        'domReady' : {
          exports : 'domReady'
        },
        'isotope' : {
          exports : 'isotope',
          deps : ["jquery"]
        },
        'appear' : {
          exports : 'appear',
          deps : ["jquery"]
        },
        'backstretch' : {
          exports : 'backstretch',
          deps : ["jquery"]
        },
        'modernizr' : {
          exports : 'modernizr',
          deps : ["jquery"]
        },
        'welcomeCustom' : {
          exports : 'welcomeCustom',
          deps : ["backstretch"]
        },
        'easing' : {
          exports : 'easing',
          deps : ["jquery"]
        },
        'fullPage' : {
          exports : 'fullPage',
          deps : ["jquery"]
        },
        'mainHome' :{
            exports : 'mainHome',
            deps : ["jquery"]
        }
      },
      deps : ['bootstrap']

    });

require(['angular', 
         'app', 
         'domReady', 
         'vendor/ui-router', 
         'webuploader', 
         'controllers/controllers', 
         'controllers/addAttrController', 
         'controllers/caasController', 
         'controllers/dashBoardController',
         'controllers/dataGridCommonController', 
         'controllers/productController', 
         'controllers/imgController', 
         'controllers/dataManagementExportController',
         'controllers/dataManagementImportController',
         'controllers/dataManagementComparisonController',
         'controllers/accountController', 
         'controllers/mainHomeController',
         'controllers/profileController', 
         'services/services', 
         'services/caasService',
         'services/commonService', 
         'services/translateService', 
         'directives/directives', 
         'directives/dashBoardDirective', 
         'directives/caasDirective', 
         'directives/productDirective',
         'directives/dataManagementExportDirective', 
         'directives/dataManagementImportDirective', 
         'directives/dataManagementComparisonDirective', 
         'directives/accountDirective', 
         'controllers/welcomeController', 
         'directives/dataGridCommonDirective'

    ], function(angular, app, domReady) {
      'use strict';
      domReady(function() {
            angular.bootstrap(document, ['webapp']);
          });
    });
