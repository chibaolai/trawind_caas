define(['app'], function(webapp) {

      // routes
      var registerRoutes = function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/dashboard");

        // route
        $stateProvider

        .state("caas", {
              url : "/caas",
              templateUrl : "views/caas.html",
              controller : "caasCtrl",
              params : {
                'formId' : {},
                'searchId' : {}
              }
            })

        .state("addAttr", {
              url : "/addAttr",
              templateUrl : "views/addAttr.html",
              controller : "addAttrCtrl"
            })

        .state("dashboard", {
              url : "/dashboard",
              templateUrl : "views/dashboard.html",
              controller : "dashBoardCtrl"
            })

        .state("dataGridCommon", {
              url : "/dataGridCommon",
              templateUrl : "views/dataGridCommon.html",
              controller : "dataGridCommonCtrl",
              params : {
                'request' : {},
                'url' : {}
              }
            })

        .state("product", {
              url : "/product",
              templateUrl : "views/product.html",
              controller : "productCtrl",
              params : {
                'formId' : {},
                'searchId' : {},
                'productId' : {}
              }
            })

        .state("dataManagement_export", {
              url : "/dataManagement_export",
              templateUrl : "views/dataManagementExport.html",
              controller : "dataManagementExportCtrl",
              params : {
                'formId' : {},
                'searchId' : {}
              }
            })
            
         .state("dataManagement_import", {
            	url : "/dataManagement_import",
            	templateUrl : "views/dataManagementImport.html",
            	controller : "dataManagementImportCtrl",
            	params : {
            		'formId' : {},
            		'searchId' : {}
            	}
            })
            
          .state("dataManagement_comparison", {
            	url : "/dataManagement_comparison",
            	templateUrl : "views/dataManagementComparison.html",
            	controller : "dataManagementComparisonCtrl",
            	params : {
            		'formId' : {},
            		'searchId' : {}
            	}
            })

        .state("account", {
              url : "/account",
              templateUrl : "views/account.html",
              controller : "accountCtrl"
            })

        .state("productPay", {
              url : "/productPay",
              templateUrl : "views/productPay.html",
              controller : "productPayCtrl"
            })

        .state("profile", {
              url : "/profile",
              templateUrl : "views/profile.html",
              controller : "profileCtrl"
            });
      };

      webapp.config(["$stateProvider", "$urlRouterProvider", registerRoutes]);

    });