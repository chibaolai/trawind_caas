(function () {
  'use strict';
  angular.module('webapp.widget', []);
})();
(function () {
	'use strict';
	angular.module('webapp.widget').controller('widgetController', ['$scope', function ($scope) {
		var ctrl = this;
	}]);
	angular.module('webapp.widget').directive("widgetDirective", function($q, $http, $compile, draggle) {
		return {
			restrict : 'E',
			replace : true,
			template: '<div name="widget" ng-include="getContentUrl()"></div>',
			scope : {
				widgetOptions : '='
			},
			controllerAs : 'ctrl',
			bindToController : true,
			controller : 'widgetController',
			link : function(scope, elem, attrs) {
				var ctrl = scope.ctrl;
				ctrl.options = scope.$parent.$parent[scope.ctrl.widgetOptions] || ctrl.widgetOptions;
				scope.getContentUrl = function() {
					if (ctrl && ctrl.options && ctrl.options.type) {
						switch (ctrl.options.type) {
							case "input":
								return 'views/direct_template/inputWidget.html';
							case "chk":
								return 'views/direct_template/checkBoxWidget.html';
							case "textarea":
								return 'views/direct_template/textAreaWidget.html';
							case "select":
								return 'views/direct_template/selectWidget.html';
							case "grid":
								return 'views/direct_template/gridWidget.html';
							case "image":
								return 'views/direct_template/imageUploadWidget.html';
							case "date":
								return 'views/direct_template/dateWidget.html';
							default:
								break;
						}
					}
				};
				scope.$on("$includeContentLoaded", function(event, templateName){
					angular.forEach($(".demo").find('div[name="widget"]'), function(value, key) {
						if ($(value).attr("class").indexOf("ui-draggable") != -1) {
							draggle.init(value, "");
						}
					});
					angular.forEach($(".sidebar-nav").find('div[name="widget"]'), function(value, key) {
						draggle.init(value, "clone");
					});
				});
			}
		};
	});
})();
(function () {
  'use strict';
  angular.module('webapp.widget').service('gridService', ['$compile', function ($compile) {
	  var service = {
		  initGrid : function (options){
			  options = (typeof(options) !== 'undefined') ? options : {};
		  }
	  };
	  return service;
  }]);
})();



