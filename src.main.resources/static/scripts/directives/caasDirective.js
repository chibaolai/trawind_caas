define(['directives/directives'], function(directives) {

      var layoutEditView = null;
      directives.directive('header', function() {
            return {
              restrict : 'E',
              replace : true,
              templateUrl : "views/header.html"
            };
          }).directive('mainheader', function() {
            return {
              restrict : 'E',
              replace : true,
              templateUrl : "views/mainheader.html"
            };
          }).directive('footer', function() {
            return {
              restrict : 'E',
              replace : true,
              templateUrl : "views/footer.html"
            };
          }).directive('formTemplate', function($q, $http, $compile, dateServ, imgServ) {
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            var data = {};
            function isNullObj(obj) {
              for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                  return false;
                }
              }
              return true;
            }

            if (isNullObj(scope.specifiedFormId)) {
              data.formId = "";
            } else {
              data.formId = scope.specifiedFormId;
            }

            scope.idList = [];
            scope.index = 0;
            var deferred = $q.defer();
            $http({
                  method : 'post',
                  url : 'userForms/getUserForms',
                  'data' : data
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            scope.tab_index = 0;
            var tabs;
            function addTabs(tabs) {
              var tab_title = $("#tab_title").val();
              tabs.find(".ui-tabs-nav").find("li:last").before('<li><a href="#tabs-' + scope.tab_index + '">' + tab_title
                  + '</a><span class="ui-icon ui-icon-close" role="presentation">Remove Tab</span></li>');
              tabs.append('<div id="tabs-' + scope.tab_index + '" style="height: 200px;" class="ui-sortable"></div>');
              tabs.tabs("refresh");

              scope.tab_index++;
              tabs.delegate("span.ui-icon-close", "click", function() {
                    var panelId = $(this).closest("li").remove().attr("aria-controls");
                    $("#" + panelId).remove();
                    tabs.tabs("refresh");
                  });
              $(".ui-tabs-panel").sortable({
                    connectWith : ".column",
                    opacity : .35,
                    handle : ".drag",
                    start : function(e, t) {
                    },
                    stop : function(e, t) {

                    }
                  });
              tabs.tabs({
                    active : 0
                  });
            }
            var html = "";
            deferred.promise.then(function(data) {
                  console.log('请求成功');
                  scope.gridIds = [];
                  if (data.form) {
                    $("#currentFormId").text(data.form.id);
                    $("#currentFormName").text(data.form.name);

                    $("#saveUpdateFrom").removeAttr("disabled");
                    $("#saveUpdateFrom").attr("checked", "checked");

                    $("#saveCreateFrom").removeAttr("checked");
                    if (data.form.id == "") {
                      $("#saveUpdateFrom").attr("disabled", "disabled");
                      $("#saveUpdateFrom").removeAttr("checked");

                      $("#saveCreateFrom").attr("checked", "checked");
                    }
                    var dJson = JSON.parse(data.form.directJson);
                    scope.dJson = dJson;
                    if (scope.dJson != null && scope.items != null) {
                      for (var i = 0; i < scope.items.length; i++) {
                        var notExist = true;
                        for (var j = 0; j < scope.dJson.length; j++) {
                          angular.forEach(scope.dJson[j], function(value, key) {
                                if (key == scope.items[i].widgetId) {
                                  notExist = false;
                                }
                              });
                          if (!notExist) {
                            scope.items.splice(i, 1);
                            break;
                          }
                        }

                      }
                    }
                    for (var i = 0; i < dJson.length; i++) {
                      angular.forEach(dJson[i], function(value, key) {
                            if (value.type == "grid") {
                              scope.gridIds.push(key);
                              scope[key] = {
                                enableColumnResizing : true
                              };
                              scope[key].data = [];
                              scope[key].columnDefs = value.columnDefs;
                            }
                            value.isForm = "form";
                            value.widgetId = key;
                            scope[key] = value;
                          });
                    }
                    scope.$on("$includeContentLoaded", function(event, templateName) {
                          // for (var i = 0; i < scope.dJson.length; i++) {
                          // angular.forEach(scope.dJson[i], function(value,
                          // key) {
                          // if
                          // ($("#elmBase").find("input[value='"+key+"']").parents(".box.box-element").length
                          // > 0) {
                          // $("#elmBase").find("input[value='"+key+"']").parents(".box.box-element").hide();
                          // }
                          // });
                          // }

                          for (var i = 0; i < scope.gridIds.length; i++) {
                            if ($(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").length > 0
                                && $(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".grid").attr("ui-grid") == undefined) {
                              $(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".grid").attr("ui-grid", scope.gridIds[i]);
                              $compile($(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".box").contents())(scope);
                            } else if ($(".demo").find(".grid").length > 0 && $(".demo").find(".grid").parents("div[widget-options='" + scope.gridIds[i] + "']").length > 0
                                && $(".demo").find(".grid").attr("ui-grid") == '') {
                              $(".demo").find(".grid").attr("ui-grid", scope.gridIds[i]);
                              $compile($(".demo").find(".grid").parents("div[widget-options='" + scope.gridIds[i] + "']").contents())(scope);
                            }
                          }
                          
                          console.log(event);
                          $("[data-toggle='tooltip']").tooltip();
                        });
                    if (data.form.command) {
                      if (scope.command == null) {
                        scope.command = {
                          all : [],
                          selCommand : [],
                          menu : []
                        };
                      }

                      scope.command.selCommand = JSON.parse(data.form.command);

                      var allCommand = scope.command.all;
                      var selCommand = scope.command.selCommand;
                      var box = $(".commanddiv").find("[command-group-box='']");
                      var allCheckBox = box.find("input[type='checkbox']");
                      angular.forEach(allCheckBox, function(val, idx) {
                            $(val).prop("checked", false);

                            for (var s in selCommand) {
                              if ($(val).val() == selCommand[s].key) {
                                $(val).prop("checked", true);
                                break;
                              }
                            }
                          });
                    }
                    elem.html(data.form.layout);
                    $compile(elem.contents())(scope);

                  } else if (data.temp) {
                    $("#currentFormId").text("");
                    $("#currentFormName").text("");

                    $("#saveUpdateFrom").attr("disabled", "disabled");
                    $("#saveUpdateFrom").removeAttr("checked");

                    $("#saveCreateFrom").attr("checked", "checked");
                    scope.baseAttrList = data.baseAttrList;
                    if (scope.baseAttrList.length > 0) {
                      for (var i = 0; i < scope.baseAttrList.length; i++) {
                        angular.forEach(scope.baseAttrList[i], function(value, key) {
                              value.isForm = "form";
                              value.widgetId = key;
                              value.baseFlag = "1";
                              scope[key] = value;
                            });
                      }
                    }
                    elem.html(data.temp.layout);
                    $compile(elem.contents())(scope);
                  }

                  scope.doSourcePreview();

                  if ($(".demo .accordion-toggle").length > 0) {
                    $(".demo .accordion-toggle").each(function(e, t) {
                          if ($(t).hasClass("collapsed")) {
                            $(t).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-plus pull-right");
                          } else {
                            $(t).find(".glyphicon").removeClass().addClass("glyphicon glyphicon-minus pull-right");
                          }
                        });
                  }

                  scope.indent = function($event, direction) {
                	  var box = $($event.target).parents("div.box");
                	  var boxWidth = box.width();
                	  var view = box.find(".view");
                	  var width = view.width();
                	  
                	  var newWidth = 0;
                	  switch (direction) {
					  	case "left":
					  		newWidth = parseInt(width) - 100;
		                	if (newWidth < 200) {
		                	  newWidth = 200;
		                	}
							break;
					  	case "right":
					  		newWidth = parseInt(width) + 100;
		                	if (newWidth > boxWidth) {
		                	  newWidth = boxWidth;
		                	}
					  		break;
						default:
							newWidth = boxWidth;
							break;
					   }
                	  var widgetId = box.find("input[name='widgetId']").val();
                	  var widget_width = {};
                	  if (scope.widget_width) {
                		  if (!scope.widget_width[widgetId]) {
                			  scope.widget_width[widgetId] = {};
                		  }
                	  } else {
                		  scope.widget_width = {};
                		  scope.widget_width[widgetId] = {};
                	  }
                	  if (newWidth != boxWidth) {
                		  scope.widget_width[widgetId] = newWidth;
                	  } else {
                		  delete scope.widget_width[widgetId];
                	  }
                	  view.width(newWidth);
                  }
                  
                  function initSortable(e, t) {
                    var widgetId = t.item.find("input[name='widgetId']").val();
                    if (scope.items != null) {
                      for (var i = 0; i < scope.items.length; i++) {
                        if (scope.items[i].widgetId == widgetId) {
                          scope.items.splice(i, 1);
                          break;
                        }
                      }
                    }
                    if (t.item.find(".grid").length > 0) {
                      t.item.find(".grid").attr("ui-grid", t.item.find("input[name='widgetId']").val());
                      $compile(t.item.contents())(scope);
                    }
                    if (t.item.find("div[name='tabs']").length > 0) {
                      tabs = t.item.find("div[name='tabs']").tabs();
                      var dialog = $("#dialog").dialog({
                            autoOpen : false,
                            modal : true,
                            buttons : {
                              Add : function() {
                                addTabs(tabs);
                                tabs.find(".ui-tabs-nav").sortable({
                                      axis : "x",
                                      stop : function() {
                                        tabs.tabs("refresh");
                                      }
                                    });
                                $(this).dialog("close");
                              },
                              Cancel : function() {
                                $(this).dialog("close");
                              }
                            },
                            close : function() {
                              $("#dialog").find("form")[0].reset();
                            }
                          });
                      dialog.dialog("open");
                      tabs.delegate("#addTab", "click", function() {
                            tabs = $(this).parents("div[name='tabs']").tabs();
                            dialog.dialog("open");
                          });
                    }
                    t.item.find(".view input:last").attr("id", "item" + scope.index);
                    t.item.find(".view textarea:last").attr("id", "item" + scope.index);
                    t.item.find(".view select:last").attr("id", "item" + scope.index);
                    scope.idList.push("item" + scope.index);
                    // 图片属性
                    if (t.item.find("div[name='uploader']").length > 0) {
                      t.item.find("div[name='uploader']").attr("id", "uploader" + "_" + widgetId);
                      t.item.find("div[name='filePicker']").attr("id", "filePicker" + "_" + widgetId);
                      t.item.find("div[name='dndArea']").attr("id", "dndArea" + "_" + widgetId);
                      t.item.find(".queueList").attr("id", "queueList" + "_" + widgetId);
                      imgServ.init(widgetId);
                    }
                    // 时间控件触发
                    dateServ.init('.form_date', 'en');
                    if (t.item.find("ul[name='indention']").length > 0) {
                    	t.item.find("a[rel='text-left']").attr("ng-click", "indent($event, 'left')");
                    	t.item.find("a[rel='text-right']").attr("ng-click", "indent($event, 'right')");
                    	t.item.find("a[rel='text-default']").attr("ng-click", "indent($event, 'default')");
						$compile($(t.item.find("a[rel='text-left']").parents("ul")).contents())(scope);
					}
                    scope.index++;
                  }
                  $(".demo, .demo .column").sortable({
                        connectWith : ".column",
                        opacity : .35,
                        handle : ".drag",
                        start : function(e, t) {
                        },
                        stop : function(e, t) {
                          if (t.item.find(".accordion").length > 0) {
                            $(".demo, .demo .column").sortable({
                                  connectWith : ".column",
                                  opacity : .35,
                                  handle : ".drag",
                                  start : function(e, t) {
                                  },
                                  stop : function(e, t) {
                                    initSortable(e, t);
                                  }
                                });
                          }  else if (t.item.find("div[name='custom']").length > 0) {
                        	  t.item.find(".column").each(function(i, e) {
            					$(e).mousemove(function(event) {
            						if (scope.mousedown == 1) {
            							if ($(e).attr("class").indexOf("selected-column") == -1) {
            								$(e).addClass("selected-column");
										}
            						}
            					});
            				  });
                          	  t.item.mouseup(function(event) {
  	                  	  			scope.mousedown = 0;
  	                  	  	  });
                          	  scope.mousedown = 0;
  	                  	  	  t.item.mousedown(function(event) {
  	                  	  		  event.preventDefault();
  	                  	  		  scope.mousedown = 1;
  	                  	  		  t.item.find(".column").each(function(i, e) {
  	                  	  			  $(e).removeClass("selected-column");
  	                  	  		  });
  	                  	  	  });
  	                  	  		
                            } else {
                            	initSortable(e, t);
	                        }
                        }
                      });

                }, function(data) {
                  console.log('请求失败');
                });

          },
          controller : function($scope) {

          }
        };
      }).directive('caaslayoutwidget', function($q, $http, $compile, dateServ) {
            return {
              restrict : 'E',
              replace : true,
              link : function(scope, elem, attrs) {
                var deferred = $q.defer();
                $http({
                      method : 'post',
                      url : 'widget/getLayoutCompList'
                    }).success(function(data) {
                      deferred.resolve(data);
                    }).error(function(data) {
                      deferred.reject(data);
                    });
                var html = "";
                deferred.promise.then(function(data) {
                      angular.forEach(data, function(value, key) {
                            html += value.layout;
                          });
                      console.log('caaslayoutwidget 请求成功');
                      elem.html(html);
                      $(".sidebar-nav .lyrow").draggable({
                            connectToSortable : ".demo, .ui-tabs-panel",
                            helper : "clone",
                            handle : ".drag",
                            start : function(e, t) {
                            },
                            drag : function(e, t) {
                              t.helper.width(400);
                            },
                            stop : function(e, t) {
                              function initSortable(e, t) {
                                var widgetId = t.item.find("input[name='widgetId']").val();
                                $("#elmBase").find("input[value='" + widgetId + "']").parents(".box.box-element").hide();
                                if (t.item.find(".grid").length > 0) {
                                  t.item.find(".grid").attr("ui-grid", t.item.find("input[name='widgetId']").val());
                                  $compile(t.item.contents())(scope);
                                }
                                if (t.item.find("div[name='tabs']").length > 0) {
                                  tabs = t.item.find("div[name='tabs']").tabs();
                                  var dialog = $("#dialog").dialog({
                                        autoOpen : false,
                                        modal : true,
                                        buttons : {
                                          Add : function() {
                                            addTabs(tabs);
                                            tabs.find(".ui-tabs-nav").sortable({
                                                  axis : "x",
                                                  stop : function() {
                                                    tabs.tabs("refresh");
                                                  }
                                                });
                                            $(this).dialog("close");
                                          },
                                          Cancel : function() {
                                            $(this).dialog("close");
                                          }
                                        },
                                        close : function() {
                                          $("#dialog").find("form")[0].reset();
                                        }
                                      });
                                  dialog.dialog("open");
                                  tabs.delegate("#addTab", "click", function() {
                                        tabs = $(this).parents("div[name='tabs']").tabs();
                                        dialog.dialog("open");
                                      });
                                }
                                t.item.find(".view input:last").attr("id", "item" + scope.index);
                                t.item.find(".view textarea:last").attr("id", "item" + scope.index);
                                t.item.find(".view select:last").attr("id", "item" + scope.index);
                                scope.idList.push("item" + scope.index);
                                // 图片属性
                                if (t.item.find("div[name='filePicker']").length > 0) {
                                  t.item.find("div[name='filePicker']").attr("id", "filePicker" + scope.index);
                                  t.item.find("div[name='uploader']").attr("id", "uploader");
                                  t.item.find("div[name='uploader']").addClass("uploader" + scope.index);
                                  t.item.find("div[name='dndArea']").attr("id", "dndArea");
                                  t.item.find(".queueList").attr("id", "queueList" + scope.index);
                                  t.item.find("#imgCtlDiv").attr("ng-controller", "imgCtrl");
                                  $compile(t.item.contents())(scope);
                                }
                                // 时间控件触发
                                dateServ.init('.form_date', 'en');
                                if (t.item.find("ul[name='indention']").length > 0) {
                                	t.item.find("a[rel='text-left']").attr("ng-click", "indent($event, 'left')");
                                	t.item.find("a[rel='text-right']").attr("ng-click", "indent($event, 'right')");
                                	t.item.find("a[rel='text-default']").attr("ng-click", "indent($event, 'default')");
            						$compile($(t.item.find("a[rel='text-left']").parents("ul")).contents())(scope);
            					}
                                scope.index++;
                              }
                              $(".demo, .demo .column").sortable({
                                    connectWith : ".column",
                                    opacity : .35,
                                    handle : ".drag",
                                    start : function(e, t) {
                                    },
                                    stop : function(e, t) {
                                    	if (t.item.find("span[name='dragBt']").length > 0) {
                                    		t.item.find("span[name='dragBt']").show();
										}
                                      if (t.item.find(".accordion").length > 0) {
                                        $(".demo, .demo .column").sortable({
                                              connectWith : ".column",
                                              opacity : .35,
                                              handle : ".drag",
                                              start : function(e, t) {
                                              },
                                              stop : function(e, t) {
                                                initSortable(e, t);
                                              }
                                            });
                                      } else if (t.item.find("div[name='custom']").length > 0) {
                                    	  t.item.find(".column").each(function(i, e) {
                          					$(e).mousemove(function(event) {
                          						if (scope.mousedown == 1) {
                          							if ($(e).attr("class").indexOf("selected-column") == -1) {
                          								$(e).addClass("selected-column");
  													}
                          						}
                          					});
                          				  });
                                        	  t.item.mouseup(function(event) {
                	                  	  			scope.mousedown = 0;
                	                  	  	  });
                                        	  scope.mousedown = 0;
                	                  	  	  t.item.mousedown(function(event) {
                	                  	  		  event.preventDefault();
                	                  	  		  scope.mousedown = 1;
                	                  	  		  t.item.find(".column").each(function(i, e) {
                	                  	  			  $(e).removeClass("selected-column");
                	                  	  		  });
                	                  	  	  });
                	                  	  		
                	                  	  		
                                          } else {
	                                      	initSortable(e, t);
	                                      }
                                    }
                                  });
                            }
                          });
                      $('body .demo').on("click", "[data-target=#layoutEditorModal]", function(e) {
                            e.preventDefault();
                            layoutEditView = $(this).parent().parent().find('.view');
                            var titleHtmlObj = layoutEditView.find("ul.nav.nav-list");
                            var textHtmlObj;
                            if (titleHtmlObj.length > 1) {
                              textHtmlObj = $(titleHtmlObj.get(0)).find("h4");
                            } else {
                              textHtmlObj = titleHtmlObj.find("h4");
                            }
                            var lindHtmlObj = titleHtmlObj.find("li.divider");

                            $("#chkSeparatorbar").attr("disabled", false);
                            $("#titleName").attr("disabled", false);
                            $("#selectLineColor").attr("disabled", false);

                            $("#titleName").val(textHtmlObj.text());
                            if (titleHtmlObj.css("display") == 'none') {
                              $("#chkSeparatorbar").removeAttr("checked");
                            } else {
                              $("#chkSeparatorbar").attr("checked", "checked");
                            }
                          });
                      $("#saveSeparatorbar").click(function(e) {
                            e.preventDefault();

                            if (layoutEditView != null) {
                              var titleHtmlObj = layoutEditView.find("ul.nav.nav-list");
                              var textHtmlObj;
                              if (titleHtmlObj.length > 1) {
                                textHtmlObj = $(titleHtmlObj.get(0)).find("h4");
                              } else {
                                textHtmlObj = titleHtmlObj.find("h4");
                              }
                              var requireHtmlObj = titleHtmlObj.find("span");
                              var lindHtmlObj = titleHtmlObj.find("li.nav-divider");

                              textHtmlObj.css("float", "none");
                              requireHtmlObj.text("");
                              if ($("#chkSeparatorbar").is(':checked') == true) {
                                titleHtmlObj.css("display", "block");
                                textHtmlObj.text($("#titleName").val());
                                lindHtmlObj.css("background-color", $("#selectLineColor").val());
                                if ($("#chkRequire").is(':checked') == true) {
                                  textHtmlObj.css("float", "left");
                                  requireHtmlObj.text("*");
                                }
                              } else {
                                titleHtmlObj.css("display", "none");
                              }
                            }
                            layoutEditView = null;
                          });
                      $(elem).parent().slideUp();

                    }, function(data) {
                      console.log('caaslayoutwidget 请求失败');
                    });
              }
            };
          }).directive('linkSearch', function($q, $http, $compile) {
            return {
              restrict : 'E',
              replace : true,
              link : function(scope, elem, attrs) {
                if (scope.specifiedSearchId != null && scope.specifiedSearchId.length > 0) {
                  var data = {};
                  data.searchId = scope.specifiedSearchId;
                  var deferred = $q.defer();
                  $http({
                        method : 'post',
                        url : 'information/getLinkSearchInfo',
                        'data' : JSON.stringify(data)
                      }).success(function(data) {
                        deferred.resolve(data);
                      }).error(function(data) {
                        deferred.reject(data);
                      });
                  var html = "";
                  deferred.promise.then(function(data) {
                        setTimeout(function() {
                              $("#search").val(data.seachkey);
                            }, 500);

                        console.log('linkSearch 请求成功');
                      }, function(data) {
                        console.log('linkSearch 请求失败');
                      });
                }
              }
            };
          }).directive('formtempl', function($q, $http, $compile, transl) {
              return {
                  restrict : 'E',
                  replace : true,
                  link : function(scope, elem, attrs) {
                	  var type = attrs.option;
                	  scope.type = type;
                	  $http({
	                      method: 'GET',
	                      params:{type:type},
	                      url: '/userForms/getFormByType',
	                      responseType:'json'	
	                  }).then(function successCallback(response) {
	                	  if (response.data) {
	                		  var directJson = JSON.parse(response.data.form.directJson);
	                		  var dataList = [];
	                		  if (response.data.formData) {
	                			  dataList = response.data.formData;
	                		  }
                  			  scope.directJson = directJson;
                  			  var layout = response.data.form.layout;
                  			  for (var i = 0; i < directJson.length; i++) {
                  				  $.each(directJson[i], function(j, el) {
	              					  scope[j] = el;
	              					  if (dataList.length > 0) {
	              						  for (var k = 0; k < dataList.length; k++) {
	              							  if (j == dataList[k].key) {
	              								 scope[j].defaultVal = dataList[k].value;
											  }
										  }
									  }
                  				  });
                  			  }
                  			  $(elem).html(layout);
                  			  $compile($(elem).contents())(scope);
      					  }
                  		
                  	  }, function errorCallback(response) { 
                  	  });
                	  
                      scope.save = function($event) {
                    	  var type = "";
                    	  $($event.target).parents("div.container-fluid").find(".profile").find("ul[name='title_tab']").find("li").each(function(i, el) {
                    		  if ($(el).attr("class") && $(el).attr("class").indexOf("active") > -1) {
                    			  type = $(el).attr("id");
                    		  }
                    	  });
                    	  if (type == "") {
							 return;
                    	  }
                      	  var profile = [];
                      	  var pro = {};
      					  for (var i = 0; i < scope.directJson.length; i++) {
      						 $.each(scope.directJson[i], function(i, el) {
      							switch (el.type) {
      								case "input":
      									pro = {};
      									pro.key = i;
      									pro.value = $("#"+i+"").val();
      									profile.push(pro);
      									break;
      	
      								default:
      									break;
      							}
      						});
      					  }
      					  var formtempl = {};
      					  formtempl.widgets = profile;
      					  formtempl.category = type;
                      	  $http({
            	    			method: 'POST',
            	    			url: '/userForms/saveFormData',
            	    			headers : {'Content-Type': 'application/json'},
            	    			data: formtempl
      	      			  }).then(function successCallback(response) {
      	      				
      	      			  }, function errorCallback(response) {
      	      				
      	      			  });
      				  };
                  }
              }
          });
    });