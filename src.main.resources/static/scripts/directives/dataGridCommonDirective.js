define(['directives/directives'], function(directives) {

      directives.directive('mappingTempleteItems', ['$http', '$q', '$compile', function($http, $q, $compile) {
                return {
                  restrict : 'A',
                  replace : true,
                  link : function(scope, elem, attrs) {
                    scope.mappingTemplete = {
                      all : [],
                      selected : {}
                    };

                    var deferred = $q.defer();
                    $http({
                          method : 'post',
                          url : 'product/getAllMappingTemplete'
                        }).success(function(data) {
                          deferred.resolve(data);
                        }).error(function(data) {
                          deferred.reject(data);
                        });
                    deferred.promise.then(function(data) {
                          if (data) {
                            var htmlStr = "";
                            scope.mappingTemplete.all = data;

                            angular.forEach(data, function(v, k) {
                                  var id = v._id.$oid;
                                  var filename = v.filename;

                                  var item = '<li class=""><a rel="" href="#" ng-click="selMappingTmp(\'' + id + '\',$event)" >' + filename + '</a></li>';
                                  htmlStr = htmlStr + item;
                                });

                            elem.html(htmlStr);
                            $compile(elem.contents())(scope);
                          }

                          console.log('mappingTempleteItems 请求成功');
                        }, function(data) {
                          console.log('mappingTempleteItems 请求失败');
                        });
                  }
                };
              }]);
    });