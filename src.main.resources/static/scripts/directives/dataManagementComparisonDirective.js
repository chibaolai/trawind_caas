define(['directives/directives'], function(directives) {

  var layoutEditView = null;
  directives.directive('mainheader', function() {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/mainheader.html"
        };
      });
});