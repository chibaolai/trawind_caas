define(['directives/directives'], function(directives) {

  var layoutEditView = null;
  directives.directive('mainheader', function() {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/mainheader.html"
        };
      }).directive('productTemplate', function($q, $http, $compile, dateServ, imgServ) {
    return {
      restrict : 'A',
      replace : true,
      link : function(scope, elem, attrs) {
        var data = {};

        data.productId = "";
        data.formId = scope.specifiedFormId;
        data.productId = scope.specifiedProductId;

        scope.idList = [];
        var deferred = $q.defer();
        $http({
              method : 'post',
              url : 'product/getProductAndData',
              'data' : data
            }).success(function(data) {
              deferred.resolve(data);
            }).error(function(data) {
              deferred.reject(data);
            });

        var html = "";
        deferred.promise.then(function(data) {
          console.log('请求成功');
          scope.gridIds = [];
          scope.selectIds = [];
          if (data.form) {
            scope.specifiedFormId = data.form.id;
            var dJson = JSON.parse(data.form.directJson);
            for (var i = 0; i < dJson.length; i++) {
              angular.forEach(dJson[i], function(value, key) {

                if (value.type == "grid") {
                  scope.gridIds.push(key);
                  scope[key] = {
                    enableColumnResizing : true,
                    enableRowSelection : true,
                    multiSelect : false,
                    modifierKeysToMultiSelect : false,
                    noUnselect : true
                  };
                  scope[key].data = [];
                  scope[key].columnDefs = value.columnDefs;

                  for (var j = 0; j < scope[key].columnDefs.length; j++) {
                    if (scope[key].columnDefs[j].name == "action") {
                      scope[key].columnDefs.splice(j, 1);
                    }
                  }
                  var rowTemplet = "<div class=\"container-fluid\"><div class=\"row cell-action-style\"><div class=\"col-xs-3 text-center\" ><div class=\"div-click\"  ng-click=\"grid.appScope.goToDelete(row,'"
                      + key + "')\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\" style=\"line-height: 30px;\"></span></div></div><div></div></div></div>";
                  scope[key].columnDefs.push({
                        name : 'action',
                        enableCellEdit : false,
                        cellTemplate : rowTemplet
                      });
                } else if (value.type == "select") {
                  if (data.productDataJson != null) {
                    for (var i = 0; i < data.productDataJson.length; i++) {
                      if (key == data.productDataJson[i].key) {
                        if (data.productDataJson[i].value.indexOf("string:") > -1) {
                          scope[key] = value;
                          scope[key].seletedId = data.productDataJson[i].value.substr(7);
                        }else{
                        	scope[key] = value;
                        	scope[key].seletedId = data.productDataJson[i].value;
                        }
                      }
                    }
                  }
                }
                value.isForm = "product";
                value.widgetId = key;
                scope[key] = value;

              });
            }
            scope.$on("$includeContentLoaded", function(event, templateName) {
                  for (var i = 0; i < scope.gridIds.length; i++) {
                    if ($(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").length > 0
                        && $(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".grid").attr("ui-grid") == undefined) {
                      $(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".grid").attr("ui-grid", scope.gridIds[i]);
                      $compile($(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".box").contents())(scope);
                    } else if ($(".demo").find(".grid").length > 0 && $(".demo").find(".grid").parents("div[widget-options='" + scope.gridIds[i] + "']").length > 0
                        && $(".demo").find(".grid").attr("ui-grid") == '') {
                      $("div[widget-options='" + scope.gridIds[i] + "']").find(".grid").attr("ui-grid", scope.gridIds[i]);
                      $compile($(".demo").find("div[widget-options='" + scope.gridIds[i] + "']").find(".box").contents())(scope);
                    }
                  }
                  if ($(".demo").find("div[name='uploader']").length > 0 && $(".demo").find("div[name='uploader']").attr("id") == undefined) {
                    var widgetId = $(".demo").find("div[name='uploader']").parents("div[name='widget']").attr("widget-options");
                    $(".demo").find("div[name='uploader']").attr("id", "uploader" + "_" + widgetId);
                    $(".demo").find("div[name='filePicker']").attr("id", "filePicker" + "_" + widgetId);
                    $(".demo").find("div[name='dndArea']").attr("id", "dndArea" + "_" + widgetId);
                    $(".demo").find(".queueList").attr("id", "queueList" + "_" + widgetId);
                    imgServ.init(widgetId);
                  }
                });
            elem.html(data.form.layout);
            $compile(elem.contents())(scope);
          }

          if (data.product) {
            scope.specifiedProductId = data.product.id;
          }
          scope.doSourcePreview();

          // if(scope.specifiedProductId == "" || scope.specifiedProductId ==
          // null){
          var menuHtml = "";
          if (data.formList != null) {
            for (var m = 0; m < data.formList.length; m++) {
              $("#tempListMenuId").append("<li class=\"\"><a ng-click=\"goNewDemo('" + data.formList[m].id + "')\" >" + data.formList[m].name + "</a></li>");
              $compile($("#tempListMenuId"))(scope);
            }
            $("#tempListMenuId").append(menuHtml);
          }
          // }
          // widgetID
          scope.widgetIds = data.form.widgetIds;
          // widget value
          if (data.productDataJson != null) {
            scope.widget = data.productDataJson;
          }
          // save time
          if (data.timeStamp != null) {
            scope.timeStamp = data.timeStamp;
          }

          // setCommandItems
          scope.setCommandItems(data.form, $(".command-items"));
        }, function(data) {
          console.log('请求失败');
        });
        scope.$watch('companyName', function(value) {
            });

      },
      controller : function($scope) {

      }
    };
  });
});