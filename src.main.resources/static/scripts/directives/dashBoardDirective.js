define(['directives/directives'], function(directives) {
  // 左侧导航中取得布局控件
  directives.directive('homelayoutwidget', function($q, $http, $compile) {
        return {
          restrict : 'E',
          replace : true,
          link : function(scope, elem, attrs) {
            var deferred = $q.defer();
            $http({
                  method : 'post',
                  url : 'homeForms/getLayoutCompList'
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            var html = "";
            deferred.promise.then(function(data) {
                  setElemHtml(elem, data);

                  $("homelayoutwidget div.lyrow").each(function(idx, o) {
                        changeCommonLang($(o).find(".row"), $compile, scope);
                      });

                  $compile($("homelayoutwidget div.lyrow .preview").contents())(scope);

                  $(".sidebar-nav .lyrow").draggable({
                        connectToSortable : ".demo",
                        helper : "clone",
                        handle : ".drag",
                        start : function(e, t) {
                        },
                        drag : function(e, t) {
                          t.helper.width(400);
                        },
                        stop : function(e, t) {
                          var e = $(".demo div[lyrow]");
                          if (e.length > 0) {
                            var t = randomNumber();
                            var n = "lyrow_" + t;
                            e.attr("id", n);
                            e.attr("getlyrow", "");
                            e.removeAttr("lyrow");

                            $compile(e.contents())(scope);
                          }

                          $(".demo .column").sortable({
                                opacity : .35,
                                connectWith : ".column",
                                start : function(e, t) {
                                },
                                stop : function(e, t) {
                                  chartsResize();
                                  scope.gridResize();
                                }
                              });
                        }
                      });
                  $(elem).parent().slideDown();

                  console.log('homelayoutwidget 请求成功');
                }, function(data) {
                  console.log('homelayoutwidget 请求失败');
                });
          }
        };
      }).directive('homecustomwidget', function($q, $http, $compile) {// 左侧导航中取得自定义控件
        return {
          restrict : 'E',
          replace : true,
          link : function(scope, elem, attrs) {
            var deferred = $q.defer();
            $http({
                  method : 'post',
                  url : 'homeForms/getCustomCompList'
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            var html = "";
            deferred.promise.then(function(data) {
                  setElemHtml(elem, data);
                  $compile(elem.contents())(scope);
                  $(elem).parent().slideDown();

                  console.log('homecustomwidget 请求成功');
                }, function(data) {
                  console.log('homecustomwidget 请求失败');
                });
          }
        };
      }).directive('widgetNews', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/newsHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetChart', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/chartHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetSyslog', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/syslogHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetNotice', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/noticeHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetLink', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/linkHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetFormLink', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/formLinkHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetProductLink', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/productLinkHomeWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('widgetProductAuditing', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'E',
          replace : true,
          templateUrl : "views/direct_template/productAuditingWidget.html",
          scope : {},
          controllerAs : 'ctrl',
          bindToController : true,
          controller : function() {
            var ctrl = this;
          },
          link : function(scope, ele, attrs) {
            regDragHandle($q, $http, $compile, scope, $state, transl);
          }
        };
      }).directive('formtemplate', function($q, $http, $compile) {// 从DB里取得页面模板信息
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            scope.idList = [];
            scope.index = 0;
            var deferred = $q.defer();
            $http({
                  method : 'post',
                  url : 'homeForms/getHomePageFromDB'
                }).success(function(data) {
                  deferred.resolve(data);
                }).error(function(data) {
                  deferred.reject(data);
                });
            var html = "";
            deferred.promise.then(function(data) {
                  setElemHtml(elem, data);
                  $compile(elem.contents())(scope);

                  $(".demo, .demo .column").sortable({
                        connectWith : ".column",
                        opacity : .35,
                        handle : ".drag",
                        start : function(e, t) {
                        },
                        stop : function(e, t) {
                          t.item.find(".view input:last").attr("id", "item" + scope.index);
                          scope.idList.push("item" + scope.index);
                          scope.index++;
                        }
                      });

                  console.log('formtemplate 请求成功');
                }, function(data) {
                  console.log('formtemplate 请求失败');
                });
          }
        };
      }).directive('getnewsinfo', function($q, $http, $compile, $state, transl) {// 新闻
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getNewsItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getchartsinfo', function($q, $http, $compile, $state, transl) {// 图表
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getChartsItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getnoticeinfo', function($q, $http, $compile, $state, transl) {// 通知
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getNoticeItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getsysloginfo', function($q, $http, $compile, $state, transl) {// 系统
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getSysLogItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getlinkinfo', function($q, $http, $compile, $state, transl) {// 历史
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getLinkItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getformlinkinfo', function($q, $http, $compile, $state, transl) {// 表单
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getFormLinkItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getproductlinkinfo', function($q, $http, $compile, $state, transl) {// 产品
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getProductLinkItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      }).directive('getlyrow', function($q, $http, $compile, $state, transl) {// 布局
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            changeCommonLang($(elem).find(".row"), $compile, scope);
            transl.reflashLanguage();
          }
        };
      }).directive('authorizeHasRole', function($q, $http, $compile, $state, transl) {
        return {
          restrict : 'A',
          scope : {
            specifiedRole : "@authorizeHasRole"
          },
          replace : true,
          link : function(scope, elem, attrs) {
            // $http.get('roles/getCustomUserRoles').success(function(data) {
            // var hasRole = $.inArray(scope.specifiedRole, data) == -1 ? false
            // : true;
            // if (!hasRole) {
            // // elem && elem.remove && elem.remove();
            // }
            // });
          }
        };
      }).directive('getproductauditinginfo', function($q, $http, $compile, $state, transl) {// 产品
        return {
          restrict : 'A',
          replace : true,
          link : function(scope, elem, attrs) {
            getProductAuditingItemDataFromDB($q, $http, elem, $compile, scope, $state, transl);
          }
        };
      });

  function getDataFromServer($q, $http, $compile, scope, $state, transl) {
    // 新闻组件
    getNewsItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 图表组件
    getChartsItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 通知组件
    getNoticeItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 系统日志组件
    getSysLogItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 链接组件
    getLinkItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 表单链接组件
    getFormLinkItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 产品链接组件
    getProductLinkItemDataByDrag($q, $http, $compile, scope, $state, transl);
    // 产品审核栏组件
    getProductAuditingItemDataByDrag($q, $http, $compile, scope, $state, transl);
  }

  function getChartsItemDataByDrag($q, $http, $compile, scope, $state) {
    var id = "chartItem";
    var e = $(".demo #" + id);
    if (e.length > 0) {
      var t = randomNumber();
      var n = id + "_" + t;
      var r;
      e.attr("id", n);

      e.find(".submenu-indent a").attr("own_id", n);

      var selectA = e.parent().parent().find(".configuration .dropdown-menu .active a");
      setChartsTitle(e, selectA.text());

      e.find("#chartItemBody").attr("styleId", selectA.attr("style-id"));
      e.find("#chartItemBody").attr("typeId", selectA.attr("type-id"));
      e.find("#chartItemBody").attr("getchartsinfo", "");
      var bodyId = id + "Body_" + t;
      e.find("#chartItemBody").attr("id", bodyId);

      getChartsItemDataFromDB($q, $http, e.find("#" + bodyId), $compile, scope);
    }

    return false;
  }

  function setChartsTitle(e, txt) {
    var title = e.parent().parent().find(".view .panel-heading");
    var t1 = "<b>" + title.children().eq(0).text() + "</b>";
    var t2 = "<span>(" + txt + ")</span>";
    title.empty();
    title.append(t1);
    title.append(t2);
  }

  function getNewsItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #newsItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "newsItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getnewsinfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getNewsItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  function getNoticeItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #noticeItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "noticeItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getnoticeinfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getNoticeItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  function getSysLogItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #syslogItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "syslogItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getsysloginfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getSysLogItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  function getLinkItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #linkItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "linkItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getlinkinfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getLinkItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  function getFormLinkItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #formLinkItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "formLinkItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getformlinkinfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getFormLinkItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  function getProductLinkItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #productLinkItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "productLinkItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getproductlinkinfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getProductLinkItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  function getProductAuditingItemDataByDrag($q, $http, $compile, scope, $state, transl) {
    var e = $(".demo #productAuditingItem");
    if (e.length > 0) {
      var t = randomNumber();
      var n = "productAuditingItem_" + t;
      var r;
      e.attr("id", n);
      e.attr("getproductauditinginfo", "");

      e.find(".submenu-indent a").attr("own_id", n);

      getProductAuditingItemDataFromDB($q, $http, e, $compile, scope, $state, transl);
    }

    return false;
  }

  //图表构建
  var arrayObj = new Array();
  function getChartsItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).parent().parent().find("div._title");
    titleObj.html('{{"main_page.ctrl.news_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'chart/getChartTypeInfo'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
          var ul = obj.parent().parent().parent().parent().find(".configuration .dropdown-menu");
          var selectA = ul.find(".active a");
          var styleIdOld = selectA.attr("style-id");
          var typeIdOld = selectA.attr("type-id");

          ul.empty();
          angular.forEach(data, function(value, key) {
                var li = "";
                if (value.styleId == styleIdOld && value.typeId == typeIdOld) {
                  li = li + "<li class=\"active\">";
                } else {
                  li = li + "<li class=\"\">";
                }
                li = li + "<a href=\"#\" rel=\"\" style-id=\"" + value.styleId + "\" type-id=\"" + value.typeId + "\">" + value.name + "</a>";
                li = li + "</li>";

                ul.append(li);
              });

          var lis = obj.parent().parent().parent().parent().find(".configuration .dropdown-menu li");
          if (lis.length > 0) {
            lis.each(function(index, el) {
                  $(el).find("a").click(function(sel) {
                        setChartsTitle(obj.parent().parent(), $(sel.currentTarget).text());
                        obj.attr("styleId", $(sel.currentTarget).attr("style-id"));
                        obj.attr("typeId", $(sel.currentTarget).attr("type-id"));

                        $compile(obj.parent().parent().parent().parent().contents())(scope);
                      });
                });
          }

          var data = {};
          var deferred = $q.defer();

          data.styleId = obj.attr("styleId");
          data.typeId = obj.attr("typeId");

          $http({
                method : 'post',
                url : 'chart/getChartInfo',
                'data' : JSON.stringify(data)
              }).success(function(data) {
                deferred.resolve(data);
              }).error(function(data) {
                deferred.reject(data);
              });

          deferred.promise.then(function(data) {
                var myChart = echarts.init(document.getElementById(obj.attr("id")));
                var option = data;
                // 为echarts对象加载数据
                myChart.setOption(option);

                for (var i = 0; i < arrayObj.length; i++) {
                  var s = $(arrayObj).eq(i).attr("id").toString();
                  var t = $(myChart).attr("id").toString();

                  if (s == t) {
                    arrayObj.splice(i, 1);
                    i--;
                  }
                }
                arrayObj.push(myChart);

                console.log('getChartsItemDataFromDB 请求成功');
              }, function(data) {
                console.log('getChartsItemDataFromDB 请求失败');
              });

          console.log('getChartTypeInfo 请求成功');
        }, function(data) {
          console.log('getChartTypeInfo 请求失败');
        });
  }

  function getNewsItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.news_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'information/getNewsItemInfo'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
          var row = 0;
          angular.forEach(obj.find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                  if (key < data.length) {
                    $(value).html(autoAddEllipsis(data[row].content, 100));
                  }
                  row++;
                }
              });

          console.log('getNewsItemDataFromDB 请求成功');
        }, function(data) {
          angular.forEach(obj.find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                }
              });

          console.log('getNewsItemDataFromDB 请求失败');
        });

    return false;
  }

  function getNoticeItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.notice_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'information/getNoticeItemInfo'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
          var row = 0;
          angular.forEach(obj.find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                  if (key < data.length) {
                    $(value).html(autoAddEllipsis(data[row].content, 100));
                  }
                  row++;
                }
              });

          console.log('getNoticeItemDataFromDB 请求成功');
        }, function(data) {
          angular.forEach(obj.find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                }
              });

          console.log('getNoticeItemDataFromDB 请求失败');
        });

    return false;
  }

  function getSysLogItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.system_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var item = $(obj).find("div.list-group-item");
    item.empty();

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'information/getSystemLogItemInfo'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
          var item = $(obj).find("div.list-group-item");
          if (item.length > 0) {
            item.append("<div class=\"syslogGrid\" style=\"height: 300px\" ui-grid=\"syslogGridOptions\" ui-grid-auto-resize></div>");

            scope.syslogGridOptions = {
              enableFiltering : true,
              enableColumnResizing : true,
              onRegisterApi : function(gridApi) {
                scope.gridApi = gridApi
              },
              columnDefs : [{
                    field : 'name',
                    displayName : "name",
                    width : "150"
                  }, {
                    field : 'journalType',
                    displayName : "Type",
                    width : "70"
                  }, {
                    field : 'updateUser',
                    displayName : "User",
                    width : "100"
                  }, {
                    field : 'updateTime',
                    displayName : "Time"
                  }],
              data : data
            }

            $compile(item.contents())(scope);
            $(".syslogGrid").resize();
          }

          console.log('getSysLogItemDataFromDB 请求成功');
        }, function(data) {
          console.log('getSysLogItemDataFromDB 请求失败');
        });

    return false;
  }

  function getLinkItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.link_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'information/getLinkItemInfo'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
          var row = 0;
          angular.forEach($(obj).find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                  if (key < data.length) {
                    $(value).append("<a ui-sref=\"caas({formId:'', searchId:'" + data[row].id + "'})\" ></a>");
                    $(value).find("a").text(data[row].name);

                    $compile($(value).contents())(scope);
                  }
                  row++;
                }
              });

          console.log('getLinkItemDataFromDB 请求成功');
        }, function(data) {
          angular.forEach($(obj).find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                }
              });

          console.log('getLinkItemDataFromDB 请求失败');
        });

    return false;
  }

  function getProductLinkItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    scope.productItemView = function(row) {
      $state.go("product", {
            formId : '',
            searchId : '',
            productId : row.entity.id
          });
    };

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.product_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var item = $(obj).find("div.list-group-item");
    item.empty();

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'product/getProductLinkItemInfo/8'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
      var item = $(obj).find("div.list-group-item");
      if (item.length > 0) {
        item.append("<div class=\"productGrid\" style=\"height: 300px\" ui-grid=\"productGridOptions\" ui-grid-auto-resize></div>");

        scope.productGridOptions = {
          onRegisterApi : function(gridApi) {
            scope.gridApi = gridApi
          },
          columnDefs : [{
            field : 'name',
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}"><a style="cursor: pointer" ng-click="grid.appScope.productItemView(row)">{{row.entity.name}}<a></div>',
            displayName : "ProductName"
          }, {
            field : 'primaryId',
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.primaryId}}">{{row.entity.primaryId}}</div>',
            displayName : "PrimaryId"
          }, {
            field : 'updateTime',
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
            displayName : "LastUpdateTime"
          }, {
            field : 'content',
            enableFiltering : false,
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" style="cursor: pointer" ng-click="grid.appScope.viewUpdateHistory(row)">{{row.entity.firstContent}}</div>',
            displayName : "UpdateContent"
          }],
          data : data,
          selectedRows : []
        }

        $compile(item.contents())(scope);
        $(".productGrid").resize();
      }

      console.log('getProductLinkItemDataFromDB 请求成功');
    }, function(data) {
      console.log('getProductLinkItemDataFromDB 请求失败');
    });

    return false;
  }

  function getFormLinkItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.form_list.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'userForms/getFormLinkItemInfo'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
          var tmpData = $(obj).parent().parent().find("div.tmpData");
          tmpData.empty();

          var row = 0;
          angular.forEach($(obj).find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                  if (row < data.length) {
                    $(value).append("<a ui-sref=\"caas({formId:'" + data[row].id + "', searchId:''})\" ></a>");
                    $(value).find("a").text(data[row].name);

                    $compile($(value).contents())(scope);
                  }
                  row++;
                }
              });

          console.log('getFormLinkItemDataFromDB 请求成功');
        }, function(data) {
          angular.forEach($(obj).find("div.list-group-item"), function(value, key) {
                if ($(value).attr("class").indexOf("active") < 0) {
                  $(value).empty();
                }
              });
          $("#saveModal").hide();

          console.log('getFormLinkItemDataFromDB 请求失败');
        });

    return false;
  }

  function getProductAuditingItemDataFromDB($q, $http, obj, $compile, scope, $state, transl) {

    scope.productItemView = function(row) {
      $state.go("product", {
            formId : '',
            searchId : '',
            productId : row.entity.id
          });
    };

    if ($.isFunction(scope.adjustIndent) == false && $.isFunction(scope.$parent.adjustIndent) == true) {
      scope.adjustIndent = function(type, direction, $event) {
        scope.$parent.adjustIndent(type, direction, $event);
      };
    }

    var titleObj = $(obj).find("div._title");
    titleObj.html('{{"main_page.ctrl.product_auditing.name" | translate}}');
    $compile(titleObj.contents())(scope);

    changeCommonLang(obj, $compile, scope);

    var item = $(obj).find("div.list-group-item");
    item.empty();

    var data = {};
    var deferred = $q.defer();
    $http({
          method : 'post',
          url : 'product/getProductAuditingItemInfo/8'
        }).success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });

    deferred.promise.then(function(data) {
      var item = $(obj).find("div.list-group-item");
      if (item.length > 0) {
        item.append("<div class=\"productAuditingGrid\" style=\"height: 300px\" ui-grid=\"productAuditingGridOptions\" ui-grid-auto-resize></div>");

        scope.productAuditingGridOptions = {
          onRegisterApi : function(gridApi) {
            scope.gridApi = gridApi
          },
          columnDefs : [{
            field : 'name',
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.name}}"><a style="cursor: pointer" ng-click="grid.appScope.productItemView(row)">{{row.entity.name}}<a></div>',
            displayName : "ProductName"
          }, {
            field : 'updateTime',
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.updateTime}}">{{row.entity.updateTime}}</div>',
            displayName : "LastUpdateTime"
          }, {
            field : 'content',
            enableFiltering : false,
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="{{row.entity.content}}" ng-click="">{{row.entity.firstContent}}</div>',
            displayName : "UpdateContent"
          }, {
            field : 'accept',
            width : 60,
            enableColumnResizing : false,
            enableFiltering : false,
            enableSorting : false,
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="Accept" ><a class="btn btn-xs btn-primary" ng-click="grid.appScope.acceptProductData(row)">Accept</a></div>'
          }, {
            field : 'reject',
            width : 60,
            enableColumnResizing : false,
            enableFiltering : false,
            enableSorting : false,
            cellTemplate : '<div class="ui-grid-cell-contents ng-binding ng-scope" title="Reject" ><a class="btn btn-xs btn-primary" ng-click="grid.appScope.rejectProductData(row)">Reject</a></div>'
          }],
          data : data,
          selectedRows : []
        }

        $compile(item.contents())(scope);
        $(".productAuditingGrid").resize();
      }

      console.log('getProductAuditingItemDataFromDB 请求成功');
    }, function(data) {
      console.log('getProductAuditingItemDataFromDB 请求失败');
    });

    return false;
  }

  function randomNumber() {
    return randomFromInterval(1, 1e6);
  }

  function randomFromInterval(e, t) {
    return Math.floor(Math.random() * (t - e + 1) + e);
  }

  setTimeout(function() {
        $(window).bind("resize", function resizeWindow(e) {
              chartsResize();
            });
      }, 100);

  function chartsResize() {
    setTimeout(function() {
          $(arrayObj).each(function(index, el) {
                if ($("#" + el.dom.id).length > 0) {
                  el.resize();
                }
              });
        }, 300);
  }

  function regDragHandle($q, $http, $compile, scope, $state, transl) {
    $(".sidebar-nav .box").draggable({
          connectToSortable : ".column",
          helper : "clone",
          handle : ".drag",
          start : function(e, t) {
          },
          drag : function(e, t) {
            t.helper.width(400)
          },
          stop : function(e, t) {
            getDataFromServer($q, $http, $compile, scope, $state, transl);
            chartsResize();
            scope.$parent.gridResize();
          }
        });
  }

  function setElemHtml(elem, data) {
    var html = "";
    angular.forEach(data, function(value, key) {
          html += value.layout;
        });
    elem.html(html);
  }

  function changeCommonLang(obj, $compile, scope) {

    var removeObj = $(obj).parent().parent().find("a.remove");
    if (removeObj.length > 0) {
      var ihtml = removeObj.find("i").prop('outerHTML');
      removeObj.html(ihtml + '{{"main_page.ctrl.common.btn_remove" | translate}}');
      if ($compile) {
        $compile(removeObj.contents())(scope);
      }
    }

    var moveObj = $(obj).parent().parent().find("span.move");
    if (moveObj.length > 0) {
      var ihtml = moveObj.find("i").prop('outerHTML');
      moveObj.html(ihtml + '{{"main_page.ctrl.common.btn_move" | translate}}');
      if ($compile) {
        $compile(moveObj.contents())(scope);
      }
    }

    var moreObj = $(obj).parent().find("a.more");
    if (moreObj.length > 0) {
      moreObj.html('{{"main_page.ctrl.common.btn_more" | translate}}');
      if ($compile) {
        $compile(moreObj.parent().contents())(scope);
      }
    }

    var issuedObj = $(obj).parent().find("a.issued");
    if (issuedObj.length > 0) {
      issuedObj.html('{{"main_page.ctrl.common.btn_issued" | translate}}');
      if ($compile) {
        $compile(issuedObj.contents())(scope);
      }
    }

    var createObj = $(obj).parent().find("a.create");
    if (createObj.length > 0) {
      createObj.html('{{"main_page.ctrl.common.btn_create" | translate}}');
      if ($compile) {
        $compile(createObj.contents())(scope);
      }
    }

    var submenuObj = $(obj).parent().parent().find("li.dropdown-submenu");
    if (submenuObj.length > 0) {
      if ($compile) {
        submenuObj.find("a.indent_title").html('{{"main_page.ctrl.common.menu_indent_title" | translate}}');
        submenuObj.find("a.indent_add_left").html('{{"main_page.ctrl.common.menu_indent_add_left" | translate}}');
        submenuObj.find("a.indent_add_right").html('{{"main_page.ctrl.common.menu_indent_add_right" | translate}}');
        submenuObj.find("a.indent_subtra_left").html('{{"main_page.ctrl.common.menu_indent_subtra_left" | translate}}');
        submenuObj.find("a.indent_subtra_right").html('{{"main_page.ctrl.common.menu_indent_subtra_right" | translate}}');

        $compile(submenuObj.contents())(scope);
      }
    }
  }

  /*
   * 处理过长的字符串，截取并添加省略号 注：半角长度为1，全角长度为2
   * 
   * pStr:字符串 pLen:截取长度
   * 
   * return: 截取后的字符串
   */
  function autoAddEllipsis(pStr, pLen) {

    var _ret = cutString(pStr, pLen);
    var _cutFlag = _ret.cutflag;
    var _cutStringn = _ret.cutstring;

    if (pStr.length == _cutStringn.length) {
      return _cutStringn + " " + "<a href=\"#\">Read more...</a>";
    } else {
      return _cutStringn + "... " + "<a href=\"#\">Read more...</a>";
    }

  }

  /*
   * 取得指定长度的字符串 注：半角长度为1，全角长度为2
   * 
   * pStr:字符串 pLen:截取长度
   * 
   * return: 截取后的字符串
   */
  function cutString(pStr, pLen) {

    // 原字符串长度
    var _strLen = pStr.length;
    var _tmpCode;
    var _cutString;
    // 默认情况下，返回的字符串是原字符串的一部分
    var _cutFlag = "1";
    var _lenCount = 0;
    var _ret = false;

    if (_strLen <= pLen / 2) {
      _cutString = pStr;
      _ret = true;
    }

    if (!_ret) {
      for (var i = 0; i < _strLen; i++) {
        if (isFull(pStr.charAt(i))) {
          _lenCount += 2;
        } else {
          _lenCount += 1;
        }

        if (_lenCount > pLen) {
          _cutString = pStr.substring(0, i);
          _ret = true;
          break;
        } else if (_lenCount == pLen) {
          _cutString = pStr.substring(0, i + 1);
          _ret = true;
          break;
        }
      }
    }

    if (!_ret) {
      _cutString = pStr;
      _ret = true;
    }

    if (_cutString.length == _strLen) {
      _cutFlag = "0";
    }

    return {
      "cutstring" : _cutString,
      "cutflag" : _cutFlag
    };
  }

  /*
   * 判断是否为全角
   * 
   * pChar:长度为1的字符串 return: true:全角 false:半角
   */
  function isFull(pChar) {
    if ((pChar.charCodeAt(0) > 128)) {
      return true;
    } else {
      return false;
    }
  }
});